package fr.inria.apisense.common.test

import fr.inria.bsense.test.ScalaTestCase
import org.junit.Test
import fr.inria.apisense.token.TokenManagerComposite
import fr.inria.apisense.token.ImplTokenManagerMap
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.common.utils.Http
import org.junit.Ignore

//@Ignore
class SecurityTest extends ScalaTestCase {

  
  @Test def test(){
    
    val tokenManager = new TokenManagerComposite[ImplTokenManagerMap] 
    tokenManager.save;
    tokenManager.start
    
    val app = new AppT;app.save;
    app.start
    
    
    var tokenAdmin : String = null
    var tokenUser  : String = null
    notfail("connection with account"){
    	tokenAdmin = Http get("http://localhost:18000/auth/connect") authentication("admin","admin") asString;
    	tokenUser  = Http get("http://localhost:18000/auth/connect") authentication("user","user") asString;
    }
    
    notfail("call authorized service"){
      Http get("http://localhost:18000/service/get/forall") header Map("Token" -> tokenAdmin) asString;
      
    }
    
    mustfail("call unauthorized service"){
         Http get("http://localhost:18000/service/get/forall") header Map("Token" -> tokenUser) asString
    }
    
    tokenManager stop;
    app stop;
    
  }
}
