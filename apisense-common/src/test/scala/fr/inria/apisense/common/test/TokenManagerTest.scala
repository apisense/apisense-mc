package fr.inria.apisense.common.test

import org.junit.Assert
import org.junit.Ignore
import org.junit.Test

import fr.inria.apisense.security.UserSecurityContext
import fr.inria.apisense.token.ImplTokenManagerMap
import fr.inria.apisense.token.TokenManager
import fr.inria.apisense.token.TokenManagerComposite
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.test.ScalaTestCase

@Ignore
class TokenManagerTest extends ScalaTestCase{

  @Test def test(){
    
    // create new token manager composite
    val tokenManager = new TokenManagerComposite[ImplTokenManagerMap] 
    
    // start composite in FraSCAti container
    tokenManager.start
    
    // get runtime token service 
    val service = tokenManager.rs[TokenServiceManager](TokenManager.TOKEN_MANAGER_SERVICE)
  
    var token : String = null
    
    // test : generate token
    notfail("Generate token"){
     
     // create an anonymous security context 
     var sc = UserSecurityContext.anonymous
     // generate new token 
     token = service.createToken(sc)

     Log.d("generated token "+token)
   }
    
   // test : get security context from token 
   notfail("get User from token"){
      val user = service.getUserPrincipal(token)
      Assert.assertTrue(user.getUserPrincipal.getName().equals("Anonymous"))
   }
   
   // test : try get security context from a bad token
   notfail("Try bad token"){
      Assert.assertTrue(service.getUserPrincipal("bad token") == null)
   }
    
   
   tokenManager.stop
 }
  
}