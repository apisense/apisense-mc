package fr.inria.apisense.common.test

import fr.inria.bsense.test.ScalaTestCase
import org.junit.Test
import org.junit.Assert
import frascala.frascati.REST
import fr.inria.apisense.intent.BindingIntent
import fr.inria.bsense.common.utils.Http
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.common.http.Base64
import org.junit.Ignore

@Ignore
class LifeCycleTest extends ScalaTestCase {

  
 @Test def test(){
    
    var composite : AppT = null
    
    notfail("Create Application test composite"){ composite = new AppT }
    notfail("Save composite definition"){ composite save }
    notfail("Start application"){ composite start }
    notfail("Check lifecycle"){
      Assert.assertTrue(TEST_CHECKER.ON_START_CLIENT)
      Assert.assertTrue(TEST_CHECKER.ON_START_SERVER)
    }
    composite stop
  }
  
}