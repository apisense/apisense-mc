package fr.inria.apisense.common.test

import fr.inria.bsense.common.cbse.BSenseComposite
import frascala.sca.Java
import frascala.sca.Bean
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.common.service.LifeCycleService
import org.osoa.sca.annotations.Scope
import java.util.concurrent.CountDownLatch
import com.google.common.collect.Synchronized
import frascala.frascati.REST
import fr.inria.apisense.intent.BindingIntent
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.apisense.token.TokenManager
import javax.ws.rs.core.SecurityContext
import java.security.Principal
import fr.inria.apisense.intent.BindingIntentComponent
import javax.ws.rs.core.Context
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.apisense.security.UserAuthorityPrincipal
import fr.inria.apisense.security.APISecureContext




object TEST_CHECKER{
  
  var ON_START_CLIENT = false
  var ON_START_SERVER = false
  
}

/**
 * 
 * Define a basic client server architecture
 * using FraSCAla Component
 * 
 */
/*  *************************************** CBSE Level ***************************************  */



case class ServerComponent extends BSenseComponent("apisense.server"){
  this.uses(Bean[Server])
    
  val srvGet = service("srv") exposes Java[ServerService]
  
  val srvConnect = service("srv-connect") exposes Java[ConnectService]
  
  reference("tokenManager") as TokenManager.getAddress

}

case class ClientComponent extends BSenseComponent("apisense.client"){
  this.uses(Bean[Client])
  
  val srv = service("srv-client") exposes Java[ClientService]
  val ref = reference("srv-test")
}


/* create simple application test with client server communication */
class AppT extends BSenseComposite("ApplicationTest"){
	
  implicit def componentInComposite[T <: BSenseComponent]( cpt : BSenseComponent ) = { "" }
  
  val serverIntent = component(BindingIntent.authorizationCpt)
  serverIntent.rule("/service/*", Array("ROLE_ADMIN"))
  
  /* create service */
  val cptServer = component(ServerComponent())
  
  cptServer.srvGet.as(REST("/service"),Array(
      BindingIntent.authenticationToken,
      serverIntent
  ))
  
  cptServer.srvConnect.as(REST("/auth"),Array(
      BindingIntent.authenticationBasic,
      serverIntent
  ))
  
  /* create client */
  val cptClient = component(ClientComponent())
  

  /* bind client and server */
  wire(cptClient.ref,cptServer.srvGet)
  
  /* expose services at composite level */
  promoteService(cptClient.srv)
  promoteService(cptServer.srvGet)
  
}

/* *************************************** Service Level ***************************************  */

trait ServerService{ 
  import javax.ws.rs._;
  @GET @Path("/get/forall") def getForAll(@Context sc : SecurityContext) : String 
  @GET @Path("/get/foradmin") def getForAdmin(@Context sc : SecurityContext) : String 
}

trait ConnectService{
  import javax.ws.rs._; 
  import javax.ws.rs.core._; 
  @GET @Path("/connect") def connect(@Context sc : SecurityContext) : String 
}

trait ClientService { 
  def print
}

/* *************************************** Implementation Level ***************************************  */

class Server extends ServerService with LifeCycleService with ConnectService {
  
  @Reference
  val tokenManager : TokenServiceManager = null
  
  def connect(sc : SecurityContext ) = { 
     
    val user = sc.getUserPrincipal().asInstanceOf[UserAuthorityPrincipal] 
    val username = user.getName
    val password = user.getPassword
    
    // check username and password in database
    // if valid, put all allowed role of user in 
    // securityContext and register context in 
    // tokenManager
   
    var role : String = "ROLE_USER"
    if (username.equals("admin"))
      role = "ROLE_ADMIN"
    
    val securityContext = new APISecureContext(username,username,Array(role))
    
    tokenManager.createToken(securityContext)
  }
  
  def getForAdmin( sc : SecurityContext) = "Server say Hello for Admin"
  def getForAll(   sc : SecurityContext)   = "Server say Hello for All"
  
  def onStart(c : BSenseComposite){ TEST_CHECKER.ON_START_SERVER = true }
  def onStop(){ }
}

class Client extends ClientService with LifeCycleService{
  
  @Reference(name="srv-test")
  var srv : ServerService = null
 
  def print { srv.getForAll(UserSecurityContext.anonymous) }
  
  def onStart(c : BSenseComposite){TEST_CHECKER.ON_START_CLIENT = true}
  def onStop(){}
  
}
