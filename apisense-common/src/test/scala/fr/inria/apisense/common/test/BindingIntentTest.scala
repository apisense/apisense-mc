package fr.inria.apisense.common.test

import fr.inria.bsense.common.cbse.BSenseComposite
import javax.ws.rs.POST
import javax.ws.rs.Path
import frascala.frascati.REST
import frascala.sca.Java
import frascala.sca.Bean
import fr.inria.bsense.common.utils.Http
import org.junit.Test
import fr.inria.apisense.intent.BindingIntent
import frascala.sca.COMPOSITE
import org.junit.Ignore




class BIApp extends BSenseComposite("BITApp"){
  
  val intent = new component("myitent") uses COMPOSITE(BindingIntent.logging)
  
  new component("cpt1"){ service("s1") exposes Java[Service1] as(REST("/s1"),Array(intent))} uses Bean[Service1Impl]
  new component("cpt2"){service("s2") exposes Java[Service2]  as(REST("/s2"),Array(intent))} uses Bean[Service2Impl]
}

trait Service1{ @POST @Path("/f") def service =  { println("service 1");"" } }
trait Service2{ @POST @Path("/f") def service =  { println("service 2");"" } }

class Service1Impl extends Service1
class Service2Impl extends Service2

@Ignore
object BindingIntentTest {

  
  def main(args: Array[String]) {
	  
    val app = new BIApp
    app save;
    app start;
    
    //org.ow2.frascati.explorer.FrascatiExplorerLauncher.main(Array(app.compositeFile))
    
    println("**********************************************")
    Http post("http://localhost:18000/s1/f") asString;
    println("**********************************************")
    Http post("http://localhost:18000/s2/f") asString;
  }

  
}