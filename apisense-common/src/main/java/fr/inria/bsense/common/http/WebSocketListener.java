/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.http;

public interface WebSocketListener {
	public void onMessage(WebSocket socket, String msg);

	public void onOpen(WebSocket socket);

	public void onClose(WebSocket socket);

	public void onError(WebSocket webSocket, Throwable t);

}
