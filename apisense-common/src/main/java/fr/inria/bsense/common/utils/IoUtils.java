/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.zip.ZipInputStream;

public class IoUtils {

	final static String TAG = IoUtils.class.getSimpleName();

	private static final int BUFFER_SIZE = 1024 * 8;

	public static int copy(InputStream input, OutputStream output) throws Exception, IOException {
		byte[] buffer = new byte[BUFFER_SIZE];

		BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
		BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
		int count = 0, n = 0;
		try {
			while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
				out.write(buffer, 0, n);
				count += n;
			}
			out.flush();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				//Log.e(TAG,e.getMessage(), e);
				e.printStackTrace();
			}
			try {
				in.close();
			} catch (IOException e) {
				//Log.e(TAG,e.getMessage(), e);
				e.printStackTrace();
			}
		}
		return count;
	}


	/**
	 * Copy File without close stream
	 *
	 * @param input
	 * @param output
	 * @return
	 * @throws Exception
	 * @throws IOException
	 */
	public static int copy2(InputStream input, OutputStream output) throws Exception, IOException {
		byte[] buffer = new byte[BUFFER_SIZE];

		BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
		BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
		int count = 0, n = 0;
		while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
				out.write(buffer, 0, n);
				count += n;
		}

		return count;
	}

	public static int copy(ZipInputStream input, OutputStream output) throws Exception, IOException {
		byte[] buffer = new byte[BUFFER_SIZE];

		BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
		BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
		int count = 0, n = 0;
		try {
			while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
				out.write(buffer, 0, n);
				count += n;
			}
			out.flush();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				//Log.e(TAG,e.getMessage(), e);
				e.printStackTrace();
			}
		}
		return count;
	}

	public static byte[] fromFile(final File file) throws IOException {

		byte[] buffer = new byte[BUFFER_SIZE];

		final BufferedInputStream in = new BufferedInputStream(new FileInputStream(file), BUFFER_SIZE);
		final ByteArrayOutputStream out = new ByteArrayOutputStream();

		int n = 0;
		while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
			out.write(buffer, 0, n);
		}
		in.close();
		final byte[] results = out.toByteArray();
		out.flush();
		out.close();
		return results;
	}

	public static String read(InputStream open) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(open));
		StringWriter writer = new StringWriter();

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				writer.append(line).append("\n");
			}
		} finally {
			reader.close();
		}

		return writer.toString();
	}

}