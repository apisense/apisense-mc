/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils;

import java.io.OutputStreamWriter;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;


public class Log {
	
	private static Logger log = null;
	
	private static Logger getLogger(){
		if (log == null){
			
			log = Logger.getLogger("APISENSE");
			
			final ConsoleAppender co = new ConsoleAppender();
			co.setLayout(new SimpleLayout());
            co.setWriter(new OutputStreamWriter(System.out));
            log.addAppender(co);
            log.setLevel(Level.ALL);
            
            // ADD File Appender ???
		}
		
		return log;
	}
	
	private static String getTag() {
		StackTraceElement[] stackTraceElements = Thread.currentThread()
				.getStackTrace();
		String fullClassName = stackTraceElements[4].getClassName();
		String className = fullClassName.substring(fullClassName
				.lastIndexOf(".") + 1);
		
		if (className.contains("$")) 
			className = className.substring(0,className.indexOf("$"));
		
		int lineNumber = stackTraceElements[4].getLineNumber();
		return "("+lineNumber+") "+className;
		
	}
	
	final public static void e(String message, Throwable athrow){
		getLogger().log(Level.ERROR,getTag()+" "+message);
	}
	
	final public static void e(Throwable athrow){
		getLogger().log(Level.ERROR,getTag()+" "+athrow.getMessage());
	}
	
	final public static void w(String message){
		getLogger().log(Level.WARN,getTag()+" "+message);
	}
	
	
	final public static void i(String message){
		getLogger().log(Level.INFO,getTag()+" "+message);
	}
	
	final public static void d(String message){
		getLogger().log(Level.DEBUG,getTag()+" "+message);
	}

}
