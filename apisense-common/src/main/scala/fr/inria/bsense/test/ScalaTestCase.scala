/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.test
import net.liftweb.json._
import java.util.Scanner
import fr.inria.bsense.common.utils.Log


trait ScalaTestCase {

  case class MustFailedException(_mess: String) extends Exception(_mess)
  case class NotFailedException(_mess: String)  extends Exception(_mess)

  
  def printTitle(mess : String){
    
    print("\n\033[1;37m")
    println("***************************************************")
    println("TEST "+mess)
    println("***************************************************")
    print("\033[0m \n")
  }
  
  def printError(mess : String){
    print("\033[1;31m")
    print("TEST STATE ERROR : ")
    print(mess)
    print("\033[0m \n")
  }
  
  def printSuccess(mess : String){
    print("\033[1;33m")
    print("TEST STATE Success : ")
    print(mess)
    print("\033[0m \n")
  }
  
  def jcanfail(mess: String)(body: => String) {
    
    printTitle("Can Fail "+mess)
    
    try{ 
      
      printSuccess(body) 
      
    }catch{case e : Throwable => e.printStackTrace() }
  }

  def jmustfail(mess: String)(body: => String) {

    printTitle("MUST FAIL : "+mess)
    
    val json = JsonParser.parse(body) \ "error"
    if (JNothing.canEqual(json)) {
      printError(mess)
      throw MustFailedException(mess)
    }

    val JString(message) = json \\ "message"
    val JString(_type) = json \\ "type"
    printSuccess("Exception of type " + _type + " and message " + message)

  }

  def jnotfail(mess: String)(body: => String) = {

    printTitle("NOT FAIL : "+mess)
    
    var result: String = null
    try {
      result = body
    } catch {
      case e  : Exception =>
        e.printStackTrace();
        printError(e.getMessage())
        throw NotFailedException(mess)
    }

    val json = JsonParser.parse(result) \ "success"
    if (JNothing.canEqual(json)) {
       printError(result)
       throw NotFailedException(mess)
    }
    printSuccess(result)
    json

  }

  def notfail(mess : String)(body : => Unit){
    
     printTitle("NOT FAIL : "+mess)
    
     try{ body }
     catch{
       case t : Throwable =>
         printError(t.getMessage());
         throw t
     }
     
     printSuccess("TEST SUCCESSFUL")
   }
  
  def mustfail(mess: String)(body: => Unit) {

    printTitle("MUST FAIL : "+mess)
    
    try {
      body
      throw MustFailedException(mess)
    } catch {
      case ex: MustFailedException => 
        printError(" Function '" + mess + "' not fail")
        throw MustFailedException(" Function '" + mess + "' not fail")
      case ex: Exception => 
        printSuccess("Exception Caused " + mess + "\t: OK : " + ex.getMessage())
    }
  }
  

  def canfail(mess: String)(body: => Unit) {
    printTitle(mess)
    try { body } 
    catch { case ex : Throwable => 
      printSuccess("Exception : " + ex.getMessage() + " OK")
    }
  }
  
  def w {
    val  s = new Scanner(System.in);
    s.next;
  }

}