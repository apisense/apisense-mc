/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.cbse

import frascala.sca.Composite
import frascala.frascati.ScaComposite
import java.io.File
import org.ow2.frascati.FraSCAti
import frascala.sca.scaComponent
import frascala.cbse.Multiple
import frascala.sca.scaPrimitive
import frascala.sca.ScaPrimitiveMembrane
import frascala.sca.scaComposite
import frascala.sca.ScaService
import frascala.sca.ScaReference
import frascala.frascati.ScaComposite
import fr.inria.bsense.common.utils.DefaultConfiguration
import fr.inria.bsense.common.utils.FileUtil
import fr.inria.bsense.common.service.LifeCycleManagerComponent
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.utils.Log
import frascala.sca.Java
import frascala.sca.Bean
import frascala.sca.ScaImplementation



object CompositeConfiguration extends DefaultConfiguration("fr.inria.frascala") {

  final val compositeFolder = rootPath +"/composite"

  def init {FileUtil.initFolder(compositeFolder)}
  def init(dir : String) {FileUtil.initFolder(compositeFolder+"/"+dir)}

}


class BSenseComposite(_name : String) extends Composite(_name){

  /**
   * Runtime instance of composite
   */
  var _runtime : ScaComposite = null

  /**
   * Sca composite file
   */
  var compositeFile : String = null

  /**
   * Return true if composite running
   */
  def isRun = _runtime != null

  val lifeCycleManager = component(LifeCycleManagerComponent())
  promoteService(lifeCycleManager.srvManager)

  /**
   * Store composite file definition
   * in file system
   */
  def save() {

    val dir = CompositeConfiguration.compositeFolder

    CompositeConfiguration.init

    this.basedir = dir
    this.toFile(new File(dir))
    this.compositeFile = dir +"/"+_name+".composite"
  }

  /**
   * Store composite in file definition
   * in file system
   * @param root folder
   */
  def save(_dir : String) {

    var dir = new java.io.File("").getAbsolutePath()
    dir = dir + "/frascala"

    CompositeConfiguration.init(_dir)

    this.basedir = dir
    this.toFile(new File(dir))
    this.compositeFile = dir+"/"+_name+".composite"

  }

  /**
   * Start composite
   */
  def start(){ this.start(BSRuntime.frascati) }

  /**
   * Start composite in a specific FraSCAti container
   * @param FraSCAty container
   */
  def start(fsca : FraSCAti){

    if (compositeFile == null) save

	if (this._runtime == null){
		Log.d("start component "+this.getClass())
	    this._runtime = BSRuntime.getComponent(compositeFile,fsca)
		this.onStart
	} else Log.w("Cannot start component : already running")
  }

  def stop(){
    if (this._runtime != null){
       this.onStop
       BSRuntime.removeComponent(this._runtime.name)
       this._runtime = null
    }
    else Log.w("Cannot stop component "+this.name+" : not running ")
  }

  private def onStart{
    try{
     Log.d("on start "+this.getClass())
     rs[LifeCycleService](LifeCycleManagerComponent.SERVICE_LF_MANAGER).onStart(this)
    }catch{
      case e : Throwable =>
        Log.e(e)
        e.printStackTrace()
    }
  }

   private def onStop{
    try{
     rs[LifeCycleService](LifeCycleManagerComponent.SERVICE_LF_MANAGER).onStop
    }catch{
      case e : Throwable =>
        Log.e(e)
        e.printStackTrace()
    }
  }

  /**
   * Add a new component
   */
  def component[T<: BSenseComponent](c: T) = {
    components += c;
    c
  }

  /**
   * Promote component service at composite level
   */
  def promoteService( srv : ScaService){ service(srv.name) promotes srv }

  /**
   * Promote component reference at composite level
   */
  def promoteReference( ref : ScaReference){ reference(ref.name) promotes ref }

  /**
   * Return runtime instance of composite
   */
  def runtime = {
    if (!isRun) throw new Exception("Composite "+this.name+" not running")
    _runtime
  }

  /**
   * @return Return runtime service instance
   */
  def rs[T](name : String) = {
    runtime.>[T](name)
  }
}


class BSenseComponent(_name : String) extends ScaPrimitiveMembrane with scaPrimitive {
  import scala.reflect._

  type OWNER = scaComposite
  this.name = _name

  def setProperty[T](name : String,value : T){
    this.properties(name).asInstanceOf[property[T]].is(value)
  }
}


