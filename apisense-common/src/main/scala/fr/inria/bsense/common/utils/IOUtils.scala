/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils

import java.io.InputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.io.Source
import java.io.File
import org.json.simple.parser.JSONParser
import scala.reflect.io.ZipArchive
import java.io.ByteArrayOutputStream
import java.util.zip.ZipOutputStream
import java.util.zip.ZipEntry
import java.io.FileOutputStream

object IOUtils {


//  final public static String convertInputStreamToString(final InputStream in, int maxline) throws IOException{
//
//		final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
//		final StringBuilder sb = new StringBuilder();
//
//		String line = null;
//		int i = 0;
//		while ((line = reader.readLine()) != null) {
//
//			if (i >= maxline) break;
//
//			sb.append(line + "\n");
//			i++;
//		}
//		in.close();
//		line = null;
//
//		return sb.toString();
//
//	}
   def inputStreamToString(in : InputStream) : String = Source.fromInputStream(in).getLines().mkString("\n")


   def inputStreamToString(in : InputStream, maxline : Int) : String = {
     import scala.util.control.Breaks._

     val reader = new BufferedReader(new InputStreamReader(in));
     val buffer = new StringBuilder();

     var line : String = null
     var i = 0

     breakable {

       try{
    	   var line = reader.readLine()
    	   while( line != null ){
             if (i >= maxline) break
    		 buffer append line
    		 buffer append "\n"
    		 i = i + 1
    		 line = reader.readLine()
           }
       }finally{ reader.close()  }
     }

     buffer toString
   }

   def hierarchy( file : File, rootName : String) : String = {


    val buf = new StringBuffer();
    buf append "{\"nodeName\" : \""+rootName+"\", "
    buf append "\"childs\" : ["
    _hierarchy(file, buf)
    buf append "]"
    buf.append("}")

    buf.toString()
  }


  def _hierarchy( file : File, buf : StringBuffer){

	  file.listFiles().foreach{

	    c =>

	    	buf append "{\"nodeName\" : \""+c.getName()+"\""
	    	if (c.isDirectory()){

	    	    buf.append(", \"childs\" : [")
	    	    _hierarchy(c, buf)
	    	    buf.append("]")
	    	}
	    	buf append "},"
	  }

	  var lastChar = buf.charAt(buf.length()-1);
	  if (lastChar.equals(',')) buf.deleteCharAt(buf.length()-1)

  }

  private def zipFile( basePath : String, signature : StringBuffer, file : File, out : ZipOutputStream ){

	  var _path = basePath
	  if (!basePath.equals(""))
	    _path += "/"

	  _path += file.getName()

      out.putNextEntry(new ZipEntry(_path));
	  val byte = IoUtils.fromFile(file)
	  out.write(byte)

	  // append md5 hash of script file in experiment signature
	  // This signature will be used when user report
	  // data to the collector node in order to check
	  // if script has not been modified by the user
	  if (signature != null) signature append fr.inria.bsense.common.utils.MD5.hash(new String(byte))
  }



  private def zipFolder(basePath : String, signature : StringBuffer, folder : File, out : ZipOutputStream){

	var _path = basePath
	if (!basePath.equals(""))
	    _path += "/"

	_path +=  folder.getName()

    folder.listFiles().foreach{
      file =>


      	if (file.isDirectory()){ zipFolder(_path, signature, file, out) }
      	else zipFile(_path,signature,file,out)
    }
  }

  def zip(folder : File, output : ZipOutputStream) = {

	  val buffer = new StringBuffer

	  if (folder.isDirectory()){

	   folder.listFiles().foreach{
	     f =>
	       if (f.isDirectory())
	         zipFolder("",buffer,f,output)
	       else
	         zipFile("", buffer, f, output) }

	  }else zipFile("", buffer, folder, output)

	  buffer.toString()
  }



  def main(args: Array[String]) {
	  //println(hierarchy(new File("/home/haderer/BEES_HIVE/experiment/PublisherModuleTest2/scripts"),"root"))

	val data = new FileOutputStream(new File("/home/haderer/test/test.zip"))
    val output = new ZipOutputStream(data)

	zip(new File("/home/haderer/BEES_HIVE/experiment/PublisherModuleTest2/scripts"),output)

	output.close();
	output.flush();
  }

}