/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.cbse

import frascala.frascati.ScaComposite
import java.io.File
import java.net.URLClassLoader


/**
 *
 * Helper object to manipulate FraSCAti runtime
 *
 */
object BSRuntime {

  Thread.currentThread.setContextClassLoader(this.getClass.getClassLoader)

  // FraSCAti instance
  var _frascati : org.ow2.frascati.FraSCAti = null

  def frascati = {

    if (_frascati == null){
      _frascati = org.ow2.frascati.FraSCAti.newFraSCAti()
    }
    _frascati
  }

  def setFraSCAti( frascati : org.ow2.frascati.FraSCAti ){

    _frascati = frascati;
  }

  var runtimeClassLoader : ClassLoader = Thread.currentThread.getContextClassLoader

  def loadJar( jarFiles : Array[File] ){

	  val loader = Thread.currentThread.getContextClassLoader.asInstanceOf[URLClassLoader];

	  var loaderURLS = loader.getURLs.toList
	  jarFiles.foreach{
	    jarFile => loaderURLS = jarFile.toURL :: loaderURLS
	  }

      runtimeClassLoader = new URLClassLoader(loaderURLS.toArray)

  }

  /**
   * Return if FraSCAti runtime contains a composite
   * named ''cptName''
   *
   *
   * @param cptName Composite name
   * @return
   */
  def hasComponent(cptName : String) : Boolean = {
    frascati.getCompositeManager().getCompositeNames().contains(cptName)
  }

  /**
   *
   * Remove composite named ''cptName'' from
   * FraSCAti runtime
   *
   * @param cptName Composite name
   */
  def removeComponent(cptName : String){
    frascati.getCompositeManager().removeComposite(cptName)
  }

  /**
   *
   * Return runtime instance of composite
   * named ''cptName''
   *
   * @param cptName Composite name
   */
  def getComponent(cptName : String) = {
    new ScaComposite(frascati.getCompositeManager.getComposite(cptName,runtimeClassLoader));
  }


  def getComponent(cptName : String, fsca : org.ow2.frascati.FraSCAti) = {
    new ScaComposite(fsca.getCompositeManager.getComposite(cptName,runtimeClassLoader));
  }


}




