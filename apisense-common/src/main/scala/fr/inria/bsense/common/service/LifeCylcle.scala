/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.service

import fr.inria.bsense.common.cbse.BSenseComponent
import frascala.sca.Java
import frascala.sca.Bean
import org.osoa.sca.annotations.Service
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.utils.Log



//-------------------------------------------------------------  Service

@Service trait LifeCycleService {

  /**
   * Called by runtime after composite start
   */
  def onStart( composite : BSenseComposite )

  /**
   * Called by runtime before composite stop
   */
  def onStop()

}


trait LifeCycleManagerService extends LifeCycleService{
  import org.osoa.sca.annotations.Reference

  @Reference(name = LifeCycleManagerComponent.SERVICE_LF)
  var components : java.util.List[LifeCycleService] = null

  def onStart(composite : BSenseComposite){

    val iterator = components.iterator()

    Log.d("Start all sub-components")
    while (iterator.hasNext()){
      val component = iterator.next();
      Log.d("On Start Component "+component)
      component.onStart(composite)
    }

  }

  def onStop(){

    val iterator = components.iterator()

    while (iterator.hasNext()){
      iterator.next().onStop
    }

  }

}

//-------------------------------------------------------------  Implementation

case class LifeCycleManagerImpl extends LifeCycleManagerService


//-------------------------------------------------------------  Component


object LifeCycleManagerComponent {

  final val SERVICE_LF_MANAGER = "apisense.service.lifecM"

  final val SERVICE_LF = "apisense.service.lifeCy"

}

case class LifeCycleManagerComponent extends BSenseComponent("cpt-lifecycle") {
  this.uses(Bean[LifeCycleManagerImpl])

  reference(LifeCycleManagerComponent.SERVICE_LF).exposes(Java[LifeCycleService]).multiple.optional.autowired

  val srvManager = service(LifeCycleManagerComponent.SERVICE_LF_MANAGER) exposes Java[LifeCycleManagerService]

}