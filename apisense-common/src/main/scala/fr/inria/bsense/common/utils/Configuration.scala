/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils
import scala.util.Properties
import java.util.Properties
import scala.io.Source
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.InputStream
import java.io.ByteArrayOutputStream


object FileUtil {

  def initFolder(path : String) : Boolean = {

    val f : File = new File(path)
    if (!f.exists()){
      if (!f.mkdirs()) Log.i("Cannot create folder "+f.getAbsoluteFile())
      else Log.i("Create folder "+f.getAbsoluteFile())
      false
    }
    true
  }

  def initFile(path : String) : Boolean = {
    val f : File = new File(path)
    if (!f.exists()){
      if (!f.createNewFile()) Log.i("Cannot create file "+f.getAbsoluteFile())
       else Log.i("Create file "+f.getAbsoluteFile())
      false
    }
    true
  }

  def toByteArray(in : InputStream) : Array[Byte] = {


    var bytes  = new  Array[Byte](512*1024);

    val out = new ByteArrayOutputStream
    var read = in.read(bytes)
    while( 0 < read){
      out.write(bytes,0,read)
      read = in.read(bytes)
    }

    in.close
    out.toByteArray
  }

  def toByteArray(file : File)   : Array[Byte]   = { toByteArray(new FileInputStream(file)) }
  def toByteArray(file : String) : Array[Byte]   = { toByteArray(new FileInputStream(file)) }

  def delete(file : File) {
    if (file.isDirectory) file.listFiles().foreach(child => FileUtil.delete(child))
    else file.delete()
  }

  def delete(file : String){ FileUtil.delete(new java.io.File(file))}

}

class DefaultConfiguration( propertyName : String ) {
   final val DEFAULT_PATH_ROOT = "/BeeSenseData";
   final val rootPath =  System.getProperty(propertyName,System.getProperty("user.home")+DEFAULT_PATH_ROOT)
}


class FileConfiguration(propertyName : String, _folder : String) extends DefaultConfiguration(propertyName){
  final val compositeFolder = rootPath +"/"+_folder

  FileUtil.initFolder(compositeFolder)
  def contain(filename : String) = new File(compositeFolder+"/"+filename).exists()
  def getFile = new File(compositeFolder)
  def getFiles = new File(compositeFolder).listFiles
  def file(filename : String) = new File(compositeFolder+"/"+filename)
  def getFileInputStream(filename : String) = new FileInputStream(compositeFolder+"/"+filename)
  def getFile(filename : String) = scala.io.Source.fromFile(compositeFolder+"/"+filename).mkString
  def updateFileWithParent(filename : String, content : String) {

    val _file = new File(compositeFolder+"/"+filename)
    val parent = _file.getParentFile()
    if (!parent.exists()){
      parent.mkdirs();
    }

    if (!_file.exists) _file.createNewFile;

    val writer = new FileWriter(_file)
    writer.append(content)
    writer.flush
  }
  def updateFile(filename : String, content : String) {
    val _file = new File(compositeFolder+"/"+filename)
    if (!_file.exists) _file.createNewFile;

    val writer = new FileWriter(_file)
    writer.append(content)
    writer.flush
  }

  def folder(name : String) = {

    val folder = new File(compositeFolder+"/"+name)
    if (!folder.exists()){
      folder.mkdirs()
    }

    folder
  }

  def write(filename : String, inputStream : InputStream){

    val _file = new FileOutputStream(new File(compositeFolder+"/"+filename));

    IoUtils.copy2(inputStream, _file)
    _file.flush();
    _file.close();
    //IoUtils.copy(inputStream, new File)
    //val writer = new FileWriter(_file)
    //writer.write(Source.fromInputStream(inputStream).toArray)
    //writer.close
    //writer.flush
  }

  def deleteFile(filename : String){ new File(compositeFolder+"/"+filename).delete }
  def delete(){ getFiles.foreach{ file => deleteFile(file.getName) };   new File(compositeFolder).delete}
  def deleteAllFiles(){ getFiles.foreach{ file => deleteFile(file.getName) }}

}


class Configuration(propertyName : String, propertiesName : String) extends DefaultConfiguration(propertyName){

  private final val DEFAULT_PATH_CONFIGURATION = "/configuration";

  private final val configPath = {
    val r = rootPath + DEFAULT_PATH_CONFIGURATION + "/" + propertiesName
    FileUtil.initFolder(rootPath + DEFAULT_PATH_CONFIGURATION)
    FileUtil.initFile(rootPath + DEFAULT_PATH_CONFIGURATION +"/"+propertiesName)
    r
  }

  def default(key : String, value : String){
    if (!has(key)){
      put(key,value)
      store
    }
  }

  final val properties : Properties = {
    val p = new Properties()
    load(p)
    p
  }

  private def load(props : Properties) {props load(new FileInputStream(configPath))}
  def load  { load(properties) }

  def get(key : String) = this.properties.getProperty(key)
  def get(key : String, value : String) = this.properties.getProperty(key,value)
  def get = this.properties.keys

  def put(key : String, value : String)  { properties.put(key,value); store }
  def put(key : String, value : Integer) { properties.put(key,value); store }

  def has(key : String) = properties.containsKey(key)

  def store { properties.store(new FileOutputStream(configPath),null) }

}
