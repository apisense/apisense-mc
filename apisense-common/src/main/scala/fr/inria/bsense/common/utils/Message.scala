/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils

import net.liftweb.json.JsonAST.JValue
import javax.ws.rs.core.Response

object Message {

  final val ERROR_MESSAGE = "error"
  final val SUCCESS_MESSAGE = "error"

  def exception[T <: Throwable](exception: T): String = {

    "{ \"error\" : { \"type\" : \"" + exception.getClass().getSimpleName() + "\"," +
      "\"message\" : \"" + exception.getMessage() + "\" } }"
  }

  def exception(exceptionType : String, exceptionMessage: String): String = {

    "{ \"error\" : { \"type\" : \"" + exceptionType + "\"," +
      "\"message\" : \"" + exceptionMessage + "\" } }"
  }

  def successJSON(message : Any) = "{  \"success\" : "+ message.toString +"}"

  def resSucess(message : Any) = {
	  Response.ok(message)
    	.header("Access-Control-Allow-Origin", "*")
    		.build()
  }

  def resSucessJson(message : Any) = { resSucess(success(message)) }

  def success(message: Any): String = {
    import net.liftweb.json._

    message match {
      case n: JValue => "{  \"success\" : " + compact(render(n)) + " }"
      case n: Node => "{  \"success\" : " + n.toJSONString + " }"
      case u: Unit => "{  \"success\" : \"\" }"
      case j: String => "{  \"success\" : \"" + message + "\" }"
      case _ => "{  \"success\" : " + message.toString + " }"
    }
  }

}