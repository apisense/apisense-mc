/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils

import net.liftweb.json._
import scala.xml.Elem

/**
 *
 * Abstract representation of XML and JSON documents
 * functionalities
 *
 */
trait Node {

  /**
   * Find direct child nodes
   */
  def \(name: String): Node

  /**
   * Find child nodes
   */
  def \\(name: String): Node

  /**
   * Return node label
   */
  def label : String

  /**
   * Insert child node
   */
  def :<(value: Node): Node

  /**
   * Insert child node with
   * label 'field' and value 'value'
   */
  def :<(field: String, value: Any): Node

  /**
   * Return node text
   */
  def text: String


  override def toString: String

  /**
   * Return JSON representation
   * of node as String
   */
  def toJSONString: String

  /**
   * Return XML representation
   * as String
   */
  def toXMLString: String

  /**
   * Return scala XML object
   */
  def toXMLObject: scala.xml.NodeSeq

  /**
   * Return liftweb JSON Object
   */
  def toLiftJSONObject: net.liftweb.json.JsonAST.JValue

  /**
   * Return node type
   */
  def getType : String

  def foreachChild( el  : (Node) => Any )

  /**
   * Return true if current node
   * is null
   */
  def isNull : Boolean

  /**
   * Return node number
   */
  def size : Int

  /**
   *
   */
  def foreach( el  : (Node) => Any )

}

/**
 * Scala XML Node Wrapper
 */
class XMLNode(scalaNode: scala.xml.NodeSeq) extends Node {
  import scala.xml.XML

  final val element: scala.xml.NodeSeq = scalaNode
  def label : String = {
     val l = element.map{ _.label }
     l(0)
  }
  def \(that: String): Node = new XMLNode(element \ that)
  def \\(that: String): Node = new XMLNode(element \\ that)

  def text: String = element text

  def foreachChild( el  : (Node) => Any ){
	element.\("_").foreach{
		child => el(new XMLNode(child))
	}
  }


  def :<(field: String, value: Any): Node = {

    val newEl = <foo>{ value }</foo>.copy(label = field)
    element match {
      case Elem(prefix, label, attribs, scope, child @ _*) =>
        new XMLNode(Elem(prefix, label, attribs, scope, child ++ newEl: _*))
      case _ => error("Can only add children to elements! "+element.getClass())
    }
  }
  def :<(value: Node): Node = {
    element match {
      case Elem(prefix, label, attribs, scope, child @ _*) =>
        new XMLNode(Elem(prefix, label, attribs, scope, child ++ value.toXMLObject: _*))
      case _ => error("Can only add children to elements! "+element.getClass())
    }
  }

  override def toString: String = element toString
  def toJSONString: String = compact(render(toLiftJSONObject))
  def toXMLString: String = toString
  def toXMLObject: scala.xml.NodeSeq = element
  def toLiftJSONObject: net.liftweb.json.JsonAST.JValue = net.liftweb.json.Xml.toJson(element)
  def getType = element.getClass.toString
  def isNull : Boolean =  element.length == 0

  def size = 0
  def foreach( el  : (Node) => Any ){
    element.foreach{
      node => el(new XMLNode(node))
    }
  }
}

class XMLNodeParser(xmlStr: String) extends XMLNode(scala.xml.XML.loadString(xmlStr))

/**
 * Net Liftweb JSON  XML Node Wrapper
 */
class LiftWebNode(liftNode: net.liftweb.json.JValue) extends Node {

  final val element: net.liftweb.json.JValue = liftNode

  def foreachChild( el  : (Node) => Any ){
    val childs = element.children
    if (childs.length > 1)
      throw new Exception("Cannot loop child of "+this.toString)
    childs(0).asInstanceOf[JField].value.children.foreach{
      child =>

        child match{

            case jobject :JObject => el(new LiftWebNode(jobject))
            case jfield : JField  =>
              jfield.value match{
                case JArray(jarray) =>
                  jarray.foreach{
                    node => el(new LiftWebNode(JObject(List(JField(jfield.name,node)))))
                  }
                case _ => el(new LiftWebNode(JObject(List(jfield))))
              }

        	case _ => throw new Exception("Cannot loop child of node "+el)
        }

    }



  }



  def \(that: String): Node = {

    element \ label \ that match{
      case jarray : JArray => new LiftWebNode(jarray)
      case _ => new LiftWebNode(_)
    }
    new LiftWebNode(JObject(List(JField(that, element \ label \ that))))
  }
  def \\(that: String): Node = new LiftWebNode(element \\ that)

  def text: String = {

    element match{
      case jfield : JField  =>
        jfield.value match{
          case JString(value) => value
          case _ => compact(render(jfield.value))
        }
      case jobject: JObject => jobject.children.map{ new LiftWebNode(_).text }.mkString
      case JString(jstring) => jstring
      case JDouble(jvalue) => jvalue.toString
      case JInt(jvalue) => jvalue.toString
      case JBool(jvalue) => jvalue.toString
      case _ => throw new Exception("Not implement text for "+element+"\n"+element.toString)
    }

  }

  def label : String = {

    element match {
      case field   : JField  => field.name
      case jobject : JObject =>
        if (jobject.children.length == 1){
           jobject.children(0).asInstanceOf[JField].name
        }
        else { throw new Exception("Cannot find label for node "+element.toString) }

      case _ => throw new Exception("Cannot find label for node "+element.toString)
    }
  }

  def :<(field: String, value: Any): Node = {
    element match{
      case jobject : JObject =>
         new LiftWebNode(this.element.replace(List(label),jobject.\(label) ++ JField(field,JString(value.toString))))
      case _ => throw new Exception("Insert function not implemented for "+element.getClass()+" object")
    }
  }

  def :<(value: Node): Node = {
    element match{
      case el : JArray  =>
        new LiftWebNode(el ++ value.toLiftJSONObject)
      case _ => throw new Exception("No implemented for object "+element.getClass())
    }
  }


  override def toString: String = { compact(render(element)) }
  def toJSONString: String = compact(render(element))
  def toXMLString: String = net.liftweb.json.Xml.toXml(element).toString()
  def toXMLObject: scala.xml.NodeSeq = net.liftweb.json.Xml.toXml(element)
  def toLiftJSONObject: net.liftweb.json.JsonAST.JValue = element
  def getType = element.getClass.toString

  def isNull : Boolean = {

    element match{
    	case JNothing => true
    	case jobject : JObject =>
    	   if (jobject.children.length == 0) return true
    	   if (jobject.children.length == 1){
    	 		jobject.children(0).asInstanceOf[JField].value match{
    	 		  case JNothing => true
    	 		  case _=> false
    	 		}
    	   }
    	   else false
    	case _ => false
    }
  }
  def size = element match { case JArray(jArray) => jArray.length case _ => 1 }

  def foreach( el  : (Node) => Any ){
    element match{
      case JArray(jArray) => jArray.foreach{ node => el(new LiftWebNode(node))}
      case _ => el(this)
    }
  }

}


object NodeParser{

  def parseXML(str : String) = {

    if (str.equals("")) new LiftWebNode(JNothing)
    else new XMLNodeParser(str)
  }

  def parJSON(str : String) = {

    if (str.equals("")) new LiftWebNode(JNothing)
    else new JSONLiftNodeParser(str)
  }

  def createEmptyNode(nodeName : String) = new XMLNodeParser("<"+nodeName+"></"+nodeName+">")

}

class XMLLiftNodeParser(xmlStr: String) extends LiftWebNode(net.liftweb.json.Xml.toJson(scala.xml.XML.loadString(xmlStr)))
class JSONLiftNodeParser(jsonStr: String) extends LiftWebNode(net.liftweb.json.JsonParser.parse(jsonStr))


object Main{
  def main(args: Array[String]) {

    val str = """ {  "success" : {"experiments":{"experiment":{"userId":"7f86e4c3c7a80969c30b724fb3bca6f","experimentId":"0"}}} } """

    var node : Node = NodeParser.parJSON(str)
    println(node \ "experiments" \\ "experiment")


  }
}

