/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.common.utils

import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.message.BasicNameValuePair
import org.apache.http.NameValuePair
import org.apache.http.client.methods.HttpGet
import org.apache.http.conn.scheme.SchemeRegistry
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.scheme.PlainSocketFactory
import java.security.KeyStore
import fr.inria.bsense.common.http.TrustSSLSocketFactory
import org.apache.http.params.BasicHttpParams
import org.apache.http.conn.params.ConnManagerPNames
import org.apache.http.conn.params.ConnPerRouteBean
import org.apache.http.params.HttpProtocolParams
import org.apache.http.HttpVersion
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
import org.apache.http.impl.conn.PoolingClientConnectionManager
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.client.HttpClient
import org.apache.http.protocol.BasicHttpContext
import org.apache.http.HttpResponse
import scala.io.Source
import java.io.InputStream
import fr.inria.bsense.common.http.Base64
import scala.collection.immutable.HashMap

object Http {
  
  def post( url : String ) : HttpRequestBuilder = new HttpPostRequestBuilder(url)
  def get( url : String )  : HttpRequestBuilder  = new HttpGetRequestBuilder(url)

  def newHttpClient = {
    val trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	trustStore.load(null, null);
	
	val  sf = new TrustSSLSocketFactory(trustStore);
	val schemeRegistry = new SchemeRegistry();
    schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    schemeRegistry.register(new Scheme("https", sf, 443));
    
	val	params = new BasicHttpParams();
	params.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 1);
	
	HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	HttpProtocolParams.setContentCharset(params, "utf8");
		
    val manager = new PoolingClientConnectionManager(schemeRegistry);

    new DefaultHttpClient(manager, params);
  }
  
  def verify(httpResponse : HttpResponse) : InputStream = {
    if ((httpResponse.getStatusLine().getStatusCode() >= 200)
				&& (httpResponse.getStatusLine().getStatusCode() < 300))
    		httpResponse.getEntity().getContent();
	else{
	    
	    val error = Source.fromInputStream(httpResponse.getEntity().getContent())
		throw new Exception("Http Error Code "
					+ httpResponse.getStatusLine().getStatusCode()
					+ " "
					+ error.getLines.mkString("\n"));
	}
  }
}


abstract class HttpRequestBuilder(url : String){
  
  var _param : Map[String,String] = HashMap()
  
  var _header : Map[String,String] = HashMap()
  
  def header( h : Map[String,String] ) : this.type = {_header = h ++ _header;this}
  def param( h :  Map[String,String] ) : this.type = {_param  = h ++ _param ;this}
  
  
  def request() : InputStream = {
    
     val httpRequestBase = createHttpRequestBase(_param)
    
     _header.foreach{
       	case(key,value) =>
       	  httpRequestBase.addHeader(key, value)
     }
    
     val httpClient = Http.newHttpClient
     Http.verify(httpClient.execute(httpRequestBase,new BasicHttpContext()))
  }
  
  def authentication(username : String, password : String) : this.type = {
 
    val headerAuth = ( "Authorization" , "Basic " + new String(Base64.encodeToString((username
				+ ":" + password).getBytes(), false)))
    _header += headerAuth
    this
  }
  
  def asString : String = Source.fromInputStream(this.request).getLines.mkString("\n")
  def asStream : InputStream = this.request   
 
  
  protected def createHttpRequestBase(param : Map[String,String]) : HttpRequestBase
}

class HttpPostRequestBuilder(_url : String) extends HttpRequestBuilder(_url){
  
  override def createHttpRequestBase(param : Map[String,String]) : HttpRequestBase = {
    
    val request = new HttpPost(_url);
    
    val contentType = ("Content-Type","application/x-www-form-urlencoded")
    _header += contentType
     
    val listParam = new java.util.ArrayList[NameValuePair]
    param.foreach{
      case (key,value) =>
        listParam.add(new BasicNameValuePair(key,value))
    }
    
    request.setEntity(new UrlEncodedFormEntity(listParam))
    request
  }
  
}

class HttpGetRequestBuilder(_url : String) extends HttpRequestBuilder(_url){
  def createHttpRequestBase(param : Map[String,String]) : HttpRequestBase = {
    
    val contentType = ("Content-Type","application/x-www-form-urlencoded")
    _header += contentType
    
    new HttpGet(_url)
  }
}