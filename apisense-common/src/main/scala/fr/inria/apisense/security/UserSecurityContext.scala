/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.security

import java.security.Principal
import scala.collection.mutable.{Map,HashMap}
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.Log


object UserSecurityContext{

  /**
   * Create an Anonymous security context
   * being as user role '' ROLE_ANONYMOUS ''
   */
  def anonymous = new APISecureContext("Anonymous","Anonymous",Array("ROLE_ANONYMOUS"))


  def superAdmin = new APISecureContext("ADMIN","SUPER_ADMIN",Array("ROLE_SUPER_ADMIN","ROLE_ADMIN","ROLE_SCIENTIST","ROLE_USER"))

}

class UserAuthorityPrincipal(_user : String, _password : String) extends Principal {
  override def getName() = _user
  def getPassword =_password
}


class UserSecurityContext(
    _principal : Principal ,
    _allowedRoles : Array[String],
    authenticationScheme : String) extends SecurityContext {

  val principal : Principal = _principal

  override def  getAuthenticationScheme() = authenticationScheme
  override def  getUserPrincipal() = principal
  override def  isUserInRole(role : String) = {
    _allowedRoles.find{ case (v) => v.equals(role)} != None
  }

  override def  isSecure() = authenticationScheme != null
}


class APISecureContext(_userId : String, _username : String,_allowedRoles : Array[String] )
	extends UserSecurityContext(
		new Principal(){def getName = _userId},
		_allowedRoles,
		""){

  val username = _username
  var token : String = _

}
