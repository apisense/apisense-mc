/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */

package fr.inria.apisense.intent.cxf{

	import org.apache.cxf.phase.Phase
    import org.apache.cxf.message.Message
    import org.apache.cxf.interceptor.Interceptor
    import org.apache.cxf.interceptor.InterceptorProvider
    import org.apache.cxf.phase.AbstractPhaseInterceptor
    import org.objectweb.fractal.bf.connectors.common.BindingIntentHandler
    import fr.inria.bsense.common.service.LifeCycleService
    import fr.inria.bsense.common.cbse.BSenseComposite

	@org.oasisopen.sca.annotation.Scope("COMPOSITE")
	@org.oasisopen.sca.annotation.Service(Array(classOf[BindingIntentHandler]))
	abstract class AbstractBindingIntentHandler extends BindingIntentHandler with LifeCycleService {

		private var _interceptorProvider : InterceptorProvider = null

		override def apply(proxy : Any) { 
		  
		  proxy match{
		    case server : org.apache.cxf.endpoint.Server => 
		        this._interceptorProvider = server.getEndpoint()
		    	this.configure(this._interceptorProvider)
		    case _ => 
		       
		    	throw new IllegalArgumentException(proxy + " of type "+proxy.getClass+" is not an Apache CXF Web Service/REST client/server!");
		  }
		}

		override def onStart(composite : BSenseComposite){}
	    override def onStop(){ removeConfiguration(_interceptorProvider) }
	    
	    def configure(endPoint : InterceptorProvider)
	    def removeConfiguration(endPoint : InterceptorProvider)
	}

	
	class ArrayBindingIntentInterceptor[T <: Interceptor[Message]](inIn : Array[T], inOut : Array[T]) extends AbstractBindingIntentHandler{

		override def configure(endPoint : InterceptorProvider){
		    inIn.foreach { in  => endPoint.getInInterceptors().add(in) }
			inOut.foreach{ out => endPoint.getOutFaultInterceptors().add(out) }
		}

		override def removeConfiguration(endPoint : InterceptorProvider){
			inIn.foreach { in => endPoint.getInInterceptors().remove(in) }
			inOut.foreach{ out => endPoint.getOutFaultInterceptors().remove(out) }
		}
	}
}