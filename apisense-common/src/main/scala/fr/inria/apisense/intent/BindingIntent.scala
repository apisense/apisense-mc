/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent

import org.apache.cxf.interceptor.InterceptorProvider
import org.osoa.sca.annotations.Property
import org.osoa.sca.annotations.Reference
import fr.inria.apisense.intent.cxf.ArrayBindingIntentInterceptor
import fr.inria.apisense.intent.cxf.AbstractBindingIntentHandler
import fr.inria.apisense.intent.cxf.AuthenticationBasicCXFHandler
import fr.inria.apisense.intent.cxf.AuthorizationCXFHandler
import fr.inria.apisense.intent.cxf.MyCXFLoggingInterceptor
import fr.inria.apisense.intent.cxf.TokenAuthenticationHandler
import fr.inria.apisense.intent.cxf.OutRealmCXFHandler
import fr.inria.apisense.token.TokenManager
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.common.cbse.BSenseComposite
import frascala.sca.Bean
import frascala.sca.Java
import scala.util.matching.Regex
import fr.inria.bsense.common.utils.Log
import fr.inria.apisense.intent.cxf.OutRealmCXFHandler
import fr.inria.apisense.intent.cxf.OutRealmCXFHandler
import fr.inria.apisense.intent.cxf.OutRealmCXFHandler
import fr.inria.apisense.intent.cxf.OutTokenFaultCXFHandler
import scala.util.Random
import fr.inria.bsense.common.service.LifeCycleService


/**
 * 
 * Helper object to create binding intent component
 * 
 * 
 */
object BindingIntent{
  
  private def generateId( str : String ) = {
    "apisense.bintent."+str+"-"+System.currentTimeMillis()
  }
  
  private def intent[T <: BindingIntentComponent](cpt : T) =  {
    
    // create new composite
    val intent = new BSenseComposite(cpt.name){
      
      // insert intent component
      // and promote intent service
      // at composite level
      val c = component(cpt)
      promoteService(c.srvIntent)
    }
    
    intent save;
    intent
  }
  
  /**
   * Generate a simple logging binding intent component
   */
  def logging    = intent(new BindingIntentComponent(generateId("logging"))  uses Bean[LoggingInterceptorImpl])
  def loggingCpt = new BindingIntentComponent(generateId("logging"))  uses Bean[LoggingInterceptorImpl]
  
  /**
   * Generate a basic authentication binding intent component
   * 
   */
  def authenticationBasic = 
    intent(
        new BindingIntentComponent(generateId("auth-basic")) uses Bean[BasicAuthenticationInterceptorImpl])

  /**
   * Generate a token authentication binding intent component
   * 
   */
  def authenticationToken     = intent(new AuthenticationIntentComponent(generateId("auth-token")))
  def authenticationTokenCpt  = new AuthenticationIntentComponent(generateId("auth-token"))
       
  def authorization = new AuthorizationIntent(generateId("authorization"))
  
  def authorizationCpt = new AuthorizationIntentComponent(generateId("authorization"))
  
}



// ***************************************************  BINDING INTENT COMPONENT DEFINITION

class BindingIntentComponent(_name : String) extends BSenseComponent(_name){
  val srvIntent = service("intent") exposes Java[org.objectweb.fractal.bf.connectors.common.BindingIntentHandler]
}

class AuthenticationIntentComponent(_name : String) extends BindingIntentComponent(_name){
  this.uses(Bean[TokenAuthentificationInterceptorImpl])
  reference("tokenManager") exposes Java[TokenServiceManager] as TokenManager.getAddress
}



// ***************************************************  BINDING INTENT IMPLEMENTATION 

class BasicAuthenticationInterceptorImpl extends AbstractBindingIntentHandler{
  val cxfInHandler  = AuthenticationBasicCXFHandler()
  val cxfOutHandler = OutRealmCXFHandler()
  def configure(endPoint : InterceptorProvider){  
     endPoint.getInInterceptors.add(cxfInHandler)
     endPoint.getOutFaultInterceptors.add(cxfOutHandler) 
  }
  def removeConfiguration(endPoint : InterceptorProvider){ 
    endPoint.getInInterceptors.remove(cxfInHandler)
    endPoint.getOutFaultInterceptors.remove(cxfOutHandler) 
  }
}


  
class TokenAuthentificationInterceptorImpl extends AbstractBindingIntentHandler with LifeCycleService{
  
   @Reference
   val tokenManager : TokenServiceManager = null
  
   var inHandler : TokenAuthenticationHandler = _
   var outFaultHandler : OutTokenFaultCXFHandler = _
    
   def configure(endPoint : InterceptorProvider){
     
     if (inHandler == null){
       inHandler = new TokenAuthenticationHandler(tokenManager)
       outFaultHandler = new OutTokenFaultCXFHandler
     }
     endPoint.getInInterceptors().add(this.inHandler)
     endPoint.getOutFaultInterceptors.add(this.outFaultHandler)
   }
	
   def removeConfiguration(endPoint : InterceptorProvider){ 
       endPoint.getInInterceptors().remove(inHandler) 
       endPoint.getOutFaultInterceptors().remove(outFaultHandler)
   }
   
}


class LoggingInterceptorImpl extends ArrayBindingIntentInterceptor(
      Array(MyCXFLoggingInterceptor()),
      Array())


