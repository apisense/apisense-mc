/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent.cxf

import org.apache.cxf.phase.AbstractPhaseInterceptor
import org.apache.cxf.phase.Phase
import org.apache.cxf.message.Message
import fr.inria.bsense.common.utils.Log

case class MyCXFLoggingInterceptor extends AbstractPhaseInterceptor[Message](Phase.RECEIVE) {

  override def handleMessage(message : Message){
    println("Receive message "+message.toString())
    
    val keyIterator = message.keySet().iterator()
    while (keyIterator.hasNext){
      val key = keyIterator.next()
      val v = message.get(key)
    
     if (v != null) println(key+" --> "+v.getClass()+" : "+v)
     else println(key)
    }
  
  }
  
}