/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent.cxf

import org.apache.cxf.phase.AbstractPhaseInterceptor
import org.apache.cxf.message.Message
import org.apache.cxf.phase.Phase
import javax.servlet.http.HttpServletRequest
import fr.inria.apisense.token.TokenServiceManager
import javax.ws.rs.core.SecurityContext
import org.apache.cxf.interceptor.Fault
import javax.servlet.http.HttpServletResponse
import org.apache.cxf.transport.http.AbstractHTTPDestination
import org.apache.cxf.interceptor.security.AccessDeniedException

case class OutRealmCXFHandler extends AbstractPhaseInterceptor[Message](Phase.PRE_STREAM){

  override def handleMessage(message : Message){
    
    val fault : Fault = message.getContent(classOf[Exception]).asInstanceOf[Fault]
    if (fault.getCause().isInstanceOf[SecurityException]){
     
     val response = message.getExchange().getInMessage()
    		 .get(AbstractHTTPDestination.HTTP_RESPONSE).asInstanceOf[HttpServletResponse]
         
     response.setStatus(401)
     if (fault.getCause().isInstanceOf[AccessDeniedException]){
       response.setStatus(403)
     }
     
     response.setHeader("WWW-Authenticate","Basic realm=apisense.fr");
     response.getOutputStream().write(fault.getCause().getMessage().getBytes());
     response.getOutputStream().flush();
     message.getInterceptorChain().abort();
     
    }
  }
  
}