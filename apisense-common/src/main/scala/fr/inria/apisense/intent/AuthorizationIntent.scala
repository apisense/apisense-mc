/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent

import scala.util.matching.Regex

import org.apache.cxf.interceptor.InterceptorProvider
import org.osoa.sca.annotations.Property
import org.osoa.sca.annotations.Service

import fr.inria.apisense.intent.cxf.AbstractBindingIntentHandler
import fr.inria.apisense.intent.cxf.AuthorizationCXFHandler
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.Node
import frascala.sca.Bean
import frascala.sca.Java




class AuthorizationIntent(_name : String) extends BSenseComposite(_name){

  // declare intent component
  val intentComponent = component(
      AuthorizationIntentComponent(_name)
  )
  promoteService(intentComponent.srvIntent)
  
  def rule( regex : String, allowedRoles : Array[String] ){
    intentComponent.rule(regex, allowedRoles)
    this.save
  }
  
  this.save
}


case class AuthorizationIntentComponent(_name : String) extends BindingIntentComponent(_name){
  this.uses(Bean[AuthorizationInterceptorImpl])
  
  val authorisationRules = property[String]("authorisationRules") is "[]"
  
  service("lifeM") exposes Java[LifeCycleService]
  
  def rule( regex : String, allowedRoles : Array[String] ){

	  val builder = new StringBuilder 
	  builder append "{"
	  builder append """ "regex":"""
	  builder append " \""+regex+"\" "
	  builder append ","
	  builder append """ "roles":"""
	  builder append "["
	  if (!allowedRoles.isEmpty){
		 allowedRoles.foreach{ r => builder append "\""+r+"\"," }
		 builder.deleteCharAt(builder.length-1)
	  }
	  builder append "]"
	  builder append "}"

	  var jsonRules : Node = new JSONLiftNodeParser(authorisationRules.getValue) 
	  jsonRules = jsonRules :< new JSONLiftNodeParser(builder toString)

	  authorisationRules is jsonRules.toJSONString
  }
}



class AuthorizationInterceptorImpl extends AbstractBindingIntentHandler with LifeCycleService{
  
  var cxfHandler : AuthorizationCXFHandler = new AuthorizationCXFHandler()
  
  @Property
  var authorisationRules : String = null
  
  def setAuthorisationRules( rules : String){
	this.authorisationRules = rules
	update
  }
  
  def update(){
    if (cxfHandler != null){
      try{
    	  val authorizationMap = jsonToMap(this.authorisationRules)
    	  cxfHandler.authorizationRules = authorizationMap
      }catch{
        case th : Throwable =>
          Log.w("Cannot parse authorization rules "+this.authorisationRules)
      }
    }
  }
    //
  // Example of authorization rules JSON format
  // *******************************************
  //
  // {  "rules" : [
  //    { "regex" : "/service/*", "roles" : [ "ROLE_ADMIN", "ROLE_USER" ] },
  //    { "regex" : "/service2/*", "roles" : [ "ROLE_USER" ] },
  //    { "regex" : "/service/*", "roles" : [] }
  // ]}
  //
  def jsonToMap(str : String) = {
    
    // create map result
    var resultMap : Map[Regex,List[String]] = Map()
    
    // convert JSON string in Node object
    val rules = new JSONLiftNodeParser(str)
    
    // for each elements
    rules.foreach{ node =>
        
        var roles : List[String] = List()
        
        node \\ "roles" foreach{ role => roles = role.text :: roles }
        
        // insert regex and roles in result map
        resultMap = resultMap + ( node.\\("regex").text.r -> roles) 
    }
    
    resultMap
  }
  
  def configure(endPoint : InterceptorProvider){  
     this.cxfHandler = new AuthorizationCXFHandler()
     endPoint.getInInterceptors().add(this.cxfHandler)
     update()
  }
	
  def removeConfiguration(endPoint : InterceptorProvider){ 
     endPoint.getInInterceptors().remove(cxfHandler) 
  }
}