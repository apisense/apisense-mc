/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent.cxf

import org.apache.cxf.phase.AbstractPhaseInterceptor
import org.apache.cxf.message.Message
import org.apache.cxf.phase.Phase
import javax.servlet.http.HttpServletRequest
import fr.inria.apisense.token.TokenServiceManager
import javax.ws.rs.core.SecurityContext
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.bsense.common.utils.Log
import org.apache.cxf.jaxrs.ext.MessageContextImpl

case class InvalidTokenException extends SecurityException("Invalid Token")

class TokenAuthenticationHandler(tokenManager : TokenServiceManager) extends AbstractPhaseInterceptor[Message](Phase.RECEIVE) {

  var COOKIES_NAME = "fr.apisense.token"

  override def handleMessage(message : Message){

    // search token in header
    var token = message.get("HTTP.REQUEST").asInstanceOf[HttpServletRequest].getHeader("TOKEN")

    // if token cannot be found
    // search token in cookies
    if (token == null) {

       val cookie = new MessageContextImpl(message).getHttpHeaders().getCookies()
       if (cookie.containsKey(COOKIES_NAME)){
          token = cookie.get(COOKIES_NAME).getValue()
       }
    }

    // if token is null
    // create anonymous connection
    if (token == null){
      message.put(classOf[SecurityContext],
          UserSecurityContext.anonymous)
      return
    }

    // else search in the token manager
    // user credential
    val securityContext = tokenManager.getUserPrincipal(token)
    if (securityContext != null){
      message.put(classOf[SecurityContext], securityContext)
    }
    else throw InvalidTokenException()
  }

}