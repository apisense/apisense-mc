/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent.cxf

import org.apache.cxf.configuration.security.AuthorizationPolicy
import org.apache.cxf.message.Message
import org.apache.cxf.phase.AbstractPhaseInterceptor
import org.apache.cxf.phase.Phase

import fr.inria.apisense.security.UserAuthorityPrincipal
import fr.inria.apisense.security.UserSecurityContext
import javax.ws.rs.core.SecurityContext

case class AuthenticationBasicCXFHandler extends AbstractPhaseInterceptor[Message](Phase.RECEIVE) {

  override def handleMessage(message : Message){
    val authorization : AuthorizationPolicy = message.get(classOf[AuthorizationPolicy])
    if (authorization == null) message.put(classOf[SecurityContext], UserSecurityContext.anonymous)
    else message.put(classOf[SecurityContext], new UserSecurityContext(
    		new UserAuthorityPrincipal(authorization.getUserName(),authorization.getPassword()),
    		Array(),
    		authorization.getAuthorizationType()
    ))
  }
  
}