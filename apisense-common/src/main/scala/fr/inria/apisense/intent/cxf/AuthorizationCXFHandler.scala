/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.intent.cxf

import org.apache.cxf.phase.AbstractPhaseInterceptor
import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.apache.cxf.phase.Phase
import org.apache.cxf.message.Message
import javax.ws.rs.core.SecurityContext
import scala.util.matching.Regex
import fr.inria.bsense.common.utils.Log

case class MissingCredentialException extends SecurityException("No Security Context")

class AuthorizationCXFHandler extends AbstractPhaseInterceptor[Message](Phase.PRE_STREAM){

   var authorizationRules : Map[Regex,List[String]] = Map()

   override def handleMessage(message : Message){

       // get service path
       val requestPath : String = message.get(Message.PATH_INFO).asInstanceOf[String]

       // find all authorized roles defined in authorization rules for
       // service call
       val authorizedRoles = findAllowedRole(requestPath, authorizationRules)

       // If no authorization rule has been defined
       // message is not intercepted
       if (authorizedRoles.isEmpty){
         Log.w("no authorization has been defined for service "+requestPath)
         return
       }

       println(authorizedRoles)

       val securityContext = message.get(classOf[SecurityContext])

       // If message has no security context
       // throw security exception
       if (securityContext == null){
         val exception = MissingCredentialException()
         message.put(classOf[Exception],exception)
         throw exception
       }

       val authorized = authorizedRoles.find( (role) => securityContext.isUserInRole(role) )
       if (authorized == None){
          val exception = new AccessDeniedException("Access denied Exception")
          message.put(classOf[Exception],exception)
         throw exception
       }
   }

   private def findAllowedRole( path : String, authorizationRule : Map[Regex,List[String]]) : List[String] = {

    var authorizedRoles = List[String]()
    authorizationRule.foreach{
    	 case(regex,value) =>
    	 	val find = regex.findFirstIn(path)
    	 	if (find != None){
    	 	  authorizedRoles = value ::: authorizedRoles
    	 	}
    }
    authorizedRoles
  }

}
