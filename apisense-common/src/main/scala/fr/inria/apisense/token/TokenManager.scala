/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.token

import org.osoa.sca.annotations.Service
import org.osoa.sca.annotations.Scope
import org.osoa.sca.annotations.Property
import fr.inria.bsense.common.service.LifeCycleService
import java.awt.event.ActionListener
import java.awt.event.ActionEvent
import javax.swing.Timer
import fr.inria.bsense.common.cbse.BSenseComposite
import frascala.sca.Java
import fr.inria.bsense.common.utils.MD5
import java.util.Date
import frascala.sca.Bean
import TokenManager._
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.apisense.security.APISecureContext

object TokenManager {

  final val TOKEN_MANAGER_SERVICE = "intent"

  final val SCA_PROPERTY_SECRET_KEY = "apisense.property.secret_key"

  final val SCA_PROPERTY_DROP_TOKEN = "apisense.property.drop_token"

  final val SCA_PROPERTY_PERIOD = "apisense.property.drop_period"

  def getAddress = frascala.frascati.SCA(SCA_COMPOSITE_NAME+"/intent")

  final val SCA_COMPOSITE_NAME = "apisense.tokenManager"

}

trait TokenServiceManager {

  @Property(name = TokenManager.SCA_PROPERTY_SECRET_KEY) var secretKey : java.lang.String = _

  @Property(name = TokenManager.SCA_PROPERTY_DROP_TOKEN) var dropToken : java.lang.Boolean = _

  @Property(name = TokenManager.SCA_PROPERTY_PERIOD) var period : Long = _

  def createToken(user : APISecureContext) : String

  def getUserPrincipal(token : String) : UserSecurityContext

  def removeUserPrincipal(token : String)

  protected def generateToken(user : UserSecurityContext) =
    MD5.hash(user.getUserPrincipal.getName()+secretKey+new Date().toGMTString())

}

@Scope("COMPOSITE")
abstract class AbstractTokenImpl extends TokenServiceManager with ActionListener with LifeCycleService {

  def onStart(c : BSenseComposite){ if (dropToken) daemon }

  def onStop(){}

  private def daemon(){

    val timer = new Timer(period.toInt,this);
    timer.setInitialDelay(period.toInt)
    timer start
  }

  protected def actionPerformed(e : ActionEvent) { }

}


class TokenManagerComposite[T <: AbstractTokenImpl](implicit var cls: Manifest[T]) extends BSenseComposite(TokenManager.SCA_COMPOSITE_NAME){

  val tokenComponent = new component(SCA_COMPOSITE_NAME){

    //val secretKey = property[String](SCA_PROPERTY_SECRET_KEY) is "secret"
    //val dropToken = property[Boolean](SCA_PROPERTY_DROP_TOKEN) is false
    //val period    = property[Long](SCA_PROPERTY_PERIOD) is (60 * 60 * 1000)

    val intent = service(TOKEN_MANAGER_SERVICE) exposes Java[TokenServiceManager]

  } uses Bean[T]

  promoteService(tokenComponent.intent)
}


case class InvalidTokenException extends Exception("Invalid token")
