/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.token

import scala.collection.mutable.Map
import scala.collection.mutable.HashMap
import fr.inria.bsense.common.utils.MD5
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.apisense.security.APISecureContext

case class LastConnection(_user : UserSecurityContext) {
  var _time : Long = System.currentTimeMillis()
  def user = _user
}

class ImplTokenManagerMap extends AbstractTokenImpl with TokenServiceManager {

  var tokenMap : Map[String,LastConnection] = HashMap()

  def createToken(user : APISecureContext) : String = {
    val token = generateToken(user)
    user.token = token
	tokenMap.put(token, LastConnection(user))
    token
  }

  def getUserPrincipal(token : String) : UserSecurityContext = {

    if (!tokenMap.contains(token)) return null

    val connection = tokenMap(token)
    connection._time = System.currentTimeMillis()
    connection._user
  }

  def removeUserPrincipal(token : String) {

     if (!tokenMap.contains(token))
       throw InvalidTokenException()

     tokenMap.remove(token)
  }

}