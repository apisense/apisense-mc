/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.node

import net.liftweb.json.{JsonAST => jast}
import net.liftweb.json.{Xml => jxml}
import net.liftweb.{ json => j}
import scala.xml.XML

object NodeJSONHelper {
  
  implicit def jvalue2jvaluAug(element : jast.JValue) = JValueAug(element)
  
  def parseJSON(str : String) = { j.parse(str) }
  def parseXML(str : String) = jxml.toJson(XML.loadString(str))
}


case class JValueAug( element : jast.JValue ) {
  
  def \(elName : String) = element.\(elName)
  def \\(elName : String) = element.\\(elName)
  
  def isEmpty : Boolean = {
    element match{ 
    	case jast.JNothing => true 
    	case jobject : jast.JObject => jobject.children.length == 0
    	case jarray  : jast.JArray   => jarray.arr.length == 0
    	case _ => false 
    }
  }
  
  def text: String = { 
    
    element match{
      case jfield : jast.JField  =>  
        jfield.value match{
          case jast.JString(value) => value
          case _ => j.compact(j.render(jfield.value))
        }
      case jobject: jast.JObject => jobject.children.map{ JValueAug(_).text }.mkString
      case jast.JString(jstring) => jstring
      case _ => throw new Exception("Not implement text for "+element+"\n"+element.toString)
    }
    
  }
  
  
  def asXMLString  = jxml.toXml(element).toString()
  def asJSONString = j.compact(j.render(element))
}