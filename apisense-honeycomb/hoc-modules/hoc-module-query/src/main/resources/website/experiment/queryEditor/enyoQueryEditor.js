enyo.kind({

	name: "apisense.web.QueryEditor",

	constructor : function(object){

        this.inherited(arguments);

        this.service = object.service || "./";
        this.divId   = object.id || "editor";
        this.height  = object.height || "500px";

        var self = this;

        this.editor = new APISEditor({
        	onFileSelected : function(editor,filename){

        		filename = filename.substring(filename.indexOf('/')+1);

        		var request = new APIS.POST(self.service+"/query/get/file",{filename: filename});
        		request.onSuccess = function(content){
        			editor.open(filename,decode(content));
        		}

        	}
        });

        this.editor.renderInto(this.divId);


        this.updateTree();

        var tool = this.editor.toolbar();

		tool.newfile(function(f){self.addfile(f)},[".xq"]);
		tool.deletefile(function(f){ self.deletefile(f)});
		tool.separator();
		tool.savefile(function(f,c){ self.savefile(f,c) })

		tool.menu("run","icon-play-circle",function(){self.play();})
		tool.menu("download","icon-download-alt",function(){self.download();})

        this.editor.initConsole();
	},

	create : function(object){

		this.inherited(arguments);
	},

	updateTree : function(){

		var self = this;

		var request = new APIS.POST(this.service+"/query/get/files");
		request.onSuccess = function(tree){

			self.editor.updateTreeModel(tree)
		}
	},

	addfile : function(file){

		var defaultQuery = ""
		defaultQuery += ' import module namespace apis = "http://fr.inria.apisense/xquery"; \n'
		defaultQuery += '\n'
		defaultQuery += 'declare variable $xp := "'+xpName+'";'
		defaultQuery += '\n'
		defaultQuery += '\n'
		defaultQuery += 'apis:traces($xp)'

		var self = this;

		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/query/update/file", {filename : file, query:encode(defaultQuery) });
		request.onSuccess = function(mess){

			self.updateTree();
		}
	},

	deletefile : function(file){

		var self = this;
		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/query/delete/file", {filename : file});
		request.onSuccess = function(mess){

			self.updateTree();
		}
		request.onError = function(err){ alert(err) }
	},

	savefile : function(file,content){

		var self = this;
		file = file.substring(file.indexOf('/')+1);


		new APIS.POST(this.service+"/query/update/file", {filename : file,query : encode(content)});
	},

	play : function(){

		var self = this;

		var file = this.editor.getAceEditor().caption;
		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/query/process/file", {filename : file},{parse:false});
		request.onSuccess = function(mess){

			self.editor.setConsoleValue(mess)
		}
	},

	download : function(){

		var self = this;

		var file = this.editor.getAceEditor().caption;
		file = file.substring(file.indexOf('/')+1);

		var url =  this.service+"/query/get/process/file/"
		url += file
		redirect(url,{},"GET")
		var request = new APIS.POST(this.service+"/query/process/file", {filename : file},{parse:false});
		request.onSuccess = function(mess){

			self.editor.setConsoleValue(s)
		}
	}




})



