/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.query

import fr.inria.bsense.node.service.ExperimentModuleService
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.FormParam
import javax.ws.rs.core.Response
import javax.ws.rs.GET
import javax.ws.rs.PathParam
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context

trait QueryService extends ExperimentModuleService {

  /**
   * Execute query in database and store result in server
   * Statistic and a subset of result is returned
   */
  @POST
  @Path("/process")
  def process( @Context sc : SecurityContext , @FormParam("query") query : String) : String

  /**
   * Execute query in database and return result
   * Statistic and a subset of result is returned
   */
  @POST
  @Path("/mprocess")
  def mprocess(
      @Context sc : SecurityContext ,
      @FormParam("query") query : String,
      @FormParam("params") params : String) : Response

  /**
   * Execute query in database defined in query file ''filename''
   * stored in server
   */
  @POST
  @Path("/process/file")
  def processQueryFile(
      @Context sc : SecurityContext,
      @FormParam("filename") filename : String,
      @FormParam("params") params : String) : String

 /**
  * Execute query in database defined in query file ''filename''
  * stored in server
  */
  @POST
  @Path("/mprocess/file")
  def mprocessQueryFile(
      @Context sc : SecurityContext,
      @FormParam("filename") filename : String,
      @FormParam("params") params : String) : Response

  /**
   * Return all query files stored in server
   */
  @POST
  @Path("/get/files")
  def getQueryFiles(@Context sc : SecurityContext) : String

  def getQueryInFiles(sc : SecurityContext) : Array[String]
  def getQueryOutPath(sc : SecurityContext, filename : String) : String



  /**
   * Delete query file ''filename''
   */
  @POST
  @Path("/delete/file")
  def deleteQuery(@Context sc : SecurityContext, @FormParam("filename") filename : String) : String

  /**
   * Return content of query file ''filename''
   */
  @POST
  @Path("/get/file")
  def getQuery(@Context sc : SecurityContext, @FormParam("filename") filename : String) : String

  /**
   *
   * Update query file and store it in local server
   * Create a new query file if not exist
   *
   */
  @POST
  @Path("/update/file")
  def updateQueryFile(@Context sc : SecurityContext, @FormParam("filename") filename : String, @FormParam("query") query : String) : String

  /**
   * Download last query result file
   */
  @GET
  @Path("/get/process")
  def download(@Context sc : SecurityContext) : Response

  /**
   * Download last query result file processed
   * by file ''filename''
   */
  @GET
  @Path("/get/process/file/{filename}")
  def download(@Context sc : SecurityContext,@PathParam("filename") filename : String) : Response

  @GET
  @Path("/get/process/result/{filename}")
  def result(@Context sc : SecurityContext,@PathParam("filename") filename : String) : Response

}