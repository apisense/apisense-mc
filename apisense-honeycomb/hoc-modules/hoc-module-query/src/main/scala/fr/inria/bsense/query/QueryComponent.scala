/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.query

import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.cbse.DatabaseComposite
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.node.cbse.ExperimentModule
import frascala.frascati.SCA.apply
import frascala.sca.Java.apply
import fr.inria.bsense.query.impl.QueryImpl





class QueryComponent extends ExperimentModule("fr.inria.bsense.query"){
  import frascala.sca._
  import frascala.frascati._
  
  this.uses(Bean[QueryImpl])
  
  // database reference
  reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService] as SCA(DatabaseComposite.COMPOSITE_NAME + "/" + DatabaseComponent.SERVICE_QUERY)
  reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService] as SCA(DatabaseComposite.COMPOSITE_NAME + "/" + DatabaseComponent.SERVICE_QUERY_UPDATE)
  
  val queryService = service("srv-query") exposes Java[QueryService]
  
  deployRest(queryService, "query", Array())
  
  def getModuleName: String = "Service Rest Query"
  def getModuleURI: String  = "/query"
  
}

