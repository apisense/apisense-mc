/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.query.impl

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import fr.inria.bsense.common.http.Base64
import fr.inria.bsense.common.utils.IOUtils
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.node.FunctionalityDatabase
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.query.QueryService
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import org.json.simple.parser.JSONParser
import org.json.simple.JSONObject
import scala.util.parsing.json.JSONArray
import java.io.OutputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream

class QueryImpl  extends QueryService with FunctionalityDatabase {


  def process( sc : SecurityContext,  query : String) : String = exec {

    val userId = sc.getUserPrincipal().getName()

    // get output folder absolute path
    var outputFile = NodeFileConfiguration("experiment/"+experimentName+"/process/cache").compositeFolder
    outputFile += "/cache.data"

    //create cache file for store query result
    var out = new FileOutputStream(outputFile)

    val queryInfo = queryService.query(new String(Base64.decode(query.getBytes())),out)
    val queryData = IOUtils.inputStreamToString(new FileInputStream(outputFile), 200)

    queryInfo+"\n\n"+queryData

  }

  def mprocess( sc : SecurityContext,  query : String, params : String) : Response = {

    val response = Response.ok()

    val stream : ByteArrayOutputStream = new ByteArrayOutputStream;

    var paramsTuples : Array[(String,Any,String)] = null;
    exec{

    	if (params != null){

    		paramsTuples = paramStringToTuple(params);
    		queryService.queryWithParams(
    			new String(Base64.decode(query.getBytes())),
    	    	stream,
    	    	paramsTuples)
    	}
    	else{

    		queryService.query(new String(Base64.decode(query.getBytes())), stream)
    	}
    }

    Response.ok(stream.toByteArray()).build()
  }




   def mprocessQueryFile( sc : SecurityContext,  filename : String, params : String) : Response = {

		var paramsTuples : Array[(String,Any,String)] = null;
  		if (params != null){

  			paramsTuples = paramStringToTuple(params);
  		}

  		var out = new ByteArrayOutputStream
  		exec {

  			// get query content
  			var query = NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn").getFile(filename)

  			if (paramsTuples != null){

  				queryService.queryWithParams(query,out,paramsTuples)

  			}else{

  				queryService.query(query,out)
  			}
  		}

  		Response.ok(out.toByteArray()).build()
  }

  def processQueryFile( sc : SecurityContext,  filename : String, params : String) : String = {

    val userId = sc.getUserPrincipal().getName()

    var paramsTuples : Array[(String,Any,String)] = null;

    if (params != null){

    	paramsTuples = paramStringToTuple(params);
    }

    var result : String = null
    var message = exec {

      // get output folder absolute path
      var outputFile = NodeFileConfiguration("experiment/"+experimentName+"/process/queryOut").compositeFolder
      outputFile += "/"+filename+".data"

      //create cache file for store query result
      var out = new FileOutputStream(outputFile)

      // get query content
      var query = NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn").getFile(filename)

      var queryInfo : String = ""
      if (paramsTuples != null){

        queryInfo = queryService.queryWithParams(query,out,paramsTuples)

      }else{

        queryInfo = queryService.query(query,out)
      }

      val queryData = IOUtils.inputStreamToString(new FileInputStream(outputFile), 200)

      result = queryInfo+"\n\n"+queryData
   }

   if (result == null) message
   else result
  }

   def updateQueryFile(sc : SecurityContext, filename : String, query : String) = {
    val userId = sc.getUserPrincipal().getName()
    NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn").updateFile(filename, new String(Base64.decode(query.getBytes())))
    Message.success()
  }

  def getQueryInFiles(sc : SecurityContext) : Array[String] = {
    NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn")
    	.getFiles.map{ x => x.getName() }
  }

  def getQueryOutPath(sc : SecurityContext, filename : String) : String = {

    val files = NodeFileConfiguration("experiment/"+experimentName+"/process/queryOut")
    if (files.contain(filename+".data"))
      files.getFile(filename+".data")
    else null
  }

  def getQueryFiles(sc : SecurityContext) : String = {

    val rootFolder = NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn").getFile

    Message.success(new JSONLiftNodeParser(IOUtils.hierarchy(rootFolder, experimentName)))
  }

  def getQuery(sc : SecurityContext, filename : String) : String = {

    val userId = sc.getUserPrincipal().getName()

    val queryContent = NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn").getFile(filename)
    Message.success(Base64.encodeToString(queryContent.getBytes(),false))
  }

  def deleteQuery(sc : SecurityContext, filename : String) : String = {

    val userId = sc.getUserPrincipal().getName()

    NodeFileConfiguration("experiment/"+experimentName+"/process/queryIn").deleteFile(filename);
    Message.success()

  }

  def download(sc : SecurityContext) : Response = {

    val userId = sc.getUserPrincipal().getName()

    // get output folder absolute path
    var outputFile = NodeFileConfiguration("experiment/"+experimentName+"/process/cache").compositeFolder
    outputFile += "/cache.data"

    var file = new File(outputFile)

    Response.ok(file).header("Access-Control-Allow-Origin","*").header("Content-Disposition",
	  		"attachment; filename="+file.getName()).build();

  }

   def result(sc : SecurityContext, filename : String) : Response = {

    // get output folder absolute path
    var outputFile = NodeFileConfiguration("experiment/"+experimentName+"/process/queryOut").getFile(filename+".data")


    Response.ok(outputFile).header("Access-Control-Allow-Origin","*").build();

  }

  def download(sc : SecurityContext, filename : String) : Response = {

    val userId = sc.getUserPrincipal().getName()

    // get output folder absolute path
    var outputFile = NodeFileConfiguration("experiment/"+experimentName+"/process/queryOut").compositeFolder
    outputFile += "/"+filename+".data"

    var file = new File(outputFile)

    Response.ok(file).header("Access-Control-Allow-Origin","*").header("Content-Disposition",
	  		"attachment; filename="+file.getName()).build();

  }

  private def paramStringToTuple( params : String) = {

    var paramsTuples : Array[(String,Any,String)] = null;

  	new JSONParser().parse(params) match {

  	case json : org.json.simple.JSONObject =>

  		paramsTuples = new Array(1);
  		paramsTuples(0) = (
		  json.get("name").toString,
		  json.get("value"),
		  json.get("type").toString
		)

  	case jsonArray : org.json.simple.JSONArray =>

  		paramsTuples = new Array(jsonArray.size());
  		for (i <- 0 until jsonArray.size()){

  			jsonArray.get(i) match{

  				case json : org.json.simple.JSONObject =>

  					paramsTuples(i) = (
  						json.get("name").toString,
  						json.get("value"),
  						json.get("type").toString
  					)
  			}
  		}
  	}
  	paramsTuples
  }


}