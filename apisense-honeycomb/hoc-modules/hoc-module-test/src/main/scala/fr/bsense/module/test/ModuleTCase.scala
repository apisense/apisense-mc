/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.bsense.module.test

import org.junit.Before
import fr.inria.bsense.common.cbse.BSRuntime
import fr.inria.bsense.node.cbse.ExperimentManagerComponent
import fr.inria.bsense.node.cbse.UserManagerComponent
import fr.inria.bsense.node.factory.CBSEFactory.generate
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.test.ScalaTestCase
import fr.inria.sspl.dsl.SPL.feat2dsl
import fr.inria.sspl.dsl.SPL.load
import fr.inria.sspl.dsl.SPL.slice2dsl
import net.liftweb.json.JsonAST.JString
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.http.HttpRequest
import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.test.ScalaTestCase
import java.io.File
import java.io.BufferedReader
import java.io.InputStreamReader
import org.junit.After
import org.apache.http.conn.HttpHostConnectException
import fr.inria.bsense.server.run.StartServer
import fr.inria.bsense.common.utils.Http
import fr.inria.bsense.common.utils.MD5



object ModuleTCase{

  var HOST = "http://localhost:18000"

  // Declare central server services url
  final val CENTRAL_HOST = "http://localhost:18001";
  final val CENTRAL_SERVICE_CREATE_BEE_USER 		= CENTRAL_HOST+"/user/add/user/bee"
  final val CENTRAL_SERVICE_CONNECT           		= CENTRAL_HOST+"/user/get/token";
  final val CENTRAL_SERVICE_SUBSCRIBE_EXPERIMENT    = CENTRAL_HOST+"/store/user/update/subscribe"
  final val CENTRAL_SERVICE_UNSUBSCRIBE_EXPERIMENT  = CENTRAL_HOST+"/store/user/update/unsubscribe"
  final val CENTRAL_SERVICE_GET_SUBSCRIBED_XP       = CENTRAL_HOST+"/store/user/get/subscribed"
  final val CENTRAL_SERVICE_DOWNLOAD_EXPERIMENT     = CENTRAL_HOST+"/store/user/get/experiment"
  final val CENTRAL_SERVICE_CREATE_SCIENTIST  = CENTRAL_HOST+"/user/add/user/organization";
  final val CENTRAL_SERVICE_DROP_Experiment  = CENTRAL_HOST+"/user/add/user/organization";

}

abstract class ModuleTCase extends ScalaTestCase{



  var HOST = "http://localhost:18000"


  var admin_token : String = null;
  var scientist_token : String = null;
  var bee_token : String = null;

  final val http = new HttpRequest();

  def startHive : Boolean = true
  var centralServerProcess : Process = null;

  @Before
  def inialize(){
      import fr.inria.sspl.dsl.SPL._
      import fr.inria.bsense.node.factory.CBSEFactory._


     if (startHive){
     //----
     // Start central node and create
     // an organization account
     centralServerProcess = StartServer.AsynchStart("../../../../")
     Http post(ModuleTCase.CENTRAL_SERVICE_CREATE_SCIENTIST) param Map(
        "organization" -> "Tester Organization",
		"description"  -> "Tester Organization",
		"username"     -> "inria-tester",
		"password"     -> "inria-tester",
		"email"        -> "scientist.bsense.com")
     }

     // configure hive node connection
     // with central server
     ServerNodeConfiguration.setUrl(ModuleTCase.CENTRAL_HOST)
	 ServerNodeConfiguration.password("inria-tester")
	 ServerNodeConfiguration.username("inria-tester")

	 // set default uri to expose REST service
     System.setProperty("org.ow2.frascati.binding.uri.base",HOST)

	 // start hive node
     fr.inria.bsense.node.start.HoneyCombStarter.start(Array("Beehive Website"))

     // create scientist user on hive node
	 this.createUser

	 // create experiment on Hive node
	 this.createExperiment
  }

  def createUser(){

    // get admin token from central server
	var JString(_tmp0) =  jnotfail("get scientist token"){
    Http post("http://localhost:18000/user-manager/get/token") param Map(
        "username" -> "admin",
        "password" -> MD5.hash("admin")) asString
    }
    this.admin_token = _tmp0

    // create scientist user
    jcanfail("create scientist user"){
      Http.post("http://localhost:18000/user-manager/add/user/scientist") param Map(
        "fullname"     -> "test",
		"username"     -> "test",
		"password"     -> "test",
		"email"        -> "test.apisense.fr"
      ) header Map("TOKEN" -> admin_token) asString
    }

    // get scientist user token
    var JString(_tmp) =  jnotfail("get scientist token"){
       Http post("http://localhost:18000/user-manager/get/token") param Map(
         "username" -> "test",
         "password" -> MD5.hash("test")) asString
    }

    scientist_token = _tmp

    // create bee user in central node
    // can fail if bee account is already created
    jcanfail("create bee user"){
      Http.post(ModuleTCase.CENTRAL_SERVICE_CREATE_BEE_USER) param Map(
        "fullname"     -> "Bee",
		"username"     -> "beeuser",
		"password"     -> "beeuser",
		"email"        -> "bee@bsense.com"
      ) asString
    }

    // get bee user token
	var JString(_tmp2) =  jnotfail("get bee token"){
       Http post(ModuleTCase.CENTRAL_SERVICE_CONNECT) param Map(
         "username" -> "beeuser",
         "password" -> MD5.hash("beeuser")) asString
    }

	bee_token = _tmp2

  }

  def createExperiment(){

    // drop experiment
    // can fail if experiment doesn't exist
    jcanfail("drop experiment "+getExperimentNameTest){
      Http post("http://localhost:18000/experiment-manager/delete/xp") param Map(
        "name" -> getExperimentNameTest
      )  header Map("TOKEN" -> scientist_token) asString
    }

    //create new experiment
    jnotfail("create experiment "+getExperimentNameTest){
       val host = "http://localhost:18000/"+getExperimentNameTest
       Http post("http://localhost:18000/experiment-manager/add/xp") param Map(
         "name" -> getExperimentNameTest,
         "niceName" -> getExperimentNameTest,
         "description" -> "none",
         "copyright" -> "none",
         "baseUrl" -> host,
         "spl" -> getSPL
      ) header Map("TOKEN" -> scientist_token) asString;
    }

    // start experiment
    jnotfail("start experiment "+getExperimentNameTest){
      Http post("http://localhost:18000/experiment-manager/update/start") param Map(
        "name" -> getExperimentNameTest
      )  header Map("TOKEN" -> scientist_token) asString
    }



  }

  def getSPL : String
  def getExperimentNameTest : String


  @After def killProcess(){
   if (startHive){
    centralServerProcess.destroy()
   }

   jnotfail("stop experiment "+getExperimentNameTest){
      Http post("http://localhost:18000/experiment-manager/update/stop") param Map(
        "name" -> getExperimentNameTest
      )  header Map("TOKEN" -> scientist_token) asString
    }

 }


}