package fr.inria.apisense.spi

import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context
import javax.ws.rs.FormParam
import fr.inria.bsense.node.service.ExperimentModuleService
import javax.ws.rs.Path
import javax.ws.rs.POST

trait SpacialIndex extends ExperimentModuleService {

  @POST
  @Path("/add/index")
  def createSpacialIndex(
      @Context sc : SecurityContext,
      @FormParam("name") name : String,
      @FormParam("filter") filter : String) : String
   
  @POST
  @Path("/add/collection")
  def insertCollection( 
      @Context sc : SecurityContext,
      @FormParam("name") spacialIndexName : String,
      @FormParam("collection") collectionName : String ) : String
 
  @POST
  @Path("/add/point")
  def insertPoint(
      @Context sc : SecurityContext,
      @FormParam("name") spacialIndexName : String,
      @FormParam("point") point : String
  ) : String
      
  @POST
  @Path("/delete/index")
  def dropSpacialIndex(@FormParam("name") name : String) : String
  
}