package fr.inria.apisense.spi

import fr.inria.bsense.node.cbse.ExperimentModule
import frascala.sca.Java
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.service.IDBQueryService
import frascala.frascati.SCA
import fr.inria.bsense.db.api.cbse.DatabaseComposite
import fr.inria.apisense.spi.basex.QuadTreeBaseXImpl
import fr.inria.bsense.node.collects.CollectorService
import frascala.sca.Bean


abstract class SpacialIndexComponent extends ExperimentModule("fr.inria.bsense.spacialIndex"){

  val srvSPI = service("srv-spi") exposes Java[SpacialIndex]
  deployRest(srvSPI,"spi", Array())
  
  // Define database dependency
  val refQuery = 
	  reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService] as SCA(DatabaseComposite.COMPOSITE_NAME + "/" + DatabaseComponent.SERVICE_QUERY)

  val refUpdateQuery =  
	  reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService] as SCA(DatabaseComposite.COMPOSITE_NAME + "/" + DatabaseComponent.SERVICE_QUERY_UPDATE)
  
  var refCollector = reference("ref-collector") exposes Java[CollectorService] autowired
  
  override def getModuleURI: String = "/spi"
  
}


class QuadTreeBaseXSpacialIndex extends SpacialIndexComponent{
  
  // Define component implementation
  this.uses(Bean[QuadTreeBaseXImpl])
  
  override def getModuleName: String = "QuadTree Spacial Index Implementation "
}