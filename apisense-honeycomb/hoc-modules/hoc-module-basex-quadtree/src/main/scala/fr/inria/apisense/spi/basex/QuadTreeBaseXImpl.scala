package fr.inria.apisense.spi.basex

import fr.inria.apisense.spi.SpacialIndex
import javax.ws.rs.core.SecurityContext
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.query.QueryService
import fr.inria.bsense.node.collects.CollectorService
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.node.FunctionalityDatabase

class QuadTreeBaseXImpl extends SpacialIndex with FunctionalityDatabase {

  //@Reference
  //var processService : QueryService = null
  
  
  @Reference(name = "ref-collector")
  var collectorService : CollectorService = null
  
  
  /*
   * 
   * Create a new spacial index
   * 
   * JSON Filter proprety
   * 
   * header : Array of header attribute to insert
   * data   : Array of data attribute to insert
   * maxlat   : Maximum latitude 
   * maxlon   : Maximum longitude 
   * minlat   : Minimum Latitude
   * minlon   : Minimum Longitude
   * latName  : Latitude property name
   * lonName  : Longitude property name
   * childs   : determine maximum number of child
   */
  def createSpacialIndex( sc : SecurityContext, name : String, filter : String) : String = {
    
    var jsonFilter : Node = null
    
    try{
      // try to parse filters as JSON object
      // and check if json filter contains header
      // and data property
      
      jsonFilter = NodeParser.parJSON(filter)
      
      if (jsonFilter \\ "header"    isNull)  throw new Exception("Cannot find header property")
      if (jsonFilter \\ "data"      isNull)    throw new Exception("Cannot find data property")
      if (jsonFilter \\ "maxLat"    isNull)  throw new Exception("Cannot find maxlat property")
      if (jsonFilter \\ "maxLon"    isNull)  throw new Exception("Cannot find maxlon property")
      if (jsonFilter \\ "minLat"    isNull)  throw new Exception("Cannot find minlat property")
      if (jsonFilter \\ "minLon"    isNull)  throw new Exception("Cannot find minlon property")
      if (jsonFilter \\ "latName"   isNull) throw new Exception("Cannot find latName property")
      if (jsonFilter \\ "lonName"   isNull) throw new Exception("Cannot find lonName property")
      if (jsonFilter \\ "maxChilds" isNull) throw new Exception("Cannot find maxChilds property")
      if (jsonFilter \\ "maxDepth" isNull) throw new Exception("Cannot find maxDepth property")
      
      spiFolder(name).updateFile("spi-conf", filter)
      
      exec{
      
       
        updateService.addDocument(session, experimentName, "spacialIndex", """ { "name" : "%s" } """.format(name))
        
        val buffer = new StringBuilder
        buffer append """ {  "node" : { """
        buffer append """ "name"   : "%s",  """.format(name)
        buffer append """ "depth"      : 1 ,  """
        buffer append """ "maxDepth"   : %s ,  """.format(jsonFilter \\ "maxDepth" text)
        buffer append """ "maxLat" : "%s",  """.format(jsonFilter \\ "maxLat" text)
        buffer append """ "maxLon" : "%s",  """.format(jsonFilter \\ "maxLon" text)
        buffer append """ "minLon" : "%s",  """.format(jsonFilter \\ "minLon" text)
        buffer append """ "minLat" : "%s",  """.format(jsonFilter \\ "minLat" text)
        buffer append """ "maxChilds" : "%s",  """.format(jsonFilter \\ "maxChilds" text)
        buffer append """ "points" : {},  """
        buffer append " } }"
        
        updateService.create(session, experimentName+"-"+name)
        updateService.addDocument(session, experimentName+"-"+name, "index", buffer.toString)
      }
      
    }
    catch{
      case t : Throwable => 
         t.printStackTrace()
         return Message.exception("Filter error",t.getMessage())
    }
    
    ""
  }
   
  def insertCollection( sc : SecurityContext, name : String, collectionName : String ) : String = ""
 
  
  def insertPoint(sc : SecurityContext, spacialIndexName : String,point : String) : String = {
    
    ""
  }
  
  def dropSpacialIndex(name : String) : String = {
    
    exec{
      
    	updateService.drop(session, experimentName+"-"+name)
    }
    
    
    
  }
    
    
  private def openSpiFolder = NodeFileConfiguration("experiment/"+experimentName+"/spi") 
  private def spiFolder(mapName : String) = NodeFileConfiguration("experiment/"+experimentName+"/spi/"+mapName) 
  
}