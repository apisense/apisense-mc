package fr.inria.apisense.spi.test

import fr.bsense.module.test.ModuleTCase
import org.junit.Test
import fr.inria.apisense.client.APISENSEClient

class QuadTreeTest extends ModuleTCase {

  override def startHive = false;
  override def getExperimentNameTest = "SPI1"
  override def getSPL = """ { 
     "Experiment" : 1,
     "Experiment Website":1,
     "Module Manager" : 1,
     "CKUID" : 1,
     "QuadTree" : 1, 
  }"""
  
    
  @Test def test(){
    
    val honeyService = APISENSEClient.honey
    honeyService.host(ModuleTCase.HOST)
    honeyService.connect(scientist_token)
    
    println(scientist_token)
    
    val spatial = honeyService.experiment("SPI1").spi
    
    canfail("drop spatial database"){
      spatial.dropIndex("MyQuad")
    }
    
    notfail("create spatial index"){
      spatial.createIndex("MyQuad","""{
            "header" : [] ,
    		"data"   : [] ,
            "maxDepth" : 10,
            "lonName" :"lon",
            "latName" : "lat" ,
            "maxLat" : 59.245 ,
    		"maxLon" : 2.98,
    		"minLat" : 48.578,
    		"minLon" : 1.629 ,
    		"maxChilds" : 3
       }""")
    }
    
  }
}