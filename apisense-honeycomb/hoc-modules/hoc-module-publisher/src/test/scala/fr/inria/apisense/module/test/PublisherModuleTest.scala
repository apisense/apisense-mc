package fr.inria.apisense.module.test

import org.junit.Test
import fr.bsense.module.test.ModuleTCase
import fr.inria.apisense.client.APISENSEClient
import fr.inria.bsense.common.utils.Http
import net.liftweb.json.JsonAST.JString




class PublisherModuleTest extends ModuleTCase {


  override def startHive = true;
  override def getExperimentNameTest = "TestModulePublisherExperiment"
  override def getSPL = """ {
     "Experiment" : 1,
     "Configuration" : 1,
     "Experiment Website":1,
     "Module Manager" : 1,
     "Android" : 1,
     "Script editor" : 1
  }"""

  @Test def test(){

	val honey = APISENSEClient.honey
	honey.host(HOST)
	honey.connect(scientist_token)

	notfail("add script file "){ honey.experiment(getExperimentNameTest).publisher.updateScript("main.js", """ log.toast("test") """); }

	notfail("update experiment configuration"){ honey.experiment(getExperimentNameTest).publisher.updateConfiguration("1.0",true, "javascript",HOST+"/"+getExperimentNameTest,"main.js") }

	notfail("publish experiment"){ honey.experiment(getExperimentNameTest).publisher.publish }

	Thread.sleep(1000);

	1 to 5 foreach{i => addUser(i)}

	notfail("update experiment configuration"){ honey.experiment(getExperimentNameTest).publisher.updateConfiguration("2.0",true, "javascript",HOST+"/"+getExperimentNameTest,"main.js") }
	notfail("publish experiment"){

	  honey.experiment(getExperimentNameTest).publisher.publish

	}

	6 to 10 foreach{i => addUser(i)}

	//notfail("update experiment configuration"){ honey.experiment(getExperimentNameTest).publisher.updateConfiguration("3.0",true, "javascript",HOST+"/"+getExperimentNameTest,"main.js") }
	//notfail("publish experiment"){ honey.experiment(getExperimentNameTest).publisher.publish }

	//10 to 20 foreach{i => addUser(i)}
  }


  private def addUser(index : Integer){

   val h =  APISENSEClient.hive
   h.host(ModuleTCase.CENTRAL_HOST)


   // create bee user in central node
   // can fail if bee account is already created
   jcanfail("create bee user"){
     Http.post(ModuleTCase.CENTRAL_SERVICE_CREATE_BEE_USER) param Map(
       "fullname"     -> "Bee",
	   "username"     -> ("beeuser-"+index),
	   "password"     -> "beeuser",
	   "email"        -> "bee@bsense.com"
      ) asString
    }

    h.connect(("beeuser-"+index), "beeuser")

    canfail("subscribe user to an experiment"){  h.store.subscribe(getExperimentNameTest, """{}""") }
    notfail("Download experiment"){

	  val node = h.search.searchExperiment(Array(getExperimentNameTest))
	  val JString(experimentId) = node \\ "id"

	  h.store.downloadExperiment(experimentId)
	}


  }


}