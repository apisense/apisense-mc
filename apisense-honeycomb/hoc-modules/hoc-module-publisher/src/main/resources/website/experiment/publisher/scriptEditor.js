
var host = undefined;

$(function(){

    //host = window.location.toString()
	//host = host.substring(0,host.indexOf("website"))

	//$("#pb_save").click(function(){ saveDialogConfiguration() })


	buildEditor();


})

//--------------------------------------------------- Global variable

var editor = undefined



//---------------------------------------------------- Configuration dialog

var publishDialog = undefined

var publish = function(){

	post("../../../"+xpName+"/publisher/update/publish",{},function(){

		editor.addConsoleValue("new experiment published at "+new Date())

	},function(err){
		editor.setConsoleValue("Error : "+err)
	})

}

var configuration = function(){

	updateDialogConfiguration()
	$('#modal-configuration').modal({})
}

var updateDialogConfiguration = function(){

	$("#table_scripts").find("label").remove()

	var request = new APIS.POST("../../../"+xpName+"/publisher/get/scripts")
	request.onSuccess = function(mess){

		var scripts = mess[0].children
   		for (i in scripts){

   			var scriptName = scripts[i].label
   			scriptName = scriptName.substring(0,scriptName.lastIndexOf("."))

   			var str = "<label class='radio'>"
   			str += scripts[i].label
   			str += "<input id=radio_"+scriptName+" type='radio' name='script' value='"+scripts[i].label+"'></input>"
   			str += "</label>"

   			$("#table_scripts").append(str)
   		}

   		fillDialogConfiguration()
	}
}

var fillDialogConfiguration = function(){

	var request = new APIS.POST("../../../experiment-manager/get/xp",{name:xpName})
	request.onSuccess = function(xps){

		var xp = xps.experiment
    	xp.baseUrl ? $("#pb_baseUrl").val(xp.baseUrl) : $("#pb_baseUrl").val(host+xpName)
		xp.version ? $("#pb_version").val(xp.version) : "1.0.0"
		xp.visible == "true" ? $("#pb_visible_true").attr('checked', true) : $("#pb_visible_false").attr('checked', true)
		xp.language ? $("#radio_"+xp.language).attr('checked', true) : undefined
		if (xp.mainScript){
		  var scriptName = xp.mainScript
		  scriptName = scriptName.substring(0,scriptName.lastIndexOf("."))
		  $("#radio_"+scriptName).attr('checked', true)
		}
	}

}

var saveDialogConfiguration = function(){

	post("../../../"+xpName+"/publisher/update/configuration",{
		visible   : $("input[name='visible']:checked").val() == "true" ? true : false,
		language  : $("input[name='language']:checked").val(),
		version   : $("#pb_version").val(),
		baseUrl   : $("#pb_baseUrl").val(),
		mainScript: $("input[name='script']:checked").val()
	  },
	  function(){},
	  function(err){ alert(err) }
	)
}


var statistic = function(){

	post("../../../experiment-manager/get/regsitered",{name : xpName},function(data){

		var datasets = {}

		if (!data.registration.user){

			new Modal({
			 title : "Experiment deployment",
			  message : "None"
		    })
			return;
		}

		if (data.registration.user.length){

		  for (var i in data.registration.user){

			var user = data.registration.user[i]
			if (!datasets[user.version]){
				datasets[user.version] = 0
			}
			datasets[user.version] = datasets[user.version] + 1
		  }

		}else{

			datasets[data.registration.user.version] = 1;
		}

		var chart = {
	      chart   : {renderTo: "chart-deployed-experiment",},
	      title   : { text: 'Experiment deployment' },
	      subtitle: { text: 'Experiment : '+xpName },
	      plotOptions: { pie: { allowPointSelect: true,showInLegend: true}},
	      series   : [{
	    	  type: 'pie',
	    	  name: 'User number',
	          data: []
	      }]
		}

		try{
		for (var j in datasets){
			chart.series[0].data.push(["version "+j,datasets[j]])
		}

		new Modal({
			title : "Experiment deployment",
			message : "<div id='chart-deployed-experiment'style='min-width: 450px; height: 350px; margin: 0 auto'></div>"
		})

		new Highcharts.Chart(chart)

		}catch(e){alert(e)}

	})






}

//----------------------------------------------------

var buildEditor = function(){

	var height = $(window).height() - 50;

	var updateTree = function(){

		post("../../../"+xpName+"/publisher/get/scripts",{},
			function(tree){

				editor.updateTreeModel(tree)
			},
			function(err) { alert(err) })
	}


	var editor = new APISEditor({

		getContentFile : function(filename){ return "conent of "+filename; },

		style : "background:#F5F5F5;width:100%;height:"+height+"px;position:relative"
	});

	editor.renderInto("editor");

	updateTree();

}

var build = function(){

		var editor = new APISEditor({

				getContentFile : function(filename){

					return "conent of "+filename;
				},

				style : "width:90%;margin:50px;background:#DDD"
			});

			var tree1 = {
				nodeName : "RootFolder",
				childs : [
					{nodeName : "file1.js" },
					{nodeName : "Html", childs : [
						{nodeName : "file1.css" },
					    {nodeName : "file1.html" },
					    {nodeName : "file1.py" },
					    {nodeName : "file1.xq" }
					]}
				]
			};

			editor.updateTreeModel(tree1)

			editor.renderInto(document.getElementById("editor"));

			var tool = editor.toolbar()

			tool.newfolder(function(filename){ alert("create folder "+filename)});
			tool.newfile(function(filename){ alert("create file "+filename) });
			tool.deletefile(function(filename){ alert("delete file "+filename)})

			alert("ok")

	/*var onItemSelected = function(item){
		post("../../../"+xpName+"/publisher/get/script",{filename: item.label.toString() },
			function(mess){

				editor.addTab(item.label,{content:decode(mess)})
			},
			function(err){alert(err)}
		);
	}

	// update code tree view function
	var updateTree = function(){

		post("../../../"+xpName+"/publisher/get/scripts",{},
			function(mess){ editor.updateTreeModel(mess); },
			function(err){new Dialog("Error",err,function(){})})
	}

	// code editor view configuration
	//
	editor = new CodeEditor("editor",{},onItemSelected)
	editor.ready(function(){

		// add menu button
		// menu : add new file
		editor.addMenuNewFile(function(file){
			post("../../../"+xpName+"/publisher/update/script",{filename:file.toString(),content:""},
					function(mess){ updateTree(); },
					function(err){new Dialog("Error",err,function(){})})
		},[".js",".py",".css",".html"]);

		// add menu button
		// menu : remove current file
		editor.addMenuDeleteFile(function(file){
			post("../../../"+xpName+"/publisher/delete/script",{filename:file.toString()},
				 function(s){updateTree()},
				 function(e){new Dialog("Error",err,function(){})}
			);
		})

		// add menu button
		// menu: remove current file
		editor.addMenuSaveFile(function(file,content){
				post("../../../"+xpName+"/publisher/update/script",{filename:file.toString(),content:encode(content.toString())},
					 function(s){},
					 function(e){new Dialog("Error",err,function(){})}
				);
		});


		// add menu button
		// publish experiment
		editor.addMenu("configuration", function(){
			configuration();
		}, "icon-wrench")

		editor.addMenu("clear", function(){
			editor.setConsoleValue("")
		},"icon-refresh")

		// add menu button
		// publish experiment
		editor.addMenu("publish", function(){
			publish();
		},"icon-globe")

		editor.addMenu("statistic", function(){
			try{ statistic() }catch(e){alert(e)}
		},"icon-signal")



		editor.addMenu("Mobile push", function(){

			var _username = $("#devUsername").val()
			var _password = $("#devPassword").val()

			var request = new APIS.POST("../../../"+xpName+"/publisher/deploy/dev",
				{username : _username,password : _password},{parse : false})

			request.onSuccess = function(mess){
				editor.addConsoleValue(mess)
			}
			request.onError = function(mess){
				editor.addConsoleValue(mess)
			}

		},"icon-user","publish_to_user")

		editor.addMenuInput("username","devUsername","70px")
		editor.addSeparator()
		editor.addMenuInputPassword("password","devPassword","70px")

		updateTree()
	})*/


}







