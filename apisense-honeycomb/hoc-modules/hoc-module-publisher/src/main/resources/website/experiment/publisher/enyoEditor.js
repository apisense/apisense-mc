enyo.kind({

	name: "apisense.web.Editor",

	constructor : function(object){

        this.inherited(arguments);

        this.service = object.service || "./";
        this.divId   = object.id || "editor";

        var self = this;

        this.editor = new APISEditor({
        	onFileSelected : function(editor,filename){

        		filename = filename.substring(filename.indexOf('/')+1);

        		var request = new APIS.POST(self.service+"/publisher/get/script",{filename: filename});
        		request.onSuccess = function(content){
        			editor.open(filename,decode(content));
        		}

        	}
        });

        this.editor.renderInto(this.divId);


        this.updateTree();

        var tool = this.editor.toolbar();
        tool.newfolder(function(f){self.addfolder(f)});
		tool.newfile(function(f){self.addfile(f)});
		tool.deletefile(function(f){ self.deletefile(f)});
		tool.separator();
		tool.savefile(function(f,c){ self.savefile(f,c) })

		tool.menu("configuration","icon-wrench",function(){self.configuration();})
		tool.menu("statistic","icon-signal",function(){self.statistic();})
		tool.menu("publish","icon-globe",function(){self.publishScript();})


		tool.separator();
		tool.menu("Push To User","icon-user",function(){self.publishDev();})
		this.username = tool.input("username");
		this.password = tool.inputpwd("password");


        this.editor.initConsole();
	},

	publishScript : function(){
		var self = this;

		var request = new APIS.POST(this.service+"/publisher/update/publish");
		request.onSuccess = function(){
			self.editor.addConsoleValue("new experiment published at "+new Date()+"\n")
		}
		request.onError = function(err){
			self.editor.addConsoleValue("Error : "+err+" at "+new Date()+"\n")
		}
	},

	publishDev : function(){

		var self = this;

		var _username = this.username.getValue();
		var _password = this.password.getValue();

		var request = new APIS.POST(this.service+"/publisher/deploy/dev",{
			username : _username,
			password : _password},
			{parse : false});

		request.onSuccess = function(mess){ self.editor.addConsoleValue(mess+"\n") }
		request.onError = function(mess){ self.editor.addConsoleValue(mess+"\n") }
	},

	create : function(object){

		this.inherited(arguments);
	},

	updateTree : function(){
		var self = this;

		var request = new APIS.POST(this.service+"/publisher/get/scripts");
		request.onSuccess = function(tree){

			self.editor.updateTreeModel(tree)
		}
	},

	addfolder : function(folder){
		var self = this;

		folder = folder.substring(folder.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/publisher/update/folder", {folder : folder });
		request.onSuccess = function(){

			self.updateTree();
		}
	},

	addfile : function(file){

		var self = this;

		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/publisher/update/script", {filename : file, content : "" });
		request.onSuccess = function(mess){

			self.updateTree();
		}
	},

	deletefile : function(file){

		var self = this;
		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/publisher/delete/script", {filename : file});
		request.onSuccess = function(mess){

			self.updateTree();
		}
		request.onError = function(err){ alert(err) }
	},


	savefile : function(file,content){

		new APIS.POST(this.service+"/publisher/update/script", {filename : file,content : encode(content)});
	},

	statistic : function(){

		var self = this;

		var request = new APIS.POST("../../../experiment-manager/get/regsitered", {name : xpName});
		request.onSuccess = function(data){

			var modal = new apisense.BaseModal({ title : "Experiment deployment" });
			modal.renderInto(self.editor);

			var datasets = {}
			if (!data.registration.user){

				modal.setMessage("No Deployed");
				modal.show();
				return;
			}

			if (data.registration.user.length){

				for (var i in data.registration.user){

					var user = data.registration.user[i]
					if (!datasets[user.version]){
						datasets[user.version] = 0
					}
					datasets[user.version] = datasets[user.version] + 1
				}
			}
			else{

				datasets[data.registration.user.version] = 1;
			}

		try{

			var chart = {
					chart   : {renderTo: "chart-deployed-experiment",},
					title   : { text: 'Experiment deployment' },
					subtitle: { text: 'Experiment : '+xpName },
					plotOptions: { pie: { allowPointSelect: true,showInLegend: true}},
					series   : [{
						type: 'pie',
						name: 'User number',
						data: []
					}]
			}

			for (var j in datasets){
				chart.series[0].data.push(["version "+j,datasets[j]])
			}

			modal.setHtml({ content : "<div id='chart-deployed-experiment'style='min-width: 450px; height: 350px; margin: 0 auto'></div>" })
			modal.show();
			new Highcharts.Chart(chart)

		}catch(e){alert(e)}
	   }
	},

	configuration : function(){


		var self = this;

		var _files = this.editor.tree().files;
		var files = []
		for (var i in _files){

			var file = _files[i];
			file.substring(file.indexOf('/')+1);
			files.push(file)
		}

		var request = new APIS.POST("../../../experiment-manager/get/xp",{name:xpName})
		request.onSuccess = function(xp){

			 var host = window.location.toString()
			 host = host.substring(0,host.indexOf("website"))

			 var modal = new apisense.ExperimentConfiguration({

				 title : "Experiment Manifest",
				 onValid : function(data){


				   	data.main = data.main.substring(data.main.indexOf('/')+1);

				 	// modal window is validate
				    // push new configuration to the node server
				 	var r = new APIS.POST(self.service+"/publisher/update/configuration",{
				 		visible    : (data.visible == "True") ? true : false,
				 		language   : "Javascript",
				 		version    : data.version,
				 		baseUrl    : data.url,
				 		mainScript : data.main
				 	});

				 	r.onError = function(err){
				 		alert(err);
				 	}

			 	}
			 })

			 xp = xp.experiment;

			 modal.renderInto(self.editor);

			 var baseUrl    = xp.baseUrl || host+xpName;
			 var version    = xp.version || "1.0";
			 var visible    = xp.visible || "True";
			 visible == "true" ? visible = "True" : visible = "False";
			 var mainScript = xp.mainScript || files[0];

			 modal.input("version","Experiment Version",version);
			 modal.input("url","Experiment Services ",baseUrl);
			 modal.list("main","Main File",files,mainScript);
			 modal.list("visible","Visible",["True","False"],visible);

			 modal.show();
		}
	}
})



