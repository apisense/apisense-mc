
enyo.kind({
	name : "apisense.ExperimentConfiguration",
	kind : "apisense.BaseModal",

	create : function(object){ this.inherited(arguments);

		this.items = [];
		this.onValid  = object.onValid  || function(){};
		this.onCancel = object.onCancel || function(){};

		this.$.modalContent.itemSelected = this.itemSelected;
	},

	valid : function(){

		var results = {};
		for (var i in this.items){

			var r = this.items[i]();
			results[r.name] = r.value;
		}

		this.onValid(results);
		this.hide();this.destroy();
	},

	input : function(name,label,defaultValue){

		defaultValue = defaultValue || ""

		this.$.modalContent.createComponent({
			 kind : "FittableColumns", style:"padding-bottom:30px;",components : [
			     {content : label, style : "font-weight:bold;padding-top:8px;width : 200px;"},
			     {kind: "onyx.InputDecorator", components: [ {kind: "onyx.Input", style:"width:400px;", name : name, value : defaultValue} ]}
			]}
		)

		var self = this;
		this.items.push(function(){

			return {
				name  : name,
				value : self.$.modalContent.$[name].getValue()
			}
		})
	},

	label : function(name){ this.$.modalContent.createComponent({classes: "onyx-sample-divider", content: name});},

	list : function(name,label,_items, defaultValue){

		defaultValue = defaultValue || ""

		var items = [];
		for (var i in _items){
			items.push({content: _items[i], index : name, style:"min-width:100px;"});
		}

		var p = this.$.modalContent.createComponents([
			{content : label, style : "font-weight:bold;padding-top:8px;padding-bottom:10px;width:200px;"},
			{kind: "onyx.MenuDecorator", onSelect: "itemSelected",
				components: [
					{name : name, content: defaultValue },
					{kind: "onyx.Menu", floating: true, vertical: "auto", components: items}
			]}
		])

		var self = this;
		this.items.push(function(){

			return {
				name  : name,
				value : self.$.modalContent.$[name].getContent()
			}
		})
	},

	itemSelected: function(inSender, inEvent) {
		var el = inEvent.originator;
		this.$[el.index].setContent(el.content)
	},
})
