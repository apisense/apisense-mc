/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.publisher

import java.io.ByteArrayOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import org.apache.commons.codec.binary.Base64
import org.osoa.sca.annotations.Init
import fr.inria.bsense.common.utils.IoUtils
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.db.api.ValuePair
import fr.inria.bsense.db.api.path
import fr.inria.bsense.node.BService
import fr.inria.bsense.node.FunctionalityDatabase
import fr.inria.bsense.node.cbse.Experiment
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.apisense.client.HiveClient
import fr.inria.apisense.client.HiveClient
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import org.osoa.sca.annotations.Scope
import fr.inria.apisense.client.MD5
import fr.inria.bsense.common.utils.Log
import java.io.File
import fr.inria.bsense.common.utils.IOUtils

@Scope("COMPOSITE")
class PublisherModuleImpl extends PublisherService with FunctionalityDatabase with LifeCycleService {

  var _hive : HiveClient = null

  override  def onStart( composite : BSenseComposite){}
  override def onStop(){
    if (_hive != null){

      _hive.disconnect
    }
  }

  final val EXPERIMENT_FOLDER = "experiment"


  def getHiveClient = {

    if (_hive == null){
      _hive = new HiveClient()
      _hive.host(ServerNodeConfiguration.getUrl)
      _hive.connect(ServerNodeConfiguration.username, ServerNodeConfiguration.password)
    }

    _hive
  }

  /*
   * Return in json format all sensing task scripts
   * stored in experiment repository
   * JSON format returned is already under form
   * of dijit.Tree of dojo Toolkit
   *
   * @see http://dojotoolkit.org/reference-guide/1.8/dijit/Tree.html
   */
  def getScripts(sc : SecurityContext) : String = {

    val rootFolder = NodeFileConfiguration(EXPERIMENT_FOLDER+"/"+experimentName+"/scripts").getFile

    Message.success(new JSONLiftNodeParser(IOUtils.hierarchy(rootFolder, experimentName)))
  }


  def deploy(sc : SecurityContext,username : String,password : String) : String = {

     val mess = "{\"$forceUpdate\" : \""+this.experimentName+"\"}"

     val conf = " { \"timeToLive\" : \"20\", \"collapseKey\" : \"DevUpdate\" } ";

     getHiveClient.crowd.pushMessageToDevUser(username, MD5.hash(password),new String(Base64.encodeBase64(mess.getBytes(),true)),conf)
  }

  /*
   * Return script content of filename
   * Returned content is Base64 encoded
   */
  def getScript( sc : SecurityContext,  filename : String) : String = {

    // get file content
    val file = NodeFileConfiguration(EXPERIMENT_FOLDER+"/"+experimentName+"/scripts").getFile(filename)

    //return file coded in base64
    Message.success(new String(Base64.encodeBase64(file.getBytes)));
  }

  def createFolder(sc : SecurityContext, name : String) : String = {

    NodeFileConfiguration(EXPERIMENT_FOLDER+"/"+experimentName+"/scripts").folder(name)

    Message.success();
  }

  /*
   * Update sensing task content. If file
   * doesn't exist, a new file is created.
   * File content must be base64 encoded
   */
  def updateScript( sc : SecurityContext,  filename : String, contentBase64 : String) : String = {

    // Decode base64 content
    val content = new String(Base64.decodeBase64(contentBase64.getBytes));

    // Store file
    NodeFileConfiguration(EXPERIMENT_FOLDER+"/"+experimentName+"/scripts").updateFile(filename, content);

    Message.success()
  }

  /*
   * Delete file filename
   */
  def deleteScript( sc : SecurityContext,  filename : String) : String = {

    NodeFileConfiguration(EXPERIMENT_FOLDER+"/"+experimentName+"/scripts").deleteFile(filename);
    Message.success();
  }

  /*
   * Update sensing experiment configuration
   * in local database
   */
  def updateConfiguration(sc : SecurityContext,version : String, visible : Boolean, language : String, baseUrl:String,mainScript : String) : String = exec{

    updateService.set(session, Experiment.DATABASE_EXPERIMENTS, this.experimentName,
        path("experiment"),Array(
            new ValuePair("version",version),
            new ValuePair("type","android"),
            new ValuePair("language",language),
            new ValuePair("mainScript",mainScript),
            new ValuePair("visible",visible),
            new ValuePair("baseUrl",baseUrl),
            new ValuePair("collector","/upload")
        ))
  }

  /*
   * Update one property configuration
   * of a sensing experiment in local database
   */
  def updateConfiguration(propertyName : String, propertyValue : String) = exec {

    updateService.set(session, Experiment.DATABASE_EXPERIMENTS, this.experimentName,
        path("experiment"),Array(new ValuePair(propertyName,propertyValue))
    )
  }

  private def compressScript() : (String,Array[Byte]) = {

    Log.d("compress experiment "+this+" hasCode "+this.hashCode())

    val folder = NodeFileConfiguration(EXPERIMENT_FOLDER+"/"+experimentName+"/scripts")
    val data = new ByteArrayOutputStream()
    val zip = new ZipOutputStream(data)

    var experimentSignature  =  IOUtils.zip(folder.getFile, zip)

    exec{
       val node = queryService.getDocument(Experiment.DATABASE_EXPERIMENTS, this.experimentName, path("experiment"))
       zip.putNextEntry(new ZipEntry("manifest"));
       zip.write(node.toJSONString.getBytes())
    }

    zip.close
	zip.flush

	(experimentSignature.toString,data.toByteArray())
  }




  def getZippedApplication(sc : SecurityContext) = compressScript._2

  /*
   * Publish a sensing experiment in central server
   *
   * publish a sensing experiment is done in 3 steps
   *   1- compress all sensing task
   *   2- save signature of sensing in local database
   *   3- send compressed file and sensing configuration in central server
   */
  def publish(sc : SecurityContext) : String = {

    val compressedScripts = compressScript

    val bservice = BService()
    bservice.connect

    // update experiment script to the central server
    bservice.service(BService.UPDATE_EXPERIMENT_SCRIPT_SERVICE+"/"+this.experimentName, compressedScripts._2)

    var result : String = null;

    // update experiment description to the central server
    val error = exec{

       // create new version of experiment
       // by default, a version correspond
       // to the current time
       // val version = new Date().getTime()
       //updateService.set(session, Experiment.DATABASE_EXPERIMENTS, this.experimentName,
       //path("experiment"),Array(
       //   new ValuePair("version",version)
       //))

       // get current experiment description
       val node = queryService.getDocument(Experiment.DATABASE_EXPERIMENTS, this.experimentName, path("experiment"))

       // send description to central server

       result = bservice.service(BService.UPDATE_EXPERIMENT_CONFIGURATION_SERVICE,
           Map("description"->node.toJSONString))


       //val signatureNode = new XMLNode(
       //    <signature>
       //	    <version>{version}</version>
       //	    <value>{experimentSignature.toString}</value>
       //	   </signature>
       //)

       // add experiment signature in signature collection
       // of experiment database
       //updateService.addDocument(session, experimentName, "signature", signatureNode.toJSONString)
    }

    bservice.disconnect

    if (result == null) error
    else result
  }




}