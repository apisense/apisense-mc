/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.publisher

import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.node.cbse.ExperimentModule
import fr.inria.bsense.node.cbse.ServerNode
import fr.inria.bsense.node.web.cbse.ExperimentWebModuleComponent
import frascala.sca.Bean
import frascala.sca.Java

//----------------------------------------------------------------  Static Variable definition

object PublisherModule {

  final val COMPONENT_NAME = "rest-module-publisher"

  final val SERVICE_PUBLISHER = "service-publisher"

}

//----------------------------------------------------------------- Component Definition

class PublisherModuleComponent extends ExperimentModule(PublisherModule.COMPONENT_NAME) {
	import PublisherModule._

	this.uses(Bean[PublisherModuleImpl])

	val srvPublisher =  service(SERVICE_PUBLISHER) exposes Java[PublisherService]

    deployRest(srvPublisher, "publisher",Array())

	val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
	refQuery as ServerNode.scaUrlDBQuery


	val refUpdateQuery = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
    refUpdateQuery as ServerNode.scaUrlDBUpdate

	def getModuleName: String = "Android publisher"
    def getModuleURI: String  = "/publisher"

}

class ScriptEditorModuleWeb extends ExperimentWebModuleComponent("web-module-scriptEditor"){
  def getModuleName = "Mobile Task"
  def getModuleURL  = "publisher/moduleEditor.html"
}