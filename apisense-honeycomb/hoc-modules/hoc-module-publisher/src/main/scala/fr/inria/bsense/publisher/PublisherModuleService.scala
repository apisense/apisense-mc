/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.publisher


import fr.inria.bsense.node.service.ExperimentModuleService
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context
import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.Consumes


trait PublisherService extends ExperimentModuleService {

  @POST
  @Path("/get/scripts")
  def getScripts(@Context sc : SecurityContext) : String

  @POST
  @Path("/get/script")
  def getScript(@Context sc : SecurityContext, @FormParam("filename") filename : String) : String

  @POST
  @Path("/get/application")
  def getZippedApplication(@Context sc : SecurityContext) : Array[Byte]

  @POST
  @Path("/update/script")
  def updateScript(@Context sc : SecurityContext, @FormParam("filename") filename : String, @FormParam("content") content : String) : String

  @POST
  @Path("/update/folder")
  def createFolder(@Context sc : SecurityContext, @FormParam("folder") folder : String) : String

  @POST
  @Path("/delete/script")
  def deleteScript(@Context sc : SecurityContext, @FormParam("filename") filename : String) : String

  @POST
  @Path("/update/configuration")
  def updateConfiguration(
      @Context sc : SecurityContext,
      @FormParam("version") version : String,
      @FormParam("visible") visible : Boolean,
      @FormParam("language") language : String,
      @FormParam("baseUrl") baseUrl : String,
      @FormParam("mainScript") mainScript : String) : String

  def updateConfiguration(propertyName : String, propertyValue : String) : String

  @POST
  @Path("/update/publish")
  def publish(@Context sc : SecurityContext) : String


  @POST
  @Path("/deploy/dev")
  def deploy(@Context sc : SecurityContext,
      @FormParam("username") username : String,
      @FormParam("password") password : String) : String
}




