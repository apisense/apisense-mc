/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.modules.start

import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.common.cbse.BSRuntime
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.cbse.UserManagerComponent
import fr.inria.bsense.node.cbse.ExperimentManagerComponent
import net.liftweb.json.JsonAST.JString
import fr.inria.bsense.common.utils.MD5
import fr.inria.bsense.common.utils.Log
import java.io.File
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.Scanner
import fr.inria.bsense.common.utils.Http
import java.net.NetworkInterface

object ModuleStarter {

  // Declare central server services url
  var HIVE_HOST = "http://localhost:18001";
  final def CENTRAL_SERVICE_CREATE_BEE_USER 		= HIVE_HOST+"/user/add/user/bee"
  final def CENTRAL_SERVICE_CONNECT           		= HIVE_HOST+"/user/get/token";
  final def CENTRAL_SERVICE_SUBSCRIBE_EXPERIMENT    = HIVE_HOST+"/store/user/update/subscribe"
  final def CENTRAL_SERVICE_UNSUBSCRIBE_EXPERIMENT  = HIVE_HOST+"/store/user/update/unsubscribe"
  final def CENTRAL_SERVICE_GET_SUBSCRIBED_XP       = HIVE_HOST+"/store/user/get/subscribed"
  final def CENTRAL_SERVICE_DOWNLOAD_EXPERIMENT     = HIVE_HOST+"/store/user/get/experiment"
  final def CENTRAL_SERVICE_CREATE_SCIENTIST  = HIVE_HOST+"/user/add/user/organization";
  final def CENTRAL_SERVICE_DROP_Experiment  = HIVE_HOST+"/user/add/user/organization";
   
  final val EXPERIMENT = "Experiment"
  var SPL : String = null
    
  var userManager : UserManagerService = null;
  var experimentManager : ExperimentManagerService = null;
  
  val http = new HttpRequest();
  var scientist_token : String = null
  var bee_token : String = null
   
  def main(args: Array[String]) {
  
     Http post(CENTRAL_SERVICE_CREATE_SCIENTIST) param Map(
    	"organization" -> "Tester Organization",
		"description"  -> "Tester Organization",
		"username"     -> "inria-tester",
		"password"     -> "inria-tester",
		"email"        -> "scientist.bsense.com")
      
     val urlHive  = System.getProperty("fr.inria.apisense.hive.uri","localhost:18001")
     val urlHoney = System.getProperty("fr.inria.apisense.honey.uri","localhost:18000")
     
     HIVE_HOST = "http://"+urlHive
     
     System.setProperty("org.ow2.frascati.binding.uri.base","http://"+urlHoney)
      
     var splBuilder = new StringBuilder()
     splBuilder append "{"
     0 to (args.length -1)foreach{
       i => splBuilder append "\""+args(i)+"\":1,"
     }
     splBuilder.deleteCharAt(splBuilder.length-1)
     splBuilder append "}"
     SPL = splBuilder toString
     
     Log.i("generate architecture "+SPL)
       
	 ServerNodeConfiguration.setUrl(HIVE_HOST)
	 ServerNodeConfiguration.password("inria-tester")
	 ServerNodeConfiguration.username("inria-tester")
	  
	 fr.inria.bsense.node.start.HoneyCombStarter.start(Array("Beehive Website"))
	 
	 
	 val sc = new Scanner(System.in);
	 print("Please press enter to exit")
	 sc.next()
	
  }
  
  
  
}