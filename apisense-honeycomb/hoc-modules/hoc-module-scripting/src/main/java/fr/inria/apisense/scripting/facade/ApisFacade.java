/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import fr.inria.asl.facade.AbstractFacade;
import fr.inria.asl.facade.FacadeManager;

abstract public class ApisFacade extends AbstractFacade{

	final protected String experimentName;

	public ApisFacade(FacadeManager facadeManager, final String experimentName) {
		super(facadeManager);
		this.experimentName = experimentName;
	}
}
