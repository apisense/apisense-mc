/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting;

import fr.inria.apisense.scripting.facade.ApisFacade;
import fr.inria.asl.facade.FacadeManager;
import fr.inria.asl.facade.IFacade;
import fr.inria.asl.facade.IFacadeFactory;
import fr.inria.asl.utils.Log;

public class FacadeFactory implements IFacadeFactory {

	final public String experimentName;

	public FacadeFactory(final String experimentName){
		this.experimentName = experimentName;
	}

	public IFacade newInstance(FacadeManager facadeManager,
			Class<? extends IFacade> clazz) {

		try{

			if (ApisFacade.class.isAssignableFrom(clazz)){
				return
					clazz.getConstructor(FacadeManager.class,String.class)
					  .newInstance(facadeManager,experimentName);
			} else return
					clazz.getConstructor(FacadeManager.class)
						.newInstance(facadeManager);
		}
		catch(Exception e){
			Log.e(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

}
