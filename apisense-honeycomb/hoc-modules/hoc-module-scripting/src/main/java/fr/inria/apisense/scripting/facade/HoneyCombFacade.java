/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import fr.inria.apisense.client.APISENSEClient;
import fr.inria.apisense.client.HoneyCombClient;
import fr.inria.asl.facade.FacadeAnnotation.ScriptMethod;
import fr.inria.asl.facade.FacadeManager;
import fr.inria.asl.utils.Log;
import fr.inria.bsense.node.configuration.ServerNodeConfiguration;

public class HoneyCombFacade extends ApisFacade {

	private HoneyCombClient honey;

	public HoneyCombFacade(FacadeManager facadeManager, final String experimentName) {
		super(facadeManager, experimentName);
	}

	@ScriptMethod public String connect(final String username,String password){

		honey  = APISENSEClient.honey();
		honey.host(ServerNodeConfiguration.getHoneyBaseUrl());
		return honey.connect(username, password);
	}

	@ScriptMethod public String mprocess(final String file,final String params){

		if (!params.replaceAll(" ","").equals("{}")){

			return honey.experiment(experimentName).query().mprocess(file,params);
		}
		else{

			return honey.experiment(experimentName).query().mprocess(file);
		}
	}


	@ScriptMethod public String mprocessf(final String file,final String params){

		if (!params.replaceAll(" ","").equals("{}")){

			return honey.experiment(experimentName).query().mprocessFile(file,params);
		}
		else{

			return honey.experiment(experimentName).query().mprocessFile(file);
		}
	}

	@ScriptMethod public String processf(final String file, final String params){

		if (!params.replaceAll(" ","").equals("{}")){

			return honey.experiment(experimentName).query().processFile(file,params);
		}
		else{

			return honey.experiment(experimentName).query().processFile(file);
		}
	}

	@ScriptMethod public String processfResult(final String filename){

		return honey.experiment(experimentName).query().getProcessFile(filename);
	}

	@ScriptMethod public void disconnect(){

		 honey.disconnect();
		 honey = null;
	}

	@Override public String getReferenceName() {

		return "$honey";
	}

	@Override public void setup() {}

	@Override public void shutdown() {

		if (honey != null){
			try{

				honey.disconnect();

			}catch(Throwable e){

				Log.e(e);
			}
		}

	}

}
