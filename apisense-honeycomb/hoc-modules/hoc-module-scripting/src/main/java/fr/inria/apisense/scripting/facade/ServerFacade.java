/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.inria.apisense.APISENSE;
import fr.inria.apisense.APISWebSocket;
import fr.inria.apisense.ApisRESTService;
import fr.inria.apisense.RESTServiceListenner;
import fr.inria.asl.engine.IScriptFunction;
import fr.inria.asl.facade.FacadeAnnotation.ScriptMethod;
import fr.inria.asl.facade.FacadeManager;

public class ServerFacade extends ApisFacade{

	final List<WebSocketWrapper> websockets;

	final List<ApisRESTService> rests;

	/**
	 * @class
	 * Reference name : server
	 * <p>
	 *   Provide method to create remote services
	 * </p>
	 */
	public ServerFacade(FacadeManager facadeManager, String experimentName) {
		super(facadeManager, experimentName);
		this.websockets = new ArrayList<WebSocketWrapper>();
		this.rests = new ArrayList<ApisRESTService>();
	}

	@Override public String getReferenceName() {
		return "server";
	}

	@ScriptMethod public String experimentName(){
		return this.experimentName;
	}

	/**
	 *
	 * Create a websocket
	 *
	 * @param path websocket path
	 *
	 * @return
	 */
	@ScriptMethod public WebSocketWrapper websocket(final String path){

		final WebSocketWrapper websocket = new WebSocketWrapper(
				APISENSE.webserver().createWebSocket("/websocket/"+this.experimentName+"/"+path));

		this.websockets.add(websocket);

		return websocket;
	}

	/**
	 *
	 * Create POST service
	 *
	 * @param path service path
	 * @param function(arguments) callback
	 */
	@ScriptMethod public void post(String path, final IScriptFunction callback){

		if (path.startsWith("/"))
			path = path.substring(1);

		final ApisRESTService service = APISENSE.webserver().post(host()+"/"+path,new RESTServiceListenner() {
			public String run(Map<String, List<String>> args) {

				final Object result = callback.call(args);
				if (result != null){
					return result.toString();
				}
				else return "{ \"error\" : NULL }";
			}
		});

		this.rests.add(service);
	}

	/**
	 *
	 * Create GET service
	 *
	 * @param path service path
	 * @param function(arguments) callback
	 */
	@ScriptMethod public void get(final String path, final IScriptFunction callback){

		final ApisRESTService service = APISENSE.webserver().get(this.experimentName+"/"+path,new RESTServiceListenner() {
			public String run(Map<String, List<String>> args) {
				return callback.call(args).toString();
			}
		});

		this.rests.add(service);
	}

	@ScriptMethod public String host(){ return "/socko/"+this.experimentName; }

	@Override
	public void shutdown() {

		for (final WebSocketWrapper websocket : this.websockets){
			websocket.cancel();
		}

		for (final ApisRESTService rest : this.rests){
			APISENSE.webserver().deleteRESTService(rest);
		}
	}

	@Override
	public void setup() {}

	//------------------------------------------------------------------ INNER CLASS

	public class WebSocketWrapper{

		final private APISWebSocket websocket;

		private IScriptFunction callback;

		public WebSocketWrapper(APISWebSocket websocket){
			this.websocket = websocket;
		}

		@ScriptMethod public void broadcast(final String message){
			websocket.broadcast(message);
		}

		@ScriptMethod public void cancel(){
			APISENSE.webserver().deleteWebSocket(websocket);
		}

		@ScriptMethod public void onMessage(IScriptFunction callback){
			this.callback = callback;
		}
	}

}