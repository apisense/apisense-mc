/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import org.json.JSONException;
import org.json.JSONObject;

import fr.inria.asl.event.LeafEvent;

public class ExceptionEvent extends LeafEvent {

	final public String error;
	
	final public String type;
	
	public ExceptionEvent(final Exception exception){
		this.error = exception.getMessage();
		this.type = exception.getClass().getSimpleName();
	}
	
	public ExceptionEvent(final String json) throws JSONException{
		final JSONObject jsonObject = new JSONObject(json);
		this.error = jsonObject.getJSONObject("error").getString("message");
		this.type  = jsonObject.getJSONObject("error").getString("type");
	}
	
}
