/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import fr.inria.bsense.common.utils.FileConfiguration;
import fr.inria.bsense.node.configuration.ServerNodeConfiguration;


public class MediaFolder extends FileConfiguration{



	public MediaFolder(final String experimentName) {
		super(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY(), "experiment/"+experimentName+"/medias");

	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;



}
