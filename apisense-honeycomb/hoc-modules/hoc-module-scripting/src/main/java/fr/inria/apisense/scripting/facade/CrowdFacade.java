/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import fr.inria.apisense.client.APISENSEClient;
import fr.inria.apisense.client.HiveClient;
import fr.inria.asl.engine.IScriptFunction;
import fr.inria.asl.facade.FacadeAnnotation.ScriptMethod;
import fr.inria.asl.facade.FacadeManager;
import fr.inria.asl.http.Base64;
import fr.inria.bsense.node.configuration.ServerNodeConfiguration;

public class CrowdFacade extends ApisFacade {

    private HiveClient hive = APISENSEClient.hive();

	/**
	 * @class
	 * <strong>Reference name : </strong> crowd
	 * <p>
	 * Push message to mobile devices
	 * </p>
	 *
	 *  <h2 id="config" >Message configuration</h2>
	 * <table>
	 *  <tr><th>Types</th><th>Fields</th><th>Descriptions</th></tr>
	 *  <tr>
	 *  		<td>long</td>
	 *  		<td>timeToLive</td>
	 *  		<td>
	 *  How long (in seconds) the message should be kept on GCM
	 *  storage if the device is offline.
	 *  		</td>
	 *  </tr>
	 * 	<tr>
	 * 		<td>string</td>
	 * 		<td>collapseKey</td>
	 * 		<td>
	 * 			collapse a group of like messages when the
	 * 			device is offline.
	 * 		</td>
	 *  </tr>
	 * 	<tr>
	 * 		<td>boolean</td>
	 * 		<td>delayWhileIdle</td>
	 * 		<td>
	 * 			If included, indicates that the message should
	 * 			not be sent immediately if the device is idle.
	 * 		</td>
	 *  </tr>
	 * </table>
	 */
    public CrowdFacade(FacadeManager facadeManager, final String name) {
		super(facadeManager,name);
	}

	public String getReferenceName() {return "crowd";}

	public void setup() {
		hive.host(ServerNodeConfiguration.getUrl());
		hive.connect(
		  ServerNodeConfiguration.username(),
		  ServerNodeConfiguration.password());
	}

	public void shutdown() {
		//hive.disconnect();
	}

	/**
	 *
	 * Push a message to a specific user participating to the experiment
	 *
	 * @param {Array} userId Id generated on user subscription of current experiment
	 *
	 * @param {JSON} message Event to push to the experiment of the participant
	 *
	 * @param {JSON} configuration
	 * See <a href="#config">Message configuration</a>
	 *
	 * @param {Function} function(messageResult) Callback called with statistical results of the push
	 */
	@SuppressWarnings("unchecked")
	@ScriptMethod public void user(
			final String userId,
			final String message,
			final String configuration,
			final IScriptFunction... callback){

		final JSONObject json = ((JSONObject)JSONValue.parse(message));
		json.put("$process",this.experimentName);

		final String messageResult = hive.crowd().pushMessageToUser(userId, Base64.encodeToString(json.toJSONString().getBytes(),true), configuration);
		if (callback.length > 0){
			callback[0].call(((JSONObject)JSONValue.parse(messageResult)));
		}
	}

	/**
	 *
	 * Push a message to all users participating to the experiment
	 *
	 * @param data Message to broadcast
	 * <table>
	 *  <tr><th>Types</th><th>Fields</th><th>Descriptions</th></tr>
	 *  <tr><td>string</td><td>$command</td><td>
	 *  	command to execute by the device.
	 *  	<ul>
	 *  		<li> uninstall  (uninstall the experiment)      </li>
	 *  		<li> update     (try to update the experiment)  </li>
	 *  		<li> start      (start the experiment)          </li>
	 *  		<li> stop       (stop the experiment)           </li>
	 *  	</ul>
	 *  <td></tr>
	 *  <tr><td>JSON</td><td>$event</td><td>push event</td>
	 * </table>
	 *
	 * @param {JSON} configuration GCM message configuration
	 * See <a href="#config">Message configuration</a>
	 *
	 * @param {Function} function(messageResult) Callback called with statistical results of the push
	 */
	@ScriptMethod public void experiment(final String data, final String configuration, final IScriptFunction... callback){

		final String message = hive.crowd().pushMessageToExperimentUsers(this.experimentName, Base64.encodeToString(data.getBytes(),true), configuration);
		if (callback.length > 0){
			callback[0].call(((JSONObject)JSONValue.parse(message)));
		}
	}

	/**
	 *
	 * Push a message to all users where APISENSE is installed
	 *
	 * @param data Message to broadcast
	 * <table>
	 *  <tr><th>Types</th><th>Fields</th><th>Descriptions</th></tr>
	 *  <tr><td>string</td><td>$notification</td><td>Show a notification on mobile devices<td></tr>
	 *  <tr><td>string</td><td>$toast</td><td>Show  a toast on mobile devices<td></tr>
	 *  <tr><td>string</td><td>$upadte</td><td>Try to update APK application<td></tr>
	 * </table>
	 *
	 * @param {JSON} configuration GCM message configuration
	 * See <a href="#config">Message configuration</a>
	 *
	 * @param {Function} function(messageResult) Callback called with statistical results of the push
	 */
	@ScriptMethod public void broadcast(final String data, final String configuration, final IScriptFunction... callback){

		final String message = hive.crowd().pushMessageToAllUsers(Base64.encodeToString(data.getBytes(),true), configuration);
		if (callback.length > 0){
			callback[0].call(((JSONObject)JSONValue.parse(message)));
		}
	}


}