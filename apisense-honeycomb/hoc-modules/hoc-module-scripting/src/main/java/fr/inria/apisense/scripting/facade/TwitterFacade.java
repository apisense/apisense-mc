/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.facade;

import java.io.File;
import java.util.Map;

import twitter4j.GeoLocation;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import fr.inria.asl.facade.FacadeAnnotation.ScriptMethod;
import fr.inria.asl.facade.FacadeManager;
import fr.inria.asl.utils.Log;



public class TwitterFacade extends ApisFacade{

	public TwitterFacade(FacadeManager facadeManager, String experimentName) {
		super(facadeManager, experimentName);
	}

	@Override public String getReferenceName() {
		return "$twitter";
	}

	private  Twitter twitter;

	@Override
	public void setup() {

	}

	@ScriptMethod public void account(final Map<String,Object> account){

		final String consumerKey = (String) account.get("consumerKey");
		final String consumerSecret = (String) account.get("consumerSecret");
		final String accessToken = (String) account.get("accessToken");
		final String accessTokenSecret = (String) account.get("accessTokenSecret");

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true).setOAuthConsumerKey(consumerKey)
		.setOAuthConsumerSecret(consumerSecret)
		.setOAuthAccessToken(accessToken)
		.setOAuthAccessTokenSecret(accessTokenSecret);

		twitter = new TwitterFactory(cb.build()).getInstance();

	}

	@ScriptMethod public void sleep(String millis) throws NumberFormatException, InterruptedException{
		Thread.currentThread().sleep(Long.parseLong(millis));
	}

	@ScriptMethod public String publish(String message, final Map<String,Object> twConf) throws TwitterException{

		StatusUpdate update = new StatusUpdate(message);


		Double latitude = null;
		Double longitude = null;
		if (twConf.containsKey("lat")){
			latitude = Double.parseDouble(twConf.get("lat").toString());
		}
		if (twConf.containsKey("lon")){
			longitude = Double.parseDouble(twConf.get("lon").toString());
		}

		if (latitude != null && longitude != null){
			update.setLocation(new GeoLocation(latitude,longitude));
		}

		if (twConf.containsKey("media")){
			final String media = twConf.get("media").toString();

			final MediaFolder mediaFolder = new MediaFolder(experimentName);
			if (mediaFolder.contain(media)){

				String name = ".";
				if (twConf.containsKey("mediaName")){
					name = (String) twConf.get("mediaName");
				}

				final File mediaFile = mediaFolder.file(media);
				if (mediaFile.exists()){
				  Log.d("add media file "+mediaFile.getAbsolutePath());
				  update.setMedia(mediaFile);
				}else{
					Log.d("Cannot find media folder "+mediaFile.getAbsolutePath());
				}
			}

		}


		Status status = twitter.updateStatus(update);
		return status.getText();
	}



	//public String publish(String message, String latitude, String longitude,String imageName, String imageId) throws TwitterException{

	//class MediaStorage(experimentName : String) extends NodeFileConfiguration("experiment/"+experimentName+"/medias")

	/*final MediaStorage ms = new MediaStorage(experimentName);

		StatusUpdate update = new StatusUpdate(message);
		update.setLocation(new GeoLocation(Double.parseDouble(latitude),Double.parseDouble(longitude)));

		update.setMedia(imageName, ms.getFileInputStream(imageId));
		update.setDisplayCoordinates(true);

		Status status = twitter.updateStatus(update);
		return status.getText();*/
	//}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}



}
