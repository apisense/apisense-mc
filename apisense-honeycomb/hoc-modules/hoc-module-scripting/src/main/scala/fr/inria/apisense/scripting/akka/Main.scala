/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.akka

import fr.inria.bsense.node.configuration.NodeConfigurationDB
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout
import fr.inria.apisense.APISENSE
import fr.inria.apisense.APISENSE._
import fr.inria.apisense.common.ExperimentContext
import fr.inria.apisense.scripting.akka.ServerTaskActor._
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.apisense.APISENSE
import org.basex.BaseXServer
import scala.concurrent.Await
import akka.actor.ActorRef
import fr.inria.apisense.webserver.SockoWebserver
import org.mashupbots.socko.webserver.WebServerConfig
import fr.inria.asl.http.Base64
import fr.inria.apisense.RESTServiceListenner

object Main {

  def main(args: Array[String]) {


    ServerNodeConfiguration.setUrl("http://192.168.0.10:18001")
    ServerNodeConfiguration.setUrl("http://localhost:18001")

    startDB

    APISENSE.webserver = new SockoWebserver(new WebServerConfig())
    APISENSE.webserver.start

    test();

 }

  private def startDB(){
    val home = System.getProperty("user.home")
    val dbpath = home+"/BEES_HIVE/BaseXData";


    NodeConfigurationDB.put(NodeConfigurationDB.PROPERTY_PORT,"1985")
    NodeConfigurationDB.put(NodeConfigurationDB.PROPERTY_PORTS,"1984")
    NodeConfigurationDB.store;
    System.setProperty(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY,home+"/BEES_HIVE")
    System.setProperty("org.basex.path",dbpath)
    System.setProperty("org.basex.TIMEOUT",(1000*60*5).toString)
    System.setProperty(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY,home+"/BEES_HIVE")

    val bx = new BaseXServer("-p1985","-e1984")
  }

  private def tt(){

    val ref = APISENSE.globalSystem.actorOf(Props(new ServerTaskActor(new ExperimentContext("xp1"))))
    val ref2 = APISENSE.globalSystem.actorOf(Props(new ServerTaskActor(new ExperimentContext("xp2"))))

  }

  private def test(){

    val taskName = "MyTask"
    val taskName2 = "MyTask"
    val experimentName = "CollectorModuleTest2"




    val mainjs = """

  //      private static final String ACCESS_TOKEN = "1712191748-oRcwK3jKMU6s7rmo91ofCxeJssFc3bFcWv9EiFw";
  //  private static final String ACCESS_TOKEN_SECRET = "12ufudEKXcKWZqrqeaFWrvQugJYXf4M2KN3GTifUEI";


     	$twitter.account({
      		consumerKey      : "7iD7uE0qm8RATllwmL8Hw",
            consumerSecret   : "usfx1SGSv1INgf9rDjEjcxwHDwEkwNZH1HSv6y3UU",
      		accessToken      : "1712191748-oRcwK3jKMU6s7rmo91ofCxeJssFc3bFcWv9EiFw",
      		accessTokenSecret: "12ufudEKXcKWZqrqeaFWrvQugJYXf4M2KN3GTifUEI"
        })

     var date = new Date().getHours()+" : "+new Date().getMinutes()

     var result =  $twitter.publish("La quête de Bijou va bientôt commencer "+date,{
      lat : 0.00000000545,
      lon : 5.00000005455,
      media : "idpicture1.png",
      mediaName : "Mon bijou"
     })
     log.d(result);



    """


    val actorRef = APISENSE.globalSystem.actorOf(Props(new ServerTaskActor(new ExperimentContext(experimentName))))
    Thread.sleep(100)
    actorRef ! ServerTaskActor.CREATE_PROJECT(taskName)
    Thread.sleep(100)

    actorRef ! ServerTaskActor.ADD_SCRIPT(taskName,"main.js", Base64.encodeToString(mainjs.getBytes(),true))
    Thread.sleep(100)
    actorRef ! ServerTaskActor.START_PROJECT(taskName)




   Thread.sleep(10000)
  }

  private def call(ref : ActorRef, actorMessage : Any)  = {
    import scala.concurrent.duration._

    val futur = ref ? actorMessage

    try{
      Await.result(futur, 5 seconds)

    }catch{
     case e : Throwable => println(e);null
    }

  }

}