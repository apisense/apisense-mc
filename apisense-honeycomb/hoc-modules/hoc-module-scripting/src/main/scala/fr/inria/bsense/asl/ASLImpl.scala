/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.asl

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration._
import org.osoa.sca.annotations.Scope
import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import akka.util.Timeout
import fr.inria.apisense.APISENSE
import fr.inria.apisense.APISENSE._
import fr.inria.apisense.common.ExperimentContext
import fr.inria.apisense.scripting.akka.ServerTaskActor
import fr.inria.apisense.scripting.akka.ServerTaskActor._
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.XMLNode
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.UriInfo

@Scope("Composite")
class ASLImpl extends ASLService with LifeCycleService{

  var actorRef : ActorRef = null

  def onStart(composite : BSenseComposite){



    actorRef = APISENSE.globalSystem.actorOf(Props(new ServerTaskActor(new ExperimentContext(this.experimentName))))

  }

  def onStop(){ actorRef ! KILL }

  def createScriptingProject(sc : SecurityContext,  projectName : String) : String = { call(CREATE_PROJECT(projectName)); }

  def addScript(sc : SecurityContext, projectName : String,   filename : String, fileContent : String) : String = call(ADD_SCRIPT(projectName, filename, fileContent))

  def deleteProject(sc : SecurityContext,projectName : String) : String = call(DELETE_PROJECT(projectName))

  def deleteScript(sc : SecurityContext, projectName : String,filename : String) : String = call(DELETE_SCRIPT(projectName,filename))

  def getScriptingProjects(sc : SecurityContext) : String = { callWithResult(GET_PROJECTS()) }

  def getScriptFiles(sc : SecurityContext, projectName : String) = callWithResult(GET_SCRIPTS_FILES(projectName))

  def startProject(sc : SecurityContext,projectName : String) : String = {


    call(START_PROJECT(projectName))
  }

  def stopProject(sc : SecurityContext, projectName : String) : String = call(STOP_PROJECT(projectName))

  def getScriptContent(sc : SecurityContext,projectName : String, filename : String) = callWithResult(GET_SCRIPT_CONTENT(projectName,filename))

  def sendMessage(sc : SecurityContext, projectName : String, message : String) : String = call(SEND_EVENT(projectName,message))

  def sendMessage(sc : SecurityContext, message : String) : String = call(BROADCAST_EVENT(message))

  def createFolder(sc : SecurityContext, name : String, folder : String) : String = {

    var rootFolder =  NodeFileConfiguration("ASL/"+this.experimentName+"/scripts/"+name).folder(folder)

    Message.success();
  }


  def getLog(sc : SecurityContext, filter : String) : String = {
    val futur = ask(actorRef,GET_LOG(filter))
    var result = Await.result(futur, defaultTime)
    result.toString
  }

  def clearLog(sc : SecurityContext) : String = call(CLEAR_LOG())

  private def call( actorMessage : Any ) : String = {
    actorRef ! actorMessage
    Message.success()
  }

  private def callWithResult(actorMessage : Any) : String = {

    try{
    	 val futur = ask(actorRef,actorMessage)
         Message.success(Await.result(futur, defaultTime))
    }
    catch{
      case t : Throwable => Message.exception(actorMessage.toString,"Cannot call function : "+t.getMessage())
    }

  }

}