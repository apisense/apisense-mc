/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.asl

import fr.inria.bsense.node.service.ExperimentModuleService
import javax.ws.rs.Path
import javax.ws.rs.POST
import javax.ws.rs.FormParam
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.UriInfo


 class MediaStorage(experimentName : String) extends NodeFileConfiguration("experiment/"+experimentName+"/medias")

/**
 *
 *
 *
 *
 *
 */
trait ASLService extends ExperimentModuleService {



  /**
   * Create a new scripting project
   * @param name Name of scripting project
   */
  @POST
  @Path("/update/create")
  def createScriptingProject(@Context sc : SecurityContext, @FormParam("name") name : String) : String

  @POST
  @Path("/update/script")
  def addScript(
      @Context sc : SecurityContext,
      @FormParam("name") name : String,
      @FormParam("filename") filename : String,
      @FormParam("filecontent") fileContent : String) : String


  @POST
  @Path("/update/folder")
  def createFolder(@Context sc : SecurityContext,@FormParam("name") name : String, @FormParam("folder") folder : String) : String

  @POST
  @Path("/delete/project")
  def deleteProject(
      @Context sc : SecurityContext,
      @FormParam("name") name : String) : String

  @POST
  @Path("/delete/script")
  def deleteScript(
      @Context sc : SecurityContext,
      @FormParam("name") name : String,
      @FormParam("filename") filename : String) : String


  @POST
  @Path("/get/projects")
  def getScriptingProjects(@Context sc : SecurityContext) : String

  @POST
  @Path("/get/script/files")
  def getScriptFiles(@Context sc : SecurityContext,@FormParam("name") name : String) : String

  @POST
  @Path("/get/script/content")
  def getScriptContent(
      @Context sc : SecurityContext,
      @FormParam("name") name : String,
      @FormParam("filename") filename : String) : String


  @POST
  @Path("/get/log")
  def getLog(
      @Context sc : SecurityContext,
      @FormParam("filter") filter : String) : String

  /**
   * Clear logs
   */
  @POST
  @Path("/clear/log")
  def clearLog(@Context sc : SecurityContext) : String

  /**
   * Publish a message to all serverTasks
   *
   * @param message Message under JSON format
   *
   */
  @POST
  @Path("/publish/tasks")
  def sendMessage(@Context sc : SecurityContext, @FormParam("message") message : String) : String

  /**
   * Publish a message to a server task
   *
   * @param serverTask  ServerTask to publish message
   * @param message     Message under JSON format
   *
   */
  @POST
  @Path("/publish/task")
  def sendMessage(@Context sc : SecurityContext,@FormParam("serverTask") serverTask : String, @FormParam("message") message : String) : String


  /**
   * Start a server task
   *
   * @param name : serverTask name
   */
  @POST
  @Path("/start")
  def startProject(
      @Context sc : SecurityContext,
      @FormParam("name") name : String) : String


 /**
  * Stop a server task
  *
  * @param name : serverTask name
  */
  @POST
  @Path("/stop")
  def stopProject(
      @Context sc : SecurityContext,
      @FormParam("name") name : String) : String


}