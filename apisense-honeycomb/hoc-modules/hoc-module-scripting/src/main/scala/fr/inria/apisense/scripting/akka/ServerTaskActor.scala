/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.akka

import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import akka.actor.Actor
import akka.actor.actorRef2Scala
import fr.inria.apisense.APISENSE
import fr.inria.apisense.APISWebSocket
import fr.inria.apisense.common.ExperimentContext
import fr.inria.apisense.scripting.FacadeFactory
import fr.inria.apisense.scripting.facade.CrowdFacade
import fr.inria.asl.facade.EsperFacade
import fr.inria.asl.facade.EventStreamFacade
import fr.inria.asl.http.Base64
import fr.inria.asl.node.server.ServerASLServiceManager
import fr.inria.asl.node.server.facade.LogFacade
import fr.inria.asl.pubsub.MainEventBus
import fr.inria.asl.rhino.RhinoEngineDescriptor
import fr.inria.asl.utils.Log
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.common.utils.XMLNode
import fr.inria.bsense.node.configuration.NodeConfigurationDB
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.apisense.scripting.facade.ServerFacade
import fr.inria.asl.node.server.facade.TimerFacade
import fr.inria.asl.facade.ContextFacade
import fr.inria.apisense.scripting.facade.TwitterFacade
import fr.inria.bsense.common.utils.IOUtils
import fr.inria.asl.engine.OnScriptExit
import fr.inria.asl.engine.ScriptContext
import fr.inria.apisense.scripting.facade.HoneyCombFacade

object ServerTaskActor{
  case class CREATE_PROJECT(projectName : String)
  case class ADD_SCRIPT(projectName : String, fileName : String, fileContent : String)
  case class CLEAR_LOG()
  case class SEND_EVENT(projectName : String ,event : String)
  case class BROADCAST_EVENT(event : String)
  case class DELETE_PROJECT(projectName : String)
  case class DELETE_SCRIPT(projectName : String, fileName : String)
  case class START_PROJECT(projectName : String)
  case class STOP_PROJECT(projectName : String)
  case class GET_SCRIPT_CONTENT(projectName : String, filename : String)
  case class GET_SCRIPTS_FILES(projectName : String)
  case class GET_LOG(filter : String)
  case class GET_PROJECTS()
  case class KILL()
}


class ServerTaskActor(experimentContext : ExperimentContext) extends Actor {

  var asl : ServerASLServiceManager = _

  var webSocket : APISWebSocket = _;

  val readLogThread : Thread = new Thread(new Runnable(){override def run(){readlogDaemon}});

  override def preStart(){

    println("Actor "+this+" start "+(asl == null))

    if (asl == null){

      // create a new asl instance
      // and connected with node database
      asl = new ServerASLServiceManager(
        NodeFileConfiguration("ASL").compositeFolder,
        experimentContext.experimentName,
        new FacadeFactory(experimentContext.experimentName),
        NodeConfigurationDB host,
        NodeConfigurationDB.port.toInt,
        NodeConfigurationDB user,
        NodeConfigurationDB password
      )

      val service = asl.getInstallerService()

      // Install javascript interpreter
      service.installScriptEngine(classOf[RhinoEngineDescriptor])

      // Install scripting functionalities
      service.installFacade(classOf[LogFacade])
      service.installFacade(classOf[TwitterFacade])
      service.installFacade(classOf[CrowdFacade])
      service.installFacade(classOf[TimerFacade])
      service.installFacade(classOf[ServerFacade])
      service.installFacade(classOf[HoneyCombFacade])
      service.installFacade(classOf[ContextFacade])
      service.installFacade(classOf[EsperFacade])
      service.installFacade(classOf[EventStreamFacade])

      val websocketPath = "/websocket/"+experimentContext.experimentName+"/logs"
      webSocket = APISENSE.webserver.createWebSocket(websocketPath);


      readLogThread.start()
    }
  }

  override def postStop(){

    Log.d(" Actor  "+this+"  is stopped ");

    try{ readLogThread.interrupt() }catch{case t : Throwable => Log.e(t)}
    try{ APISENSE.webserver.deleteWebSocket(webSocket)}catch{case t : Throwable => Log.e(t)}
    if (asl != null){

    	asl.getScriptService().killAll();
    	asl = null;
    }
  }

  def receive = {
     case  ServerTaskActor.CREATE_PROJECT(projectName) => createScriptingProject(projectName)
     case  ServerTaskActor.ADD_SCRIPT(projectName, filename, fileContent) => addScript(projectName, filename, fileContent);
     case  ServerTaskActor.CLEAR_LOG() => clearLog
     case  ServerTaskActor.SEND_EVENT(projectName, event) => sendMessage(projectName, event)
     case  ServerTaskActor.BROADCAST_EVENT(event) => sendMessage(event)
     case  ServerTaskActor.DELETE_PROJECT(projectName) => deleteProject(projectName)
     case  ServerTaskActor.DELETE_SCRIPT(projectName, filename) => deleteScript(projectName, filename)
     case  ServerTaskActor.START_PROJECT(projectName) => startProject(projectName)
     case  ServerTaskActor.STOP_PROJECT(projectName) => stopProject(projectName)
     case  ServerTaskActor.GET_SCRIPT_CONTENT(projectName, filename) => sender ! getScriptContent(projectName, filename)
     case  ServerTaskActor.GET_SCRIPTS_FILES(projectName) => sender ! getScriptFiles(projectName)
     case  ServerTaskActor.GET_LOG(filter) => sender ! getLog(filter)
     case  ServerTaskActor.GET_PROJECTS() => sender ! getScriptingProjects
     case  ServerTaskActor.KILL => kill
     case _ => Log.e("Cannot find command ")
  }

  def kill(){
    postStop
    context.stop(self);
  }

  def createScriptingProject(projectName : String) {

    val service = asl.getInstallerService()
    service.installScriptFromLocalResource(
          projectName,
          "main.js",
          new ByteArrayInputStream(Array[Byte]()) , false)
  }

  def addScript(projectName : String, filename : String, fileContent : String){

    try{
    	val base64FileContent = Base64.decode(fileContent.getBytes())
    	val service = asl.getInstallerService()
    	 service.addScript(
    	  new ByteArrayInputStream(base64FileContent),
          filename,
          projectName)
    }catch{ case throwable : Throwable => Log.e(throwable) }
  }

  def deleteProject(projectName : String){

      val service = asl.getInstallerService()
      service.uninstallScript(projectName)
  }


  def deleteScript(projectName : String,filename : String){

    asl.getInstallerService().deleteScript(filename,projectName);
  }

  def getScriptingProjects = {

    // Get all project name installed
    val projectNames = asl.getInstallerService().getInstalledProjects()

    // For each installed project
    // return project name and if
    // project running or not
    var projectNodes : Node = new XMLNode(<projects></projects>)

    // Cannot parse Array[String] with foreach method
    // Check in Scala 10.0
    projectNames.foreach{
      projectName =>

        if (projectName.split(":")(0).equals(experimentContext.experimentName)){
      	    var node = <project>
    				 <name>{projectName}</name>
        			 <state>{asl.getScriptService().getScriptState(projectName.split(":")(1))}</state>
        		   </project>;

             projectNodes = projectNodes.:<(new XMLNode(node))
        }
    }

    projectNodes
  }

  def getScriptFiles(projectName : String) = {

    var rootFolder =  NodeFileConfiguration("ASL/"+experimentContext.experimentName+"/scripts/"+projectName).getFile

    NodeParser.parJSON(IOUtils.hierarchy(rootFolder, projectName))
  }

  def startProject(name : String) {

    asl.getScriptService().startScript(name, new OnScriptExit(){

      def onExit(context: fr.inria.asl.engine.ScriptContext, exitCode: Int) : java.lang.Boolean = {
        Log.d("scripting project "+context.getProcessName()+" finished with value "+exitCode)
        webSocket.broadcast("{ \"type\" :\"stop\" }")
        true
      }
    })
  }

  def stopProject( name : String) { asl.getScriptService().killScript(name) }

  def getScriptContent(name : String, filename : String) = {
	  	Base64.encodeToString(
             NodeFileConfiguration("ASL/"+experimentContext.experimentName+"/scripts/"+name).getFile(filename).getBytes()
        ,false)
  }

  def sendMessage(projectName : String, message : String) {

      val eventBus = asl.getScriptService().getEventBus(projectName)

      if (eventBus != null){
        eventBus.publish(new JSONParser().parse(message).asInstanceOf[JSONObject])

      }else Log.w("Cannot publish message to server task "+projectName+" : not running")

  }

  def sendMessage(message : String){

    MainEventBus.getInstance().broadcast(new JSONParser().parse(message).asInstanceOf[JSONObject]);
  }

  def getLog(filter : String) = {

    if ((filter != null)&&(filter.contains(":")))
      asl.getLog(filter.split(":")(0),filter.split(":")(1))
    else
      asl.getLog("*","*")
  }

  def clearLog(){

    //try{ readLogThread.interrupt(); Thread.sleep(500); }catch{case t : Throwable => Log.e(t)}
    //try{ asl.clearLog(); }catch{ case t : Throwable => Log.e(t) }
    //try{ readLogThread.start() }catch{case t : Throwable =>println(t)}

    //webSocket.broadcast("{ \"type\" :\"clear\" }")

  }

  private def readlogDaemon(){

    val command = "tail -f "+asl.getLogFile()

    Log.d("start daemon "+command+" with "+webSocket)

    val input = Runtime.getRuntime().exec(command).getInputStream()
    val reader = new BufferedReader(new InputStreamReader((input)))
    var line : String = ""
    while ((line = reader.readLine()) != null) {

       webSocket.broadcast("{ \"type\" :\"message\" , \"data\" : \""+Base64.encodeToString(line.getBytes(),true)+"\"}")
    }
  }

}