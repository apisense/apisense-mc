/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.asl

import fr.inria.bsense.node.cbse.ExperimentModule
import fr.inria.bsense.node.web.cbse.ExperimentWebModuleComponent
import frascala.sca.Java
import frascala.frascati.REST
import frascala.sca.Bean

class ASLComponent extends ExperimentModule("fr.inria.bsense.asl"){
  this.uses(Bean[ASLImpl])
  
  val srvASL = service("asl-srv-manager") exposes Java[ASLService]
  deployRest(srvASL, "asl", Array())
   
  def getModuleName: String = "Service ASL"
  def getModuleURI: String  = "/asl"
}

class ASLWebComponent extends ExperimentWebModuleComponent("fr.inria.bsense.web.asl"){

  
  def getModuleName: String  = "Server Task"
  def getModuleURL: String   = "asl/asl.html"
  
}