enyo.kind({

	name: "apisense.web.ASLEditor",

	constructor : function(object){

        this.inherited(arguments);

        var node = $(".Server")
        node.append("<ul id='id-tree-tasks' class=' '></ul>")

        node.parent().css("width","300px")
		$("#content").css("margin-left","300px")



        this.service = object.service || "./";
        this.divId   = object.id || "editor";
        this.height  = object.height || "500px";

        var self = this;

        this.editor = new APISEditor({
        	onFileSelected : function(editor,filename){

        		filename = filename.substring(filename.indexOf('/')+1);

        		var request = new APIS.POST(self.service+"/asl/get/script/content",{name:task,filename: filename});
        		request.onSuccess = function(content){
        			editor.open(filename,decode(content));
        		}

        	}
        });

        this.editor.renderInto(this.divId);

        var tool = this.editor.toolbar();

        this.taskMenu = [];

        tool.menu("New Task","icon-tasks",function(){self.newtask()});

        this.taskMenu.push(tool.menu("Delete Task","icon-trash",function(){self.deleteproject()}));

        tool.separator();

        this.taskMenu.push(tool.newfolder(function(f){self.addfolder(f)}));
        this.taskMenu.push(tool.newfile(function(f){self.addfile(f)},[".js",".xq"]));
        this.taskMenu.push(tool.deletefile(function(f){ self.deletefile(f)}));
        this.taskMenu.push(tool.separator());
		this.taskMenu.push(tool.savefile(function(f,c){ self.savefile(f,c) }))

		tool.separator();
		this.playMenu = tool.menu("Run","icon-play-circle",function(){

			if (taskState[task] == "true"){
				self.stop(task)
			}else{
				self.start(task)
			}
		});

		this.taskMenu.push(this.playMenu);



		tool.separator();
		tool.menu("Clear Log","icon-eye-open",function(){self.clearlog()})
		tool.menu("Log","icon-refresh",function(){self.getlog()})
		this.logFilter = tool.input("Filter")
		tool.separator();

		this.updateTask(function(name){});

        this.editor.initConsole();

        this.desactiveMenu();


        this.initWebSocket();
	},


	initWebSocket : function(){

		try{
			var self = this;
        	var websocketPath = "ws://"+window.location.hostname
        	// dev section
        	if (window.location.hostname.slice(0,1) == "1") websocketPath = "ws://localhost:8888"
        	if (window.location.hostname == "localhost") websocketPath = "ws://localhost:8888"

        	websocketPath = websocketPath + "/websocket/"+xpname+"/logs"

        	var socket = new APIS.WebSocket({
        		path   : websocketPath,
        		onClose : function(event){},
        		onMessage : function(message){
        			try{

        				var json = JSON.parse(message.replaceAll("\n","").replaceAll("\r",""))

        				if (json.type == "message"){self.editor.addConsoleValue(decode(json.data))}
        			    else if (json.type == "clear"){}
        			    else if (json.type == "stop"){self.updateTask()}
        		    }catch(e){
        			   editor.addConsoleValue("Cannot parse "+message)
        		    }
        		}
        	})


	     }catch(err){alert(err)}

	},

	activeMenu : function(){ for (var i in this.taskMenu){  this.taskMenu[i].applyStyle("display","inline") } },

	desactiveMenu : function(){
		for (var i in this.taskMenu){
			this.taskMenu[i].applyStyle("display","none")
		}
	},

	select : function(project){

		var self = this;

		task = project
		var request = new APIS.POST(this.service+"/asl/get/script/files",{name:project})
		request.onSuccess = function(tree){
			self.editor.updateTreeModel(tree)
			self.updateTask();
		}
	},

	getlog : function(){

		var self = this;

		var request = new APIS.POST(this.service+"/asl/get/log",{filter: this.logFilter.getValue()},{parse:false});
		request.onSuccess = function(r){
			self.editor.setConsoleValue(r);
		}

	},

	start : function(project){
		var self = this;
		new APIS.POST(this.service+"/asl/start",{name:project}).onSuccess = function(){self.updateTask()}
	},

	stop : function(project){

		var self = this;
		new APIS.POST(this.service+"/asl/stop",{name:project}).onSuccess = function(){self.updateTask()}
	},

	clearlog : function(){

		var self = this;
		self.editor.setConsoleValue("");
		//var request = new APIS.POST(this.service+"/asl/clear/log",{},{parse:false});
		//request.onSuccess = function(r){
		//	self.editor.setConsoleValue("");
			//self.initWebSocket();
		//}
		//request.onError = function(e){ self.editor.setConsoleValue(e); }
	},

	newtask : function(){

		var self = this;

		var modal = new apisense.InputModal({
			title : "Create a new Task",
			onValid : function(data){ self._newtask(data.input) }
		})
		modal.renderInto(self.editor);
		modal.show();
	},

	_newtask : function(taskname){

		var self = this;

		if (taskname == ""){
			new ModalError({message:"Task name cannot be null",onClose:function(){}})
			return;
		}

		var request = new APIS.POST(this.service+"/asl/update/create",{name: taskname});
		request.onSuccess = function(r){
			self.updateTask();
		}
	},

	updateTask : function(callback){

		var self = this;
		$("#id-tree-tasks").children().remove()

		if(task){this.activeMenu()}
		else{ this.desactiveMenu();}

		$("#NewTask").show()

		var request = new APIS.POST(this.service+"/asl/get/projects");
		request.onSuccess = function(r){
			try{
			$('.dropdown-menu').dropdown()
			if (r.projects.project == undefined){return;}

			var projects = r["projects"]["project"]

				if (projects == undefined){
					$(taskNodeName).append("<span>0</span>")
				}
				else if (projects.length == undefined){
					 $(taskNodeName).append("<span>1</span>")
					 $(taskNodeName).css("height","100px")
				}
				else{
				 $(taskNodeName).append("<span>"+projects.length+"</span>")
				 $(taskNodeName).css("height",(projects.length*65)+"px")
				}

				if (projects.length == undefined) projects = [projects]

				taskState = {}
				for (i in projects){

					var project = projects[i];
					var name = project.name.split(":")[1]
					var state = project.state

					taskState[name] = state

					if (state == "true"){ var icon = "icon-play" }
					else{ var icon = "icon-stop" }

				    var str =
					   '<li>'+
					   '<h5 class="menuProject"  onClick="editor.select(\''+name+'\')" >'+name+'<i class="'+icon+'" style="float:right;margin-right:55px;"></i></h5>'+
					   '</li>'
				   $("#id-tree-tasks").append($(str))
			       $("#drop-"+i).dropdown()


			     }

			     if (task){
			    	if (taskState[task] == "true"){

			    		var btn = self.playMenu.controls[0];
			    		btn.setContent("<div class='toolbar-icon-text'>Stop</div> <i class='icon icon-stop'></i>");
			    	 }else{
			    		var btn = self.playMenu.controls[0];
			    		btn.setContent("<div class='toolbar-icon-text'>Run</div> <i class='icon icon-play-circle'></i>");
			    	 }
			     }

			     if (callback) callback(name)

			 }catch(e){alert(e)}
		}
	},


	addfolder : function(folder){

		var self = this;
		folder = folder.substring(folder.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/asl/update/folder", {name : task, folder : folder });
		request.onSuccess = function(){

			self.select(task);
		}
	},


	addfile : function(file){

		var self = this;

		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/asl/update/script", {name : task,filename : file, filecontent : "" });
		request.onSuccess = function(mess){

			self.select(task);
		}
	},

	deletefile : function(file){

		var self = this;
		file = file.substring(file.indexOf('/')+1);

		var request = new APIS.POST(this.service+"/asl/delete/script", {name:task,filename : file});
		request.onSuccess = function(mess){

			self.select(task);
		}
		request.onError = function(err){ alert(err) }
	},


	savefile : function(file,content){

		//var self = this;
		//file = file.substring(file.indexOf('/')+1);

		new APIS.POST(this.service+"/asl/update/script", {name : task,filename : file,filecontent : encode(content)});
	},

	deleteproject : function(){

		var self = this;

		var modal = new apisense.BaseModal({
			title : "Are you sure ?",
			onValid : function(){

				var request = new APIS.POST(self.service+"/asl/delete/project",{name:task})
				request.onSuccess = function(mess){
					task = undefined;
					self.updateTask()
				}

			}
		})
		modal.renderInto(this.editor);
		modal.show();
	}
})



