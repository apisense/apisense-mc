var editor,task = undefined;
var taskNodeName = ".Server"
var taskState = {}




$(function(){

	var node = $(".Server")

	node.append("<ul id='id-tree-tasks' class='nav nav-pills nav-stacked'></ul>")

	var onItemSelected = function(item){

		var file = item.label.toString();
		new APIS.POST("../../../"+xpname+"/asl/get/script/content",{name:task,filename:file}).onSuccess =
			function(mess){
				var content = decode(mess)
				editor.addTab(item.label,{content:content})
			}

    }

	editor = new CodeEditor("editor",{},onItemSelected)
	editor.ready(function(){

		editor.addMenu("NewTask",function(){create()},"icon-tasks");

		editor.addMenu("delete Task",function(){
		  confirm("Are you sure ? ",function(){deleteProject(task)})
		},"icon-trash","id-delete-task");

		editor.addSeparator()
		editor.addSeparator()

		// add menu button
		// menu : add new file
		editor.addMenuNewFile(function(file){
		  var request = new APIS.POST("../../../"+xpname+"/asl/update/script",{name:task,filename:file,filecontent:""})
		  request.onSuccess = function(){select(task)}
		},[".js",".py",".epl",".txt"]);

		// add menu button
		// menu : remove current file
		editor.addMenuDeleteFile(function(file){
			post("../../../"+xpname+"/asl/delete/script",{name:task,filename:file.toString()},
				 function(s){select(task)},
				 function(e){alert(e)}
			);
		})

		// add menu button
		// menu: remove current file
		editor.addMenuSaveFile(function(file,content){

				post("../../../"+xpname+"/asl/update/script",{name:task,filename:file.toString(),filecontent:encode(content.toString())},
					 function(s){},
					 function(e){alert(e)}
				);
		});

		// add menu button
		// menu: remove current file
		editor.addMenu("Get log",function(){

		  var filter = $("#log-filter").val();
		  if (filter == "") filter = "*:*"

		  post("../../../"+xpname+"/asl/get/log",{filter:filter},
			 function(s){

				 editor.setConsoleValue(s)
			 },
			 function(e){alert(e)},{parse:false}
		  );

		},"icon-refresh","id-get-log");

		// CLEAR
		editor.addMenu("clear",function(){
			post("../../../"+xpname+"/asl/clear/log",{},
			  function(s){editor.setConsoleValue("");},
			  function(e){editor.setConsoleValue(e)},
			  {parse:false});
		},"icon-eye-open");

		editor.addMenuInput("Filter","log-filter","75px")

		editor.addSeparator()
		editor.addSeparator()


		editor.addMenu("Run task",function(){

			if (taskState[task] == "true"){
				stop(task)
			}else{
				start(task)
			}

		},"icon-play-circle","id-run-project");

		editor.updateTreeModel({});
		editor.disableMenu();
		$("#NewTask").show()

		// select the first project on start
		update(function(name){select(name)})

	})


	try{

		var websocketPath = "ws://"+window.location.hostname

		// dev section
		if (window.location.hostname.slice(0,1) == "1") websocketPath = "ws://localhost:8888"

		websocketPath = websocketPath + "/websocket/"+xpname+"/logs"

		var socket = new APIS.WebSocket({
		  path   : websocketPath,
	      onClose : function(event){},
		  onMessage : function(message){

	    	try{

	    	  var json = JSON.parse(message.replaceAll("\n","").replaceAll("\r",""))

	    	  if (json.type == "message"){editor.addConsoleValue(decode(json.data))}
	    	  else if (json.type == "clear"){}
	    	  else if (json.type == "stop"){update()}
	    	}catch(e){
	    		editor.addConsoleValue("Cannot parse "+message)

	    	}

		  }
	    })


	}catch(err){alert(err)}
})

var update = function(callback){

	$("#id-tree-tasks").children().remove()

	if(task) editor.activeMenu();
	else editor.disableMenu();

	$("#NewTask").show()

	post("../../../"+xpname+"/asl/get/projects",{},
			function(r){

				$('.dropdown-menu').dropdown()

				if (r.projects.project == undefined){return;}

				var projects = r["projects"]["project"]

				if (projects == undefined){
					$(taskNodeName).append("<span>0</span>")
				}
				else if (projects.length == undefined){
					 $(taskNodeName).append("<span>1</span>")
					 $(taskNodeName).css("height","100px")
				}
				else{
				 $(taskNodeName).append("<span>"+projects.length+"</span>")
				 $(taskNodeName).css("height",(projects.length*65)+"px")
				}

				if (projects.length == undefined) projects = [projects]

				taskState = {}
				for (i in projects){

					var project = projects[i];
					var name = project.name.split(":")[1]
					var state = project.state

					taskState[name] = state

					if (state == "true"){ var icon = "icon-play" }
					else{ var icon = "icon-stop" }

				    var str =
					   '<li>'+
					   '<h5  class="menuProject"  onClick="select(\''+name+'\')" >'+name+'<i class="'+icon+'" style="float:right;margin-right:55px;"></i></h5>'+
					   '</li>'
				   $("#id-tree-tasks").append($(str))
			       $("#drop-"+i).dropdown()


			     }

			     if (task){
			    	if (taskState[task] == "true"){
			    		 $("#id-run-project").find("i").addClass("icon-stop")
				 		 $("#id-run-project").find("i").removeClass("icon-play-circle")
			    	 }else{
			    		$("#id-run-project").find("i").removeClass("icon-stop")
			 			$("#id-run-project").find("i").addClass("icon-play-circle")
			    	 }
			     }

			     if (callback) callback(name)
		},
		function(err){new ModalError({message : err,onClose:function(){}})}
	)
}



/**
 * Create new scripting project
 */
var create = function(){

	new Modal({
		title   : "Create new server task",
		message : "<div class='form-inline'> "+
				  " <label> <h5>Task Name : </h5></label> "+
				  " <input  id='input-task-name' type='text' placeholder='filename' autofocus/>"+
				  "</div>",
		onOk    : function(){
		 	_create($("#input-task-name").val())
	    }
	})
}

var _create = function(taskname){

	if (taskname == ""){
		new ModalError({message:"Task name cannot be null",onClose:function(){}})
		return;
	}

	post("../../../"+xpname+"/asl/update/create",{name: taskname },
			function(mess){update()},
			function(err){new ModalError({message : err,onClose:function(){}})}
	)
}

var select = function(project){
	task = project
	var request = new APIS.POST("../../../"+xpname+"/asl/get/script/files",{name:project})
	request.onSuccess = function(mess){
		editor.updateTreeModel(mess);
		update();
	}
}

var start = function(project){
  new APIS.POST("../../../"+xpname+"/asl/start",{name:project}).onSuccess = function(){update()}
}

var stop = function(project){
  new APIS.POST("../../../"+xpname+"/asl/stop",{name:project}).onSuccess = function(){update()}
}

var deleteProject = function(project){
	var request = new APIS.POST("../../../"+xpname+"/asl/delete/project",{name:project})
	request.onSuccess = function(mess){

		if (task == project){
			editor.deleteAllTabs();
			editor.updateTreeModel("{}")
			task = undefined;
		}
		update()
	}
}
