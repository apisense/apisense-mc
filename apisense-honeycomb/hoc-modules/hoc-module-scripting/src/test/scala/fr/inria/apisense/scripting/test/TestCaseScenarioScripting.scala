package fr.bsense.module.test.scn1

import fr.bsense.module.test.ModuleTCase
import org.junit.Test
import fr.inria.apisense.client.APISENSEClient
import org.junit.Ignore

class TestCaseScenarioScripting extends ModuleTCase {

  override def startHive = true;
  override def getExperimentNameTest = "TestModuleServeurTaskExperiment"
  override def getSPL = """ {
     "Experiment" : 1,
     "Experiment Website":1,
     "Module Manager" : 1,
     "Android" : 1,
     "ASL Web": 1,
     "ASL Service" : 1
   }"""

  var script = """
    log.d("start xp")


  """

  var deviceId = "APA91bFMraD0WwmsJcVrx-NifQpHMsHmtUyFaVG0iLpW4EazuUdKIFEUqe1lEC3DrI1V7FjlIFEhBtUGRPNxfpH5LX_q2RBMfpEGHxiwjrfZv0sgPX6uE1szh_isV_Jsrr9XZivHZ76x2PwG1PPuN3x80-cZYPq6mg"

  @Test def test(){

    val client = APISENSEClient.honey
    client host HOST
    client connect TestCaseScenarioScripting.this.scientist_token

    val xpservice = client.experiment(getExperimentNameTest).scripting

    notfail("Create scripting project"){ xpservice.createScriptingProject("MyProject") }

    xpservice.createScriptingFile("MyProject", "main.js", script)

    val hive = APISENSEClient.hive
    hive.host(ModuleTCase.CENTRAL_HOST)
    hive connect TestCaseScenarioScripting.this.bee_token

    canfail("subscribe user"){hive.store.subscribe("CrowdScriptingExperiment", "{}")}
    canfail("register device"){hive.crowd.subscribeMobile(deviceId)}

    notfail("Get Project"){ println(xpservice.getProjects.toString()) }

    xpservice.startProject("MyProject")

    Thread.sleep(5000);

    xpservice.publish("MyProject","""{ "name" : "myevent" }""")

    Thread.sleep(2000);

  }

}