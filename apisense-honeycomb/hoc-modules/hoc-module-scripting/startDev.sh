#!/bin/bash

#URI="193.51.236.158"
URI="192.168.0.10"

if [ $# -gt 0 ] 
then 
	URI=$1
fi
echo mvn -o -P start -Dfr.inria.apisense.honey.uri=${URI}:18000 -Dfr.inria.apisense.hive.uri=${URI}:18001
mvn -o -P start -Dfr.inria.apisense.honey.uri=${URI}:18000 -Dfr.inria.apisense.hive.uri=${URI}:18001
