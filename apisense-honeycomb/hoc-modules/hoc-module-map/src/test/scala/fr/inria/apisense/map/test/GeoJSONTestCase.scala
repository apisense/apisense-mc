package fr.inria.apisense.map.test

import fr.bsense.module.test.ModuleTCase
import fr.bsense.module.test.ModuleTCase
import org.junit.Test
import fr.inria.apisense.client.APISENSEClient
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import org.apache.cxf.helpers.IOUtils
import net.liftweb.json.JsonAST.JString
import org.junit.Assert
import fr.inria.apisense.node.NodeJSONHelper._
import net.liftweb.json.JsonAST.JValue


class GeoJSONTestCase  extends ModuleTCase {

  override def startHive = true;
  override def getExperimentNameTest = "TestModuleMapExperiment"
  override def getSPL = """ {
     "Experiment" : 1,
     "Experiment Website":1,
     "Module Manager" : 1,
     "CKUID" : 1,
     "GeoJSONMap" : 1,
     "Open Map" : 1
  }"""

  @Test def test(){

    val honeyService = APISENSEClient.honey
    honeyService.host(ModuleTCase.HOST)
    val collector = honeyService.experiment(getExperimentNameTest).collector
    val geo = honeyService.experiment(getExperimentNameTest).geo

    val trace1 = ClassLoader.getSystemResourceAsStream("trace-1.zip")
    val trace2 = ClassLoader.getSystemResourceAsStream("trace-2.zip")

    notfail("updload dataset trace1"){
      collector.uploadData("0001", "1.0", "dlskjdhlqsdhqljdqh",
    	IOUtils.readBytesFromStream(trace1)
      )
    }

    notfail("updload dataset trace2"){
      collector.uploadData("0002", "1.0", "dlskjdhlqsdhqljdqh",
    	IOUtils.readBytesFromStream(trace2)
      )
    }

    honeyService.connect(this.scientist_token)

    notfail("create new map"){ geo.createMap("MyMap") }
    notfail("create new map"){ geo.createMap("MyMap2") }
    notfail("get maps "){
      val JString(maps) = geo.getMaps
      Assert.assertTrue(maps.split(";").length >= 2)
      Assert.assertTrue(maps.contains("MyMap"))
      Assert.assertTrue(maps.contains("MyMap2"))
    }

    notfail("get databases") {geo.getDatabases; }

    notfail(" create query map "){
    geo.updateQueryFilter("MyMap",
        """
        { "filters" : {
    			"header"     : " //operator = 'operator1'  ",
    			"data"       : "  signal > 3 ",
    			"attributes" : ["@operator","signal"],
    			"databases"	 : []
        }
        }"""
    )}

    notfail(" process query map "){
      geo.processQueryFilter("MyMap")
    }

    val map = geo.getProcessedMap("MyMap")
    println(map)
  }
}