var signalMap = apisense.area([50.614291,3.13282],[50.604159,3.15239],);

// sensing task to distribute among participants
signalMap.sense({
    trace.setHeader('gsm_operator', gsm.operator());

    location.onLocationChanged({ period: '5min', distance: '10m' }, function(loc) {
    		trace.add({
			 	loc: [loc.latitude, loc.longitude],
			 	latency : network.ping(50,"http://...").average,
			    networkType : network.type()});
           });
}).during('30 min');

signalMap.compute(function(taskId,bound,values) {
  	values = values.orderBy("latency");
  	var median = values[Math.round(values.length / 2)];
  	websocket.broadcast({ bound: bound, latency : median});
});
//select participants from static properties
signalMap.recruit(["operator = 'Operator1'", "operator = 'Operator2'"]);
// select participants from dynamic properties
signalMap.dynamicProperties(function(){
   return (network.hasNetwork() && network.connectionType() != "WIFI");
})

signalMap.duplicate(2).coverage(distance('100 m')).coverage(time('1 H'));



