var mainMapMenu,subMapMenu,leaf

var print = function(o){alert(JSON.stringify(o))}

$(function(){
	
	// get jquery node of left menu
	mainMapMenu = $(".Visualisation")
	mainMapMenu.append("<ul id='id-map-node' class='nav nav-pills nav-stacked '></ul>")
	subMapMenu  = $("#id-map-node")

	leaf = new APISMap()
	leaf.update()
	
	
})

function APISMap(){
	
	var instance = this;
	
	this.currentMap = undefined;
	
	var leafletMap = new S.LeafletMap("#id-sub-menu-content","leaflet");

	/* delete current map */
	this.deleteMap = function(){
		new Modal({
			title:"Confirm",
			message:" All map data will be removed. Are you sure ? ",
			onOk : function(){
				post("../../../"+xpName+"/openmap/delete/map",{name : instance.currentMap},
					function(value){  
						instance.currentMap = undefined
						_hideLeftMenu();
						instance.update();
					},
					function(value){}
				)
			}})
	}

		
	/* show open dialog to create a new map */
	this.createMap = function(){
		new Modal({
			title:"Create Map",
			message:"<div> Name :  <input id='mapName' type='text'></input> </div>",
			onOk : function(){
				var mapName = $("#mapName").val()
				// TODO check name format
				instance._createMap(mapName)
			}
	})}
	
	/* create a new map */
	this._createMap = function(mapName){
		var defaultConfig = {filters : {
		   header : "",
		   data  : "",
		   attributes : [],
		   databases : [],
		   render : { clusters : [] }
		}}
		
		instance.currentMap = mapName
		_serviceUpdateConfiguration(defaultConfig,function(){
			instance.update()
		})
	}

	
	/* call service : build geojson map */
	this.buildMap = function(){
		post("../../../"+xpName+"/openmap/process/filter",
		    {name : instance.currentMap},
			function(value){
				
				instance.showMap()
			
			},
			function(err){alert(err)}
		)
	}
	
	this.exportMap = function(){ alert("Not implemented") }
	
	this.update = function(){
		
		if (this.currentMap == undefined){ $("#id-map-config").hide(); }
		
		subMapMenu.children().remove()
		
		post("../../../"+xpName+"/openmap/get/maps",{},function(value){
			
			var maps = value.split(";")
			
			if (maps[0] == ""){
			   mainMapMenu.append("<span>0</span>")
			   mainMapMenu.css("height","50px")
			}
			else{
			  mainMapMenu.append("<span>"+maps.length+"</span>")
			  mainMapMenu.css("height",(maps.length*30+70)+"px")
			}
			
			for (i in maps){ 
				
				var node = "<li id='map-"+maps[i]+"' style='cursor:pointer'>"
				node = node + "<a onClick='leaf.open(\""+maps[i]+"\")' >"+maps[i]+"</a>"
				node = node + "</li>"
				subMapMenu.append(node)
				 
			}
			
		},function(err){},{method:"GET"})
	}
	
	/* call service : get GeoJSON map */
	var _serviceGetGeoJSON = function(handler){
		
		post("../../../"+xpName+"/openmap/get/processed/map/"+instance.currentMap,{},
		 function(value){handler(value)},
		 function(err){alert(err)},
		 {parse:false,method:"GET"}
	   )
	}
	
	/* call service : get map configuration */
	var _serviceGetConfiguration = function(handler){
		post("../../../"+xpName+"/openmap/get/map",
		 {name : instance.currentMap},
		 function(value){handler(value)},
		 function(){},
		 {method:"GET"}
	   )
	}
	
	/* call service : update map configuration */
	var _serviceUpdateConfiguration = function(config,handler){
	
	   post("../../../"+xpName+"/openmap/update/filter",
		 {name : instance.currentMap,filters : JSON.stringify(config)},
		 function(value){
			 if (handler != undefined) handler();
		 },
		 function(err){alert(err)}
	   )
	}

	/* open left menu */
	var _showLeftMenu = function(width){
		var w = width || "250px"
		$("#id-sub-menu").show();
		$("#id-sub-menu").css("width",w)
		$("#id-sub-menu-content").children().remove();
	}
	
	/* hide left menu */
	var _hideLeftMenu = function(){$("#id-sub-menu").hide();}
	

	/* open filter configuration data */
	this.configFilterData = function(){
		_showLeftMenu("400px")
		
		var node = "<h3>Header Filter</h3>"
		node = node + "<textarea id='id-header-filter' style='width:80%' ></textarea>"
		node = node + "<h3>Data Filter</h3>"
		node = node + "<textarea id='id-data-filter' style='width:80%' ></textarea>"
		node = node + "<div><button id='id-filter-save'  class='btn btn-danger' >save</button></div>"
		
		$("#id-sub-menu-content").append(node)
		
		var _config = undefined
		
		// fill textarea with configuration
		_serviceGetConfiguration(function(config){
			_config=config
			$("#id-header-filter").val(config["filters"]["header"])
			$("#id-data-filter").val(config["filters"]["data"])
		})
		
		$("#id-filter-save").click(function(){
			_config["filters"]["header"] = $("#id-header-filter").val();
			_config["filters"]["data"] = $("#id-data-filter").val();
			_serviceUpdateConfiguration(_config)
			
		})
	}
	
	/* open attributes configuration */
	this.configAttributes = function(){
		_showLeftMenu()
		
		var _config = undefined
		
		var node = "<h5>Attributes</h5>"
		node = node + "<i>select attributes to include in map</i>"
		node = node + "<button id='id-add-attribute'  style='margin:10px' class='btn btn-info'> add attribute </button>"
		node = node + "<button id='id-save-attribute' style='margin:10px' class='btn btn-danger'> save </button>"
		
		$("#id-sub-menu-content").append(node)
		$("#id-add-attribute").click(function(){
				$("#id-sub-menu-content").append(
					"<input class='filter-att' style='margin-top:10px;' type='text'> </input>")
		})
		$("#id-save-attribute").click(function(){

			var atts = []
			$(".filter-att").each(function(index,element){
				
				if (element.value != "")
					atts[atts.length] = element.value
			})
			
			_config["filters"]["attributes"] = atts
			_serviceUpdateConfiguration(_config)
			
		})
		
		_serviceGetConfiguration(function(config){
			_config = config
			
			var atts = _config["filters"]["attributes"]
			for (i in atts ){
				$("#id-sub-menu-content").append(
					"<input class='filter-att' style='margin-top:10px;' type='text' value='"+atts[i]+"'> </input>")
			}
		})
	}
	
	/* open render configuration */
	this.configRender = function(){
		_showLeftMenu("500px")
	
		// get map configuration
		_serviceGetConfiguration(function(config){
			
			leafletMap.showConfig(function(mapconfig){
				config["filters"]["render"] = mapconfig
				_serviceUpdateConfiguration(config,function(){ 
					instance.configRender() 
				})
			});

			// create new rendered with configuration
			//leaftRender.update(config["filters"]["render"])
			// leaftRender.showConfig(function(rconfig){
				// update configuration
			//	
				// save new configuration
			//	_serviceUpdateConfiguration(config,function(){ 
					//instance.configRender() 
			//	})
			//})
		})
	}
	
	
	this.configDatabase = function(){
		_showLeftMenu()
		
		var node = "<h5> select database </h5>"
		node = node + "<SELECT id='idselect' name='nom' size='1'>"
		node = node + "<OPTION> ALL"
		
		post("../../../"+xpName+"/openmap/get/dbs",{},
			function(value){
				
				var dbs = value["array"]["dbtrack"]	
				for (i in dbs){
					node += "<option>"+dbs[i]
				}
				node = node + "</select>"
				
				$("#id-sub-menu-content").append(node)
		
				_serviceGetConfiguration(function(config){
					
					// get database configure and setup list
					// if there no configuration, set ALL Option
					// by default
					var select = config["filters"]["databases"]
					if(select.length == 0){ $("#idselect").val("ALL")}
					else{ $("#idselect").val(select[0]) }
					
					// update configuration on
					// list change
					$("#idselect").click(function(node){
						var val = $(this).val();
						if (val == "ALL") val = []
						else val = [ val ]
						             
						config["filters"]["databases"] = val
						_serviceUpdateConfiguration(config)
					})
					
				})
				
			},
			function(){},
			{method:"GET"}
		)

		
	}

	this.showMap = function(){
		// get map configuration
	   _serviceGetConfiguration(function(config){
		 _serviceGetGeoJSON(function(geojson){
			 
			 leafletMap.setConfig(config["filters"]["render"])
			 leafletMap.setData(geojson)
			 leafletMap.showMap();
			 leafletMap.fitBounds();
		})
	   })
	}
	
	this.open = function(mapName){
		$("#id-config-detail").hide();
		subMapMenu.find(".active").removeClass("active")
		subMapMenu.find("#map-"+mapName).addClass("active")
		$("#id-map-config").show();
		
		_hideLeftMenu()
		instance.currentMap = mapName
		instance.showMap();
	}
}





	
function APISMapRendered(idmap,id){
	
	var instance = this;
	
	var map,zoom,centerX,centerY,geoJSON,geoJSONLayer,clusterLayer, mapControl = undefined;
	
	// 
	var renders = {}
	
	this.setGeoJSON = function(geo){ geoJSON = eval(geo);}
	
	this.update = function(_config){
	  config = _config || {}
	  zoom = config["zoom"] || 18;
	  centerX = config["centerX"] || 0;
	  centerY = config["centerY"] || 0;
	  
      var colorType = config["color"]["name"]
      renders["color"] = new APISMapRenderedType.color[colorType](config["color"])
      
	  var sizeType = config["size"]["name"];
	  renders["size"] = new APISMapRenderedType.size[sizeType](config["size"])
      
	  var shapeType = config["shape"]["name"]
	  renders["shape"] = new APISMapRenderedType.shape[shapeType](config["shape"])
	 }

	 

	this.saveRenderinConfiguration = function(handler){
		var c = {
		      zoom     : $("#idzoom").val(),
		      centerX  : $("#idcx").val(),
		      centerY  : $("#idcy").val(),
		      color    : renders["color"].getConfig(),
		      shape    : renders["shape"].getConfig(),
		      size     : renders["size"].getConfig()
		}
		handler(c)
		instance.update(c);
	}
	
	

	this.showMap = function(onAddPoint,onReady){
		
		//  Leaftlet Map initialization
		if (map == undefined){
		  
		   map = L.map(idmap)
		   var googleLayer    = new L.Google('ROADMAP');
		   var cloudLayer = L.tileLayer('http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png',{
			  maxZoom: 18,
			  attribution: 
			  'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
	       })
		   
	       mapControl = L.control.layers({
	    	   "Google Map"      : googleLayer,
	    	   "Open Street Map" : cloudLayer
	       })
	       map.addLayer(cloudLayer)
	       map.addControl(mapControl);
	       
	       var fullScreen = new L.Control.FullScreen(); 
	       map.addControl(fullScreen);
	       
	       
	       clusterLayer = new L.MarkerClusterGroup({
	    		   spiderfyOnMaxZoom: true,
	    		   showCoverageOnHover: true,
	    		   zoomToBoundsOnClick: true,
	    		   maxClusterRadius : 10,
	    		   singleMarkerMode : false,
	    		   
	       
	       	       iconCreateFunction: function (cluster) {
	    	   			
	    	   		//cluster.on("mouseover",function(event){alert(event)})
	    	   		//cluster.bindPopup(map.getCenter() +"<br>" + 
                    //        "Min Zoom" + map.getMinZoom() +"<br>" + 
                    //        "Max Zoom" + map.getMaxZoom());
	    	   		var childCount = cluster.getChildCount();
	    	   		var c = ' marker-cluster-';
	    	   		if (childCount < 10) {
	    	   			c += 'small';
	    	   		} else if (childCount < 100) {
	    	   			c += 'medium';
	    	   		} else {
	    	   			c += 'large';
	    	   		}

	    	   		return new L.DivIcon({
	    	   			html: '<div style="background : red;height:100%" ><span>' + childCount + '</span></div>',
	    	   			className: 'marker-cluster ' ,
	    	   			iconSize:  new L.Point(30, 30)});
}
	       });
	       
	       clusterLayer.on('clusterClick',function(a){alert(a)})

	       
	   }
		
	   //map.setView([centerX, centerY], zoom);
		
	   //if (geoJSONLayer != undefined){ 
	   //	   map.removeLayer(clusterLayer) 
	   //}
		  
	   var index = 0;
	   var indexMax = geoJSON.length
		
		
	   geoJSONLayer = L.geoJson(geoJSON,{
			onEachFeature : function (feature, layer) {
				
				var popup = "<div>"
				for (i in feature.properties){
					popup = popup + "<strong>"+i+"</strong> : "
					popup = popup + feature.properties[i]+"<br/>"
				}
				popup = popup + "</div>"
				
				layer.bindPopup(popup)
		    },
		    pointToLayer: function (feature, latlng) {
		    	var option = {
		  			radius: 8,
				    fillColor: renders["color"].render(feature),
				    color:     renders["color"].render(feature),
				    weight: 1,
				    opacity: 1,
				    fillOpacity: 0.8
			    }
		    	
		    	var point =  L.circleMarker(latlng,option);
		    	
		    	index ++;
		    	if (index == indexMax){if (onReady){onReady(index,centerX,centerY,zoom)}}
		    	if (onAddPoint){onAddPoint(latlng,option)}
		    	
		    	return point;
			}
		})
		
		if ((geoJSON != undefined) && (geoJSON != "")){
			
			for (i in geoJSON){ geoJSONLayer.addData(geoJSON[i]); }
	    }
		
	   
        clusterLayer.addLayer(geoJSONLayer);
		map.addLayer(clusterLayer);
		map.fitBounds(clusterLayer.getBounds());
		
		
	    
		//map.removeControl(mapControl)
		//map.addControl(mapControl)
		
		
		
		
	}

	this.showConfig = function(saveHandler){
		var node = ""
		
		
		// insert buttons
		node = node + "<div style='padding:1em'>"
		node = node + "<h5> Configuration </h5>"
		node = node + "<button  id='idsr' class='btn btn-danger'>Save</button> "
		node = node + "<button  id='idsm' class='btn btn-info'><i class='icon-eye-open icon-white'></i></button>"
		node = node + "</div>"
		
		node = node + "<div class='tabbable'>"
		
		node = node + "<ul class='nav nav-tabs' id='mytab'>"
		node = node + "<li> <a href='#tab-general' data-toggle='tab' class='active'> General </a> </li>"
		node = node + "<li> <a href='#tab-color'   data-toggle='tab'> Color </a> </li>"
		node = node + "<li> <a href='#tab-shape'   data-toggle='tab'> Shape </a> </li>"
		node = node + "<li> <a href='#tab-size'    data-toggle='tab'> Size </a> </li>"
		node = node + "</ul>"
		
		node = node + "<div class='tab-content' id='option-content' >"
		
		
		// general configuration tab
		node = node + "<div class='tab-pane active' id='tab-general'>"
		// zoom configuration
		node = node + "<h3>select default zoom map </h3> "
		node = node + "<input id='idzoom' style='width:100px' type='number' value='"+zoom+"'> </input>"
		node = node + "<br/><br/>"
		// centering map configuration
		node = node + "<h3>select default map center</i> </h3>"
		node = node + "<br/>"
		node = node + "<i> Longitude  </i> <input id='idcx' style='width:100px' type='text' value='"+centerX+"'> </input><br/>"
		node = node + "<i> Latitude   </i> <input  id='idcy' style='width:100px' type='text' value='"+centerY+"'> </input>"
		
		node = node + "</div>"
		node = node + "</div>"
		node = node + "</div>"
		
		
		$(id).append(node)
		$("#idsm").click(function(){instance.showMap()})
		$("#idsr").click(function(){instance.saveRenderinConfiguration(saveHandler)})
		
		
		try{
			addRenderOption($("#option-content"),"idcolor","tab-color","color");
			//addRenderOption($("#option-content"),"idsize","tab-size","size");
			//addRenderOption($("#option-content"),"idshape","tab-shape","shape");
		}catch(e){alert("Error"+e)}
		
	}
	
	var addRenderOption = function(node,id,tableId,renderType){
		
		str = "<div class='tab-pane' id='"+tableId+"'>"
		
		str = str + "<select id='"+id+"' style='width:100px;margin:7px;'>"
		for (type in APISMapRenderedType[renderType]){
			str = str + "<option>"+type
		}
		str = str + "</select>"

		str = str + "<div id='c"+id+"' >"
		str = str + "</div>"
		str = str + "</div>"
		
		node.append(str)
		
		renders[renderType].updateDom($("#c"+id))
		
		$("#"+id).val(renders[renderType].name)
		$("#"+id).click(function(){
		    
			var rconfig = { name : $(this).val() }
			if ($(this).val() == config[renderType].name){rconfig = config[renderType] }
			renders[renderType] = new APISMapRenderedType[renderType][$(this).val()](rconfig)
			$("#c"+id).children().remove()
			renders[renderType].updateDom($("#c"+id))
				
		})
		
		
	}
}

var APISMapRenderedType = { color : {}, size  : {}, shape : {} }

