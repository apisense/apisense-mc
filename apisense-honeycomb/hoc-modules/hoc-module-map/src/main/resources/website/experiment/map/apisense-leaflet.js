var S = {}
S.Class = function () {};
S.Class.extend = function (props) {
	// extended class with the new prototype
	var NewClass = function () {
		// call the constructor
		if (this.initialize) {this.initialize.apply(this, arguments);}
		// call all constructor hooks
		if (this._initHooks) {this.callInitHooks();}
	};

	// instantiate class without calling constructor
	var F = function () {};F.prototype = this.prototype;
	var proto = new F();proto.constructor = NewClass;

	NewClass.prototype = proto;

	//inherit parent's statics
	for (var i in this) {
		if (this.hasOwnProperty(i) && i !== 'prototype') {
			NewClass[i] = this[i];
		}
	}
	// mix static properties into the class
	if (props.statics) {
		L.extend(NewClass, props.statics);
		delete props.statics;
	}
	// mix includes into the prototype
	if (props.includes) {
		L.Util.extend.apply(null, [proto].concat(props.includes));
		delete props.includes;
	}
	// merge options
	if (props.options && proto.options) {
		props.options = L.extend({}, proto.options, props.options);
	}

	// mix given properties into the prototype
	L.extend(proto, props);
	proto._initHooks = [];

	var parent = this;
	NewClass.__super__ = parent.prototype;
	// add method for calling all hooks
	proto.callInitHooks = function () {

		if (this._initHooksCalled) { return; }
		if (parent.prototype.callInitHooks) {
			parent.prototype.callInitHooks.call(this);
		}
		this._initHooksCalled = true;
		for (var i = 0, len = proto._initHooks.length; i < len; i++) {
			proto._initHooks[i].call(this);
		}
	};
	return NewClass;
};





S.LeafletMap = S.Class.extend({
        
     // define all global variable variable
     initialize : function( configId, mapId  ){
		this.nodeConfig = $(configId)
		this.mapId = mapId;
		this.isInitialized = false;
		
		this.setConfig({})
		this.activedTab = undefined
		this.layers = [];
	 },
      
	 setData : function(data){
		 this.geoJSON = eval(data);
	 },
     setConfig : function(jsonConfig){
		 this.config = jsonConfig; 
    	 this.config.clusters = jsonConfig.clusters || {}
    	                                                
    	 this.clustersMarker = {};
    	 for (i in this.config.clusters){
     		var cluster = this.config.clusters[i];
     		var c = new S.Cluster(this,cluster)
     		this.clustersMarker[cluster.clname] = c
    	 }
     },

     showConfig : function(onConfigSaved){
    	this._clearConfig();
    	
    	var node = "".concat(
    		"<div style='padding:1em'>",
    	    "<h5> Cluster configuration </h5>",
    	    "<button  style='margin-right:10px' id='albsa' class='btn btn-danger'>Save</button>",
    	    "<button  id='albsh' class='btn btn-info'><i class='icon-eye-open icon-white'></i></button>",
    	    "<br>",
    	    "<button  style='margin-top:10px' id='albadd' class='btn btn-info'><i class='icon-plus icon-white'></i>Add cluster</button>",
    	    "</div>"
    	)
    	this.nodeConfig.append(node);
    	
    	// write header tabs
    	node = "".concat("<ul class='nav nav-tabs' id='mytab'>")
    	
    	for (i in this.config.clusters){
    		
    		var cluster = this.config.clusters[i];
    		var cl = ""
    		
    		
    		if (this.activedTab == undefined){
    			
    			this.activedTab = cluster.clname
    			cl = "active"
    		}
    		else{
    			
    			if (this.activedTab == cluster.clname){
    				cl = "active"
    			}
    		}
    		
    		node = node.concat(
    			"<li> <a href='#"+cluster.clname.replace(" ",""),
    					"'data-toggle='tab' class='"+cl+"'> ",
    					cluster.clname,
    			" </a> </li>")
    	}
    	node = node.concat("</ul>")
    	this.nodeConfig.append(node);

    	var tabcontent = $("<div class='tab-content' id='option-content' ></div>")
    	this.nodeConfig.append(tabcontent);
    	
    	//write content tabs
    	for (i in this.clustersMarker){
    		
    		var clusterMarker = this.clustersMarker[i]
    		     
    		var cl = ""
    		if (i == this.activedTab){cl="active";}
    		
    			
    		var tabClusterContent =  $("<div class='tab-pane "+cl+"' id='"+clusterMarker.getId()+"'></div>")
    		tabcontent.append(tabClusterContent);
    		clusterMarker.showConfig(tabClusterContent)
    	}
    	
    	var instance = this;
    	
    	// button refresh listerner
    	$("#albsh").click(function(){
    		for (jcluster in instance.clustersMarker){ 
    			instance.clustersMarker[jcluster].clear(instance.map);
    		}
    		instance.showMap();
    	})
    	
    	// button add cluster listenner
    	$("#albadd").click(function(){
    		new Modal({
    			title   : "Cluster",
    			message : "Enter new cluster name <input id='alic'></input>",
    			onOk : function(){
    				var clusterName = $("#alic").val();
    				if (clusterName != ""){
    				 instance.config.clusters[clusterName] = {
    				      clname     : clusterName,
    				      clfilters  : [],
    				      clcolor    : undefined,
    				      clsize     : undefined,
    				      clcalcul   : "childCount"
    			     }
    				 instance.setConfig(instance.config)
    				 instance.showConfig(onConfigSaved)
    				}
    				
    			},
    			onCancel : function(){}
    		})
    	})
    	
    	// button save listenner
    	$("#albsa").click(function(){
    		
    		instance.config.clusters = {}
    		
    		for (i in instance.clustersMarker){
    			instance.config.clusters[i] = instance.clustersMarker[i].getConfig();
    		}
    		
    		onConfigSaved(instance.config)
    	})

     },
     
     // update map content from geoJSON data
     showMap : function(){
    	 
    	 if (!this.isInitialized){ this._initializeMap() }
    	 else this._clearMap()
    	 
    	 try{
    	   
    		 for (idata in this.geoJSON){
    			for (jcluster in this.clustersMarker){ 
    				this.clustersMarker[jcluster].addPoint(this.geoJSON[idata])
    			}
    		 }
    		 
         }catch(e){alert(e)}
    	 
    	 for (jcluster in this.clustersMarker){ 
    		
    	    var layer = this.clustersMarker[jcluster].show()
    	    this.layers[this.layers.length] = layer
    	    this.map.addLayer(layer)
    	 }
     },
     
     fitBounds : function(){
    	 this.map.fitBounds(this.layers[0].getBounds());
     },
     
     removeCluster : function(clusterName){
    	  
    	 this._clearMap();
    	 delete this.config.clusters[clusterName]
    	 this.setConfig(this.config)
    	 $("#albsa").click()
    	 this.showMap();
     },
     
     // remove all layers in map
     _clearMap : function(){
    	 
    	 for (i in this.clustersMarker){  this.clustersMarker[i].clear(this.map)}
    	 for (i in this.layers){
    		 this.map.removeLayer(this.layers[i])
    	 }
    	 this.layers = [];
     },
     
     _clearConfig : function(){ this.nodeConfig.children().remove(); },

     // create leaflet map
     _initializeMap : function(){
    	
    	this.map = L.map(this.mapId);
		
    	
    	var googleLayer = new L.Google('ROADMAP');
		var cloudLayer  = L.tileLayer('http://{s}.tile.cloudmade.com/BC9A493B41014CAABB98F0471D759707/997/256/{z}/{x}/{y}.png',{
			  maxZoom: 18,
			  attribution: 
			  'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
	    })
	    
	    var mapControl = L.control.layers({
	    	 "Google Map"      : googleLayer,
	    	 "Open Street Map" : cloudLayer
	    })
	    
	    this.map.addLayer(cloudLayer)
	    this.map.addControl(mapControl);
	    
	    
		var fullScreen = new L.Control.FullScreen(); 
	    this.map.addControl(fullScreen);

	    this.isInitialized = true;
     }
    


})