

S.Cluster = S.Class.extend({
	
	// define all global variable variable
    initialize : function( leafletMap, jsonConfig ){
		
		this.leafletMap = leafletMap;
		
		this.config = jsonConfig;
		this.id = jsonConfig.clname.replace(" ","")
		
		this.config.clfilters = jsonConfig.clfilters || [];
		
		this.config.maxClusterRadius = jsonConfig.maxClusterRadius || 50;

		this.config.clColorName  = jsonConfig.clColorName   || ""
		this.config.clColorStart = jsonConfig.clColorStart  || "000"
		this.config.clColorEnd   = jsonConfig.clColorEnd    || "fff"
		this.config.clColorStartValue = jsonConfig.clColorStartValue || 1
		this.config.clColorEndValue   = jsonConfig.clColorEndValue || 100
		
		this.colors = this._generateGradiantArray(this.config.clColorStart,this.config.clColorEnd,100);             
		
		
		
		this.config.clSizeName       = jsonConfig.clSizeName        || ""
		this.config.clSizeStart      = jsonConfig.clSizeStart       || "10"
		this.config.clSizeEnd        = jsonConfig.clSizeEnd         || "50"
		this.config.clSizeStartValue = jsonConfig.clSizeStartValue  || 1
		this.config.clSizeEndValue = jsonConfig.clSizeEndValue  || 100
		
		
		var instance = this
		this.clusterIndex = 0
		
		this.clusterLayer = new L.MarkerClusterGroup({
 		   spiderfyOnMaxZoom: true,
		   showCoverageOnHover: true,
		   zoomToBoundsOnClick: true,
		   maxClusterRadius : instance.config.maxClusterRadius,
		   singleMarkerMode : false,
		   
		
           // create icons
		   iconCreateFunction: function(cluster) {
			 
			  var _html = ""
			  var _color = "rgba(241, 128, 23, 0.9)" 
			  var _className = ''
			  var _iconSize  =   new L.Point(20, 20) 
			  var index = instance.clusterIndex
			  
			  var popup =  "<div class='scluster-pop'>"
			  popup = popup + "<strong>Childs number : </strong>"+cluster.getChildCount()+"<br/><br/>"
			  
			  // if cluster has a color configuration
			  if ( instance.config.clColorName != "" ){

				  if (instance.config.clColorName == "child"){
			     		 var valueColor = cluster.getChildCount()
				  }
				  else{
					  
					  // calcul average of a proproties 
					  // from all points in the cluster
					  var average = instance.calculateAverage(instance.config.clColorName,cluster)
					  var valueColor = average.av;
					  popup = popup + 
				  	   " <h4> Color property </h4> " +
				  	   "<strong>Property : </strong>"+instance.config.clColorName+"<br/>"+
				       "<strong>Max :</strong> "+average.max+"<br/>"+
					   "<strong>Min :</strong> "+average.min+"<br/>"+
					   "<strong>Average :</strong> "+average.av+"<br/><br/>"
				  }
				
				  var level = Math.round( valueColor * 100 / (instance.config.clColorEndValue - instance.config.clColorStartValue) )
				  _color = instance._getColor(level)
				   
				  
				 
			  }
			  
			  _html = "<div id='idcluster-"+instance.clusterIndex+"' class='scluster' style='background:"+_color+"'>"
			  

			  if (instance.config.clSizeName != ""){

				  if (instance.config.clSizeName == "child"){
			     		 var value = cluster.getChildCount()
				  }
				  else{
					  
					  var average = instance.calculateAverage(instance.config.clSizeName,cluster);
					  var value = average.av;
					  popup = popup + 
				  	   " <h4> Size property </h4> " +
				  	   "<strong>Property : </strong>"+instance.config.clSizeName+"<br/>"+
				       "<strong>Max :</strong> "+average.max+"<br/>"+
					   "<strong>Min :</strong> "+average.min+"<br/>"+
					   "<strong>Average :</strong> "+average.av+"<br/>"
					    
				  }
				  
				  var level = Math.round(value *100 / (instance.config.clSizeEndValue - instance.config.clSizeStartValue) )
				  
				  if (level >= 100) var size = instance.config.clSizeEnd
				  else {
					  
					 var size = ((Number(instance.config.clSizeEnd) - Number(instance.config.clSizeStart)) * level) / 100
				     size = Math.round(Number(size)) + Number(instance.config.clSizeStart)
					
					  
				  } 
				 
				  _iconSize = new L.Point(size,size) 
				
			  }
				  
			  popup = popup + "</div>"
			  
			  cluster.on("mouseover",function(event){ $("#leaflet").append(popup); })
			  cluster.on("mouseout",function(event){ $("#leaflet").append(popup).find(".scluster-pop").remove() })

			  instance.clusterIndex = instance.clusterIndex + 1;

			  
		      _html = _html + "</div>"
		     
			  var marker = { 
				  html: _html,
				  className: _className,
				  iconSize : _iconSize
			  }

			  return new L.DivIcon(marker);
		   }
		})
		
		this.geoJSONLayer = L.geoJson([],{
			onEachFeature : function (feature, layer) {
			   var popup = "<div>"
               for (i in feature.properties){
	            popup = popup + "<strong>"+i+"</strong> : "
	            popup = popup + feature.properties[i]+"<br/>"
               }
               popup = popup + "</div>"
               layer.bindPopup(popup)
            }
	    })
	    
	    

	},
	
	calculateAverage : function(property, cluster){
		
		var markers = cluster.getAllChildMarkers();
		
		var _max = Number(-1000000.00);
		var _min =  Number(100000.00)
		var _sum = 0
		var _len = 0
		
		for (imarker in markers){
		  try{
		       var props = markers[imarker].feature.properties
		       var _value = Number(props[property])
			   
		       if (!isNaN(_value)){
		       
		          if (_value > _max)      _max = _value;
		          if(_value < _min)  _min = _value
		       
		         _sum = _sum + _value
			     _len = _len + 1
			   }
			   
			   
		  }catch(e){}
		}
		
		if (_len == 0) _len = 1
		return { av : Number(_sum / _len), max : _max, min : _min, len : _len} 
	},

	
	addPoint : function(data){ 
		
	    for (ifilter in this.config.clfilters){
			var filter = this.config.clfilters[ifilter];
			
			if (data.properties[filter.name] != filter.value){
				return;
			}
		}
		
		this.geoJSONLayer.addData(data)
	},

	getId : function(){return this.id},
	
	showConfig : function(node){
		var html = "".concat(
			"<div>",
			"<h3>"+this.config.clname+"  <button id='"+this.id+"-br' class='btn btn-danger' style='margin-left:20px;' >Remove Cluster</button></h3> ",
			"<div class='span2'> Max Cluster Radius </div> <input id='"+this.id+"-in-cluster-radius' type='text'      value='"+this.config.maxClusterRadius+"' ></input><br/>",
			"<h5> Filters </h5> <button id='"+this.id+"-baf' class='btn btn-info'>Add Filter</button> <br/>",
			"<table class='table' style='width : 75%' >",
			  "<thead> <tr> ",
				"<th>Attribute Name</th>",
				"<th>operator</th>",
				"<th>Attribute Value</th></tr></thead>",
				"<tbody id='"+this.id+"-tbody'></tbody>",
			"</table>",
			"<h5> Cluster Color  </h5>",
			"<div class='span2'> From attribute </div> <input id='"+this.id+"-iacn' type='text'      value='"+this.config.clColorName+"' ></input><br/>",
			"<div class='span2'> Start Color    </div> <input id='"+this.id+"-iacs' type='text'      value='"+this.config.clColorStart+"'></input><br/>",
			"<div class='span2'> End Color      </div> <input id='"+this.id+"-iace' type='text'      value='"+this.config.clColorEnd+"'></input><br/>",
			"<div class='span2'> Start Value    </div> <input id='"+this.id+"-in-col-sv' type='text' value='"+this.config.clColorStartValue+"'></input><br/>",
			"<div class='span2'> End Value      </div> <input id='"+this.id+"-in-col-ev' type='text' value='"+this.config.clColorEndValue+"'></input><br/>",
			"<h5> Cluster Size  </h5>",
			"<div class='span2'> From attribute </div> <input id='"+this.id+"-iasn' type='text' value='"+this.config.clSizeName+"'></input><br/>",
			"<div class='span2'> Start Size     </div> <input id='"+this.id+"-iass' type='text' value='"+this.config.clSizeStart+"'></input><br/>",
			"<div class='span2'> End Size       </div> <input id='"+this.id+"-iase' type='text' value='"+this.config.clSizeEnd+"' ></input><br/>",
			"<div class='span2'> Start Value    </div> <input id='"+this.id+"-in-size-sv' type='text' value='"+this.config.clSizeStartValue+"'></input><br/>",
			"<div class='span2'> End Value      </div> <input id='"+this.id+"-in-size-ev' type='text' value='"+this.config.clSizeEndValue+"' ></input><br/>",
			"</div>"
		)
		node.append(html)
		
		for (i in this.config.clfilters){
			var filter = this.config.clfilters[i]
			this._addFilter(filter.name,filter.operator,filter.value)
		}
		
		// remove cluster
		$("#"+this.id+"-br").click(function(){
			instance.leafletMap.removeCluster(instance.config.clname)
			
		})

		var instance = this;
		$("#"+this.id+"-baf").click(function(){
			node.children().remove();
			instance.config.clfilters[instance.config.clfilters.length] = {
			    name      : " ",
			    operator  : "==",
			    value     : " ",
			}
			instance.showConfig(node)
		})
    },
    
    _addFilter : function(name,operator,value){
        var node = "<tr class='"+this.id+"-acfilter'>"
    	node = node +  "<td><input class='"+this.id+"-acfilterAtt' style='width : 100px;' value='"+name+"'  type='text'></input></td>"
    	node = node +  "<td>"+operator+"</td>"
    	node = node +  "<td><input class='"+this.id+"-acfilterVal' style='width : 50px;'  value='"+value+"'  type='text'></input></td>"
    	node = node + "</tr>"
    	$("#"+this.id+"-tbody").append(node);
    },

    getConfig : function(){ 
	                
    	var newFilters = []
    	var instance = this;
    	$("."+this.id+"-acfilter").each(function(){
    		
    		var _name  = $(this).find("."+instance.id+"-acfilterAtt").val().replace(/ /g,'')
    		var _value = $(this).find("."+instance.id+"-acfilterVal").val().replace(/ /g,'')
    		
    		if ((_name != "")&(_value != "")){
    			newFilters[newFilters.length] = { name : _name, operator : "==", value : _value }
    	    }
    			
    	})
    	
    	this.config.clfilters = newFilters;
    	
    	this.config.maxClusterRadius = $("#"+this.id+"-in-cluster-radius").val().replace(/ /g,''); 

		this.config.clColorName       = $("#"+this.id+"-iacn").val().replace(/ /g,'');
		this.config.clColorStart      = $("#"+this.id+"-iacs").val().replace(/ /g,'');
		this.config.clColorEnd        = $("#"+this.id+"-iace").val().replace(/ /g,'');
		this.config.clColorStartValue = $("#"+this.id+"-in-col-sv").val().replace(/ /g,'');
		this.config.clColorEndValue   = $("#"+this.id+"-in-col-ev").val().replace(/ /g,'');
		
		this.colors = this._generateGradiantArray(this.config.clColorStart,this.config.clColorEnd,100);   
	
		this.config.clSizeName  = $("#"+this.id+"-iasn").val().replace(/ /g,'');
		this.config.clSizeStart = $("#"+this.id+"-iass").val().replace(/ /g,'');
		this.config.clSizeEnd   = $("#"+this.id+"-iase").val().replace(/ /g,'');
		this.config.clSizeStartValue = $("#"+this.id+"-in-size-sv").val().replace(/ /g,'');
		this.config.clSizeEndValue = $("#"+this.id+"-in-size-ev").val().replace(/ /g,'');
		
    	return this.config 
    },

    clear : function(){ 
	    this.geoJSONLayer.clearLayers();
    	this.clusterLayer.clearLayers();
    },

    show : function(){
    	
    	this.clusterLayer.addLayer(this.geoJSONLayer);
    	
    	return this.clusterLayer;
    	
    },
	
	_generateGradiantArray : function( color1, color2, state ){
		
		var c1 = new RGBColor(color1)
		var c2 = new RGBColor(color2)
		state = state || 4
		
		var red = ((c2.r - c1.r) / (state))
		var green = ((c2.g - c1.g) / (state))
		var blue = ((c2.b - c1.b) / (state))
		
		var colors = []
		for (var i = 0;i< state;i++){
			
			var nr = Math.round(c1.r + red   * i) ;
			var ng = Math.round(c1.g + green * i) ;
			var nb = Math.round(c1.b + blue  * i) ;
			
			colors[i] =  "rgba("+nr+","+ng+","+nb+", 0.9)"
		}
		return colors;
	},
	_getColor : function(level){
		
		if (level >= this.colors.length) 
			return this.colors[this.colors.length -1]
		else{
			return this.colors[level];
		}
	}

	

})
