/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.map

import akka.actor.Actor
import fr.inria.apisense.common.ExperimentContext
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.common.utils.NodeParser

class GeoJSONActor(_experimentContext : ExperimentContext) extends Actor{

  final val QUERY_CONFIG  = "filter.json"
  final val QUERY_FILE    = "query.xq"
  final val QUERY_RESULT = "process.data"
  
  override def receive = {
    case _ =>
  }
  
  
  def createMap(mapName : String){
    
    // create a new map folder in openMap folder
    // all query and data result will be inserted
    // in this folder
    mapFolder(mapName).updateFile(QUERY_CONFIG,"{}")
  }
   
  def getMapNames() = {
    
    // return all folder name contained
    // in openFolder
    var builder = new StringBuilder
    openMapFolder.getFiles.foreach{
      file =>
      	builder append file.getName()
      	builder append ";"
    }
    
    if (!builder.isEmpty) builder.deleteCharAt(builder.size - 1)
    builder toString 
  }
   
  def getMapDefinitions(mapName : String) = {
    // return map query filter definition
    NodeParser.parJSON(mapFolder(mapName).getFile(QUERY_CONFIG))
  }
  
  //def getDbNames = //exec {
	// query : return dbtrack node of experiment database
    // all database associated with the associated experiment
    // are contained in dbtrack node
   
  //  queryService.getDocuments(experimentName,path("dbtrack"))
  //}
   
   
  private def openMapFolder = NodeFileConfiguration("experiment/"+_experimentContext.experimentName+"/openMap") 
  private def mapFolder(mapName : String) = NodeFileConfiguration("experiment/"+_experimentContext.experimentName+"/openMap/"+mapName) 
  private def mapSharedFolder(mapName : String) = NodeFileConfiguration("public/map") 
}