/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.map

import org.osoa.sca.annotations.Reference
import fr.inria.bsense.query.QueryService
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.LiftWebNode
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.node.FunctionalityDatabase
import fr.inria.bsense.db.api.path
import java.io.FileOutputStream
import fr.inria.bsense.common.utils.NodeParser
import java.util.StringTokenizer
import java.io.File
import fr.inria.bsense.common.http.Base64
import javax.ws.rs.core.Response
import fr.inria.bsense.common.utils.Log

class GeoJSONMapImpl  extends OpenMapService with FunctionalityDatabase {

  final val QUERY_CONFIG  = "filter.json"
  final val QUERY_FILE    = "query.xq"
  final val QUERY_RESULT = "process.data"
  
  def createMap(sc : SecurityContext, mapName : String) = {
    
    // create a new map folder in openMap folder
    // all query and data result will be inserted
    // in this folder
    mapFolder(mapName).updateFile(QUERY_CONFIG,"{}")
    
    Message.success()
  }
  
  def getMapNames(sc : SecurityContext) = {
    
    // return all folder name contained
    // in openFolder
    var builder = new StringBuilder
    openMapFolder.getFiles.foreach{
      file =>
      	builder append file.getName()
      	builder append ";"
    }
    
    if (!builder.isEmpty) builder.deleteCharAt(builder.size - 1)
   
    Message.success(builder toString) 
  }
  
  def getMapDefinitions(sc : SecurityContext, mapName : String) = {
    // return map query filter definition
    val node = NodeParser.parJSON(mapFolder(mapName).getFile(QUERY_CONFIG))
    Message.success(node)
  }
  
  def getDbNames(sc : SecurityContext) = exec {
	// query : return dbtrack node of experiment database
    // all database associated with the associated experiment
    // are contained in dbtrack node
   
    queryService.getDocuments(experimentName,path("dbtrack"))
  }
  
  def updateQueryFilter(sc : SecurityContext, mapName : String, filters : String ) : String = {
    
    var jsonFilter : Node = null
    
    try{
      // try to parse filters as JSON object
      // and check if json filter contains header
      // and data property
       
      jsonFilter = NodeParser.parJSON(filters)
    
      var headerFilter = jsonFilter \\ "header"
      var dataFilter   = jsonFilter \\ "data"
      var attributesFilter   = jsonFilter \\ "attributes"
      var databaseFilter   = jsonFilter \\ "databases"
       
      if (headerFilter.isNull) throw new Exception("Cannot find header attribute in filter")
      if (dataFilter.isNull) throw new Exception("Cannot find data attributes in filter")     
      if (attributesFilter.isNull) throw new Exception("Cannot find attributes in filter")    
      if (databaseFilter.isNull) throw new Exception("Cannot find databases in filter")    
      
    
      val query = buildQuery(mapName,jsonFilter)
      mapFolder(mapName).updateFile(QUERY_FILE,query)
      
      // update queryFilter file in map folder
      mapFolder(mapName).updateFile(QUERY_CONFIG,jsonFilter.toJSONString)
    
      Message.success()
      
    }
    catch{
       case t : Throwable =>
         t.printStackTrace()
         return Message.exception("Filter error",t.getMessage())
    }
  }
  
  
  var toGEOJSONQuery = """
    declare function local:toGEOJSON($els,$properties){
      <json objects="point properties" arrays="json coordinates" numbers="value">
      {
        for $trace in $els//trace%s return( 
          for $data in $trace//data%s return(
            <point>
              <type>Point</type>
              <coordinates>
		  			<value>{$data/lon/text()}</value>
		  			<value>{$data/lat/text()}</value>
		  	  </coordinates>
              <properties>
              {
                for $propertie in $properties return(
                  if (fn:starts-with($propertie,"@"))
                  then $trace/header/node()[ name() = substring($propertie,2) ]
                  else ( $data/node()[name()=$propertie])
                 )
              }
              </properties>
             </point>
          )
         )
       }
       </json>
}; """
    
  
  
  private def buildQuery(mapName : String, node : Node) = {
    
    // get properties from json object
    
    var databases = node.\\("databases");
    var attributes = node.\\("attributes").toJSONString;
    var headerFilter = node \\ "header" text;
    var dataFilter = node \\ "data" text;
    
    var getData = ""
    
    // from database property, create xquery expression
    // to get collected data from database
   if (databases.size == 0){
      getData = """ apis:traces("%s") """.format(experimentName)
   }
   else{
     
      // databases is a JSON Array including all tracks database
      // ex : [ "xpname-collection1" , "xpname-collection2" ]
      val str = databases.toJSONString
      
      // replace [ and ] by ( and )
      var xqueryList = "("+str.substring(1,str.size - 1)+")"
      
      getData = """ apis:getTraces(%s) """.format(xqueryList)
   }
  
    // parse header expression
    // insert [ and ] at the begining and at the 
    // the end of the property
      
    if (!headerFilter.equals("")){ headerFilter = "[%s]".format(headerFilter) }
    
    // parse data expression
    // insert [ and ] at the begining and at the 
    // the end of the property
    
    if (!dataFilter.equals("")){ dataFilter = "[%s]".format(dataFilter) }
  
    // step  : transform json array to xquery list
    attributes = attributes.substring(1,attributes.length()-1)   
    attributes = "(%s)".format(attributes)
    
    // get xquery method and add header and data filter 
   
    val _toGEOJSONQuery = this.toGEOJSONQuery.format(headerFilter,dataFilter)
    
    // final xquery expression
    
    """
      
      import module namespace apis = "http://fr.inria.apisense/xquery";
      declare option output:method "json";
    
      %s
      
      local:toGEOJSON(%s,%s)
      
   """.format(_toGEOJSONQuery,getData,attributes)
    
  }
  
  def processQueryFilter(sc : SecurityContext, mapName : String) = exec {
    
     val folder = mapFolder(mapName)
    
     var output = folder.compositeFolder+"/"+QUERY_RESULT
     val stream = new FileOutputStream(output)
    
     queryService.query(folder.getFile(QUERY_FILE),stream)
     
     "Map built with success"
  }
  
   def getMap(sc : SecurityContext,  mapName : String) : String = {
     
     try{ mapFolder(mapName).getFile(QUERY_RESULT) }
     catch{ case t : Throwable => "" }
   }
  
   def deleteMap(sc : SecurityContext, mapName : String) : String = {
     
     val folder = mapFolder(mapName)
     folder.deleteAllFiles
     folder.delete
     
     Message.success()
   }
  
  def shareMap( sc : SecurityContext,mapName : String,content : String) : String = {
    
    val folder = mapSharedFolder(mapName)
    var output = folder.compositeFolder+"/"+mapName
    
    val stream = new FileOutputStream(output)
    stream.write(Base64.decode(content))
    stream.flush
    stream.close
    
    Message.success(mapName)
  }
  
  def getSharedMap(sc : SecurityContext, mapName : String) : Response = {
    
    val folder = mapSharedFolder(mapName)
    var output = folder.compositeFolder+"/"+mapName
    
    var file = new File(output)
    
    Response.ok(file).header("Content-Disposition",
	  		"attachment; filename="+mapName).build();
   
  }
   
   
  private def openMapFolder = NodeFileConfiguration("experiment/"+experimentName+"/openMap") 
  private def mapFolder(mapName : String) = NodeFileConfiguration("experiment/"+experimentName+"/openMap/"+mapName) 
  private def mapSharedFolder(mapName : String) = NodeFileConfiguration("public/map") 
  
}