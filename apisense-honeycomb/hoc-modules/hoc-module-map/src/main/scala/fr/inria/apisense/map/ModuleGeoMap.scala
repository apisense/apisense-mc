/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.map

import fr.inria.bsense.node.web.cbse.ExperimentWebModuleComponent
import fr.inria.bsense.node.cbse.ExperimentModule
import frascala.sca.Java
import fr.inria.bsense.query.QueryService
import frascala.sca.Bean
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.node.cbse.ServerNode


class VisualisationMapComponent extends ExperimentModule("cpt-module-map") {
  this.uses(Bean[GeoJSONMapImpl])

  val srv = service("srv-geojson") exposes Java[OpenMapService]
  deployRest(srv, "openmap", Array())

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery as ServerNode.scaUrlDBQuery

  val refUpdateQuery = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdateQuery as ServerNode.scaUrlDBUpdate

  def getModuleName: String = "GeoJSON Generator"
  def getModuleURI: String  = "/openmap"

}

class VisualizationMapWeb extends ExperimentWebModuleComponent("web-module-map"){
  def getModuleName = "Visualisation Map"
  def getModuleURL  = "map/visumap.html"
}

