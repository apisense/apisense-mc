/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.map

import fr.inria.bsense.node.service.ExperimentModuleService
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context
import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.GET
import javax.ws.rs.QueryParam
import javax.ws.rs.PathParam
import javax.ws.rs.core.Response

trait OpenMapService extends ExperimentModuleService {

   /**
    * Create a new experiment Geo Map
    */
   @POST
   @Path("/add/map")
   def createMap(@Context sc : SecurityContext, @FormParam("name")mapName : String) : String
    
   /**
    * Return all created maps
    * The String returned contains all maps name
    * separated by a ;
    */
   @GET
   @Path("/get/maps")
   def getMapNames(@Context sc : SecurityContext) : String
   
   /**
    * Return map query filter
    */
   @GET
   @Path("/get/map")
   def getMapDefinitions(@Context sc : SecurityContext, @QueryParam("name") mapName : String) : String
   
   /**
    * Return all databases containing collected data
    * by the experiment
    */
   @GET
   @Path("/get/dbs")
   def getDbNames(@Context sc : SecurityContext) : String
   
   /**
    * Update query filter 
    */
   @POST
   @Path("/update/filter")
   def updateQueryFilter(
       @Context sc : SecurityContext, 
       @FormParam("name") mapName : String,
       @FormParam("filters") xpathHeader : String
   ) : String
 
   /**
    * Process defined filter on collected data
    */
   @POST
   @Path("/process/filter")
   def processQueryFilter(@Context sc : SecurityContext,@FormParam("name") mapName : String) : String
   
   @POST
   @Path("/update/export")
   def shareMap(@Context sc : SecurityContext,@FormParam("name") mapName : String, @FormParam("content")content : String) : String
   
   @GET
   @Path("/get/export/{name}")
   def getSharedMap(@Context sc : SecurityContext, @PathParam("name")mapName : String) : Response
   
   @GET
   @Path("/get/processed/map/{name}")
   def getMap(@Context sc : SecurityContext, @PathParam("name") name : String) : String
   
   @POST
   @Path("/delete/map")
   def deleteMap(@Context sc : SecurityContext, @FormParam("name") name : String) : String
   
}