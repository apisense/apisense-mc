/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.map

import org.basex.server.ClientSession
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.node.configuration.NodeConfigurationDB

object Main {

  def main(args: Array[String]) {
  
    val atClient = new DatabaseClientSession
    atClient.setHost(NodeConfigurationDB host)
    atClient.setPort(NodeConfigurationDB port)
    atClient.setUsername(NodeConfigurationDB user)
    atClient.setPassword(NodeConfigurationDB password)
    
  
    
	var client = new ClientSession(
    atClient.getHost(),
    atClient.getPort().toInt,
    atClient.getUsername(),
    atClient.getPassword())
    
    var client2 = new ClientSession(
    atClient.getHost(),
    atClient.getPort().toInt,
    atClient.getUsername(),
    atClient.getPassword())
  
    
  }
  
}