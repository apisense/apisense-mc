APIS.PieVersionUpload = APIS.Chart.extend({

	setDate : function(sd,ed){
	    this.labelX = arrayFromDuration(new Date(sd.getTime()),new Date(ed.getTime()))
	    this.datasets = {}
	},

	addData : function(_data){

		var date   = _data.stat.date;
		var version = _data.stat.version;
		var size = _data.stat.sizeByte;

		if (!this.datasets[version]){ this.datasets[version] = 0; }

		this.datasets[version] = this.datasets[version] + parseInt(size);
	},

    draw : function(){

		var cd = this.defaultChart();
		cd.title.text =' Uploaded Data by Experiment Version '
		cd.yAxis.title = 'Size (byte)'

		cd.plotOptions = { pie: { allowPointSelect: true,showInLegend: true}}

		var data = []
		for (var i in this.datasets){
			data.push(["version "+i,this.datasets[i]])
		}

		cd.series.push({
			type : "pie",
		    name : "Version ",
		    data : data
		})

		try{this.chartObject = new Highcharts.Chart(cd)}catch(e){alert(e)}
	}
})