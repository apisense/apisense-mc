/**
 *
 * Bar Chart : Data upload size from users
 *
 */

APIS.PieHeaderUpload = APIS.Chart.extend({

	setConfig : function(_conf){

		this.header = _conf.headerProperty;
		this.chartName = _conf.chartName;
	},



	setDate : function(sd,ed){
	    this.labelX = arrayFromDuration(new Date(sd.getTime()),new Date(ed.getTime()))
	    this.datasets = {}
	},

	addData : function(_data){

		var date   = _data.stat.date;
		var size = _data.stat.sizeByte;
		var headerValue = _data.header[this.header];

		if (headerValue){
			if (!this.datasets[headerValue]){ this.datasets[headerValue] = 0; }
			this.datasets[headerValue] = this.datasets[headerValue] + parseInt(size);

		}
	},

    draw : function(){

		var cd = this.defaultChart();
		cd.title.text =this.chartName

		cd.plotOptions = { pie: { allowPointSelect: true,showInLegend: true}}

		var data = []
		for (var i in this.datasets){
			data.push([this.header+" "+i,this.datasets[i]])
		}

		cd.series.push({
			type : "pie",
		    name : this.chartName,
		    data : data
		})

		try{this.chartObject = new Highcharts.Chart(cd)}catch(e){alert(e)}
	}
})