
var gridtster,pickerStart,pickerEnd = undefined;

widget_base_width = 100;
widget_base_height = 100;

widget_full_size_width  = 9;
widget_full_size_height = 4;
widget_min_size_width = 4;
widget_min_size_height = 3;

$(function(){

  // Chart grid configuration
  gridtster = $(".gridster > ul").gridster({
     widget_margins: [10, 10],
     widget_base_dimensions: [widget_base_width,widget_base_height],
     max_size_x: 20
  }).data('gridster');

  // Date picker DOM configuration
  pickerStart = $('.pickerStart').datepicker({ format : "yyyy-mm-dd" })
  pickerEnd   = $('.pickerEnd').datepicker({ format : "yyyy-mm-dd" })

  // Configure duration with the current current week
  var startDate = new Date( new Date().getTime() - 7 * 24 * 60 * 60 * 1000 );
  var endDate   = new Date();
  $('.pickerStart').datepicker('setValue', startDate)
  $('.pickerEnd').datepicker('setValue', endDate)

  //var startDate = new Date(2012,00,01,1,0,0,0);
  //var endDate = new Date(2012,00,07,1,0,0,0);
  //$('.pickerStart').datepicker('setValue',startDate)
  //$('.pickerEnd').datepicker('setValue',endDate)


 try{

	  var dashbord = new APIS.DashBord();

	  dashbord.addChart("chart-bar-versions",APIS.BarVersionUpload,{ fullsize : true , color : "#FFF"})
	  dashbord.addChart("chart-bar-all",APIS.BarAllUpload,{ fullsize : false, color : "#FFF"})
	  dashbord.addChart("chart-pie-version",APIS.PieVersionUpload,{fullsize : false , color : "#36A9E1"})
	  //dashbord.addChart("chart-bar-users",APIS.BarUserUpload,{ fullsize : false , color : "#FFF"})
	  dashbord.addChart("chart-line-day",APIS.LineDayUpload,{ fullsize : false , color : "#FFF"})
	  dashbord.update(startDate,endDate)

	  var websocketPath = "ws://"+window.location.hostname

	  // dev section
	  if (window.location.hostname.slice(0,1) == "1")  websocketPath = "ws://localhost:8888"
	  else if(window.location.hostname == "localhost") websocketPath = "ws://localhost:8888"

	  websocketPath = websocketPath + "/websocket/"+xpName+"/upload"

	  var socket = new APIS.WebSocket({
		 path   : websocketPath,
		 onClose : function(event){},
		 onMessage : function(message){

			 $("#chart-update").click()
	     }
	  })


  }catch(e){alert(e)}

  // Plug add chart button
  $("#chart-add").click(function(){
	  var str = "<div align='left'>"
	  str = str + ' <label>Chart Name : </label> <input  type="text" class="span2"  id="dialog-chart-name"> '
	  str = str + ' <label>Select header property : </label> <select id="dialog-chart-tag" >'

	  var headerTags = dashbord.getTags();
	  for (var hi in headerTags){
		  str = str + '<option>'+headerTags[hi]+'</option>'
	  }

	  str = str + "</div>"

	  new Modal({
			title:"Create Pie chart",
			message:str,
			onOk : function(){

		  		var props = $("#dialog-chart-tag").val();
		  		var name  =  $("#dialog-chart-name").val()

		  		dashbord.addChart("chart-pie-header",APIS.PieHeaderUpload,{
		          chartName      : name,
		          headerProperty : props,
		          width : widget_min_size_width,
		          height : widget_min_size_width,
		          color : "#F5F5F5",
		          winColor : "#F5F5F5"
	            })

	            $("#chart-update").click();
	  		}
	  })
  })

  // Plug update button
  $("#chart-update").click(function(){

	 var sd = $('.pickerStart').val().split("-");
     var ed = $('.pickerEnd').val().split("-");

     var d1 = new Date(parseInt(sd[0]),parseInt(sd[1]) -1,parseInt(sd[2]),1,0,0)
	 var d2 = new Date(parseInt(ed[0]),parseInt(ed[1]) -1,parseInt(ed[2])+1,1,0,0)


     dashbord.update(d1,d2)
  })

})


var arrayFromDuration = function(start,end){
	var result = []
	result.push(start.toISOString().substring(0,10))
	while (start < end){
		start.setTime( start.getTime() + 24 * 60 * 60 * 1000 )
		result.push(start.toISOString().substring(0,10))
	}
	return result;
}

var pr = function(el){alert(JSON.stringify(el))}

APIS.DashBord = APIS.Class.extend({

	initialize : function(){
	    this.chartIndex = 0;
		this.charts = {}
		this.dataHeader = {};


		var self = this;
		$(window).resize(function(){
			self.resize();
		})
	},

	addChart : function(chartId,chart,_conf){

		var conf = _conf || {  }
		var color = conf.color || "red"
		var winColor = conf.winColor || "#36A9E1"
		var fullsize = conf.fullsize;
		chartId = chartId+"-"+this.chartIndex

		this.chartIndex = this.chartIndex + 1;

		var str = '<li style="background : '+color+' " id="'+chartId+'-gw" data-sizex="5" data-sizey="1">'
	    str = str + '<div class="shadow3" style="background : '+winColor+' " align="right">'
	    str = str + '<button id="'+chartId+'-zoomOut" class="btn transparent"> <i class="icon-chevron-down"></i> </button>'
	    str = str + '<button id="'+chartId+'-zoomIn"  class="btn transparent"> <i class="icon-fullscreen"></i> </button>'
	    str = str + '</div>'
	    str = str + '<div id='+chartId+' style=" background : '+color+'; margin: 0 auto"></div>'
		str = str + '</li>'

		var gwWidth  = widget_min_size_width;
		var gwHeight = widget_min_size_height;
		if (fullsize){
			var gwWidth = $(".gridster").css("width")
			gwWidth = (parseInt(gwWidth.split(" ")) - 300) / widget_base_width
			gwWidth = Math.round(gwWidth)
			if (gwWidth > 19) gwWidth = 19
			gwHeight = widget_full_size_height
		}

        var widget = gridtster.add_widget(str,gwWidth,gwHeight);

		this.charts[chartId] = new chart(chartId,conf)
		this.charts[chartId].setConfig(conf)
		this.charts[chartId].setFullSize(fullsize);
		var self = this;

		$("#"+chartId+"-zoomIn").click(function(ev){
			self.charts[chartId].setFullSize(true)
		})

		$("#"+chartId+"-zoomOut").click(function(ev){
			self.charts[chartId].setFullSize(false)
		})

		$("#"+chartId+"-gw").watch('width', function(a){
			var height = $("#"+chartId+"-gw").css("height").split(" ")
			$("#"+chartId).css("height",parseInt(height[0]) - 30)
			self.charts[chartId].redraw();
		})
	},

	getTags : function(){
		return Object.keys(this.dataHeader)
	},

	resize : function(){

		for (var ci in this.charts){
			this.charts[ci].resize();
		}
	},

	update : function(startdate,enddate){

		var self = this;

		var sISO = startdate.toISOString().substring(0,10)
		var eISO = enddate.toISOString().substring(0,10)

		// update all charts configuration
		for (var j in this.charts){
			this.charts[j].clear();
			this.charts[j].setDate(startdate,enddate);

			var height = $("#"+this.charts[j].chartId+"-gw").css("height").split(" ")
			$("#"+this.charts[j].chartId).css("height",parseInt(height[0]) - 30)
		}

		var request = new APIS.POST("../../../"+xpName+"/upload/metrics/all/"+sISO+"/"+eISO,{},{method : "GET"})
		request.onSuccess = function(result){

			for (index in result){
				for (var j in self.charts){

					for (var hindex  in result[index].header){

						if (!self.dataHeader[hindex]){
							self.dataHeader[hindex] = 0;
						}
					}


					try{self.charts[j].addData(result[index])}catch(e){alert(e)}
				}
			}

			for (var j in self.charts){ self.charts[j].draw() }

		}
   }
})


APIS.Chart = APIS.Class.extend({

	initialize : function(id,conf){
		this.chartId = id;
		this.color = conf.color || "#FFF"
		this.fullSize = false;
		this.gwWidth = widget_min_size_width;
		this.gwHeight = widget_min_size_height;

	},

	redraw : function(){
		if (this.chartObject != undefined){
			this.chartObject.destroy();
			this.chartObject = undefined;
			this.draw()
		}
	},

	setFullSize : function(isFullSize){

		if (isFullSize != this.fullSize){

		  this.fullSize = isFullSize
		  this.resize();
		}
	},

	resize : function(){

	  this.gwWidth  = widget_min_size_width;
	  this.gwHeight =  widget_min_size_height;

	  if (this.fullSize){
		this.gwWidth = $(".gridster").css("width")
		this.gwWidth = (parseInt(this.gwWidth.split(" ")) - 300) / widget_base_width
		this.gwWidth = Math.round(this.gwWidth)
		if (this.gwWidth > 19) this.gwWidth = 19
		this.gwHeight = widget_full_size_height
	  }
	  gridtster.resize_widget( $("#"+this.chartId+"-gw"),this.gwWidth ,this.gwHeight)
	},

	defaultChart : function(){
	 return {
	     	  chart   : {backgroundColor : this.color,renderTo: this.chartId,},
	     	  title   : { text: 'Uploaded Data By User' },
	     	  subtitle: { text: 'Experiment : '+xpName },
	     	  xAxis   : { categories: this.labelX },
	     	  yAxis: {min: 0,title: {text: 'Size (byte)' } },
	     	  series   : []
	 }
	},

	setConfig : function(_conf){},

	setDate : function(sd,ed){},

	clear : function(){
		this.labelX = []
		if (this.chartObject != undefined){
			this.chartObject.destroy();
		}
		this.chartObject = undefined;
	}
})









