/**
 *
 * Bar Chart : Data upload size from users
 *
 */

APIS.BarUserUpload = APIS.Chart.extend({


	setDate : function(sd,ed){
	    this.labelX = arrayFromDuration(new Date(sd.getTime()),new Date(ed.getTime()))
	    this.datasets = {}
	},


	addData : function(_data){

		var date   = _data.stat.date;
		var userId = _data.stat.userId;
		var size = _data.stat.sizeByte;

		if (!this.datasets[userId]){
			this.datasets[userId] = []
			for (var i in this.labelX){
		      this.datasets[userId][i] = 0
	        }
		}

		var index = this.labelX.indexOf(date)
		if (index != -1){
			this.datasets[userId][index] = this.datasets[userId][index] + parseInt(size)
		}
	},

    draw : function(){

		var cd = this.defaultChart();
		cd.chart.type = 'column'
		cd.title.text ='Uploaded Data By User'
		cd.yAxis.title.text = 'Size (byte)'

		for (i in this.datasets){
			cd.series.push({
				name : "user "+i,
				data : this.datasets[i]
			})
		}

		this.chartObject = new Highcharts.Chart(cd)
	}
})