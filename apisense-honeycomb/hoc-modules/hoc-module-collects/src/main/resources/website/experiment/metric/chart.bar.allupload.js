APIS.BarAllUpload = APIS.Chart.extend({

	setDate : function(sd,ed){
	    this.labelX = arrayFromDuration(new Date(sd.getTime()),new Date(ed.getTime()))
	    this.datasets = []
		for (var i in this.labelX){ this.datasets[i] = 0;}
	},

	addData : function(_data){

		var date   = _data.stat.date;
		var size = _data.stat.sizeByte;

		var index = this.labelX.indexOf(date)
		if (index != -1){
			this.datasets[index] = this.datasets[index] + parseInt(size)
		}
	},

    draw : function(){

		var cd = this.defaultChart();
		cd.chart.type = 'column'
		cd.title.text ='Uploaded Data'
		cd.yAxis.title.text = 'Size (byte)'
		cd.series = [{name : "Total",data : this.datasets}]



		this.chartObject = new Highcharts.Chart(cd)
	}
})