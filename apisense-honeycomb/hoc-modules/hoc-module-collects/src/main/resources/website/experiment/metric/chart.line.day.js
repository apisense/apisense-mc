/**
 *
 * Bar Chart : Data upload size from users
 *
 */

APIS.LineDayUpload = APIS.Chart.extend({

	setDate : function(sd,ed){

	    this.labelX = ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"]
	    this.datasets = {data : []}
	    this.datasets.name = "All"
	    for (i in this.labelX){
	    	this.datasets.data.push(0);
	    }
	},


	addData : function(_data){

		var time   = _data.stat.uploadedTime;
		var size = _data.stat.sizeByte;

		var hour = new Date(parseInt(time)).getHours()

		this.datasets.data[hour] = this.datasets.data[hour] + parseInt(size);

	},

    draw : function(){

		var cd = this.defaultChart();
		cd.chart.type = 'area'
		cd.title.text =' Uploaded Data by Hour '
		cd.yAxis.title.text = 'Size (byte)'
		cd.series.push(this.datasets)

		this.chartObject = new Highcharts.Chart(cd)
	}
})