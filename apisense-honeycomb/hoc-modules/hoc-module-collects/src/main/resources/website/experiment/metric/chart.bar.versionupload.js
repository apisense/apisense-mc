/**
 *
 * Bar Chart : Data upload size from users
 *
 */

APIS.BarVersionUpload = APIS.Chart.extend({

	setDate : function(sd,ed){
	    this.labelX = arrayFromDuration(new Date(sd.getTime()),new Date(ed.getTime()))
	    this.datasets = {}
	},

	addData : function(_data){

		var date   = _data.stat.date;
		var version = _data.stat.version;
		var size = _data.stat.sizeByte;

		if (!this.datasets[version]){
			this.datasets[version] = []
			for (var i in this.labelX){
		      this.datasets[version][i] = 0
	        }
		}

		var index = this.labelX.indexOf(date)
		if (index != -1){
			this.datasets[version][index] = this.datasets[version][index] + parseInt(size)
		}
	},

    draw : function(){

		var cd = this.defaultChart();
		cd.chart.type = 'column'
		cd.title.text ='Uploaded Data by Experiment Version'
		cd.yAxis.title.text = 'Size (byte)'

		for (i in this.datasets){
			cd.series.push({
				name : "Version "+i,
				data : this.datasets[i]
			})
		}

		this.chartObject = new Highcharts.Chart(cd)
	}
})