/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.collects

import fr.inria.bsense.node.cbse.ExperimentModule
import frascala.sca.Bean
import frascala.sca.Java
import frascala.frascati.REST
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import frascala.frascati.SCA
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.cbse.DatabaseComposite
import fr.inria.bsense.node.web.cbse.ExperimentWebModuleComponent

abstract class CollectorComponent extends ExperimentModule("fr.inria.bsense.collector"){

  val srvCollector = service("srv-collector") exposes Java[CollectorService]
  deployRest(srvCollector, "upload", Array())


  // Define database dependency
  val refQuery =
	  reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService] as SCA(DatabaseComposite.COMPOSITE_NAME + "/" + DatabaseComponent.SERVICE_QUERY)

  val refUpdateQuery =
	  reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService] as SCA(DatabaseComposite.COMPOSITE_NAME + "/" + DatabaseComponent.SERVICE_QUERY_UPDATE)

  override def getModuleURI: String = "/upload"
}



class CollectorKeepingUserId extends CollectorComponent{

  // Define component implementation
  this.uses(Bean[CollectorKeepingUserIdImpl])

  override def getModuleName: String = "Collector Keeping User Id "
}

class UploadMetricsWeb extends ExperimentWebModuleComponent("web-module-upload-metric"){
  def getModuleName = "Upload Dashboard"
  def getModuleURL  = "metric/metric.html"
}


