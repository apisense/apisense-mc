/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.collects

import fr.inria.bsense.node.FunctionalityDatabase
import javax.ws.rs.core.HttpHeaders
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.utils.Log
import org.json.XML
import org.json.JSONObject
import fr.inria.bsense.common.utils.XMLNode
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.db.api.{path,field,value}
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.common.utils.Node
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.apisense.APISENSE

@Scope("COMPOSITE")
class CollectorKeepingUserIdImpl extends CollectorService with LifeCycleService {


  def onStart( composite : BSenseComposite ){

    this.initializeWebsocket;
  }


  def onStop{

    this.destroyWebsocket();
  }

  def upload(date : String, userId : String, data : JSONObject) : String = {

    // Data are inserted in a database
	// corresponding to the name of experiment
	// and the current date
	// there are one database per day
	val dbName = experimentName+"-"+date

    Log.i("send traces to experiment to database "+dbName)

    // simple parsing
	// remove { and } symbols and replace its by [ and ]
	// Cause : { } are special symbol in Xquery
	//var pdata = data.replaceAll("\\{", " &#123;")
	//pdata = pdata.replaceAll("}"," &#125;");

    if (!data.getJSONObject("trace").has("header"))
		data.getJSONObject("trace").put("header", new JSONObject)

	data.getJSONObject("trace").getJSONObject("header").put("userId", userId)

	val errorCode = updateService.addDocument(session, dbName, userId, data.toString())
	if (errorCode > 0){
		if (!queryService.exists(dbName)){

	      Log.i("create database "+dbName)
	      updateService.addDocument(session, experimentName, "collections",
	        new XMLNode(<dbtrack>{dbName}</dbtrack>).toJSONString)

	      updateService.create(session, dbName)

	      val errorCode = updateService.addDocument(session, dbName, userId, data.toString())
	      if (errorCode > 0){
	        throw new CannotInsertDataException("Cannot insert data Error: "+errorCode);
	      }
		}
	}

	Message.success()
  }

  def calculMetric(version : String,date : String, userId : String, data : JSONObject){

    var sizeByte = data.toString().getBytes().length
    var sizeNumber   = data.getJSONObject("trace").getJSONArray("data").length

    var node : Node = NodeParser.createEmptyNode("upload")

    var stat : Node = NodeParser.createEmptyNode("stat")
    stat = stat :<("sizeByte",sizeByte)
    stat = stat :<("sizeNumber",sizeNumber)
    stat = stat :<("uploadedTime",System.currentTimeMillis())
    stat = stat :<("version",version)
    stat = stat :<("date",date)
    stat = stat :<("userId",userId)

    node = node :< stat

    if (data.getJSONObject("trace").has("header")){

      var header : Node = NodeParser.createEmptyNode("header")
      header = header :<(NodeParser.parJSON(data.getJSONObject("trace").getJSONObject("header").toString()))

      node = node :< header
    }

    updateService.addDocument(session, experimentName, userId, node.toJSONString)
  }

  /**
   *
   * Get upload statistic for a specific user
   *
   */
  def getUploadMetricAfter(sc : SecurityContext, date : String, userId : String ) = {

    val query = """

      declare option output:method "json";

      <json objects="stat header" arrays="json">
      {

      let $_dbname := "%s"
      let $_date := "%s"
      let $_userId := "%s"
      return(
         for $data in db:open($_dbname,$_userId)//upload[ stat/date > $_date ]
         order by $data/stat/date
         return $data/stat
      )
      }
      </json>


    """.format(experimentName,date,userId)

    try{

       Message.successJSON(exec(false){queryService.query(query)})

    }catch{
      case th : Throwable => {
    	 th.printStackTrace()
         Message.exception(th)
      }
    }
  }

  /**
   *
   * Get upload statistic for a specific user
   *
   */
  def getUploadMetricBetween(sc : SecurityContext, startdate : String, enddate : String, userId : String ) = {

      val query = """

      declare option output:method "json";

      <json objects="stat header" arrays="json">
      {

      let $_dbname := "%s"
      let $_sdate := "%s"
      let $_edate := "%s"
      let $_userId := "%s"
      return(
         for $data in db:open($_dbname,$_userId)//upload[ stat/date >= $_sdate and stat/date <= $_edate ]
         order by $data/stat/date
         return $data/stat
      )
      }
      </json>


    """.format(experimentName,startdate,enddate,userId)

    Log.d("exec : "+query)

     try{

       Message.successJSON(exec(false){queryService.query(query)})

    }catch{
      case th : Throwable => {
    	 th.printStackTrace()
         Message.exception(th)
      }
    }
  }

  /**
   *
   * Get upload statistic for all users
   *
   */
  def getAllUploadMetricAfter(sc : SecurityContext, date : String ) = {

      val query = """

      declare option output:method "json";

      <json objects=" upload stat header" arrays="json">
      {

      let $_dbname := "%s"
      let $_date := "%s"
      return(
         for $data in db:open($_dbname)//upload[ stat/date > $_date ]
         order by $data/stat/date
         return $data
      )
      }
      </json>


    """.format(experimentName,date)

    Log.d("exec : "+query)

    try{

       Message.successJSON(exec(false){queryService.query(query)})

    }catch{
      case th : Throwable => {
    	 th.printStackTrace()
         Message.exception(th)
      }
    }


  }

  /**
   *
   * Get upload statistic for all users
   *
   */
  def getAllUploadMetricBetween(sc : SecurityContext, startdate : String, enddate : String) = {


      val query = """

      declare option output:method "json";

      <json objects=" upload stat header" arrays="json">
      {

      let $_dbname := "%s"
      let $_sdate := "%s"
      let $_edate := "%s"
      return(
         for $data in db:open($_dbname)//upload[ stat/date >= $_sdate and stat/date <= $_edate ]
         order by $data/stat/date
         return $data
      )
      }
      </json>


    """.format(experimentName,startdate,enddate)

    Log.d("exec : "+query)

     try{

       Message.successJSON(exec(false){queryService.query(query)})

    }catch{
      case th : Throwable => {
    	 th.printStackTrace()
         Message.exception(th)
      }
    }

  }


  def deleteData(sc : SecurityContext, userId : String) : String = {
    Message.exception(
        "Not Implemented",
        "service not implemented. Please signal us if you want to drop your data"
    )
  }

}