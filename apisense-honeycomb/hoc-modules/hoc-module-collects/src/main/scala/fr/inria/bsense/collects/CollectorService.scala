/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.collects

import fr.inria.bsense.node.service.ExperimentModuleService
import javax.ws.rs.Path
import javax.ws.rs.POST
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.Context
import fr.inria.bsense.common.utils.Message
import java.util.zip.ZipInputStream
import java.io.ByteArrayInputStream
import fr.inria.bsense.node.FunctionalityDatabase
import org.osoa.sca.annotations.Property
import fr.inria.bsense.db.api._
import org.json.JSONObject
import org.json.JSONArray
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.QueryParam
import fr.inria.bsense.common.utils.Log
import javax.ws.rs.PathParam
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.apisense.APISENSE
import fr.inria.apisense.APISWebSocket
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.common.utils.IoUtils
import javax.ws.rs.Produces
import javax.ws.rs.core.Response
import java.io.FileInputStream
import javax.ws.rs.core.Response.Status
import javax.ws.rs.core.MediaType



class BadSignatureException(message : String) extends Exception(message)
class CannotInsertDataException(message : String) extends Exception(message)


class MediaStorage(experimentName : String) extends NodeFileConfiguration("experiment/"+experimentName+"/medias")

trait CollectorService extends ExperimentModuleService with FunctionalityDatabase {

  var webSocket : APISWebSocket = _;


  def initializeWebsocket(){
    webSocket = APISENSE.webserver.createWebSocket("/websocket/"+experimentName+"/upload")
  }

  def destroyWebsocket(){
	  if (webSocket != null){
	    APISENSE.webserver.deleteWebSocket(webSocket)
	  }
  }

  @GET
  @Path("/get/media/{mediaId}")
  @Produces(Array("image/png"))
  def media(
      @Context sc : SecurityContext,
      @PathParam("mediaId")mediaId : String) : Response = {

    val mediaStorage = new MediaStorage(this.experimentName)
    if (mediaStorage.contain(mediaId)){

    	 val input= mediaStorage.getFileInputStream(mediaId)

    	 //Response.status(Status.NOT_FOUND).build();
    	 return Response.ok(input,MediaType.valueOf("image/png")).build();
    }

    Response.status(Status.NOT_FOUND).build();

  }

  @POST
  @Path("/data/{userId}/{version}/{signature}")
  @Consumes(Array("application/octet-stream "))
  def upload(
      @Context sc : SecurityContext,
      @PathParam("userId")userId : String,
      @PathParam("version")xpVersion : String,
      @PathParam("signature")xpSignature : String,
      data: Array[Byte]) : String = {

      if ((userId == null) || (userId equals "")) throw new Exception("UserId is not defined")
      if ((xpSignature == null) || (xpSignature equals "")) throw new Exception("Signature is not defined")
      if ((xpVersion == null) || (xpVersion equals "")) throw new Exception("version is not defined")

      var experimentSignature : String = null

      val mediaStorage = new MediaStorage(this.experimentName)

      //exec{
      // Get experiment signature of experiment version
      //val experimentSignatureNode = queryService.getDocument(this.experimentName, "signature",path("signature") withs field("version") == value(headerVersion.get(0)))
      //experimentSignature = experimentSignatureNode \ "value" text;
      //}
      //throw exception if signature are not found
      //if (experimentSignature == null)
      //  throw new BadSignatureException("Cannot find signature for experiment version "+headerVersion.get(0))
      // Check signature provided by user
      // and real signature
      // throw exception if both signature are different
      //if (!(experimentSignature  equals headerSignature.get(0)))
      //  throw new BadSignatureException("Signature provided is incompatible with experiment version "+headerVersion.get(0))

      exec{

    	var trackHeader : JSONObject = null
    	var track : Map[String,JSONObject] = Map()

    	// Open zipped data sent
    	val zip = new ZipInputStream(new ByteArrayInputStream(data))
    	var entry = zip.getNextEntry()
    	while (entry != null){

    		// get file name
    		val name = entry.getName()

    		Log.d("collect entry "+name);

    		if (name.equals("header.json")){

    		    // convert data as string
    	        val data = scala.io.Source.fromInputStream(zip).getLines().mkString("")

    			trackHeader = new JSONObject(data);
    			trackHeader.put("scriptVersion", xpVersion)
    		}
    		else if(entry.isDirectory()){
    			// DO Nothing
    		}
    	    // Media File
    		else if(name.contains("medias")){

    			 mediaStorage.write(name.substring(name.indexOf("/")), zip)
    		}
    		else{

    			// TODO Check file name format
    			// file name must be under the form day-month-year

    		    // convert data as string
    	        val data = scala.io.Source.fromInputStream(zip).getLines().mkString("")

    			val json = new JSONObject
    			json.put("trace", new JSONObject)

    			if (trackHeader == null){
    				trackHeader = new JSONObject()
    				trackHeader.put("scriptVersion", xpVersion)
    			}

    			json.getJSONObject("trace").put("header", trackHeader)
    			json.getJSONObject("trace").put("data", new JSONArray(data))

    			track += name -> json
    		}

    		entry = zip.getNextEntry()
    }

    zip.close()

    // Insert data in database
    track.foreach{ case (date,value) =>

      try{

    	  upload(date,userId,value)

    	  calculMetric(xpVersion,date,userId,value)

      }catch{
      	case e :  CannotInsertDataException => {
          // Store data in cache for analyze
          Log.e(e)
        }
        case e : Throwable => Log.e(e);
      }
    }

    if (webSocket != null){
      // broadcast a web socket message
      // to prevent that new data has been
      // uploaded
      Log.d("broadcast websocket upload message")
      webSocket.broadcast("Upload");
    }
   }
  }

  @GET
  @Path("metrics/user/{userId}/{date}")
  def getUploadMetricAfter(
      @Context sc : SecurityContext,
      @PathParam("date") date : String,
      @PathParam("userId") userId : String ) : String

  @GET
  @Path("metrics/user/{userId}/{startdate}/{enddate}")
  def getUploadMetricBetween(
      @Context sc : SecurityContext,
      @PathParam("startdate") startdate : String,
      @PathParam("enddate") enddate : String,
      @PathParam("userId") userId : String ) : String

  @GET
  @Path("metrics/all/{date}")
  def getAllUploadMetricAfter(
      @Context sc : SecurityContext,
      @PathParam("date") date : String ) : String

  @GET
  @Path("metrics/all/{startdate}/{enddate}")
  def getAllUploadMetricBetween(
      @Context sc : SecurityContext,
      @PathParam("startdate") startdate : String,
      @PathParam("enddate") enddate : String) : String

  @POST
  @Path("/delete/{userId}")
  def deleteData(@Context sc : SecurityContext, @QueryParam("userId") userId : String) : String

  /**
   * Calculate Metric on uploaded data
   *
   * @param date    Date of uploaded data
   * @param userId  Experiment Id of user
   * @param data    Data
   */
  def calculMetric(version : String, date : String, userId : String, data : JSONObject)

  def upload(date : String, userId : String, data : JSONObject) : String

}