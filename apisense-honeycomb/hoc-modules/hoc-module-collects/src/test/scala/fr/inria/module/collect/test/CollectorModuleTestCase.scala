package fr.inria.module.collect.test

import fr.bsense.module.test.ModuleTCase
import org.junit.Test
import fr.inria.apisense.client.APISENSEClient
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import org.apache.cxf.helpers.IOUtils
import fr.inria.apisense.client.HoneyCombClient
import java.io.InputStream
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.LiftWebNode
import fr.inria.apisense.client.CollectorModuleClient


class CollectorModuleTestCase extends ModuleTCase {


  override def startHive = true;
  override def getExperimentNameTest = "TestModuleCollectorExperiment"
  override def getSPL = """ {
     "Experiment" : 1,
     "Experiment Website":1,
     "Module Manager" : 1,
     "CKUID" : 1,
     "Upload Metrics" : 1
  }"""

  @Test def test(){

    val honeyService = APISENSEClient.honey

    honeyService.host(ModuleTCase.HOST)
    honeyService.connect(scientist_token)

    val collector = honeyService.experiment(getExperimentNameTest).collector

    notfail("updload dataset trace1"){
      collector.uploadData("0001", "1.0", "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-2.zip")))
      collector.uploadData("0002", "1.0", "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-1.zip")))
      collector.uploadData("0002", "2.0", "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-2.zip")))
      collector.uploadData("0003", "2.0", "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-1.zip")))
      collector.uploadData("0004", "3.0", "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-2.zip")))
      collector.uploadData("0005", "4.0", "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-3.zip")))

     }


   notfail("Get user metric "){

    	val node = new LiftWebNode(honeyService.experiment(getExperimentNameTest).collector.getUserMetric("0001","2012-01-01"))
    	Log.d("Test result : "+node)
    }

    notfail("Get user metric between"){

      val node = new LiftWebNode(honeyService.experiment(getExperimentNameTest).collector.getUserMetric("0001","2012-01-01","2012-01-03"))
      Log.d("Test result : "+node)
    }

    notfail("Get all users metrics "){

      val node = new LiftWebNode(honeyService.experiment(getExperimentNameTest).collector.getAllMetric("2012-01-01"))
      Log.d("Test result : "+node)
    }

    notfail("Get all users metrics "){

      val node = new LiftWebNode(honeyService.experiment(getExperimentNameTest).collector.getAllMetric("2012-01-01","2012-01-01"))
      Log.d("Test result : "+node)
    }

  }


  private def createThreadInjector(collector : CollectorModuleClient, milli : Long ){
    new Thread(new Runnable(){
      def run(){
    	  while(true){
    	  Thread.sleep(milli)
    	  val user    = ((Math.random() * 1000) % 5).toInt
    	  val version = ((Math.random() * 1000) % 5).toInt
    	  collector.uploadData(user.toString, version.toString, "dlskjdhlqsdhqljdqh",IOUtils.readBytesFromStream(ClassLoader.getSystemResourceAsStream("trace-2.zip")))
    	  }
      }
    }).start()
  }






}