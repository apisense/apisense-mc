module namespace  bs = "http://fr.inria.bsense/modules" ;

import module namespace functx = "http://www.functx.com";

declare variable $bs:R := 6371;
declare variable $bs:rad := math:pi() div 180;

declare function bs:distance($lat1, $lon1, $lat2, $lon2){
  let $rlat1 := $lat1 * $at:rad
  let $rlon1 := $lon1 * $at:rad
  let $rlat2 := $lat2 * $at:rad
  let $rlon2 := $lon2 * $at:rad
	let $x := ($rlon2 - $rlon1) * math:cos(($rlat1 + $rlat2) div 2 ) 
	let $y := $rlat2 -$rlat1 
	return 
		math:sqrt(($x * $x) + ($y * $y)) * $at:R
};

declare function at:toCVS($xp,$els){
   at:toCVSBase($els,";",at:traces($xp))            
};

declare function at:toCVS($xp,$els,$user){
   at:toCVSBase($els,";",at:traces_of_user($xp,$user))            
};

declare function at:toCVSBase($els,$separator,$nodes){
  for $el in $els return fn:concat($el,$separator)
   ,"&#xa;",
   for $trk in  $nodes//:data 
   return(
     for $el in $els return ( $trk//node()[name() = $el]/text(),$separator ), "&#xa;"
   )
};
