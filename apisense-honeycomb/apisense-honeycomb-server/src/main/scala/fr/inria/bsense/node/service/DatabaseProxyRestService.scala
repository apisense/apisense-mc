/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.service

import javax.ws.rs.Path
import javax.ws.rs.POST
import javax.ws.rs.FormParam
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context

trait DatabaseProxyRestService {

  @POST
  @Path("/get/query")
  def query(@Context sc : SecurityContext,  @FormParam("repository") repository : String,  @FormParam("filename") filename : String) : String
  
  @POST
  @Path("/update/query")
  def update(@Context sc : SecurityContext, @FormParam("repository") repository : String, @FormParam("filename") filename : String) : String
  
}