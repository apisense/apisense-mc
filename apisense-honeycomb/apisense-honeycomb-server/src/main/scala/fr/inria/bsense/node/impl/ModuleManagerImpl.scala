/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.impl

import javax.servlet.Servlet
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.node.cbse.Experiment
import fr.inria.bsense.node.service.{ ModuleManagerService, ExperimentModuleService }
import org.osoa.sca.annotations.Property

class ModuleManagerImpl extends ModuleManagerService {

  @Reference(name = Experiment.SERVICE_MODULE)
  val modules: java.util.List[ExperimentModuleService] = null
  
  @Property(name=Experiment.PROPERTY_EXPERIMENT_NAME)
  var _experimentName : String = null

  
  def getExperimentName = _experimentName
  
  def getModuleNames: Array[String] = {
    for { i <- modules.toArray }
      yield i.asInstanceOf[ExperimentModuleService].getModuleName
  }

  def getModuleURI(name: String): String = {
    var r: String = null
    modules.toArray.foreach {
      case i: ExperimentModuleService =>
        if (i.getModuleName equals name)
          r = i.getModuleURI
    }
    r
  }


  def getModules(): java.util.List[ExperimentModuleService] = modules

}