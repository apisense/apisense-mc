/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.cbse

import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.cbse.DatabaseComposite
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.node.impl.DatabaseProxyRestImpl
import fr.inria.bsense.node.impl.ExperimentManagerImpl
import fr.inria.bsense.node.impl.UserManagerImpl
import fr.inria.bsense.node.service.DatabaseProxyRestService
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.service.UserManagerService
import frascala.frascati.REST
import frascala.frascati.SCA
import frascala.sca.Bean
import frascala.sca.Java
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.apisense.token.TokenManager
import ServerNode._
import fr.inria.apisense.token.TokenManagerComposite
import fr.inria.apisense.token.ImplTokenManagerMap
import fr.inria.apisense.intent.BindingIntent
import frascala.sca.COMPOSITE


//-------------------------------------------------------------------------
//--- Static variable definition

object ServerNode {

  /* main composite name */
  final val COMPOSITE_NAME : String = "apisense.ServerNode"

  final val scaUrlDBQuery = SCA(DatabaseComposite.COMPOSITE_NAME+"/"+DatabaseComponent.SERVICE_QUERY)

  final val scaUrlDBUpdate = SCA(DatabaseComposite.COMPOSITE_NAME+"/"+DatabaseComponent.SERVICE_QUERY_UPDATE)

  final val scaUrlUserManager = SCA(COMPOSITE_NAME+"/srv-user-manager")

  final val SERVICE_MANAGER_NAME = "srv-experiment-manager"
}

//-------------------------------------------------------------------------
//--- Composite definition

/**
 *
 * === Server Node Composite ===
 *
 *
 */
case class ServerNodeComposite extends BSenseComposite(ServerNode.COMPOSITE_NAME){


  val authentication = component(BindingIntent.authenticationTokenCpt)
  val authorization  = component(BindingIntent.authorizationCpt)

  authorization rule("/user-manager/*",Array("ROLE_ANONYMOUS","ROLE_SCIENTIST","ROLE_ADMIN"))
  authorization rule("/user-manager/add/user/*",Array("ROLE_ADMIN"))
  authorization rule("/user-manager/delete/username$",Array("ROLE_ADMIN"))
  authorization rule("/experiment-manager/*",Array("ROLE_SCIENTIST"))
  authorization rule("/experiment-manager/central/*",Array("ROLE_ANONYMOUS"))
  authorization rule("/database-proxy/*",Array("ROLE_ADMIN"))

  val cptUserManager = component(UserManagerComponent())
  cptUserManager.srvUserManager as(REST("/user-manager"),Array(
		  authorization,
		  authentication
  ))
  promoteService(cptUserManager.srvUserManager)

  val cptExperimentManager = component(ExperimentManagerComponent())
  cptExperimentManager.srvExperimentManager as(REST("/experiment-manager"),Array(
		  authorization,
		  authentication
  ))

  val cptDatabaseProxy = component(DatabaseRestProxy());
  cptDatabaseProxy.serQuery as(REST("/database-proxy"),Array(
		  authorization,
		  authentication
  ))

  promoteService(cptUserManager.srvUserManager)
  promoteService(cptExperimentManager.srvExperimentManager)

}

case class ServerTokenManagerComponent extends TokenManagerComposite[ImplTokenManagerMap]

//-------------------------------------------------------------------------
//--- Component definition

/**
 * === User Manager Component ===
 *
 */
case class UserManagerComponent extends BSenseComponent("UserManager"){
  this.uses(Bean[UserManagerImpl])


  val srvUserManager = service("srv-user-manager") exposes Java[UserManagerService]


  reference(DatabaseComponent.SERVICE_QUERY) exposes Java[IDBQueryService] as scaUrlDBQuery

  reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService] as scaUrlDBUpdate

  reference(TokenManager.TOKEN_MANAGER_SERVICE) exposes Java[TokenServiceManager] as TokenManager.getAddress

}

/**
 * === Experiment Manager Component ===
 *
 */
case class ExperimentManagerComponent extends BSenseComponent("ExperimentManager"){
  this.uses(Bean[ExperimentManagerImpl])

  val srvExperimentManager = service(SERVICE_MANAGER_NAME) exposes Java[ExperimentManagerService]

  reference(DatabaseComponent.SERVICE_QUERY) exposes Java[IDBQueryService] as scaUrlDBQuery

  reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService] as scaUrlDBUpdate

}

/**
 * === SCA Component ===
 *
 */
case class DatabaseRestProxy extends BSenseComponent("DatabaseRestProxy"){
  this.uses(Bean[DatabaseProxyRestImpl]);

  val serQuery = service("srv-query-proxy") exposes Java[DatabaseProxyRestService]

  reference(DatabaseComponent.SERVICE_QUERY) exposes Java[IDBQueryService] as scaUrlDBQuery

  reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService] as scaUrlDBUpdate

}




