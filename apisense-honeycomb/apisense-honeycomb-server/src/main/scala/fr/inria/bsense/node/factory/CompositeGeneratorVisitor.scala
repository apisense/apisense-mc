/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.factory

import fr.inria.sspl.visitor.FeatureDiagramVisitor
import fr.inria.sspl.model._
import fr.inria.bsense.common.cbse.BSRuntime
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.common.utils.Log


/**
 * 
 * 
 * Feature Diagram Visitor
 * 
 * 
 * 
 */
class CompositeGeneratorVisitor extends FeatureDiagramVisitor{

  implicit var currentComposite : BSenseComposite = null
  
  var composites = List[BSenseComposite]() 
  
  def visit( node : feature){
    
    if (node.cardinality <= 0) return
    
    val attComposite = node._attributesMap.find{att => att._type.equals("bsense-composite")}
    if (attComposite != None){
    
      currentComposite = createBSenseComposite(attComposite.get)
      composites =  currentComposite :: composites
      
    }
    else{
      
      val attComponent = node._attributesMap.find{att => att._type.equals("bsense-component")}
      if (attComponent != None){
        
         val component = createBSenseComponent(attComponent.get)
         
         // add all sca properties in component
         val attProperties =  node._attributesMap.filter{att => att.name.equals("sca-property")}
         createProperties(attProperties, component)
         
      }
      
    }
  }
  
  def createProperties[String](atts : List[attribute], component : BSenseComponent){
    atts.foreach{
      att =>   
        att._type match{
          case "string"  => component.setProperty(att.name,att._value.toString)
          case "int"     => component.setProperty(att.name,att._value.asInstanceOf[Int])
          case "double"  => component.setProperty(att.name,att._value.asInstanceOf[Double])
          case "float"   => component.setProperty(att.name,att._value.asInstanceOf[Float])
          case "boolean" => component.setProperty(att.name,(att._value equals "true"))
        }
    }
  }
  
  def createBSenseComponent(att : attribute)(implicit composite : BSenseComposite) = {
    
    Log.i("generate component "+att._value)
    val component =  BSRuntime.runtimeClassLoader.loadClass(att._value.toString).newInstance().asInstanceOf[BSenseComponent]
    
    Log.i("add component "+att._value+" in "+composite.name)
    composite component component
  }
  
  def createBSenseComposite(att : attribute) = {
    Log.i("generate composite "+att._value)
    try{
    	BSRuntime.runtimeClassLoader.loadClass(att._value.toString).newInstance().asInstanceOf[BSenseComposite]
    }
    catch{
      case e : Exception => BSRuntime.runtimeClassLoader.loadClass(att._value.toString).getConstructor(classOf[String])
    		  .newInstance(att.name).asInstanceOf[BSenseComposite]
    }
  }
  
  
  
  def visit( node : vp){}
  def visit( node : variant){}
  def visit( node : andvp){}
  def visit( node : orvp){}
  def visit( node : xorvp){}
  def visit( node : FeatureDiagram){}
  def visit( node : FeatureNode){}
}




