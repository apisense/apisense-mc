/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node

import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.DatabaseClientSession
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.db.api.ConfigurationDB
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.node.configuration.NodeConfigurationDB


trait FunctionalityDatabase {

  @Reference(name=DatabaseComponent.SERVICE_QUERY)
  val queryService : IDBQueryService = null

  @Reference(name=DatabaseComponent.SERVICE_QUERY_UPDATE)
  val updateService : IDBQueryUpdateService = null

  final val session : DatabaseClientSession = {
    val s = new DatabaseClientSession
    s.setHost(NodeConfigurationDB host)
    s.setPort(NodeConfigurationDB port)
    s.setUsername(NodeConfigurationDB user)
    s.setPassword(NodeConfigurationDB password)
    s
  }

  def update(request : => Unit) = {
    try {
    	request
    }
    catch{
      case exception : Exception =>

        throw exception
    }

  }

  def query( request : => Unit) = {
	  try{
		  request
	  }
	  catch{
	    case exception : Exception =>

	      throw exception
	  }
  }

  def exec(parse : Boolean)(body: => Any): Any = {
    import fr.inria.bsense.common.utils._

    try {

      queryService.beginConversation
      queryService.createSession(session)

      return body

    } catch { case e : Throwable =>
      e.printStackTrace()
      ""

    }
    finally {
      try {
    	  queryService.endSession
      }
      catch { case e : Exception=> e.printStackTrace() }
    }
  }

  def exec(body: => Any): String = {
    import fr.inria.bsense.common.utils._

    try {

      queryService.beginConversation
      queryService.createSession(session)

      return Message.success(body)

    } catch { case e : Exception =>
      e.printStackTrace()

      var cause = e.getCause()
      if (cause != null){
        cause = cause.getCause()
        if (cause != null){
        	return Message.exception(cause)
        }else return Message.exception(e)
      }else return Message.exception(e)

    }
    finally {
      try {
    	  queryService.endSession
      }
      catch { case e : Exception=> e.printStackTrace() }
    }
  }

}