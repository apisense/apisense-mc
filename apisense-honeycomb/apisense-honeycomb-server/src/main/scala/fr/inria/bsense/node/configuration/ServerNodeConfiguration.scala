/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.configuration

import fr.inria.bsense.common.utils.Configuration
import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.sspl.model.FeatureDiagram
import fr.inria.apisense.client.APISENSEClient

object NodeConfigurationDB extends fr.inria.bsense.db.api.ConfigurationDB(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY)

object ServerNodeConfiguration extends Configuration("fr.inria.bsense.node.path","serverNode.properties"){

  final val CONFIGURATION_NODE_PROPERTY = "fr.inria.bsense.node.path";

  default("central.server.url","http://localhost:18001")
  default("honeycomb.base.url","http://localhost:18000")

  def username = get("central.server.username",null)
  def password = get("central.server.password",null)
  def username(value : String)  { put("central.server.username",value);store}
  def password(value : String)  { put("central.server.password",value);store}
  def getUrl = get("central.server.url")
  def setUrl(url : String) { put("central.server.url",url); store }
  def getHoneyBaseUrl = get("honeycomb.base.url")
  def honeyBaseUrl(value : String) = {put("honeycomb.base.url",value);store}

  def createHiveConnection = {

    val hiveService = APISENSEClient.hive
    hiveService.host(getUrl)
    hiveService.connect(username, password)
    hiveService
  }

}

case class NodeFileConfiguration(str : String) extends
		FileConfiguration(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY,str){}

object SPLConfiguration extends FileConfiguration(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY,"spl"){

  def saveSPL(featureDiagram : FeatureDiagram){
    import fr.inria.sspl.dsl.SPL._

    this.updateFile("spl.fd",featureDiagram asXMLString)
  }

  def getSPL = this.getFile("spl.fd")
}