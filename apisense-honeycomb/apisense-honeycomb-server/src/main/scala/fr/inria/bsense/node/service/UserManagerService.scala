/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.service

import org.json.JSONObject
import fr.inria.bsense.node.impl.UserManagerException
import javax.ws.rs.Path
import javax.ws.rs.POST
import javax.ws.rs.Consumes
import javax.ws.rs.FormParam
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context
import fr.inria.apisense.security.UserSecurityContext



object UserManagerService{
  final val ROLE_ADMIN = "ROLE_ADMIN"
  final val ROLE_SCIENTIST  = "ROLE_SCIENTIST"
  final val DATABASE_USER = "Hive_Users"
}

trait UserManagerService {

  /**
   * 
   * Create a new user
   * 
   * @param fullname Full name of the user
   * @param username Login of the user.
   * @param password Password of the user
   * @param role Role of the user
   * 
   * @return Unique id of the user
   *  
   * @throws Exception if ''username'' is not unique
   */
  def createUser(fullname : String, username : String, password : String, email : String, role : String) : String
  
  
  /**
   * Create an administrater user
   * @param fullname Full name of the user
   * @param username Login of the user. An Exception is throw if the `username` already exist
   * @param password Password of the user
   * @param role Role of the user
   * @return Unique id of the user 
   */
  @POST
  @Path("/add/user/admin")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def createAdminUser(
      @FormParam("fullname") fullname : String,
      @FormParam("username") username : String,
      @FormParam("password") password : String,
      @FormParam("email")    email    : String) : String;
  
  /**
   * Create a scientist user
   * @param fullname Full name of the user
   * @param username Login of the user. An Exception is throw if the `username` already exist
   * @param password Password of the user
   * @param role Role of the user
   * @return Unique id of the user 
   */
  @POST
  @Path("/add/user/scientist")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def createScientistUser(
      @FormParam("fullname") fullname : String,
      @FormParam("username") username : String,
      @FormParam("password") password : String,
      @FormParam("email")    email    : String) : String
  
  
  
  /**
   * Drop user 
   * @param username Login of the user to drop
   */
  @POST
  @Path("/delete/user")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def dropUser(@Context sc : SecurityContext) : String
  
  @POST
  @Path("/update/password")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def changePassword(@Context sc : SecurityContext,
      @FormParam("oldPassword") oldPassword : String,
      @FormParam("newPassword") newPassword : String,
      @FormParam("newPasswordCheck") newPasswordCheck : String
  ) : String
  
  
  /**
   * Drop user 
   * @param username Login of the user to drop
   */
  @POST
  @Path("/delete/username")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def dropUser(@Context sc : SecurityContext, @FormParam("username") username : String) : String
  
  
  /**
   * 
   * Return user node 
   * 
   * @param userId node id
   * @return user node
   */
  @POST
  @Path("/get/user")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def getUser(@Context sc : SecurityContext) : String
  
  
  def getUser(token : String) : UserSecurityContext
  
  @POST
  @Path("/get/users")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def getUsers(): String 
  
  @POST
  @Path("get/token")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def connect(@FormParam("username") username : String, @FormParam("password") password : String) : String
    
  @POST
  @Path("update/disconnect")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def disconnect(@Context sc : SecurityContext) : String 
}







