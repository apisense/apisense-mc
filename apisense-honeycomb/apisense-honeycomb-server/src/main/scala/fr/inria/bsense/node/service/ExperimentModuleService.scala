/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.service

import org.osoa.sca.annotations.Property
import fr.inria.bsense.node.cbse.Experiment

trait ExperimentModuleService {
  
  @Property(name="experimentName") var experimentName : String = null

  @Property(name=Experiment.PROPERTY_MODULE_NAME)
  var _moduleName : String = null
  
  @Property(name=Experiment.PROPERTY_MODULE_URI)
  var _moduleURI : String = null
   
  def getModuleName : String = _moduleName
  def getModuleURI : String = _moduleURI
  
}

case class ExperimentModuleImpl extends ExperimentModuleService