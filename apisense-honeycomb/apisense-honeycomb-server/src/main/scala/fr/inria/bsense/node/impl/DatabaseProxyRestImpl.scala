/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.impl

import fr.inria.bsense.node.service.DatabaseProxyRestService
import fr.inria.bsense.node.FunctionalityDatabase
import fr.inria.bsense.common.utils.FileConfiguration
import org.apache.commons.codec.binary.Base64
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import javax.ws.rs.core.SecurityContext

class DatabaseProxyRestImpl extends DatabaseProxyRestService with FunctionalityDatabase{
  
  
 
  def query(sc : SecurityContext,  repository : String, filename : String) : String = {
    
    val authorId = sc.getUserPrincipal().getName()
    
    val query =  new String(Base64.decodeBase64(NodeFileConfiguration("users/"+authorId+"/"+repository).getFile(filename)));
    exec{
      queryService.query(query);
    }
  }
  
  
  def update(sc : SecurityContext, repository : String, filename : String) : String = {
    
    val authorId = sc.getUserPrincipal().getName()
    
    val query =  new String(Base64.decodeBase64(NodeFileConfiguration("users/"+authorId+"/"+repository).getFile(filename)))
    exec{
    	updateService.query(session, query);
    	""
    }
  }
  
}