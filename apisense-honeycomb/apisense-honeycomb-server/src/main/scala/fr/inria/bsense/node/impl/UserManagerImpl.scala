/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.impl

import org.osoa.sca.annotations.Init
import org.osoa.sca.annotations.Reference
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.common.utils.MD5
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.db.api.field
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.value
import fr.inria.bsense.node.FunctionalityDatabase
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.db.api.ValuePair
import fr.inria.apisense.token.TokenManager
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.apisense.security.APISecureContext
import javax.ws.rs.core.SecurityContext
import fr.inria.apisense.intent.cxf.InvalidTokenException
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.bsense.common.utils.Log

case class UserManagerException(_message: String) extends Exception(_message)

class UserManagerImpl extends UserManagerService with FunctionalityDatabase with LifeCycleService {

  @Reference(name = TokenManager.TOKEN_MANAGER_SERVICE)
  var tokenService: TokenServiceManager = null


  def onStart(c : BSenseComposite) {

    exec {


      updateService.createIfNotExist(session, UserManagerService.DATABASE_USER)

      // check if admin account is created else create it
      if (!queryService.existsCollection(UserManagerService.DATABASE_USER, "admin")) {
        val doc = generateUserDocument("admin", "admin", MD5.hash("admin"), "admin@email.com", UserManagerService.ROLE_ADMIN)
        updateService.addDocument(session, UserManagerService.DATABASE_USER, "admin", doc)
      }

    }
  }

  def onStop(){}

  def createAdminUser(fullname: String, username: String, password: String, email: String) = {
    this.createUser(fullname, username, password, email, UserManagerService.ROLE_ADMIN)
  }

  def createScientistUser(fullname: String, username: String, password: String, email: String) = {
    this.createUser(fullname, username, password, email, UserManagerService.ROLE_SCIENTIST)
  }

  def createUser(fullname: String, username: String, password: String, email: String, role: String) = exec {

    // exception if user already exist
    if (queryService.existsCollection(UserManagerService.DATABASE_USER, username))
      throw UserManagerException("User " + username + " already exist")

    // create user node
    val doc = generateUserDocument(fullname, username, MD5.hash(password), email, role)

    // add document in database
    updateService.addDocument(session, UserManagerService.DATABASE_USER, username, doc toString)

  }

  def dropUser(sc : SecurityContext) = exec {

      val userId = sc.getUserPrincipal().getName()

	  val userXml = queryService.getDocument(UserManagerService.DATABASE_USER, userId)
      val username = userXml \\ "username" text;
      println("delete user "+username)
      updateService.dropCollection(session,
          UserManagerService.DATABASE_USER, username)

  }

   def dropUser(sc : SecurityContext, username : String) = exec {

	   updateService.dropCollection(session,
	       UserManagerService.DATABASE_USER, username)
  }

  def getUser(sc : SecurityContext): String = exec {

    val userId = sc.getUserPrincipal().getName()
    // TODO check if result is a node user
    queryService.getDocument(UserManagerService.DATABASE_USER, userId)
  }

  def getUser(token : String) = {

    val principal = tokenService.getUserPrincipal(token)
    if (principal == null)
      throw new InvalidTokenException

    principal
  }

  def changePassword( sc : SecurityContext,oldPassword : String, newPassword : String, newPasswordCheck : String) : String = exec{

    val md5Old = MD5.hash(oldPassword)
    val md5New = MD5.hash(newPassword)

    val userId = sc.getUserPrincipal().getName()

    val userDoc = queryService.getDocument(UserManagerService.DATABASE_USER, userId)

    val userName = userDoc \ "username" text
    val userPassword = userDoc \ "password" text

    if (!(newPassword.equals(newPasswordCheck))){
      throw new Exception("Passwords are not identical")
    }

    if (!(md5Old.equals(userPassword)))
      throw new Exception("Password not correct")

    updateService.set(session, UserManagerService.DATABASE_USER, userName, path("user"),Array(new ValuePair("password",md5New)))

  }

   def getUsers(): String = exec {

    // TODO check if result is a node user
    queryService.getDocuments(UserManagerService.DATABASE_USER, path("user"))
  }

  def connect(username: String, password: String): String = exec {

    // verify in database if a collection
    // with username name exist
    //if (!queryService.existsCollection(UserManagerService.DATABASE_USER, username))
    //  throw UserManagerException("User " + username + " not exist")

    // Search user id in database with good username and password
    val userId = queryService.getDocumentId(
        UserManagerService.DATABASE_USER,
        username,
        path("user") withs field("password") == value(password))

    if (userId.length == 0)
      throw UserManagerException("Bad Authentication")

    val role = queryService.getDocument(
		    UserManagerService.DATABASE_USER, username,path("role")) text;


    // create new security context for user connection
	// with associate roles
	val apisecure = new APISecureContext(
	    userId(0),
	    username,
	    Array(role)
	)

    // register connection security context
	// in token manager
	this.tokenService.createToken(apisecure)
  }

  def disconnect(sc : SecurityContext) : String = { sc match {

  		case user : APISecureContext =>

  		  tokenService.removeUserPrincipal(user.token)

  		  Message.success("disconnected")

  		case _ =>

  		  Log.w("Cannot cast security context to "+classOf[APISecureContext].getName())

  		  Message.exception("DisconnectionException", "")
     }
  }

  private def generateUserDocument(fullname: String, username: String, password: String, email: String, role: String): String = {
    val doc = new StringBuilder
    doc append "{ 'user' : "
    doc append "{"
    doc append " 'fullname' : '" + fullname + "',"
    doc append " 'username' : '" + username + "',"
    doc append " 'password' : '" + password + "',"
    doc append " 'email' : '" + email + "',"
    doc append " 'role' : '" + role + "'"
    doc append "}}"
    doc toString
  }

}









