/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.factory

import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.sspl.model.FeatureDiagram


object CBSEFactory {
  import fr.inria.sspl.model.sliceNode
   
  def generate(view : sliceNode){
    
    val visitor = new CompositeGeneratorVisitor
    view accept visitor
    
    visitor.composites.foreach{
      composite =>
        composite.save
    }
    
    startComposite(visitor.composites)
  }
  
  def startComposite(composites : List[BSenseComposite]){
    
    var compositesBis = composites ::: List()
    var exception : Exception = null
   
    val size = compositesBis.length
    composites.foreach{
      composite =>
        
        try{
        	composite.start
        	compositesBis = compositesBis.filter{ el => (!(el.name equals composite.name)) }
        	
        }
        catch{
          case e : Exception => exception = e
        }
    }
    
    if (compositesBis.length != 0){
      if (compositesBis.length == size) throw exception
      startComposite(compositesBis)
    }
    
  }
  
  
  
}