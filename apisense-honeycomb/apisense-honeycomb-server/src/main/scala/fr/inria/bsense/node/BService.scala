/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node

import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.common.utils.MD5
import org.apache.http.message.BasicNameValuePair
import org.apache.http.NameValuePair
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.Http
import fr.inria.bsense.common.utils.JSONLiftNodeParser


/**
 * Constant URL of Bee.sense central server
 * services
 */
object BService {

  final val PING_SERVICE = "public/ping"
  final val CONNECT_SERVICE = "user/get/token"
  final val CREATE_EXPERIMENT_SERVICE = "store/scientist/add/experiment"
  final val DELETE_EXPERIMENT_SERVICE = "store/scientist/delete/experiment"
  final val UPDATE_EXPERIMENT_CONFIGURATION_SERVICE = "store/scientist/update/description"
  final val UPDATE_EXPERIMENT_SCRIPT_SERVICE = "store/scientist/update/experiment"

}


case class BService{
  
  val http = new HttpRequest
  var token : String = null
 
  
  def connect = {
    
   val BSenseHOST      = ServerNodeConfiguration.getUrl
   val BSenseUsername  = ServerNodeConfiguration.username
   val BSensePassword  = MD5.hash(ServerNodeConfiguration.password)
    
   token = request{ 
     http.post(BSenseHOST+"/"+BService.CONNECT_SERVICE, Array(
      new BasicNameValuePair("username",BSenseUsername),
      new BasicNameValuePair("password",BSensePassword)))
   }
   
   Log.d("connection to central server "+token)
   
   this
  }
  
  
  def connect(username : String, password : String) = {
    
    val BSenseHOST      = ServerNodeConfiguration.getUrl
    
    token = request{ 
       http.post(BSenseHOST+"/"+BService.CONNECT_SERVICE, Array(
        new BasicNameValuePair("username",username),
        new BasicNameValuePair("password",MD5.hash(password))))
    }
    
    Log.d("connection to central server "+token)
    
    this
  }
  
  def service(serviceName : String, params : Map[String,String]) = {
    
    if (token == null)
      throw new Exception("Connection token is null")
    
    val url = ServerNodeConfiguration.getUrl+"/"+serviceName
    
    Log.i("call central service "+url+" with params "+params)
    
    request{
     Http post(url) param params header(Map("TOKEN" -> token)) asString
    }
  
  }
  
  def service(serviceName : String, data : Array[Byte]){
    
    if (token == null)
      throw new Exception("Connection token is null")
    
    val BSenseHOST      = ServerNodeConfiguration.getUrl
    
    val url = BSenseHOST+"/"+serviceName
    
    Log.d("call central service "+url)
    
    request{
      http.addHeader("TOKEN",token)
      http.postData(url, data)
    }
    
  }
  
  def disconnect = this
  
  private def request( body :  => String) = {
    
    val r = new JSONLiftNodeParser(body)
    
    if (r.label.equals("error")){
      var message = r \\ "message" text;
      throw new Exception("Bee.sense service error : "+message)
    }
    
    r text
  }
  
}