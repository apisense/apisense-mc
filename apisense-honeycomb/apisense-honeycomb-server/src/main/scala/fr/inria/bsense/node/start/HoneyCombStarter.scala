/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.start

import fr.inria.bsense.node.factory.CBSEFactory._
import fr.inria.bsense.node.configuration.SPLConfiguration
import org.basex.BaseXServer
import fr.inria.bsense.node.configuration.NodeConfigurationDB
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import java.io.FileInputStream
import java.io.File
import fr.inria.bsense.common.utils.IoUtils
import java.io.FileOutputStream
import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.bsense.common.utils.FileUtil
import org.basex.core.Context
import org.basex.core.MainProp
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.cbse.BSRuntime
import fr.inria.bsense.node.cbse.ServerNode
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.cbse.ExperimentManagerComponent
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.apisense.APISENSE
import fr.inria.apisense.webserver.SockoWebserver
import org.mashupbots.socko.webserver.WebServerConfig

object HoneyCombStarter {


  def start(features : Array[String]){
    import fr.inria.sspl.dsl.SPL.feat2dsl
    import fr.inria.sspl.dsl.SPL.load
    import fr.inria.sspl.dsl.SPL.slice2dsl

    val home = System.getProperty("user.home")

    val sockoConfiguration = new WebServerConfig()
    APISENSE.webserver = new SockoWebserver(sockoConfiguration)
    APISENSE.webserver.start


    System.setProperty(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY,home+"/BEES_HIVE")



    NodeConfigurationDB.put(NodeConfigurationDB.PROPERTY_PORT,"1985")
    NodeConfigurationDB.put(NodeConfigurationDB.PROPERTY_PORTS,"1984")
    NodeConfigurationDB.store;

    val dbpath = home+"/BEES_HIVE/BaseXData";


    System.setProperty("org.basex.path",dbpath)
    System.setProperty("org.basex.TIMEOUT",(1000*60*5).toString)

    val bx = new BaseXServer("-p1985","-e1984")

    val featureDiagram = load resources "Bsense-Feature"


    val nodeView = featureDiagram slice "Sensing Node" named "nodeView"
    nodeView select("TokenManager",1)
    nodeView select("Storage",1)
    nodeView select("BaseX",1)
    nodeView select("Server",1)
    features.foreach{feature => nodeView select(feature,1)}

    featureDiagram merge nodeView;

    SPLConfiguration.saveSPL(featureDiagram)

    generate(nodeView.node)

    // get experiment manager
    val manager = BSRuntime.getComponent(ServerNode.COMPOSITE_NAME).>[ExperimentManagerService](ServerNode.SERVICE_MANAGER_NAME)
    manager.startExperiments
  }

  def main(args: Array[String]) { HoneyCombStarter.start(args) }

}