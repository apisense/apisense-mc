/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.impl

import org.osoa.sca.annotations.Init
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.common.cbse.BSRuntime
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.db.api.field
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.value
import fr.inria.bsense.node.FunctionalityDatabase
import fr.inria.bsense.node.service.ExperimentManagerService
import net.liftweb.json.JsonParser
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.common.utils.MD5
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.node.BService
import org.apache.http.message.BasicNameValuePair
import fr.inria.sspl.model.sliceNode
import fr.inria.sspl.dsl.SPL._
import fr.inria.bsense.node.factory.CBSEFactory._
import fr.inria.bsense.node.configuration.SPLConfiguration
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import fr.inria.bsense.node.cbse.Experiment
import fr.inria.bsense.db.api.ValuePair
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.utils.XMLNodeParser
import scala.xml.XML
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.FileConfiguration
import java.io.File
import fr.inria.bsense.common.utils.NodeParser
import javax.ws.rs.core.Response
import fr.inria.apisense.client.APISENSEClient
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import net.liftweb.json.JsonAST.JField
import net.liftweb.json.JsonAST.JString
import fr.inria.bsense.common.utils.LiftWebNode
import fr.inria.bsense.common.utils.Node




@Scope("COMPOSITE")
class ExperimentManagerImpl extends ExperimentManagerService with FunctionalityDatabase with LifeCycleService{



  /**
   * Initialization method, executed once when composite is loaded.
   * This method check if experiment database and association database exist
   * Create database if not exists.
   */
  def onStart(cpt : BSenseComposite){

    exec {


      updateService.createIfNotExist(session, Experiment.DATABASE_EXPERIMENTS)

      // Check if Experiment -> Users database exist
      if (!queryService.exists(Experiment.DATABASE_OWNER))
        updateService.create(session, Experiment.DATABASE_OWNER)

        queryService.query(""" repo:install("http://metronet1.inrialpes.fr/xquery/functx-1.0.xar") """)
        queryService.query(""" repo:install("http://metronet1.inrialpes.fr/xquery/apisense-1.0.xar")  """)
    }
  }

  def onStop(){}


  def startExperiments(){

    var noStartedExperiments = List[File]()

    NodeFileConfiguration("started").getFiles.foreach{
      file =>
        try{

          _startExperiment(file.getName())

        }catch{ case e : Throwable =>
          Log.e(e)
          noStartedExperiments = file :: noStartedExperiments
        }
    }

    noStartedExperiments.foreach{file => file.delete() }
  }

  def createExperiment(sc : SecurityContext, name: String, niceName : String,  description: String, copyright: String, splConfiguration: String): String = exec {

	  val authorId = sc.getUserPrincipal().getName()

      // check if experiment already exist
      if (queryService.existsCollection(Experiment.DATABASE_EXPERIMENTS, name))
        throw new Exception("Experiment " + name + " already exist")


      val fd = load featureDiagram  net.liftweb.json.Xml.toJson(XML.loadString(
			  SPLConfiguration.getSPL
	  ))

      val view = fd slice "Experiment"
      view._feature._attributesMap.find(att => att._type equals "bsense-composite").get.setName(name)

      val json = new JSONLiftNodeParser(splConfiguration)
      json.toXMLObject.foreach{ el =>
       view select(el.label,el.text.toInt)
      }

      fd merge view
      // TODO Valid configuration

      // create a new Experiment node
      val doc = new StringBuilder
      doc append """ { "experiment" : """
      doc append "{"
      doc append " \"name\" : \"" + name + "\","
      doc append " \"niceName\" : \"" + niceName + "\","
      doc append "\"uuid\" :  \"" + java.util.UUID.randomUUID.toString + "\" ,"
      doc append "\"type\" : \"unknown\","
      doc append "\"version\" : \"1.0\","
      doc append "\"remoteState\" : \""+LIFE_CYCLE_STOP+"\","
      doc append "\"visible\" : false,"
      doc append "\"description\" : \"" + description + "\","
      doc append "\"copyright\" : \"" + copyright + "\""
      doc append "}}"


      val jsonNode = new JSONLiftNodeParser(doc.toString)

      val bservice = BService()
      bservice.connect
      bservice.service(
          BService.CREATE_EXPERIMENT_SERVICE,
          Map("description" -> doc.toString)
      )
      bservice.disconnect

      updateService.addDocument(session, Experiment.DATABASE_EXPERIMENTS, name, doc toString)

      // create association between author and experiment
      val as = new StringBuilder
      as append "{ 'association' :"
      as append "{"
      as append "'authorId' : '" + authorId + "',"
      as append "'experimentId' : '" + queryService.getDocumentId(Experiment.DATABASE_EXPERIMENTS, name) + "'"
      as append "}}"
      updateService.addDocument(session, Experiment.DATABASE_OWNER, name, as toString)

      // Save Feature Diagram view in local storage
      NodeFileConfiguration("experiment/"+name).updateFile("spl.fd",view.asXMLString)


      // create database
      updateService.create(session, name)
      updateService.addDocument(session, name, "registration", "{ \"registration\" : {} }")

  }

  /* Check if an experiment name already exist */
  def validExperimentName(name : String) = exec { (!queryService.existsCollection(Experiment.DATABASE_EXPERIMENTS, name)) }

  def validExperimentConfiguration( features : String ) = exec {

    val fd = load featureDiagram net.liftweb.json.Xml.toJson(XML.loadString(
			  SPLConfiguration.getSPL
	))
    val view = fd slice "Experiment"

    val json = new JSONLiftNodeParser(features)
    json.toXMLObject.foreach{ el =>
       view select(el.label,el.text.toInt)
    }

    fd merge view

    //TODO Valid configuration
  }



  def updateExperimentConfiguration(sc : SecurityContext, name: String, splConfiguration : String): String = exec {

    val fd = load featureDiagram net.liftweb.json.Xml.toJson(XML.loadString(
			  SPLConfiguration.getSPL
	))

    val view = fd slice "Experiment"
    view._feature._attributesMap.find(att => att._type.equals("bsense-composite")).get.setName(name)

    val json = new JSONLiftNodeParser(splConfiguration)
    json.toXMLObject.foreach{ el =>
       view select(el.label,el.text.toInt)
    }

    fd merge view

    //TODO Valid configuration

    // Save Feature Diagram view in local storage
    NodeFileConfiguration("experiment/"+name).updateFile("spl.fd",view.asXMLString)
  }

  def getExperimentConfiguration(sc : SecurityContext, name: String): String = exec {

     val view = load view net.liftweb.json.Xml.toJson(XML.loadString(NodeFileConfiguration("experiment/"+name).getFile("spl.fd")))
     view.toXML
  }

  def getDefaultExperimentConfiguration(sc : SecurityContext) : String = exec{

    val fd = load featureDiagram net.liftweb.json.Xml.toJson(XML.loadString(SPLConfiguration.getSPL))
    val view = fd slice "Experiment"
    view.toXML
  }

  def dropExperiemnt(sc : SecurityContext, name: String): String = exec {

    val authorId = sc.getUserPrincipal().getName()

    autorization(authorId, name)

    if (BSRuntime.hasComponent(name))
      throw new ExperimentManagerException("Please stop experiment before drop it ")

    val bservice = BService()
    bservice.connect
    bservice.service(
        BService.DELETE_EXPERIMENT_SERVICE,
        Map("name" -> name)
    )
    bservice.disconnect

    //remove collection from main experiment database
    updateService.dropCollection(session,Experiment.DATABASE_EXPERIMENTS,name)

    updateService.dropCollection(session, Experiment.DATABASE_OWNER , name)

    updateService.drop(session, name)

    updateService.drop(session, name+"-*")

    //remove all experiments file
    NodeFileConfiguration("experiment/"+name).delete

  }

  def isRunning(sc : SecurityContext, name: String): String = exec {

    val authorId = sc.getUserPrincipal().getName()

    autorization(authorId, name)

    BSRuntime.hasComponent(name)
  }

  def startExperiment(sc : SecurityContext, name: String): String = exec {

    val authorId = sc.getUserPrincipal().getName()

    autorization(authorId, name)

    _startExperiment(name)
  }


  def _startExperiment(name : String) : String = {

    if (BSRuntime.hasComponent(name))
      throw ExperimentManagerException("Cannot start experiment " + name + "  : already running")

	val splConfiguration = NodeFileConfiguration("experiment/"+name).getFile("spl.fd")

	val view = load view net.liftweb.json.Xml.toJson(XML.loadString(NodeFileConfiguration("experiment/"+name).getFile("spl.fd")))

	generate(view)

	NodeFileConfiguration("started").updateFile(name,"")

	""
  }


  def stopExperiment(sc : SecurityContext, name: String): String = exec {
    import fr.inria.bsense.db.api._

    val authorId = sc.getUserPrincipal().getName()

    autorization(authorId, name)

    if (!(BSRuntime.hasComponent(name)))
      throw ExperimentManagerException("Cannot stop experiment " + name + "  : not running")

    BSRuntime.removeComponent(name)

    try{

       NodeFileConfiguration("started").deleteFile(name)

    }catch{
      case t : Throwable => Log.e(t)
    }

  }

  def getExperiments(sc : SecurityContext): String = {
    import fr.inria.bsense.db.api._

    val authorId = sc.getUserPrincipal().getName()

    val r = new org.json.JSONArray()
    exec {

      // Get all experiments associated with the author
      queryService.getDocumentId(Experiment.DATABASE_OWNER, path("association") withs field("authorId") == value(authorId)).foreach {
        id =>

          // Get experiment id
          val xpId = queryService.getDocument(Experiment.DATABASE_OWNER, id) \\ "experimentId" text

          // Get experiment object
          val xp = queryService.getDocument(Experiment.DATABASE_EXPERIMENTS, xpId)

          // Put experiment status
          val name = xp \\ "name" text

          r.put(new org.json.JSONObject(xp :< ("status", BSRuntime.hasComponent(name)) toJSONString))
      }
      r
    }
  }

  def getExperiment(sc : SecurityContext, name: String): String = {
    import fr.inria.bsense.db.api._
    import net.liftweb.json.JsonDSL._
    import net.liftweb.json._

    val authorId = sc.getUserPrincipal().getName()

    exec {

      autorization(authorId, name)

      queryService.getDocument(Experiment.DATABASE_EXPERIMENTS, name, path("experiment")) :< ("status", BSRuntime.hasComponent(name))

    }
  }

  def getExperimentInfo(sc : SecurityContext, name: String): Response = securedRequest(sc,name){

   var document : Node = NodeParser.createEmptyNode(name);

   val experimentNode : Node = queryService.getDocument(Experiment.DATABASE_EXPERIMENTS, name, path("experiment"));

   document = document.:<(queryService.getDocument(Experiment.DATABASE_EXPERIMENTS, name, path("experiment")));
   document = document.:<("participant",queryService.count(name,"registration", path("user")));
   document = document.:<("state",BSRuntime.hasComponent(name));


   var dbDocument : Node = NodeParser.createEmptyNode("db")

   queryService.getDocuments(name,path("dbtrack")).foreachChild{

     trackDb : Node =>

       dbDocument = dbDocument :< queryService.info(trackDb.text)
   }

   document = document :< dbDocument

   document
  }

  def getRegisteredUser(sc : SecurityContext, experimentName : String) = exec {

	 queryService.getDocument(experimentName, "registration",path("registration"))
  }

  /*
   * Authorized Service for Central Server
   *
   *
   */
  def unregisteredUser(sc : SecurityContext, experimentName : String, userId : String) = exec {


    updateService.deleteNode(session, experimentName, "registration", path("registration/user") withs field("userId") == value(userId))
  }

  /*
   * Authorized Service for Central Server
   *
   * Prevent a new deployment of an experiment version
   * to a mobile user
   */
  def registeredUser(sc : SecurityContext, experimentName : String, userId : String, experimentVersion : String) = exec {

	  val document = new StringBuffer
      document append """ { "user" : { """
      document append """"userId":"""
      document append "\""+userId+"\""
      document append ","
      document append """"version":"""
      document append "\""+experimentVersion+"\""
      document append "}}"

      if (!queryService.existsCollection(experimentName, "registration")){
        updateService.addDocument(session, experimentName, "registration","""{ "registration":{}}""")
      }

	  val queryResult =  queryService.getDocument(experimentName, "registration", path("registration") withs field("user/userId") == value(userId))
	  if (queryResult.isNull){

		  updateService.insert(session, experimentName, "registration", path("registration"),NodeParser.parJSON(document.toString()))
	  }else{

		 updateService.updateNode(session, experimentName, "registration",  path("registration/user") withs field("userId") == value(userId), document.toString())
	  }
  }



  def updateExperimentDescription( sc : SecurityContext,name : String,niceName : String, description : String,copyright : String ) : String = exec {

    val authorId = sc.getUserPrincipal().getName();

    updateService.set(session, Experiment.DATABASE_EXPERIMENTS, name,
    	path("experiment"),Array(
            new ValuePair("niceName",niceName),
            new ValuePair("description",description),
            new ValuePair("copyright",copyright)
        )
    )
  }

  val LIFE_CYCLE_START = "started"
  val LIFE_CYCLE_STOP = "stopped"
  val LIFE_CYCLE_FINISH = "finished"

  def remoteStart( sc : SecurityContext, name : String) : Response =  securedRequest(sc,name) {

	  if (BSRuntime.hasComponent(name)){

    	  "Experiment is already started"
      }

	  else  _startExperiment(name);
  }

  def remoteStop( sc : SecurityContext, name : String) : Response =  securedRequest(sc,name) {

	  if (BSRuntime.hasComponent(name)){

    	  BSRuntime.removeComponent(name)

    	  NodeFileConfiguration("started").deleteFile(name)

    	  _remoteNotAvailable(name)

    	  "Experiment Services Stopped"
      }

	  else "Experiment is not started"
  }

  def remoteAvailable( sc : SecurityContext, name : String) : Response = securedRequest(sc,name) {

    if (!BSRuntime.hasComponent(name)){

      throw new Exception("Cannot activate experiment : Services not Started");
    }

	var experimentDocument = queryService.getDocument(
    	      Experiment.DATABASE_EXPERIMENTS,
    	      name,
    	      path("experiment"));

	val experimentState  = experimentDocument \\ "remoteState" text;
	if (experimentState.equals(LIFE_CYCLE_START)){

		throw new Exception("Experiment is already started");
	}
	if (experimentState.equals(LIFE_CYCLE_FINISH)){

		throw new Exception("Cannot update experiment state : Experiment is finished");
	}

	var liftDocument = experimentDocument.toLiftJSONObject
	liftDocument = liftDocument.transform {

	  case JField("remoteState",JString(state)) => JField("remoteState",JString(LIFE_CYCLE_START))
	}

	// create connection with hive server
	val hive =  ServerNodeConfiguration.createHiveConnection;

	// put new experiment configuration

	val hiveRequestResult = hive.experimentStore.updateDescription(new LiftWebNode(liftDocument).toJSONString);

	// update experiment state in node database
	 updateService.set(
    	 session,
    	 Experiment.DATABASE_EXPERIMENTS,
    	 name,
    	 path("experiment"), Array( new ValuePair("remoteState",LIFE_CYCLE_START)));


	hive.disconnect

    hiveRequestResult
  }

  def remoteNotAvailable( sc : SecurityContext, name : String) : Response = securedRequest(sc,name) { _remoteNotAvailable(name) }

  private def _remoteNotAvailable( name : String) : Any =  {

    var experimentDocument = queryService.getDocument(
    	      Experiment.DATABASE_EXPERIMENTS,
    	      name,
    	      path("experiment"));

	val experimentState  = experimentDocument \\ "remoteState" text;
	if (experimentState.equals(LIFE_CYCLE_FINISH)){

	  throw new Exception("Cannot update experiment state : Experiment is finished");

	}

	if (experimentState.equals(LIFE_CYCLE_STOP)){

		throw new Exception("Cannot update experiment state : Experiment is already stopped");
	}

	var liftDocument = experimentDocument.toLiftJSONObject
	liftDocument = liftDocument.transform {

	  case JField("remoteState",JString(state)) => JField("remoteState",JString(LIFE_CYCLE_STOP))
	}

	// create connection with hive server
	val hive =  ServerNodeConfiguration.createHiveConnection;

	// put new experiment configuration
	val hiveRequestResult = hive.experimentStore.updateDescription(new LiftWebNode(liftDocument).toJSONString);

	// update experiment state in node database
	updateService.set(
    	 session,
    	 Experiment.DATABASE_EXPERIMENTS,
    	 name,
    	 path("experiment"), Array( new ValuePair("remoteState",LIFE_CYCLE_STOP)));

	hive.disconnect

	hiveRequestResult
  }


  def remoteDone( sc : SecurityContext,  name : String) : Response = securedRequest(sc,name) {

	val experimentDocument = queryService.getDocument(
    	      Experiment.DATABASE_EXPERIMENTS,
    	      name,
    	      path("experiment"));

	val experimentState = experimentDocument \\ "remoteState" text;

    throw new Exception("Not Implemented Yet")
  }




  //-----------------------------------------------
  //-----------------------------------------------
  //---
  //--- Private method
  //---
  //-----------------------------------------------
  //-----------------------------------------------

  /**
   * Verify if experiment exist
   * and if authorId is owner of experiment 'name'
   */
  def autorization(authorId: String, name: String) {

    // Check if experiment exist
    if (!(queryService.existsCollection(Experiment.DATABASE_EXPERIMENTS, name)))
      throw ExperimentManagerException("Cannot find experiment " + name)


    val nodeId = queryService.getDocumentId(Experiment.DATABASE_OWNER, name)

    var authorized = false

    val doc = queryService.getDocument(Experiment.DATABASE_OWNER, nodeId) \ "authorId"
    doc.foreach{ authorIdNode =>
		    //println("check "+authorIdNode.toXMLString+" "+authorId)
			if (authorIdNode.text.equals(authorId)) authorized = true
	}
  }

  /**
   * Verify if experiment exist
   * and if authorId is owner of experiment 'name'
   */
  def autorization(sc: SecurityContext, name: String) {

	  autorization(sc.getUserPrincipal().getName(), name);
  }

  def securedRequest(sc: SecurityContext, experimentName : String)(body: => Any): Response = {
    import fr.inria.bsense.common.utils._

    try {

      queryService.beginConversation

      queryService.createSession(session)

      autorization(sc, experimentName)

      return Message.resSucessJson(body)

    }
    catch {

    	case e : Exception => Log.e(e)

    	  var cause = e.getCause()
    	  if (cause != null){
    		  cause = cause.getCause()
    		  if (cause != null){
    			  return  Message.resSucess(Message.exception(cause))
    		  }else return Message.resSucess(Message.exception(e))
    	  }else return Message.resSucess(Message.exception(e))

    }
    finally {
      try {
    	  queryService.endSession
      }
      catch { case e : Exception=> Log.e(e) }
    }
  }

}

//-----------------------------------------------
//-----------------------------------------------
//---
//--- Exception definition
//---
//-----------------------------------------------
//-----------------------------------------------
case class ExperimentManagerException(_message: String) extends Exception(_message)



