/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.service

import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Response


trait ExperimentManagerService {


  @POST
  @Path("/add/xp")
  def createExperiment(
    @Context sc : SecurityContext,
    @FormParam("name") name: String,
    @FormParam("niceName") niceName: String,
    @FormParam("description") description: String,
    @FormParam("copyright") copyright: String,
    @FormParam("spl") splConfiguration: String): String

  @POST
  @Path("/get/xps")
  def getExperiments(@Context sc : SecurityContext): String

  @POST
  @Path("/get/xp")
  def getExperiment(@Context sc : SecurityContext, @FormParam("name") name: String): String


  @POST
  @Path("/update/xp/description")
  def updateExperimentDescription(
      @Context sc : SecurityContext,
      @FormParam("name") name : String,
      @FormParam("niceName") niceName : String,
      @FormParam("description") description : String,
      @FormParam("copyright") copyright : String) : String


  /**
   * Function call when server is started
   * Start all experiments started before server had stopped
   */
  def startExperiments()

  /**
   *
   * Drop Experiment
   *
   */
  @POST
  @Path("/delete/xp")
  def dropExperiemnt(@Context sc : SecurityContext, @FormParam("name") name: String): String

  /**
   *
   * Return true if Experiment composite running
   *
   */
  @POST
  @Path("/get/running")
  def isRunning(@Context sc : SecurityContext, @FormParam("name") name: String): String

   /**
   *
   * Start Experiment composite
   *
   */
  @POST
  @Path("/update/start")
  def startExperiment(@Context sc : SecurityContext, @FormParam("name") name: String): String

  /**
   *
   * Stop Experiment composite
   *
   */
  @POST
  @Path("/update/stop")
  def stopExperiment(@Context sc : SecurityContext, @FormParam("name") name: String): String


  /**
   *
   * Update Feature Diagram which define architecture
   * configuration of the Experiment
   *
   */
  @POST
  @Path("/update/configuration")
  def updateExperimentConfiguration(
      @Context sc : SecurityContext,
      @FormParam("name")name: String,
      @FormParam("spl")splConfiguration : String): String


  /**
   *
   * Return Feature Diagram which define architecture
   * configuration of the Experiment
   *
   */
  @POST
  @Path("/get/configuration")
  def getExperimentConfiguration(
      @Context sc : SecurityContext,
      @FormParam("name") name: String): String

  /**
   *
   * Return default Feature Diagram which define all
   * possible configurations of an experiment
   *
   */
  @POST
  @Path("/get/default/configuration")
  def getDefaultExperimentConfiguration(@Context sc : SecurityContext) : String


  /**
   * Return true if experiment name is not used
   */
  @POST
  @Path("/get/valid/name")
  def validExperimentName(@FormParam("name") name : String) : String

  /**
   * Return true if selected features
   * represent a valid configuration
   */
  @POST
  @Path("/get/valid/configuration")
  def validExperimentConfiguration( @FormParam("features") features : String ) : String

  /**
   * Return all registered users for a given experiment
   */
  @POST
  @Path("/get/regsitered")
  def getRegisteredUser(
      @Context sc : SecurityContext,
      @FormParam("name") experimentName : String) : String

  @POST
  @Path("/central/registration")
  def registeredUser(
      @Context sc : SecurityContext,
      @FormParam("name") experimentName : String,
      @FormParam("userId") userId : String,
      @FormParam("version") version : String) : String


  @POST
  @Path("/central/unregistration")
  def unregisteredUser(
      @Context sc : SecurityContext,
      @FormParam("name") experimentName : String,
      @FormParam("userId") userId : String) : String

  @POST
  @Path("/info")
  def getExperimentInfo(@Context sc : SecurityContext,@FormParam("name") name: String): Response

  // ************************************************************* LifeCycle Service

  /**
   *
   * Start all components services
   *
   * Experiment is not available yet for participants
   *
   * @param name Experiment Name
   */
  @POST
  @Path("/lifeCycle/remoteStart")
  def remoteStart(@Context sc : SecurityContext, @FormParam("name") name : String) : Response

  /**
   *
   * Stop all experiment services
   * All services all not available for configuration and
   * experiment is not availble for participants
   *
   * A push message is sent to mobile device to stop current experiment
   *
   * @param name Experiment Name
   */
  @POST
  @Path("/lifeCycle/remoteStop")
  def remoteStop(@Context sc : SecurityContext, @FormParam("name") name : String) : Response


  /**
   *
   * Put available experiment for participants
   *
   * A push message is sent to mobile device subscribed to the current
   * experiment to start it
   *
   * @param name Experiment Name
   */
  @POST
  @Path("/lifeCycle/start")
  def remoteAvailable(@Context sc : SecurityContext, @FormParam("name") name : String) : Response

  /**
   *
   * Put not available experiment for participants
   *
   * A push message is sent to mobile device subscribed to the current
   * experiment to stop it. All components services still available
   * for configuration
   *
   * @param name Experiment Name
   */
  @POST
  @Path("/lifeCycle/stop")
  def remoteNotAvailable(@Context sc : SecurityContext, @FormParam("name") name : String) : Response


  /**
   *
   * Finish the experiment
   *
   * @param name Experiment Name
   */
  @POST
  @Path("/lifeCycle/finish")
  def remoteDone(@Context sc : SecurityContext, @FormParam("name") name : String) : Response




}