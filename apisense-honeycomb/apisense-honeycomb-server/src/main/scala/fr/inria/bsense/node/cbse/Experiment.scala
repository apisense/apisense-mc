/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.cbse

import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.node.impl.ModuleManagerImpl
import fr.inria.bsense.node.service.ExperimentModuleService
import fr.inria.bsense.node.service.ModuleManagerService
import frascala.frascati.HTTP
import frascala.frascati.WEB
import frascala.sca.Bean
import frascala.sca.Java
import javax.servlet.Servlet
import fr.inria.apisense.intent.AuthorizationIntentComponent
import frascala.frascati.REST
import fr.inria.apisense.intent.BindingIntent
import fr.inria.apisense.intent.AuthenticationIntentComponent
import fr.inria.bsense.common.utils.Log
import fr.inria.apisense.intent.BindingIntentComponent

//-------------------------------------------------------------------------
//--- Static variable definition


object Experiment {

  /* sca property name */
  final val PROPERTY_EXPERIMENT_NAME = "props-experiments-name"
  
  /* sca property name */
  final val PROPERTY_MODULE_NAME = "props-module-name"
  
  /* sca property name */
  final val PROPERTY_MODULE_URI = "props-module-uri"
  
  /* sca service name */
  final val SERVICE_MODULE_MANAGER = "srv-manager"
  
  /* sca service name */
  final val SERVICE_MODULE = "srv-module"
  
  /* experiment database name */
  final val DATABASE_EXPERIMENTS = "Experiments"
  
  /* association between experiment and user database name */
  final val DATABASE_OWNER = "Experiment_Owner"
    
}


//-------------------------------------------------------------------------
//--- Composite definition

/**
 * === Experiment ===
 * 
 * 
 */
class Experiment(_name: String) extends BSenseComposite(_name) {

  // declare module manager component
  val moduleManager = component(ExperimentModuleManager(_name))
  promoteService(moduleManager.srvModuleManager)
  
  //val authenticationBasic  = component(BindingIntent.authenticationBasic)
  val authenticationToken  = component(BindingIntent.authenticationTokenCpt)
  val authorizationManager = component(BindingIntent.authorizationCpt)
  val logging = component(BindingIntent.loggingCpt)
  
  
  override def component[T<: BSenseComponent](c: T) = {
    super.component(c)
    
    Log.d("add component "+c.getClass()+" in experiment composite "+_name)
    
    c match{
      case cXp : ExperimentComponent => 
        cXp.propsExperimentName.is(_name)
        cXp.deployedService.foreach{
          srv =>
            srv.deploy(_name,authenticationToken, authorizationManager)
        }
      case _ =>
    }
    c
  }
  
  def getRuntimeModuleManager = rs[ModuleManagerService](Experiment.SERVICE_MODULE_MANAGER)

}

//-------------------------------------------------------------------------
//--- Component definition


/**
 * 
 * == Experiment Module Manager Component ==
 * 
 * 
 * 
 */
case class ExperimentModuleManager(_experimentName : String ) extends BSenseComponent("apis.xp.module-manager"){
  this.uses(Bean[ModuleManagerImpl])
  
  // declare experiment name as sca property
  property[String](Experiment.PROPERTY_EXPERIMENT_NAME) is _experimentName
  
  
  // declare service manager service
  val srvModuleManager = service(Experiment.SERVICE_MODULE_MANAGER) exposes Java[ModuleManagerService]
  
  // declare reference with service module
  // Each components proposing a module service
  // will be wired automatically with the manager
  reference(Experiment.SERVICE_MODULE).exposes(Java[ExperimentModuleService]).optional.multiple.autowired
  
}


abstract class ExperimentComponent(_name : String) extends BSenseComponent(_name){
  
  val propsExperimentName = property[String]("experimentName")
  
  
  trait ExperimentService{
    var scaService : service = _
    var url : String = null
    var authorizationRules : Array[Tuple2[String,Array[String]]] = _
    
    def deploy( experimentName : String,
    			authentication : AuthenticationIntentComponent,
    			authorization : AuthorizationIntentComponent)
  }
  class HttpExperimentService extends ExperimentService{
    
    override def deploy( experimentName : String,
    					 authentication : AuthenticationIntentComponent,
    					 authorityManager : AuthorizationIntentComponent){
      
      
      
      var httpUrl = "/website/"
      httpUrl += experimentName
      if (url != null) httpUrl +="/"+url
      
      Log.d("deploy http service at "+httpUrl)
      
      scaService as HTTP(httpUrl)
      
    }
    
    
  }
  
  class RestExperimentService extends ExperimentService{
    
    override def deploy( experimentName : String,
    					 authentication : AuthenticationIntentComponent,
                         authorization  : AuthorizationIntentComponent){
      
      var restUrl = "/"
      restUrl += experimentName
      restUrl += "/"+url
      
      Log.d("deploy http service at "+restUrl)
      
      authorizationRules.foreach{ r => authorization.rule(r._1, r._2) }
      scaService as(REST(restUrl),Array(authentication,authorization))
      
    }
  }
  
  
  var deployedService = List[ExperimentService]()
  
  def deployRest(service : service, url : String, rules: Array[Tuple2[String,Array[String]]]){
    
    val restService = new RestExperimentService
    restService.scaService = service
    restService.url = url
    restService.authorizationRules = rules
    
    deployedService = restService :: deployedService
    
  }
  
  def deployHttp(service : service, url : String){
    
    val httpService = new HttpExperimentService
    httpService.scaService = service
    httpService.url = url;
    
    deployedService = httpService :: deployedService
    
  }
  
}

/**
 * 
 * 
 * 
 * @param _name   Module name
 * @param _xpname Experiment name
 */
abstract class ExperimentModule(_name: String) extends ExperimentComponent(_name)
{
  import frascala.sca._
  
  val propModuleURI = property[String](Experiment.PROPERTY_MODULE_NAME) is getModuleName
  val propModuleName = property[String](Experiment.PROPERTY_MODULE_URI) is getModuleURI
  
  var srvModule = service(Experiment.SERVICE_MODULE) exposes Java[ExperimentModuleService]

  def getModuleName: String
  def getModuleURI: String
  
} 
