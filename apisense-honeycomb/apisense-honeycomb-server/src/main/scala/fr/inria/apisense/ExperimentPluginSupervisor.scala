/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.scripting.akka

import akka.actor.Actor
import fr.inria.apisense.common.ExperimentContext
import akka.actor.OneForOneStrategy
import scala.concurrent.duration._
import akka.actor.SupervisorStrategy._
import akka.actor.Props
import scala.util.Success
import scala.util.Failure
import scala.concurrent.Future
import akka.pattern.ask
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors
import akka.util.Timeout


object ExperimentPluginSupervisor{
  
  case class PluginClass(className : String)
}

class ExperimentPluginSupervisor(experimentContext : ExperimentContext) extends Actor {
  import ExperimentPluginSupervisor._
  
  implicit val ec = ExecutionContext.fromExecutorService(Executors.newScheduledThreadPool(1))
  
   implicit val timeout = Timeout(1 second)
  
  override val supervisorStrategy = 
    OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
        case _: Exception  => Resume
  }
  
  override def receive = {
    
    case PluginClass(className) =>
      val clazzContructor = ClassLoader.getSystemClassLoader().loadClass(className).getConstructor(classOf[ExperimentContext])
      val actorRef =  context.actorOf(Props( clazzContructor.newInstance(experimentContext).asInstanceOf[Actor]))
      sender ! actorRef
      
    case _ => 
  }
  
}