/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense

import akka.actor.ActorSystem
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors
import com.typesafe.config.ConfigFactory



trait APISENSEWebServer{

  val actorSystem = APISENSE.globalSystem

  def createWebSocket( _path : String ) : APISWebSocket
  def deleteWebSocket( _path : String)
  def deleteWebSocket( websocket : APISWebSocket)

  def host : String;

  def get( _path : String,callback : RESTServiceListenner )  : ApisRESTService
  def post( _path : String,callback : RESTServiceListenner ) : ApisRESTService
  def deleteRESTService(service : ApisRESTService)

  def start
  def stop

}

trait APISWebSocket{
  def broadcast(message : String);
  def broadcast( bytes : Array[Byte]);
}


trait RESTServiceListenner{
  def run(args : java.util.Map[String,java.util.List[String]]) : String
}

trait ApisRESTService{

  def path : String

}


object AKKAConf {

  val config = ConfigFactory.parseString("""

  akka-webserver-socko{



  }

  akka {

      loglevel = INFO


  }
  """)
}



object APISENSE {
  import scala.concurrent.duration._
  import akka.util.Timeout

  /**
   * Define thread executor service for
   * asynchronous call with result
   */
  implicit val executorService = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(50))

  /**
   * Define default time to wait for future result
   */
  val defaultTime = 5 seconds

  /**
   * Define implicit Timeout for akka actor
   */
  implicit val timeout = Timeout(defaultTime)

  /**
   * Main APISENSE actor system
   */


  lazy val globalSystem =  ActorSystem.create("GlobalSystem",ConfigFactory.load(AKKAConf.config));


  /**
   * Web server
   */
  var webserver : APISENSEWebServer = null

}