/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.webserver

import org.mashupbots.socko.webserver.WebServerConfig
import org.mashupbots.socko.webserver.WebServer
import org.mashupbots.socko.routes.Routes
import org.mashupbots.socko.routes.HttpRequest
import org.mashupbots.socko.routes.WebSocketHandshake
import org.mashupbots.socko.routes.WebSocketFrame
import fr.inria.bsense.common.utils.Log
import org.mashupbots.socko.events.WebSocketHandshakeEvent
import org.mashupbots.socko.events.HttpRequestEvent
import akka.actor.Props
import akka.actor.actorRef2Scala
import fr.inria.apisense.APISENSE
import fr.inria.apisense.APISENSEWebServer
import fr.inria.apisense.APISWebSocket
import org.mashupbots.socko.handlers.WebSocketBroadcastBinary
import org.mashupbots.socko.handlers.WebSocketBroadcastText
import org.mashupbots.socko.handlers.WebSocketBroadcaster
import org.mashupbots.socko.handlers.WebSocketBroadcasterRegistration
import org.mashupbots.socko.routes.Path
import akka.actor.ExtensionIdProvider
import akka.actor.ExtensionId
import akka.actor.ExtendedActorSystem
import org.mashupbots.socko.routes.GET
import org.mashupbots.socko.routes.POST
import scala.collection.JavaConversions
import fr.inria.apisense.RESTServiceListenner
import fr.inria.apisense.ApisRESTService
import org.jboss.netty.handler.codec.http.QueryStringDecoder

object SockoWebServerConfig extends ExtensionId[WebServerConfig] with ExtensionIdProvider {
  override def lookup = SockoWebServerConfig
  override def createExtension(system: ExtendedActorSystem) =
    new WebServerConfig(system.settings.config, "akka-webserver-socko")
}


case class WebSocketRequest(_request : WebSocketHandshakeEvent, _requestType : WebSocketHandshakeEvent)
case class RESTRequest( _request :HttpRequestEvent,_requestType : HttpRequestEvent )

abstract class  SockoRESTService(_path : String, _listenner : RESTServiceListenner) extends ApisRESTService{

	def path : String = _path

	def partial :  PartialFunction[RESTRequest,Unit]

	val listenner = _listenner

	val _servicePath = _path;

	def call(httpRequest : HttpRequestEvent){
	 httpRequest.response.headers.+= ("Access-Control-Allow-Origin" -> "*")
	 httpRequest.response.write(listenner.run(
	     new QueryStringDecoder(
	         httpRequest.request.content.toString,false).getParameters()))
	}
}

class SockoGetService(servicePath : String, callback : RESTServiceListenner ) extends SockoRESTService(servicePath,callback){
  def partial :  PartialFunction[RESTRequest,Unit] = {
      case RESTRequest(_request,GET(Path(servicePath))) if _servicePath.equals(_request.request.endPoint.path) =>
        call(_request)
  }
}

class SockoPOSTService(servicePath : String, callback : RESTServiceListenner ) extends SockoRESTService(servicePath,callback){

  def partial :  PartialFunction[RESTRequest,Unit] = {
      case RESTRequest(_request,POST(Path(servicePath))) if _servicePath.equals(_request.request.endPoint.path) =>{
        call(_request)
      }
  }
}

case class SockoWebSocket(_path : String) extends APISWebSocket {
  import org.mashupbots.socko.handlers.WebSocketBroadcaster
  import org.mashupbots.socko.handlers.WebSocketBroadcasterRegistration
  import org.mashupbots.socko.handlers.WebSocketBroadcastText
  import org.mashupbots.socko.handlers.WebSocketBroadcastBinary
  import org.mashupbots.socko.routes.WebSocketHandshake
  import org.mashupbots.socko.routes.WebSocketFrame
  import org.mashupbots.socko.events.WebSocketHandshakeEvent
  import org.mashupbots.socko.routes.Path
  import akka.actor.Props


  val webSocketBroadcastRef = APISENSE.globalSystem.actorOf(Props[WebSocketBroadcaster]);

  val partial :  PartialFunction[WebSocketRequest,Unit] = {

    case WebSocketRequest(request,Path((socketPath))) if socketPath.equals(_path) => {

       //Log.d("websocket registration for request "+request+" and socket path "+socketPath+" math with "+_path)

       request.authorize(onComplete = Some((event: WebSocketHandshakeEvent) => {
          // Register this connection with the broadcaster
          // We do this AFTER handshake has been completed so that the server
          // does not send data to client until
          // after the client gets a handshake response
          //Log.d("websocket registration for "+webSocketBroadcastRef)
          webSocketBroadcastRef ! new WebSocketBroadcasterRegistration(event)
       }))
    }
  }

  def delete {}
  def broadcast(message : String)    { webSocketBroadcastRef ! WebSocketBroadcastText(message) }
  def broadcast( bytes : Array[Byte]){ webSocketBroadcastRef ! WebSocketBroadcastBinary(bytes) }
}

class SockoWebserver(_config : WebServerConfig) extends APISENSEWebServer {

  /**
   * Routing table of available REST services
   */
  var RESTRountingTable : PartialFunction[RESTRequest, Unit] = { case _ => }

  /**
   * Routing table of available WEB socket services
   */
  var WSRoutingTable :  PartialFunction[WebSocketRequest,Unit] = { case _ => }

  /**
   * Set of Socko REST service available
   */
  var RESTServices: Set[SockoRESTService] = Set[SockoRESTService]()

  /**
   * Set of Socko Web socket services available
   */
  var WSServices : Set[SockoWebSocket] = Set[SockoWebSocket]()

  /**
   * Socko routing table
   */
  var sockoServerRoutes = Routes({
    case HttpRequest(httpRequest)        => { RESTRountingTable(RESTRequest(httpRequest,httpRequest))     }
    case WebSocketHandshake(wsHandshake) => { WSRoutingTable(WebSocketRequest(wsHandshake,wsHandshake))   }
    case WebSocketFrame(wsFrame) => {

      //println("web socket frame "+wsFrame.endPoint.path)
      //println(wsFrame.readText)
    }

    case _ => println("no match")
  })

  /**
   * Instance of Socko web server
   */
  val socko = new WebServer(WebServerConfig(), sockoServerRoutes, actorSystem)


  def host : String = socko.config.hostname;

  // catch shutdown of the system and turn off
  // socko web server
  Runtime.getRuntime.addShutdownHook(new Thread {
    override def run { socko.stop() }
  })

  private def buildHttpRoutingTable {

    val routingTableArray = for ( __ <- RESTServices) yield __.partial

    RESTRountingTable = null
    if (! routingTableArray.isEmpty){


    	RESTRountingTable = routingTableArray.reduceLeft( (__,__s) => __ orElse __s )
    	//add default behavior
        RESTRountingTable = RESTRountingTable orElse { case _ => println("Not defined")}
    }else RESTRountingTable = { case _ => println("Not defined")}


  }


  private def buildWSRoutingTable {

     // retrieve all partial functions from routingWSList
     val routingTableArray = for ( __ <- WSServices) yield __.partial

     // clear web socket routing table
     // and compose all partial functions.
     WSRoutingTable = null
     WSRoutingTable = routingTableArray.reduceLeft( ( __, __s ) => __ orElse __s )

     Log.d("build websocket routing ")
     WSServices.foreach{
       ws =>
         Log.d("build "+ws._path)
     }

     // add default behavior
     WSRoutingTable = WSRoutingTable orElse {case _ => println("Not defined") }
  }

  def createWebSocket( _path : String ) : APISWebSocket = route(new SockoWebSocket(_path))

  def deleteWebSocket( websocket :  APISWebSocket) { unroute(websocket.asInstanceOf[SockoWebSocket]) }

  def get( _path : String,callback : RESTServiceListenner )  = route(new SockoGetService(_path,callback))

  def post( _path : String,callback : RESTServiceListenner ) = route(new SockoPOSTService(_path,callback))

  def deleteRESTService(service : ApisRESTService){
	  unroute(service.asInstanceOf[SockoRESTService])
  }

  def deleteWebSocket( websocketPath :  String){
    WSServices.find{ websocket => websocket._path.equals(websocketPath) } match{
      case Some(websocket) => unroute(websocket)
      case None => Log.w("Cannot remove web socket with path "+websocketPath)
    }
  }

  private def route( _route : SockoRESTService) = {

    Log.d("create REST service "+_route.path)

    RESTServices = RESTServices + _route;
    buildHttpRoutingTable
    _route
  }

  private def unroute( _route : SockoRESTService){

    Log.d("delete REST service "+_route.path)

    RESTServices = RESTServices - _route
    buildHttpRoutingTable
  }

  private def route( _route :  SockoWebSocket) = {

    Log.d("create websocket "+_route._path)

    WSServices = WSServices + _route;
    buildWSRoutingTable
    _route
  }

  def unroute( _route :  SockoWebSocket){

    Log.d("delete websocket "+_route._path)

    WSServices = WSServices - _route
    buildWSRoutingTable
    _route.delete
  }

  def start { socko.start }
  def stop  { socko.stop  }
}




