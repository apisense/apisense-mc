package fr.inria.bsense.node.server.test

import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.test.ScalaTestCase
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import net.liftweb.json.JsonAST.JString
import java.util.Scanner
import fr.inria.bsense.common.utils.Http._
import fr.inria.bsense.common.utils.MD5
import fr.inria.apisense.client.APISENSEClient
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import org.junit.Assert
import fr.inria.bsense.common.utils.LiftWebNode
import fr.inria.bsense.node.server.test.ServerNodeTest

case class TCase extends ScalaTestCase {
  import fr.inria.sspl.dsl.SPL._
  import fr.inria.bsense.node.factory.CBSEFactory._

  val HOST = "http://localhost:18000"
  val SERVICE_CREATE_USER = HOST + "/user-manager/add/user/scientist"
  val SERVICE_CONNECT= HOST + "/user-manager/get/token"
  val SERVICE_DISCONNECT= HOST + "/user-manager/update/disconnect"

  val SERVICE_CREATE_EXPERIMENT = HOST + "/experiment-manager/add/xp"
  val SERVICE_DROP_EXPERIMENT   = HOST + "/experiment-manager/delete/xp"
  val SERVICE_START_EXPERIMENT  = HOST + "/experiment-manager/update/start"
  val SERVICE_STOP_EXPERIMENT   = HOST + "/experiment-manager/update/stop"
  val SERVICE_EXPERIMENT_STATE  = HOST + "/experiment-manager/get/running"

  def testcase {

    val honey = APISENSEClient.honey;
    honey.host(HOST);

    notfail("admin connection "){  honey.connect("admin","admin"); }

    canfail("create scientist user"){
      honey.userManager.createScientistUser(
        "MyScientist Fullname", "scientistbis",
        "paswword","no email")
    }

    notfail("admin disconnection "){  honey.disconnect(); }

    mustfail("scientist bad connection"){ honey.connect("scientistbis", "fakepassword") }

    notfail("scientist good connection"){ honey.connect("scientistbis", "paswword") }

    mustfail("start experiment without start components services"){ honey.experimentManager.lifeCycleStart("TestHoneyServer") }

    canfail("delete experiment "){ honey.experimentManager.dropExperiment("TestHoneyServer") }

    val conf = """ { "Experiment" : "1", "Experiment Service" : "1" } """

    notfail("create new experiment"){  honey.experimentManager.createExperiment(
        "TestHoneyServer", "A nice name", "none","none", conf)
    }


    notfail("start experiment services"){ honey.experimentManager.lifeCycleRemoteStart("TestHoneyServer") }

    notfail("get experiment state"){

      val requestResult = honey.experimentManager.getExperiment("TestHoneyServer")
      val JString(status) = requestResult \\ "status";
      Assert.assertTrue(status.equals("true"))
    }

    notfail("start experiment"){ honey.experimentManager.lifeCycleStart("TestHoneyServer") }

    notfail("get experiment state"){

      val requestResult = honey.experimentManager.getExperiment("TestHoneyServer")
      println(new LiftWebNode(requestResult).toJSONString)
      val JString(status) = requestResult \\ "remoteState";
      Assert.assertTrue(status.equals("started"))
    }

    testExperimentStateHive("TestHoneyServer", "started")

    notfail("stop experiment"){ honey.experimentManager.lifeCycleRemoteStop("TestHoneyServer") }

    testExperimentStateHive("TestHoneyServer", "stopped")

  }

  def testExperimentStateHive(xpname : String, status : String){

	val hive = ServerNodeConfiguration.createHiveConnection
    new LiftWebNode(hive.search.searchExperiment(Array(xpname))).\\("experiment").foreach{

      xp =>  if ( xp.\\("name").text.equals(xpname)){ Assert.assertTrue(xp.\\("remoteState").text.equals(status)) }
    }
  }

}