package fr.inria.bsense.node.server.test

import org.apache.http.message.BasicNameValuePair
import org.junit.After
import org.junit.Before
import org.junit.Test
import fr.inria.bsense.common.cbse.BSRuntime
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.db.cbse.BaseXComposite
import fr.inria.bsense.node.cbse.ExperimentManagerComponent
import fr.inria.bsense.node.cbse.UserManagerComponent
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.node.factory.CBSEFactory.generate
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.server.run.StartServer
import fr.inria.bsense.test.ScalaTestCase
import fr.inria.sspl.dsl.SPL.feat2dsl
import fr.inria.sspl.dsl.SPL.load
import fr.inria.sspl.dsl.SPL.slice2dsl
import fr.inria.bsense.node.configuration.NodeFileConfiguration

class ServerNodeTest extends ScalaTestCase{

  var centralServerProcess : Process = null;


  final val CENTRAL_HOST = "http://localhost:18001";
  final val SERVICE_CREATE_SCIENTIST  = CENTRAL_HOST+"/user/add/user/organization";

  final val http = new HttpRequest();

  @Before
  def init(){

    val home = System.getProperty("user.home")
    System.setProperty(ServerNodeConfiguration.CONFIGURATION_NODE_PROPERTY,home+"/BEES_HIVE")

    NodeFileConfiguration("started").deleteAllFiles

	startHive()

	System.setProperty("org.ow2.frascati.binding.uri.base","http://localhost:18000")
    fr.inria.bsense.node.start.HoneyCombStarter.start(Array())

    ServerNodeConfiguration.setUrl(CENTRAL_HOST)
	ServerNodeConfiguration.password("inria-tester")
	ServerNodeConfiguration.username("inria-tester")


  }

  def startHive(){

    //----
    // Start central node and create
    // an organization account
    centralServerProcess = StartServer.AsynchStart("../../../")

    var params = Array(
		new BasicNameValuePair("organization","Tester Organization"),
		new BasicNameValuePair("description","Tester Organization"),
		new BasicNameValuePair("username","inria-tester"),
		new BasicNameValuePair("password","inria-tester"),
		new BasicNameValuePair("email","scientist.bsense.com")
	);

	println(http.post(SERVICE_CREATE_SCIENTIST, params))
  }



  @Test
  def test(){
    TCase().testcase
  }

  @After def killProcess(){
    centralServerProcess.destroy()
  }
}