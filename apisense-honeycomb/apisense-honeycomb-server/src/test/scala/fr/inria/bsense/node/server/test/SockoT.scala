package fr.inria.bsense.node.server.test

import fr.inria.apisense.APISENSE
import fr.inria.apisense.webserver.SockoWebserver
import fr.inria.apisense.RESTServiceListenner
import org.mashupbots.socko.webserver.WebServerConfig

object SockoT {


  def main(args: Array[String]) {
	APISENSE.webserver = new SockoWebserver(new WebServerConfig())
    APISENSE.webserver.start

    APISENSE.webserver.post("/test",new RESTServiceListenner{
       def run(args : java.util.Map[String,java.util.List[String]]) : String = {

         println(args);
         "hello"
       }
    })

    println( scalaj.http.Http.post("http://localhost:8888/test").asString)
  }

}