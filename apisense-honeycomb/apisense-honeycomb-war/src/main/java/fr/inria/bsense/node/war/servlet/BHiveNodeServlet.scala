/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.war.servlet

import scala.Array.apply

import org.apache.cxf.transport.servlet.CXFNonSpringServlet
import org.ow2.frascati.servlet.FraSCAtiServlet

import javax.servlet.ServletConfig
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

class BHiveNodeServlet extends CXFNonSpringServlet{
  
  FraSCAtiServlet.singleton = new FraSCAtiServlet()
  
  override def  init(servletConfig : ServletConfig){
    
    super.init(servletConfig)
    
    System.setProperty("org.ow2.frascati.binding.uri.base","")
    
    //fr.inria.bsense.common.cbse.BSRuntime._frascati = FraSCAti.newFraSCAti()
    print("start servlet ...")
    fr.inria.bsense.node.start.HoneyCombStarter.start(Array("Beehive Website"))
  }
  
  override def service( request : ServletRequest, response : ServletResponse){
	
    val pathInfo = request.asInstanceOf[HttpServletRequest].getPathInfo();
    if (pathInfo.startsWith("/website"))
      FraSCAtiServlet.singleton.service(request, response)

    else super.service(request, response)
    
  }
   
   
}