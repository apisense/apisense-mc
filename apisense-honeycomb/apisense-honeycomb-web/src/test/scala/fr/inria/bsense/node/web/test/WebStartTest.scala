package fr.inria.bsense.node.web.test

import org.junit.Test
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.test.ScalaTestCase
import java.util.Scanner
import org.ow2.frascati.servlet.FraSCAtiServlet


object Start{
  def main(args: Array[String]) {

    new WebStartTest().test

    val sc = new Scanner(System.in);
    val str = sc.next()

  }
}

class WebStartTest extends ScalaTestCase {

  var serviceXPManager : ExperimentManagerService = null

  var serviceUserManager : UserManagerService = null

  final val CENTRAL_HOST = "http://localhost:18001";
  final val SERVICE_CREATE_SCIENTIST  = CENTRAL_HOST+"/user/add/user/organization";

  final val http = new HttpRequest();

  var centralServerProcess : Process = null;


  @Test def test(){

   // FraSCAtiServlet.singleton = new MyServlet();

	ServerNodeConfiguration.setUrl(CENTRAL_HOST)
	ServerNodeConfiguration.password("inria-tester")
	ServerNodeConfiguration.username("inria-tester")

	fr.inria.bsense.node.start.HoneyCombStarter.start(Array("Beehive Website"))
	//w
  }





}