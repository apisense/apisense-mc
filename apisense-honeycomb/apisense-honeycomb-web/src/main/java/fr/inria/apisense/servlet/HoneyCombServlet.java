/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.servlet;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.servlet.FraSCAtiServlet;
import org.ow2.frascati.servlet.api.ServletManager;

import fr.inria.bsense.common.cbse.BSRuntime;


@SuppressWarnings("serial")
public class HoneyCombServlet  extends FraSCAtiServlet
implements ServletManager
{
	// --------------------------------------------------------------------------
	// Internal state.
	// --------------------------------------------------------------------------

	/**
	 * Deployed servlets.
	 */
	private Map<String, Servlet> servlets = null;

	// --------------------------------------------------------------------------
	// Public methods.
	// --------------------------------------------------------------------------

	/**
	 * Initialization of the servlet and launches SCA composites if any.
	 */
	@Override
	public  void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);

		try {

			BSRuntime.setFraSCAti(getFraSCAti());

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.setProperty("org.ow2.frascati.binding.uri.base","");
		fr.inria.bsense.node.start.HoneyCombStarter.start(new String[]{"Beehive Website"});
	}

	@Override
	public void service(ServletRequest request, ServletResponse response)
			throws IOException, ServletException
			{

		final HttpServletRequest httpRequest = (HttpServletRequest)request;

		String pathInfo = httpRequest.getPathInfo();

		if(pathInfo != null) {

			for(final Entry<String, Servlet> entry : getServlets().entrySet()) {


				if (match(pathInfo,entry.getKey())){

					entry.getValue().service(
							new MyHttpServletRequestWrapper(
									httpRequest,
									pathInfo.substring(entry.getKey().length())),
									response
							);
					return;

				}
			}
		}

		// If no deployed servlet for this request then dispatch this request via Apache CXF.
		super.service(request, response);
	}

	public static Boolean match(final String pathInfo, final String servletPath){

		if (pathInfo.length() < servletPath.length())
			return false;

		int lastIndexServletPath = servletPath.lastIndexOf("/");
		int lastIndexPathInfo = pathInfo.indexOf('/',lastIndexServletPath+1);
		if (lastIndexPathInfo == -1){
			return servletPath.equals(pathInfo);
		}

		final String contextPathInfo = pathInfo.substring(0,lastIndexPathInfo);


		return contextPathInfo.equals(servletPath);
	}


	/**
	 * @see ServletManager#shutdown()
	 *
	 * Added for issue http://jira.ow2.org/browse/FRASCATI-105
	 */
	public void shutdown()
	{
		// Nothing to do.
	}

	@SuppressWarnings("unchecked")
	public Map<String, Servlet> getServlets(){

		if (servlets == null){

			try {

				final Field servletsField = FraSCAtiServlet.class.getDeclaredField("servlets");

				servletsField.setAccessible(true);
				this.servlets = (Map<String, Servlet>) servletsField.get(FraSCAtiServlet.singleton);

			} catch (Exception e) {

				e.printStackTrace();
			}
		}

		return this.servlets;
	}

	public FraSCAti getFraSCAti() throws Exception{

		final Field frascatiField = FraSCAtiServlet.class.getDeclaredField("frascati");
		frascatiField.setAccessible(true);
		return (FraSCAti) frascatiField.get(FraSCAtiServlet.singleton);
	}
}

class MyHttpServletRequestWrapper extends HttpServletRequestWrapper
{
	private String pathInfo;

	public MyHttpServletRequestWrapper(HttpServletRequest request, String pathInfo)
	{
		super(request);
		this.pathInfo = pathInfo;
	}

	@Override
	public String getPathInfo()
	{
		return this.pathInfo;
	}

}
