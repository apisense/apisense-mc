/*
* jQuery Cookie Plugin https://github.com/carhartl/jquery-cookie
*/
(function(e){e.cookie=function(t,n,r){if(arguments.length>1&&(!/Object/.test(Object.prototype.toString.call(n))||n===null||n===undefined)){r=e.extend({},r);if(n===null||n===undefined){r.expires=-1}if(typeof r.expires==="number"){var i=r.expires,s=r.expires=new Date;s.setDate(s.getDate()+i)}n=String(n);return document.cookie=[encodeURIComponent(t),"=",r.raw?n:encodeURIComponent(n),r.expires?"; expires="+r.expires.toUTCString():"",r.path?"; path="+r.path:"",r.domain?"; domain="+r.domain:"",r.secure?"; secure":""].join("")}r=n||{};var o=r.raw?function(e){return e}:decodeURIComponent;var u=document.cookie.split("; ");for(var a=0,f;f=u[a]&&u[a].split("=");a++){if(o(f[0])===t)return o(f[1]||"")}return null}})(jQuery)

// https://github.com/joewalnes/reconnecting-websocket/
function ReconnectingWebSocket(a){function f(g){c=new WebSocket(a);if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","attempt-connect",a)}var h=c;var i=setTimeout(function(){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","connection-timeout",a)}e=true;h.close();e=false},b.timeoutInterval);c.onopen=function(c){clearTimeout(i);if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onopen",a)}b.readyState=WebSocket.OPEN;g=false;b.onopen(c)};c.onclose=function(h){clearTimeout(i);c=null;if(d){b.readyState=WebSocket.CLOSED;b.onclose(h)}else{b.readyState=WebSocket.CONNECTING;if(!g&&!e){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onclose",a)}b.onclose(h)}setTimeout(function(){f(true)},b.reconnectInterval)}};c.onmessage=function(c){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onmessage",a,c.data)}b.onmessage(c)};c.onerror=function(c){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","onerror",a,c)}b.onerror(c)}}this.debug=false;this.reconnectInterval=1e3;this.timeoutInterval=2e3;var b=this;var c;var d=false;var e=false;this.url=a;this.readyState=WebSocket.CONNECTING;this.URL=a;this.onopen=function(a){};this.onclose=function(a){};this.onmessage=function(a){};this.onerror=function(a){};f(a);this.send=function(d){if(c){if(b.debug||ReconnectingWebSocket.debugAll){console.debug("ReconnectingWebSocket","send",a,d)}return c.send(d)}else{throw"INVALID_STATE_ERR : Pausing to reconnect websocket"}};this.close=function(){if(c){d=true;c.close()}};this.refresh=function(){if(c){c.close()}}}ReconnectingWebSocket.debugAll=false

/*
 * https://github.com/Leaflet/Leaflet/blob/master/src/core/Util.js
 * L.Util contains various utility functions used throughout Leaflet code.
 */
L={};L.Util={extend:function(e){var t=Array.prototype.slice.call(arguments,1),n,r,i,s;for(r=0,i=t.length;r<i;r++){s=t[r]||{};for(n in s){if(s.hasOwnProperty(n)){e[n]=s[n]}}}return e},bind:function(e,t){var n=arguments.length>2?Array.prototype.slice.call(arguments,2):null;return function(){return e.apply(t,n||arguments)}},stamp:function(){var e=0,t="_leaflet_id";return function(n){n[t]=n[t]||++e;return n[t]}}(),invokeEach:function(e,t,n){var r,i;if(typeof e==="object"){i=Array.prototype.slice.call(arguments,3);for(r in e){t.apply(n,[r,e[r]].concat(i))}return true}return false},limitExecByInterval:function(e,t,n){var r,i;return function s(){var o=arguments;if(r){i=true;return}r=true;setTimeout(function(){r=false;if(i){s.apply(n,o);i=false}},t);e.apply(n,o)}},falseFn:function(){return false},formatNum:function(e,t){var n=Math.pow(10,t||5);return Math.round(e*n)/n},trim:function(e){return e.trim?e.trim():e.replace(/^\s+|\s+$/g,"")},splitWords:function(e){return L.Util.trim(e).split(/\s+/)},setOptions:function(e,t){e.options=L.extend({},e.options,t);return e.options},getParamString:function(e,t){var n=[];for(var r in e){n.push(encodeURIComponent(r)+"="+encodeURIComponent(e[r]))}return(!t||t.indexOf("?")===-1?"?":"&")+n.join("&")},template:function(e,t){return e.replace(/\{ *([\w_]+) *\}/g,function(e,n){var r=t[n];if(r===undefined){throw new Error("No value provided for variable "+e)}else if(typeof r==="function"){r=r(t)}return r})},isArray:function(e){return Object.prototype.toString.call(e)==="[object Array]"},emptyImageUrl:"data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs="};(function(){function e(e){var t,n,r=["webkit","moz","o","ms"];for(t=0;t<r.length&&!n;t++){n=window[r[t]+e]}return n}function n(e){var n=+(new Date),r=Math.max(0,16-(n-t));t=n+r;return window.setTimeout(e,r)}var t=0;var r=window.requestAnimationFrame||e("RequestAnimationFrame")||n;var i=window.cancelAnimationFrame||e("CancelAnimationFrame")||e("CancelRequestAnimationFrame")||function(e){window.clearTimeout(e)};L.Util.requestAnimFrame=function(e,t,i,s){e=L.bind(e,t);if(i&&r===n){e()}else{return r.call(window,e,s)}};L.Util.cancelAnimFrame=function(e){if(e){i.call(window,e)}}})()
L.extend = L.Util.extend;
L.bind = L.Util.bind;
L.stamp = L.Util.stamp;
L.setOptions = L.Util.setOptions;

var APIS = {}
APIS.Class = function () {};
/* Class Util. Inspired by https://github.com/Leaflet/Leaflet/blob/master/src/core*/
APIS.Class.extend=function(e){var t=function(){if(this.initialize){this.initialize.apply(this,arguments)}if(this._initHooks){this.callInitHooks()}};var n=function(){};n.prototype=this.prototype;var r=new n;r.constructor=t;t.prototype=r;for(var i in this){if(this.hasOwnProperty(i)&&i!=="prototype"){t[i]=this[i]}}if(e.statics){L.extend(t,e.statics);delete e.statics}if(e.includes){L.Util.extend.apply(null,[r].concat(e.includes));delete e.includes}if(e.options&&r.options){e.options=L.extend({},r.options,e.options)}L.extend(r,e);r._initHooks=[];var s=this;t.__super__=s.prototype;r.callInitHooks=function(){if(this._initHooksCalled){return}if(s.prototype.callInitHooks){s.prototype.callInitHooks.call(this)}this._initHooksCalled=true;for(var e=0,t=r._initHooks.length;e<t;e++){r._initHooks[e].call(this)}};return t}

/* ReplaceAll by Fagner Brack (MIT Licensed) */
String.prototype.replaceAll=function(e,t,n){var r,i=-1,s;if((r=this.toString())&&typeof e==="string"){s=n===true?e.toLowerCase():undefined;while((i=s!==undefined?r.toLowerCase().indexOf(s,i>=0?i+t.length:0):r.indexOf(e,i>=0?i+t.length:0))!==-1){r=r.substring(0,i).concat(t).concat(r.substring(i+e.length))}}return r}

var print = function(json){alert(JSON.stringify(json))}

APIS.POST = APIS.Class.extend({

	initialize : function(url,_data,_conf){
		this.url = url
		_conf = _conf || {}

		var _data    = _data || {}
		var parse    = (_conf.parse != undefined)    ?  _conf.parse     : true
	    var async    = (_conf.async != undefined)    ?  _conf.async     : true
	    var method   = (_conf.method != undefined)   ?  _conf.method    : "POST"
        var hasToken = (_conf.hasToken != undefined) ?  _conf.hasToken  : true

        var instance = this;

        $.ajax( {url : url,type: method,async: async,data : _data,
	    beforeSend: function(xhrObj){
			if (hasToken)
            xhrObj.setRequestHeader("TOKEN",token());
        },
		success : function(result){
        	$(".loading").hide()

			if (parse){
			    try{

				  var json = JSON.parse(result)
				  if (json["error"]){
					if (json["error"]["type"] == "InvalidTokenException"){signout();return}
					instance.onError(json["error"]["message"])
				  }
				  else {instance.onSuccess(json["success"])}
				}
				catch(e){instance.onError(result)}
			}
			else instance.onSuccess(result)

		},
		error : function(a,b,c){
			$(".loading").hide()
			instance.onError(JSON.stringify(c))
		}
	});

	},

	onSuccess : function(message){},
	onError   : function(message){}
})

APIS.WebSocket = APIS.Class.extend({

	initialize : function( config  ){

		this.config = config || {}
		this.config.onMessage = this.config.onMessage || function(event){}
		this.config.onOpen = this.config.onOpen       || function(event){}
		this.config.onClose = this.config.onClose     || function(event){}
		this.config.onFailure = this.config.onFailure || function(event){}

		if (!window.WebSocket) { window.WebSocket = window.MozWebSocket; }
		if (window.WebSocket){

			this.socket = new ReconnectingWebSocket(this.config.path);
			this.socket.onmessage = function(event) {config.onMessage(event.data)}
			this.socket.onopen = function(event){config.onOpen(event)}
			this.socket.onclose = function(event){config.onClose(event)}

		}else this._fail()
	},

	_fail : function(mess){
		if (this.config.onFailure){
			this.config.onFailure(mess)
		}
	}
})

/**
 * Send an Ajax POST request with Basic Authentication
 * @param aturl :
 * @param atdata :
 * @param : atauth : username:password encoded in base64
 * @param : function called if response is valid
 * @param : function called if error
 */
var post = function(url,data,success,error, _conf ){
	_conf = _conf || {}
	var parse    = (_conf.parse != undefined)    ?  _conf.parse     : true
	var async    = (_conf.async != undefined)    ?  _conf.async     : true
	var method   = (_conf.method != undefined)   ?  _conf.method    : "POST"
    var hasToken = (_conf.hasToken != undefined) ?  _conf.hasToken  : true

	$(".loading").show()
	$.ajax( {
		url : url,
		type: method,
		async: async,
		data : data,
	    beforeSend: function(xhrObj){
			if (hasToken)
            xhrObj.setRequestHeader("TOKEN",token());
        },
		success : function(result){

			if (parse){

				try{
				  var json = JSON.parse(result)
				  if (json["error"]){
					if (json["error"]["type"] == "InvalidTokenException"){
						signout()
						return ;
					}
					error(json["error"]["message"])
				  }
				  else success(json["success"])
				}
				catch(e){error(result)}
			}
			else success(result)
			$(".loading").hide()
		},
		error : function(a,b,c){
			error(c);
			$(".loading").hide()
		}
	});
}

var redirect = function(path, params, method) {
    method = method || "post"; // Set method to post by default, if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action",path);

    for(var key in params) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);

        form.appendChild(hiddenField);
    }

    document.body.appendChild(form);
    form.submit();

}

var encode = function(str){return $.base64.encode(str)}
var decode = function(str){return $.base64.decode(str)}
var signout = function(){$.cookie("fr.apisense.token", "", { path: '/', expires: 0 });redirect("/website/public")}
var token = function(){
   var cook = $.cookie("fr.apisense.token")
   return cook.substring(cook.indexOf(":")+1)
}

function Modal(option){
	option = option || {}
	var modalId = option["modalId"] || "defaultModalId"
	var title = option["title"] || "Info"
	var message = option["message"] || ""
	var onClose = option["onClose"] || undefined
	var onOk = option["onOk"] || undefined
	var modal = $("#"+modalId)
	if (modal.length == 0){
		var tmp =
			'<div id="'+modalId+'" class="modal hide fade">'+
				'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
					'<h3 id="modal-title" >Modal header</h3></div>'+
						'<div id="modal-message" class="modal-body"></div>'+
						'<div id="modal-footer" class="modal-footer">'+
						'</div>'
	    $("body").append($(tmp))
	}
	else{
		$("#modal-close").unbind()
		$("#modal-ok").unbind()
		$("#modal-footer").children().remove()
		$("#modal-message").children().remove()
	}

	if (onOk) $("#modal-footer").append($('<a id="modal-ok" data-dismiss="modal" class="btn btn-primary">Ok</a></div>'))
	if (onClose) $("#modal-footer").append($('<a id="modal-close" data-dismiss="modal" class="btn">Close</a></div>'))

	$("#modal-close").click(onClose)
	$("#modal-ok").click(onOk)

	$("#modal-title").text(title)
	$("#modal-message").append($("<p>"+message+"</p>"))
	$("#"+modalId).modal({})
}

function ModalError(option){
	option = option || {}
	var modalId = option["modalId"] || "defaultModalErrorId"
	var title = option["title"] || "Error"
	var message = option["message"] || ""
	var onClose = option["onClose"] || undefined
	var onOk = option["onOk"] || undefined
	var modal = $("#"+modalId)
	if (modal.length == 0){
		var tmp =
			'<div id="'+modalId+'" class="modal hide fade">'+
				'<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
					'<h3 id="modal-error-title" >Modal header</h3></div>'+
						'<div id="modal-error-message" class="modal-body"></div>'+
						'<div id="modal-error-footer" class="modal-footer">'+
						'</div>'
	    $("body").append($(tmp))
	}
	else{
		$("#modal-error-close").unbind()
		$("#modal-error-ok").unbind()
		$("#modal-error-footer").children().remove()
		$("#modal-error-message").children().remove()
	}

	if (onOk) $("#modal-error-footer").append($('<a id="modal-error-ok" data-dismiss="modal" class="btn btn-primary">Ok</a></div>'))
	if (onClose) $("#modal-error-footer").append($('<a id="modal-error-close" data-dismiss="modal" class="btn">Close</a></div>'))

	$("#modal-error-close").click(onClose)
	$("#modal-error-ok").click(onOk)

	$("#modal-error-title").text(title)
	$("#modal-error-message").append($("<p>"+message+"</p>"))
	$("#"+modalId).modal({})
}
