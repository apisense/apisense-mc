/* Import
<link href="../public/jslibs/codemirror-2.2/lib/codemirror.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/lib/util/simple-hint.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/theme/cobalt.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/theme/eclipse.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/theme/neat.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/theme/night.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/theme/monokai.css" rel="stylesheet" type="text/css" />
<link href="../public/jslibs/codemirror-2.2/theme/rubyblue.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../public/jslibs/codemirror-2.2/lib/codemirror.js"></script>
<script type="text/javascript" src="../public/jslibs/codemirror-2.2/lib/util/simple-hint.js"></script>
<script type="text/javascript" src="../public/jslibs/codemirror-2.2/lib/util/javascript-hint.js"></script>
<script type="text/javascript" src="../public/jslibs/codemirror-2.2/mode/javascript/javascript.js"></script>
<script type="text/javascript" src="../public/jslibs/codemirror-2.2/mode/xml/xml.js"></script>
<script type="text/javascript" src="../public/jslibs/codemirror-2.2/mode/xquery/xquery.js"></script>

<script type="text/javascript" src="../public/jslibs/jqwidgets/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="../public/jslibs/jqwidgets/jqwidgets/jqxsplitter.js"></script>
<script type="text/javascript" src="../public/jslibs/jqwidgets/jqwidgets/jqxexpander.js"></script>
<script type="text/javascript" src="../public/jslibs/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="../public/jslibs/jqwidgets/jqwidgets/jqxtabs.js"></script>

*/

function AtEditor(element, _configuration) {

	// Basic configuration
	var configuration = {
			mode : "xml",
			theme : "",
			lineNumbers: true,
			readOnly : false
	}

	// overload configuration attribute
	if (_configuration != undefined){
		for ( att in _configuration){
			configuration[att] = _configuration[att]
		}
	}

	// Define id used
	var mode = "xml";
	var idbase = "idbase";
	var idmenu = "idmenu";
	var idtabs = "editorstabs";
	var idhtabs = "editorhtabs";
	var idConsoleEditor = "secondeditor"
	var idEditors = "ideditors";

	var consoleEditor = null;

	var editors = {};

	var currentId = 0;


	this.setSecondEditor = function(mode,completion){
		consoleEditor = createEditor(idConsoleEditor,{
			mode : "xml",
			theme : "cobalt",
			lineNumbers: false,
			readOnly : true,
			extraKeys: {
				
			}
		})
	}

	this.build = function(){

		// create menu toolbar
		element.append("<div id="+idmenu+" class='atdegrader atround3 atshadow3' style='margin : 0 0 5px 0; padding : 5px 25px 5px 20px; height:30px'></div>")

		// create main div containing both editors
		element.append("<div id="+idbase+" ></div>")
		
		// div containing tabs
		$("#"+idbase).append("<div id="+idtabs+"></div>")
		
		// div containing second editor
		$("#"+idbase).append("<div><textarea style='' id="+idConsoleEditor+"></textarea></div>")

		// define tabs header
		$("#"+idtabs).append("<ul id="+idhtabs+" style='margin: 30px;' id='unorderedList'> <li canselect='false' style='padding: 5px; font-style:italic; font-size: 14px; border: none; background: transparent;' hasclosebutton='false'>+</li> </ul>")
		
		// define tabs content : 
		$("#"+idtabs).append("<div></div>")
		
		// split tabs and console editor
		$("#"+idbase).jqxSplitter({orientation: "horizontal",theme: "summer", height : 1000, panels: [{ size: 400 }, { size: 400}] })

		
		// create tabs element
		$('#'+idtabs).jqxTabs({ height : 400, showCloseButtons: true,keyboardNavigation:false, theme: "darkblue"});
		$('#'+idtabs).bind('tabclick', function (event) {
			currentId = event.args.item
			if (event.args.item == ($("#"+idhtabs).find('li').length - 1)){
				addEditor("file.xq",currentId)
			}
		})

		
		$('#'+idbase).bind('resize', function (event) {
			$('#'+idtabs).jqxTabs({height:event.args.firstPanel.size})
		})
	}

	var addEditor = function(name, i){

		var id = "editor"+new Date().getTime()
		var node = "<div> <textarea id='"+id+"'> </textarea> </div>"

		$('#'+idtabs).jqxTabs('addAt',i,name,node);

		var edit = createEditor(id,configuration)
		editors[id] = edit

		$("#"+idhtabs).find('li').each(function(index){
			if(index == i)
				$(this).append("<input type='hidden' name='edit' value="+id+">")
		})
	}

	var createEditor = function(id,_conf){
		
		var ed =  CodeMirror.fromTextArea(document.getElementById(id),_conf);
		return ed;
	}

	this.addMenuButton = function(name, image, hand){
		$("#"+idmenu).append("<button id="+name+" class='button transparent small' style='color:white;'><img SRC="+image+" width='20px' height='20px' ></img><div style='float:right;margin:10px 0 0 5px;'>"+name+"<div></button>")
		$("#"+name).click(hand)
	}

	this.addMenuInput = function(name){
		$("#"+idmenu).append("<input id="+name+" style='position:absolute; width:500px;height:25px;' type='text'></input>")
	}


   /**
	* Return text of current tabs
	*/
	this.getText = function(){

	   $("#"+idhtabs).find("li").each(function(index){
		   if (index == currentId){
			   id = $(this).find("input").attr("value")
		   }
	   })

	   if (id != undefined)
		   return editors[id].getValue()
   	}

   /**
    * Return text on current tabs
    */
    this.setText = function(text){

	   $("#"+idhtabs).find("li").each(function(index){
		   
		   if (index == currentId)
			   id = $(this).find("input").attr("value")
	   })

	   if (id != undefined)
		   return editors[id].setValue(text)
   	}

   this.setTextConsole = function(text){
	   consoleEditor.setValue(text)
   }

   this.addTabs = function(name, text){
	   addEditor(name, $("#"+idhtabs).find('li').length - 1)
	   this.setText(text)
   }

}