

function FDViewver(_conf){
	
	var conf = _conf || {}
	conf["color1"] = conf["color1"] || "#dddddd"
	conf["color2"] = conf["color2"] || "#fcd3a5"
	conf["textSize"] = conf["textSize"] || "15px"
	conf["nodeWidth"] = conf["nodeWidth"] || 200
	conf["nodeHeight"] = conf["nodeHeight"] || 50

	var labelType, useGradients, nativeTextSupport, animate,fd,st;
	
	var print = function(o){alert(JSON.stringify(o))}

	var selectedFeature = {}
	this.selected = selectedFeature
	
	
	var createTip = function(node){
	  var html = "<pre>"
	  
	  if (node.data["cardMin"] == 0)        html += "<strong>Optional</strong>";
	  else if (node.data["cardMin"] == 1)   html += "<strong>Mandatory</strong>";
	  else 	html += "<strong>Cardinality</strong> : "+node.data["cardMin"]+".."+node.data["cardMax"];
	  html += "<br/>"
	  if (node.data["type"] == "orvp")        html += "<strong>Type</strong> : Or-Groups";
	  else if (node.data["type"] == "xorvp")  html += "<strong>Type</strong> : XOr-Groups";
	  else html += "<strong>Type</strong> : Variant";
	  
	  html +="<div>"
	  for (att in node.data["attributes"]){
		  if (att != "replaceAll")
		    html += "<strong>"+att+"</strong> : "+node.data["attributes"][att]["name"]
	  }
	  
	  html +="</div>"
		  
	  html += "</pre>"
	  return html;
	}
		
	var createLabel = function(node){
		var html = ""
		// Define cardinality	 
	    if (node.data["cardMin"] == 0)        html += node.name;
	    else if (node.data["cardMin"] == 1)   html += node.name+"<i style='float:right' class='icon-asterisk icon-red'></i>";
	    else 	html += "<strong>Cardinality</strong> : "+node.data["cardMin"]+".."+node.data["cardMax"];
	    
	    return "<div style ='height:"+conf["nodeHeight"]+"px;width:"+conf["nodeWidth"]+"px;cursor : pointer;text-align:center'>"+html+"</div>";
        
	}
	
	
	var init = function(){	
		var ua = navigator.userAgent,
	    iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
	    typeOfCanvas = typeof HTMLCanvasElement,
	    nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
	    textSupport = nativeCanvasSupport 
	      && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
	    //I'm setting this based on the fact that ExCanvas provides text support for IE
	    //and that as of today iPhone/iPad current text support is lame
	    labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
	    nativeTextSupport = labelType == 'Native';
	    useGradients = nativeCanvasSupport;
	    animate = !(iStuff || !nativeCanvasSupport);
	}
	
    var initGraph = function(){
		
		st = new $jit.ST({
	        'injectInto': 'infovis',
	        orientation: 'left',
	        levelDistance: 10,
	        levelsToShow: 30,
	        constrained: false,

	        offsetX: 350, offsetY: 50,
	        transition: $jit.Trans.Quart.easeInOut,
	        
	        Navigation: { enable: true, panning: 'avoid nodes', zooming: true},
	        Edge: {type: 'bezier', overridable: true},
	     
	        Events : {  
	        	enable: true,  
	        	onRightClick : function(node, eventInfo, e){  if (node) st.onClick(node.id)},
	        		
	        	onClick: function(node, eventInfo, e) {
	        		
	        		if (!node) return
	        		
	        		var max = node.data["cardMax"]
	        		if (max <= 1){
	        			if (selectedFeature[node.name]){
	        				selectedFeature[node.name] = undefined
	        			}
	        			else selectedFeature[node.name] = 1
	        		}
	        		else print("Selection of features with multiple cardinalities is not implemented")
	        		
	        		st.refresh()
	        	} 	  
	        },
	        onBeforePlotNode: function(node) {
	          if (selectedFeature[node.name]){
	        	  node.setData('color',conf["color2"]);  
	          }
	          else node.setData('color',conf["color1"]);  
	        },  
	        Tips: {  
	            enable: true,  
	            type: 'HTML',  
	            offsetX: 10,  
	            offsetY: 10,  
	            onShow: function(tip, node) { tip.innerHTML = createTip(node);}  
	        }, 
	        
	        // Node configuration
	        Node: {
	          overridable: true,
	          height: conf["nodeHeight"],
	          width: conf["nodeWidth"],
	          type: 'roundrect',
	          color: conf["color1"],
	        },
	        
	       //align: 'right',
	       onBeforeCompute: function(node){},
	       onAfterCompute: function(node){},

	       Label: { type: 'HTML'},   
	       
	       onCreateLabel: function(label, node){
	         var style = label.style;
	         label.id = node.id;
	         style.fontSize = conf["textSize"];
	         label.innerHTML = createLabel(node);
	       }
	    });	
	}
    
  
	var idCounter = 0;
	var loadFeature = function(feature){
		
		if (feature["feature"] == undefined) return undefined;
		
		var graphNode = { id : idCounter, data : { }, children : [  ]}
		idCounter = idCounter + 1
		
		for (var key in feature){
		    
			var data = {
				"type"       : feature[key]["type"],
				"cardMax"    : feature[key]["cardinalityMax"],
				"cardMin"    : feature[key]["cardinalityMin"],
				"attributes" : feature[key]["attributes"],
				"card" 		 : feature[key]["cardinality"]
			}
			
			if (feature[key]["cardinality"] != 0) 
				selectedFeature[feature[key]["name"]] = feature[key]["cardinality"];

			graphNode["name"] = feature[key]["name"]
			graphNode["data"] = data
			
			var variants = feature[key]["variants"]
		    for (var i in variants){
			
				var node = {}
				if (variants[i].length){
					alert("please check this case")
					for (j in variants[i]){
						node[i] = variants[i][j]
						var childs = loadFeature(node)
						if (childs != undefined){
							graphNode["children"][graphNode["children"].length] = childs
						}
					}
				}
				else{
					
					node[i] = variants[i]
					
					var childs = loadFeature(node[i])
					if (childs != undefined){
						graphNode["children"][graphNode["children"].length] = childs
					}
				}
			}
	    }
		return graphNode;
	}
	
	
	
	this.load = function(featureDiagram){
		
		init();
		initGraph();
		
		fd = loadFeature(featureDiagram)
		
	    st.loadJSON(fd);
	    
	    st.graph.getNode("0").setPos(new $jit.Complex(-200,0), 'current'); 
	    
	    //compute node positions and layout
	    st.compute();
	    
	    //optional: make a translation of the tree
	    st.geom.translate(new $jit.Complex(-200, 0), "current");
	    
	    //Emulate a click on the root node.
	    st.onClick(st.root);
	    
	    

	}
	
	

	//*************************************
	//*** Round Corner Node Implementation
	//*************************************
	$jit.ST.Plot.NodeTypes.implement({ 
	  'roundrect': { 
	    'render': function(node, canvas, animating) { 
	      var pos = node.pos.getc(true), nconfig = this.node, data = node.data; 
	      var width  = nconfig.width, height = nconfig.height; 
	      var algnPos = this.getAlignedPos(pos, width, height); 
	      var ctx = canvas.getCtx(), ort = this.config.orientation; 
	      ctx.beginPath(); 
	      var r = 10; //corner radius 
	      var x = algnPos.x; 
	      var y = algnPos.y; 
	      var h = height; 
	      var w = width; 
	      ctx.moveTo(x + r, y); 
	      ctx.lineTo(x + w - r, y); 
	      ctx.quadraticCurveTo(x + w, y, x + w, y + r); 
	      ctx.lineTo(x + w, y + h - r); 
	      ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h); 
	      ctx.lineTo(x + r, y + h); 
	      ctx.quadraticCurveTo(x, y + h, x, y + h - r); 
	      ctx.lineTo(x, y + r); 
	      ctx.quadraticCurveTo(x, y, x + r, y); 
	      ctx.fill(); 
	    } 
	  } 
	}); 
	
}




