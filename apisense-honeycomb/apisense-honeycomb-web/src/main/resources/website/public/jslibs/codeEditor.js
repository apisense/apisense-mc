

function confirm(text,action){
	new Modal({message : text,
		onClose:function(){},
		onOk:function(){action()}})
}

function CodeEditor(element, _configuration, onClickItem) {

	var tabContainer = undefined;
	var menubar = undefined;
	var onLoad = undefined;
	var tree = undefined;

	var editors = {};
	//var currentId = 0;

	var consoleEditor = undefined;

	this.ready = function(func){onLoad = func;}

	this.addMenu = function(label, onClick, icon,id){
		var _id = id || label
		$("#id-menu").append("<li class='cemenu' id="+_id+" style='font-size:14px; cursor:pointer;'> <i class='"+icon+"'></i> "+label+"  <span class='divider'></span></li>")
		$("#"+_id).click(onClick)
	}

	this.activeMenu  = function(){$(".cemenu").show();}
	this.disableMenu = function(){$(".cemenu").hide();}

	/**
	 * Add space in menu bar
	 */
	this.addMenuIcon = function(icon){
		$("#id-menu").append("<li> <img SRC="+icon+" width='20px' height='20px' ></img><span class='divider'></span> </li>")
	}

	/**
	 * Add input text in menu bar
	 */
	this.addMenuInput = function(label, name,width){
		$("#id-menu").append("<span class='cemenu'><strong>"+label+": </strong> <input id="+name+"  style='margin-left:10px; width:"+width+";height:15px;' type='text'></input></span>")
	}

	this.addMenuInputPassword = function(label, name,width){
		$("#id-menu").append("<span class='cemenu'><strong>"+label+": </strong> <input id="+name+"  style='margin-left:10px; width:"+width+";height:15px;' type='password'></input></span>")
	}

	this.addSeparator = function(){ $("#id-menu").append("<i style='margin-left:40px;'></i>") }



	this.addTab = function(title, config){

		if (editors[title+"-content"]){
			tabContainer.selectChild(dijit.byId(title+"-content"))
			return;
		}

		var config = config || {content:""}

		var id = "editor-"+new Date().getTime()

		var mixedMode = {
         name: "htmlmixed",
         scriptTypes: [{matches: /\/x-handlebars-template|\/x-mustache/i,
                       mode: null},
                      {matches: /(text|application)\/(x-)?vb(a|script)/i,
                       mode: "vbscript"}]
        };

		var mode = "html";
		var label = title.toString();
		var ext = label.substring((label.lastIndexOf('.')),label.lenght)
		if (ext == ".js") mode = "javascript"
		else if (ext == ".xq") mode = "xquery"
		else if (ext == ".py") mode = "python"
		else if (ext == ".css") mode = "css"
		else if (ext == ".html") mode = mixedMode

		var _groupHeight
		group = new dijit.layout.ContentPane({
			id:title+"-content",
			style:"border-radius: 0.5em 0.5em 0.5em 0.5em; padding: 2px;height:100%;",
			title : title,
			closable : "true",
		    resize: function(resizeEvent) {
			   var _ed = editors[title+"-content"];
				if (_ed){
				  _ed.setSize("100%",resizeEvent.h+"px")
				  _ed.refresh();
				}

				_groupHeight = resizeEvent.h
			},
			onClose : function(){
				delete editors[title+"-content"]
				return true;
			},
			content: "<textarea style='background:green;' id='"+id+"'> </textarea>"
	    });

		tabContainer.addChild(group);
		tabContainer.selectChild(group)

		var ed = CodeMirror.fromTextArea(document.getElementById(id),{

			// code editor language
			mode  : mode ,

			// graphical theme
			theme : config["theme"] || "eclipse" ,

			// show line number
			lineNumbers : config["lineNumbers"] || true ,

			onKeyEvent : function(ed,keyEvent){

				var widget = tabContainer.selectedChildWidget;
				var title = widget.title

				if (title.indexOf('*') != (title.length -1)){

					// if action begining by ctrl key return
					if (keyEvent["keyCode"] == 19)return ;
					if (keyEvent["keyCode"] == 17)return ;
					if (keyEvent["keyCode"] == 83)return ;

					widget.attr("title", title +" *");
				}
			},
			extraKeys: {
				"Ctrl-S": function(instance) {
						$("#Save").click()
				},
				"F11" : function(cm){

				}
			}

		})


		if (config["content"]) ed.setValue(config["content"])

		editors[group.id] = ed;

		ed.setSize("100%",_groupHeight+"px")
		ed.refresh();
	}

	this.getCurrentFilename = function(){
		var widget = tabContainer.selectedChildWidget;
		return widget.id;
	}

	/**
	 * Set message in console editor
	 * @mess {String} message
	 */
	this.setConsoleValue = function(mess){
		consoleEditor.setValue(mess)
	}

	this.addConsoleValue = function(mess){
		var line = consoleEditor.lastLine();
		consoleEditor.setLine(line,mess+"\n")
		if (line > 15){
		 $('#id-editor-console').parent().parent().animate({
			scrollTop: $('#id-editor-console').parent().parent().prop("scrollHeight")
		 }, 1);
		}
	}

	/**
	 *  Menu New File
	 *  @param funk Handler called with filename to create in argument
	 *  @param icon Icon of menu
	 */
	this.addMenuNewFile = function(funk,filters,icon){var ic = icon || "icon-file"; this.addMenu("New",function(){_newFile(funk,filters);},ic)}
	var _newFile = function(funk, filters){


		new Modal({
			title   : "Create new file",
			message :  "Filename : <input  id='dialog-input-text' type='text' placeholder='filename' autofocus/>" +
	  		"<br/><br/>" +
	  		"<button  class='id-dialog-ok 	 btn btn-primary'>Ok</button>  " +
	  		"<button data-dismiss='modal' class='id-dialog-cancel btn '>Cancel</button>"
		})

		$(".id-dialog-ok").click(function(){


			var filename = $("#dialog-input-text").val()

			// check file extension
			var i = filename.lastIndexOf('.')
			if (i == -1) {var ext = ""}
			else {  var ext =  filename.substring(i,(filename.lenght))}


			var accept = false;
			for (var i in filters){
				filter = filters[i]
				if (filter == ext){
					accept = true;
				}
			}
			if (!accept){

				new Modal({
					title   : "Error",
					message : "Error Only file with extension "+JSON.stringify(filters)+" is supported"
				})

				return;
			}

			new Modal({
					title   : "Success",
					message : "File created",
					onOk : function(){}
			})

			$("#dialog-input-text").remove()

			funk(filename)

		});
		$(".id-dialog-cancel").click(function(){dialog.hide()});

	}

	/**
	 * Menu Save file
	 */
	this.addMenuSaveFile = function(funk,icon){
		var ic = icon || "icon-hdd";
		this.addMenu("Save",function(){_savefile(funk);},ic)
	}
	var _savefile = function(funk){

		var widget = tabContainer.selectedChildWidget;

		var title = widget.id.split("-")[0];
		var content = editors[widget.id].getValue();

		funk(title,content)

		var widget = tabContainer.selectedChildWidget;
		var title = widget.title

		if (title.indexOf('*') == (title.length -1)){
			widget.attr("title", title.substring(0,title.length -2));
		}
	}


	/**
	 * Menu Delete file
	 */
	this.addMenuDeleteFile = function(funk,icon){
		var ic = icon || "icon-trash"
		this.addMenu("Delete",function(){_deleteFile(funk)},ic)
	}
	var _deleteFile = function(funk){

		var container = tabContainer.selectedChildWidget
		if (container != undefined){

			new Modal({
					title   : "Confirmation",
					message : "delete "+container.id.split("-")[0]+" ?",
					onOk : function(){
						var title = container.id.split("-")[0];
						funk(title)
					},
					onClose : function(){}
			})
		}
	}



	this.updateTreeModel = function(rawdata){


			var store = new dojo.data.ItemFileReadStore({
		          data: { identifier: 'id', label : 'label', items: rawdata }
		    });
			var store = new dijit.tree.ForestStoreModel({ store: store });



			tree.dndController.selectNone();

		    tree.model.store.clearOnClose = true;
		    tree.model.store.close();

		    // Completely delete every node from the dijit.Tree
		    tree._itemNodesMap = {};
		    tree.rootNode.state = "UNCHECKED";
		    tree.model.root.children = null;

		    // Destroy the widget
		    tree.rootNode.destroyRecursive();

		    // Recreate the model, (with the model again)
		    tree.model.constructor(store)

		    // Rebuild the tree
		    tree.postMixInProperties();
		    tree._load();
	}

	this.deleteAllTabs = function(){

		try{
			tabContainer.destroyDescendants(false);


		}catch(err){alert(err)}

	}

	require(["dojo/ready",
	         "dojo/parser",
	         "dijit/registry",
	         "dijit/layout/BorderContainer",
	         "dijit/layout/TabContainer",
	         "dijit/layout/ContentPane",
	         "dojo/data/ItemFileReadStore",
	         "dijit/tree/ForestStoreModel",
	         "dijit/Tree",
	         "dijit/Dialog"],

	         function(ready, parser, registry){
		ready(80, function(){

			// create main container
			var mainNode = new dijit.layout.BorderContainer({
				style : "width: 100%; height: "+($(window).height()-50)+"px; padding: 0;",
				class : "",
				liveSplitters:true,
			},"editor")
			mainNode.startup();

			// detect window resize event and update height of main container
			$(window).resize(function(){ mainNode.resize({h: ($(window).height()-50), w: "100%"})});

			// add menu container
			menubar = new dijit.layout.ContentPane({
				id : "id-menu",
				region:"top",
				style :  "margin : 0 0 0px 0;padding-top:5px; height:35px; border:none; background:#f5f5f5;",
				class : "breadcrumb visible-desktop ;"
			})
			mainNode.addChild(menubar)

			//-------------------------------------------------------------------------------------- File

			 var rawdata = [];
			 var store = new dojo.data.ItemFileReadStore({data: { identifier: 'id', label : 'label', items: rawdata }});
		     var model = new dijit.tree.ForestStoreModel({ store: store });

			 tree =  new  dijit.Tree({
				region:"left",
			    autoExpand:true,
				style : "width : 200px;",
				splitter : "true",
				content : "<div id='tree'></div>",
				model: model,
		        showRoot: false,
		        getIconClass: function(/*dojo.store.Item*/ item, /*Boolean*/ opened){
			            if ((!item || this.model.mayHaveChildren(item))){
			            	return (opened ? "dijitFolderOpened" : "dijitFolderClosed")
			            }
			            else{

			            	var label = item.label.toString()
			            	var ext = label.substring((label.lastIndexOf('.')),label.length)
			            	if (ext == ".js") return "ic-file-js"
			 				else if (ext == ".xq") return "ic-file-xq"
			 				else if (ext == ".py") return "ic-file-py"
			 				else if (ext == ".css") return "ic-file-css"
			 				else if (ext == ".html") return "ic-file-html"
			 				else return "dijitLeaf"

			            }

			     }
		     })
			 mainNode.addChild(tree)

			 if (onClickItem){ tree.onClick = function(item){  onClickItem(item)  } }


			//-------------------------------------------------------------------------------------- Content

			tabContainer = new dijit.layout.TabContainer({
				style : "width: 100%; height: 100%;",
				region: "center"
			})

			tabContainer.watch("selectedChildWidget", function(name, oval, nval){
				editors[nval.id].refresh()

			});
			tabContainer.startup();

			mainNode.addChild(tabContainer);

			var console = new dijit.layout.ContentPane({
				style : "background-color:#262626; border-radius: 0.5em 0.5em 0.5em 0.5em; height: 50%;",
				region:"bottom",
				splitter:true,
				content : "<div><textarea id='id-editor-console'> </textarea></div>"
			})

			mainNode.addChild(console);

			consoleEditor = CodeMirror.fromTextArea(document.getElementById("id-editor-console"),
					{mode : "xml",theme : "lesser-dark",lineNumbers: false,readOnly : true}
			)
			consoleEditor.setSize("100%","100%")

			// call callback
			if (onLoad) onLoad()
		});
	});



}

