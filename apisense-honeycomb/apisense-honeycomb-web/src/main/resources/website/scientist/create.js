

var loadFeatureDiagram = function(handler){
	post("../../experiment-manager/get/default/configuration",{},
   		 function(message){
			handler(message)
   		 },
   		 function(err){  alert("error "+err); }
   )
}

var next = function(){ $("#slider").carousel("next")}
var prev = function(){ $("#slider").carousel("prev")}

var error = function(el,mess){ $(el).text(mess); $(el).show()}
var clear = function(el){ $(el).hide() }

var validConfiguration = function(isValid){
	post("../../experiment-manager/get/valid/configuration",
			{"features" : JSON.stringify(fdViewer.selected)},
			function(mess){ isValid()},
			function(err){error(".alert",err)}
	)
}

var _name,_description,_copyright
var validForm = function(isValid){

	 clear(".alert")
	_name = $("#name").val()
	_niceName = $("#niceName").val()
	_description = $("#description").val()
	_copyright = $("#copyright").val()

	if (!_name)					{error("#error-form","Experiment name missing");return;}
	if (!_niceName)	_niceName = name;
	if (!_description) 			{error("#error-form","Description missing");return;}
	if (!_copyright)			{error("#error-form","Copyright missing");return;}

	var regex = new RegExp("^[a-zA-Z0-9]+$");
	if (!regex.test(_name)){ error(".alert","Please no special charecters or number for Experiment name");return;}

	post("../../experiment-manager/get/valid/name",{name : _name},function(mess){
		if (mess) isValid()
		else error(".alert","Name is already used")
	})
}

var validLicence = function(isValid){

	if ($("#chk-lic-accept").prop('checked')){
		isValid()
	}
	else error("#error-lic","Need check")
}

var fdViewer

$(function() {

	$('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });

	$("#bt-next-form").click(function(){

		validForm(function(){
			next();
			if (!fdViewer){
				loadFeatureDiagram(function(fd){
					fdViewer = new FDViewver({
						color1 : "#EEEEEE",
					    color2 : "#FFA500",
					    nodeWidth:150,
					    nodeHeight:20,
					    textSize : "13px"
					})
					fdViewer.load(fd["slice"]["variants"])
				});
			}
		})
	})

	$("#bt-next-fd").click(function(){
		validConfiguration(function(){
			next();
		})
	})

	$("#bt-prev-fd").click(function(){prev();})
	$("#bt-prev-lic").click(function(){prev();})


	$("#bt-create").click(function(){


		validLicence(function(){
			$("#bt-create").attr("disabled", "true");
			post(
		    		 "../../experiment-manager/add/xp",
		    		 { name : _name , niceName:_niceName, description :encode(_description) , copyright : encode(_copyright), spl : JSON.stringify(fdViewer.selected)},
		    		 function(r){
		    			 redirect("../../website/scientist")
		    		 },
		    		 function(err){ new ModalError({message:err})}
		    )
		})
	})

})