
var dialog = undefined;

$(function(){ updateTable();})

var updateTable = function(){
	
	post("../../experiment-manager/get/xps",{token : token()},
	 function(xps){
				 
		$("#xpsBody").children().remove();
		
		for (i in xps){
			
			var xp = xps[i]["experiment"]
			
			var xpname = xp["name"]
			var status = (xp["status"] == "true") ? "running" : "stop"
			
			var line = $("<tr class='success'></tr>")
			line.append($("<td>"+xpname+"</td>"))
			line.append($("<td>"+status+"</td>"))
			
			var id = xpname
			if (status == "running") {
				var idStop = "stop-"+xpname
				line.append($("<td><button class='btn btn-warning' id='"+idStop+"' >stop</button></td>"))
			
			    var idEdit = "edit-"+xpname
			    line.append($("<td><button id='"+idEdit+"' class='btn btn-link'>edit</button></td>"))
			
			}
			else{
				var idStart = "start-"+xpname
				line.append($("<td><button class='btn btn-success' id='"+idStart+"' >start</button></td>"))
				
				var idEdit = "edit-"+xpname
			    line.append($("<td><button id='"+idEdit+"' class='btn btn-link' disabled='disabled' >edit</button></td>"))
			}
			
			
			var idDelete = "delete-"+xpname
			line.append($("<td><button id='"+idDelete+"' class='btn btn-danger'>delete</button></td>"))
			
			$("#"+idStop).unbind();$("#"+idEdit).unbind();$("#"+idDelete).unbind();$("#"+idStart).unbind()
			
			$("#xpsBody").append(line);
			
			$("#"+idStop).on("click",function(ev){
				var name = ev.target.id.substring(5)
				stopExperiment(name)
			})
			$("#"+idStart).on("click",function(ev){
				var name = ev.target.id.substring(6)
				startExperiment(name)
			})
			$("#"+idEdit).on("click",function(ev){
				var name = ev.target.id.substring(5)
				editExperiment(name)
			})
			$("#"+idDelete).on("click",function(ev){
				var name = ev.target.id.substring(7)
				deleteExperiment(name)
			})
			
				
		}
		
	},
	function(error){})
	
}


var editExperiment = function(experimentName){
	redirect("../../website/"+experimentName+"/") 
}

var startExperiment = function(experimentName){
	 post("../../experiment-manager/update/start",
	  		  {token : token(), name : experimentName}, 
	  		  function(mess){
	  			  
	  			 new Modal({
	  				 title:"Success",
	  				 message : "Experiment "+experimentName+" started",
	  				 onOk : function(){}
	  			 })
	  			  
	  			  
	  			  updateTable();
	  		  },
	  		  function(er){
	  			  alert(er)
	  		  }  
	  	  ,{idLoader:true}
	 )
}

var stopExperiment = function(experimentName){
	post("../../experiment-manager/update/stop",
			  {token : token(), name : experimentName}, 
			  function(mess){
				  
				   new Modal({
	  				 title:"Success",
	  				 message : "Experiment "+experimentName+" stoped",
	  				 onOk : function(){}
	  			   })

				  updateTable();
			  },
			  function(er){}
	)
}

var deleteExperiment = function(experimentName){
	post("../../experiment-manager/delete/xp",{token : token(), name : experimentName}, 
			function(mess){
				
				new Modal({
	  				 title:"Success",
	  				 message : "Experiment "+experimentName+" dropped",
	  				 onOk : function(){}
	  			})
				updateTable();
			},
			function(er){ 
				
				new Dialog("Error",er,
	  					function(){updateTable()},
	  					function(){});
		  	   
			}
	)
}
