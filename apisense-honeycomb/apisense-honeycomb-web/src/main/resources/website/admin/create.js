
$(function() {
	clear()
	
	$("#submit").click(function(){
		
		
		var _fullname = $("#fullname").val()
		var _username = $("#username").val()
		var _password = $("#password").val()
		var _rePassword = $("#rePassword").val()
		var _email = $("#email").val()
		
		
		if (!_fullname)					{error("Missing full name");return;}
		if (!_username) 				{error("Missing username");return;}
		if (!_password)					{error("Missing password");return;}
		if (!_rePassword) 				{error("Missing password");return;}
		if (_password != _rePassword)	{error("Error password");return;}
		if (!_email) 					{error("Missing email");return;}
		
		
		post(
			"../../user-manager/add/user/scientist",
			{fullname : _fullname, username : _username, password : _password, email : _email},
			function(r){  
				
				new Modal({
	  				 title:"Success",
	  				 message : "New user created",
	  				 onOk : function(){redirect("")}
	  			 })
			},
			function(err){
				error(err)
			}
		)
		
	})
})

var clear = function(){
	$(".info").remove();
	$(".alert").remove()
}

var error = function(mess){
	
	var node = $(".alert")
	if (node.length == 0)
		$("#message").prepend("<div class='alert'></div>")

	$(".alert").text(mess)
	
}

var info = function(mess){
	
	var node = $(".info")
	if (node.length == 0)
		$(".atform").prepend("<div class='info'></div>")

	$(".info").text(mess)
	
}
