
var grid  = undefined;
var store = undefined;
var dataStore = undefined;

$(function(){
	
	
	require(["dojo/ready",
	         "dojo/parser",
	         "dijit/registry",
	         "dijit/layout/BorderContainer",
	         "dijit/layout/ContentPane",
	         "dojox/grid/DataGrid",
	         "dojo/data/ItemFileReadStore",
	         "dijit/form/Button"], 

	         function(ready, parser, registry){
		ready(80, function(){
			
			
			post("../../user-manager/get/users",{},
					function(s){
						
				store = { identifier : "id", items : [] }
				
				var users = s["array"]["user"]
				for (var i in users){
					var user = users[i];
					user["id"] = i;
					store.items.push(user)
				}
				
				function formater(arg1,arg2){
					
		            var w = new dijit.form.Button({
		                label: "delete",
		                onClick: function() {
		                	var id = grid.getItem(arg2).id
		                	var item = store.items[id]
		                	
		                	post("../../user-manager/delete/username",{token:token(),username:item.username.toString()},
		                			function(mess){redirect("")},
		                			function(err){alert(err)});
		                	
		                }
		            });
		            w._destroyOnRemove=true;
		            return w;
		        }
				
				var layout = [
				   {name: 'Id', field: 'id'},
				   {name: 'fullname', field: 'fullname'},
				   {name: 'username', field: 'username'},
				   {name: 'Role', field: 'role' ,width:'200px;'},
				   {name: 'email', field: 'email',width:'200px;'},
				   {name: 'edit', field: 'Edit', width:'75px;',formatter: formater }
				]
				                       
				               
				dataStore = new dojo.data.ItemFileReadStore( { data:store } );
				grid = new dojox.grid.DataGrid({
			        id: 'grid',
			        style:"margin-left: auto;margin-right: auto",
			        store : dataStore,
			        structure: layout,
			        autoWidth: true,
		            autoHeight: true,
			        rowSelector: '20px'},
			      "accounts");
				
				grid.startup();
				
				
					
			},
			function(err){alert(err)})
			
			
			
		})})
	
	

})



