
var fdViewer = undefined

var loadFeatureDiagram = function(handler){
	post("../../../experiment-manager/get/default/configuration",{token : token(),name:xpName},
   		 function(message){
			handler(message)
   		 },
   		 function(err){  alert("error "+err); }
   )
}



$(function() {

	$('#tab-conf a:first').tab('show');
	$('a[data-toggle="tab"]').on('shown', function (e) {
		 if (e.target.id == "id-configuration"){

		 if (!fdViewer){

		loadFeatureDiagram(function(fd){

			fdViewer = new FDViewver({
				color1 : "#EEEEEE",
				color2 : "#FFA500",
				nodeWidth:150,
				nodeHeight:20,
				textSize : "13px"
			})
			fdViewer.load(fd["slice"]["variants"])
		});
	}}})



	post("../../../experiment-manager/get/xp",{token : token(),name:xpName},function(result){


		var xp = result["experiment"]



		$("#xpname").val(xp["name"]);$("#xpname").prop('disabled', true);
		if (xp["niceName"]){
			$("#niceName").val(xp["niceName"]);
		}else $("#niceName").val(xp["name"]);

		$("#description").val(decode(xp["description"]));
		$("#copyright").val(decode(xp["copyright"]));

		$("#save-description").click(function(){

				post("../../../experiment-manager/update/xp/description",{
					token : token(),
					name:xpName,
					niceName : $("#niceName").val(),
					description : encode($("#description").val()),
				    copyright   : encode($("#copyright").val())

				},function(result){

					new Modal({
	  				 title:"Success",
	  				 message : "Experiment description saved",
	  				 onOk : function(){}
	  			    })

				},function(err){

					new Modal({
	  				 title:"Error",
	  				 message : "Error : "+err,
	  				 onOk : function(){}
	  			    })
				})

		})

		$("#save-configuration").click(function(){

			post("../../../experiment-manager/update/configuration",{
					token : token(),
					name:xpName,
					spl : JSON.stringify(fdViewer.selected)
				},
				function(result){new Modal({title:"Success",message : "Experiment configuration changed. Please restart experiment",onOk : function(){}})},
				function(err){new Modal({title:"Error",message : "Error : "+err,onOk : function(){}})})


		})
	})

})