/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.web.impl

import net.liftweb.json._
import javax.ws.rs.core.Response
import fr.inria.bsense.common.utils.MD5
import org.osoa.sca.annotations.Property
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.node.cbse.UserManagerComponent
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.node.web.service.WebConnectionManagerService
import javax.servlet.http.{ HttpServletRequest, HttpServletResponse, Cookie }
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.bsense.common.utils.FileConfiguration
import org.apache.commons.codec.binary.Base64
import fr.inria.bsense.node.configuration.NodeFileConfiguration
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.Log
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.apisense.security.APISecureContext
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.common.utils.JSONLiftNodeParser

@Scope("COMPOSITE")
class WebConnectionManagerImpl extends WebConnectionManagerService {

  @Property(name = "roles")
  var roles: String = null

  var contextPath : String = ""

  @Reference
  val userManager: UserManagerService = null

  def connect(username: String, password: String, request: HttpServletRequest, response: HttpServletResponse): String = {

    setResource(request)

    val userNode = new JSONLiftNodeParser(userManager.connect(username, MD5.hash(password)))

    val token = userNode \\ "success" text

    if (token equals "") return userNode \\ "message" text

    var urlRole = getURLRole(token, request, response)


    val cookie = new Cookie(WebConnectionManagerService.COOKIES,token)
    cookie.setPath("/")
    response.addCookie(cookie)
    request.getSession.setAttribute("session.username",username)
    response.sendRedirect(urlRole)

    ""
  }

   def disconnect(request: HttpServletRequest, response: HttpServletResponse) {

    var home = getResource

    val cookie = new Cookie(WebConnectionManagerService.COOKIES, "")
    cookie.setMaxAge(0)
    cookie.setPath("/")
    response.addCookie(cookie)
    request.getSession.setAttribute("session.username", null)


    response.sendRedirect(home);
  }

  def setResource(request : HttpServletRequest){

    val context = request.getContextPath()

    this.contextPath = request.getContextPath()
    if (this.contextPath equals "/website")
      this.contextPath = ""

  }

  def getResource()  : String = {
    this.contextPath+"/website/public"
  }
  def getCssResource : String = {getResource+"/css"}
  def getJsResource : String = {getResource+"/jslibs"}

  def hasToken(request: HttpServletRequest): Boolean = {

    val cookies = request.getCookies()
    if (cookies != null) {
      cookies.foreach {
        c => if (c.getName equals WebConnectionManagerService.COOKIES) return true
      }
    }
    false
  }

  def getUserName(request : HttpServletRequest) : String = {

    val token = getToken(request)
    if (token != null){
      try{
    	  val user = userManager.getUser(token)
    	  if (user != null)
    		  return user.asInstanceOf[APISecureContext].username
      }
      catch{ case t : Throwable => }

    }
    null
  }

  def getToken(request: HttpServletRequest): String = {

    setResource(request)

    val cookies = request.getCookies()
    if (cookies != null) {
      cookies.foreach {
        c =>
          if (c.getName equals WebConnectionManagerService.COOKIES) {
            return c.getValue()
          }
      }
    }

    null
  }

  def isUserInRole(role : String, request: HttpServletRequest,response : HttpServletResponse) {

    val errorRedirection = getResource+"/unauthorized"

    val token = getToken(request)

    if (token == null) response.sendRedirect(errorRedirection);

    val principal = userManager.getUser(token)

    if (!principal.isUserInRole(role)) response.sendRedirect(errorRedirection);

  }

  def getURLRole(token: String, request: HttpServletRequest, response: HttpServletResponse): String = {

    try{

    	val principal = userManager.getUser(token)

    	val aRoles = roles.split(";")
    	val role = aRoles.find( role =>
    	  if (!(role equals "null")){
    	    principal.isUserInRole(role.split(':')(0))
          } else false
    	)

    	// build role url home
    	var url = this.contextPath
    	if (!url.endsWith("/")) url = url + "/"
    	if (!url.startsWith("/")) url = "/" + url

    	url = url + role.get.split(":")(1)

    	Log.d("redirect to "+url)

    	if (role != None) url
    	else null

    }
    catch{ case t : Throwable => t.printStackTrace();null }
  }




  //----------------------------------------------------------------------------------------  File Manage. Need to be removed in other service

  def getFiles(sc : SecurityContext, repository : String) : String = {

    val authorId = sc.getUserPrincipal().getName()

    val builder =  new StringBuilder();
    builder append "[{"
    builder append " \"label\" : \""+repository+"\","
    builder append " \"id\" : \"1\","
    builder append " \"children\" : [  "

    var index = 1;
    NodeFileConfiguration("users/"+authorId+"/"+repository).getFiles.foreach{
      file =>
        builder append "{"
        builder append " \"label\" : \""+file.getName+"\","
        builder append " \"id\" : \"1."+index+"\""
        builder append "},"
        index +=1
    }


    builder.deleteCharAt(builder.length-1)
    builder append "]}]"

    Message.success(new JSONLiftNodeParser(builder toString))

  }


  def getFile(sc : SecurityContext,  repository : String, filename : String) : String = {
    val authorId = sc.getUserPrincipal().getName()
    Message.success(NodeFileConfiguration("users/"+authorId+"/"+repository).getFile(filename))
  }

  def updateFiles(sc : SecurityContext, repository : String, filename : String, content : String) : String = {
    val authorId = sc.getUserPrincipal().getName()
    NodeFileConfiguration("users/"+authorId+"/"+repository).updateFile(filename, content);
    Message.success()
  }

  def deleteFiles(sc : SecurityContext, repository : String, filename : String) : String = {
    val authorId = sc.getUserPrincipal().getName()
    NodeFileConfiguration("users/"+authorId+"/"+repository).deleteFile(filename);
    Message.success();
  }


  private def exec(message: String)(request: => String): String = {
    try { request }
    catch { case e : Exception => "Exception:" + message }
  }



}