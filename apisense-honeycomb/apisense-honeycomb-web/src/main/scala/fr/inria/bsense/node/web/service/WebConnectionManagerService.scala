/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.web.service

import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.Path
import javax.ws.rs.FormParam
import javax.ws.rs.POST
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext
import java.util.zip.ZipInputStream
import java.util.zip.GZIPInputStream
import java.io.ByteArrayInputStream
import scala.io.Source
import fr.inria.bsense.common.http.Base64

object WebConnectionManagerService {
  final val COOKIES = "fr.apisense.token"
}

trait WebConnectionManagerService {

  def connect(username: String, password: String, request: HttpServletRequest, response: HttpServletResponse): String

  def getResource()  : String;
  def getCssResource : String; 
  def getJsResource : String; 
  
  def getURLRole(token: String, request: HttpServletRequest, response: HttpServletResponse): String

  def hasToken(request: HttpServletRequest): Boolean

  def getToken(request: HttpServletRequest): String
  
  def getUserName(request : HttpServletRequest) : String

  def disconnect(request: HttpServletRequest, response: HttpServletResponse)
  
  def isUserInRole(role : String, request: HttpServletRequest,response : HttpServletResponse)
  
  @POST
  @Path("/get/files")
  def getFiles(@Context sc : SecurityContext, @FormParam("repository") repository : String) : String;
  
  @POST
  @Path("/get/file")
  def getFile(@Context sc : SecurityContext,@FormParam("repository") repository : String, @FormParam("filename") filename : String) : String;
  
  @POST
  @Path("/update/file")
  def updateFiles(@Context sc : SecurityContext, @FormParam("repository") repository : String,@FormParam("filename") filename : String, @FormParam("content") content : String) : String;
  
  @POST
  @Path("/delete/file")
  def deleteFiles(@Context sc : SecurityContext, @FormParam("repository") repository : String,@FormParam("filename") filename : String) : String;

}

object Main{
  
  def main(args: Array[String]) {
  
   
    
  }
  
}


