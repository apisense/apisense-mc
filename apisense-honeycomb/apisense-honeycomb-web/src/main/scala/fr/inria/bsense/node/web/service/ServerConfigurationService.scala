/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.web.service

import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.bsense.common.utils.Configuration
import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.bsense.common.cbse.BSenseComponent
import frascala.sca.Bean
import frascala.sca.Java
import fr.inria.bsense.node.configuration.ServerNodeConfiguration
import java.net.InetAddress
import fr.inria.bsense.node.BService
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.node.configuration.NodeFileConfiguration


//------------------------------------------------------------------------------------------ Component definition

case class ServerConfigurationComponent extends BSenseComponent("cpt-server-configuration"){
  this.uses(Bean[ServerConfigurationImpl])
  
  service("srv-server-configuration") exposes Java[ServerConfigurationService]
  
}



//------------------------------------------------------------------------------------------ Service definition


trait ServerConfigurationService {

  /**
   * 
   * Return file name of all configurations
   * stored in server node
   * @return
   */
  def configurations() : Array[String]
  
  /**
   * 
   * Return Server configuration
   * properties
   * @return
   */
  def getServerNodeConfiguration : Configuration
  
  /**
   * Check URL of Bee.sense central server
   */
  def pingBSenseServer(host : String) : Boolean 
  
  /**
   * Check node account
   * on Bee.sense central server
   */
  def checkNodeAccount(username : String, password : String) : Boolean
 
}

//------------------------------------------------------------------------------------------ Implementation definition

class ServerConfigurationImpl extends ServerConfigurationService{
  
  
   def configurations() : Array[String] = NodeFileConfiguration("configuration").getFiles.map{ f => f.getName }
   
   def getServerNodeConfiguration = ServerNodeConfiguration
   
   def checkNodeAccount(username : String, password : String) : Boolean = {
     
     
    val bservice =  BService()
    
    try{
    	bservice.connect(username, password)
    	true
   }catch{case e : Exception => false}
   
   }
   
   def pingBSenseServer(host : String) :Boolean = {
    
    var bsenseHost = host
    if (host.endsWith("/"))
    	bsenseHost = host.substring(0,host.length-2)
     
    try{	
        Log.i("Ping Bee.sense "+bsenseHost+"/"+BService.PING_SERVICE)
    	
        val pingResult = new HttpRequest().get(bsenseHost+"/"+BService.PING_SERVICE,Array[CharSequence]())
    	
        return pingResult.contains("success")
    }
    catch{case e : Exception => e.printStackTrace}
    
    false
  }
}
