/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.web.cbse

import frascala.sca.Java
import javax.servlet.Servlet
import frascala.frascati.HTTP
import frascala.frascati.WEB
import fr.inria.bsense.node.service.ModuleManagerService
import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.node.cbse._
import fr.inria.bsense.node.service.ExperimentModuleImpl
import fr.inria.bsense.node.service.ExperimentManagerService
import fr.inria.bsense.node.impl.ModuleManagerImpl
import frascala.sca.Bean
import org.osoa.sca.annotations.Property
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.node.web.service.WebConnectionManagerService
import frascala.frascati.SCA

//-------------------------------------------------------------------------
//--- Component definition


class ExperimentWebSite extends ExperimentComponent("fr.inria.bsense.website"){
  this.uses(WEB("website/experiment", "index/index.html"))
  
  
  val httpService = service("srv-velocity") exposes Java[Servlet]
  deployHttp(httpService,null)
   
  reference("webmanager").exposes(Java[WebModuleManagerService]).optional.autowired
  
  reference("manager").exposes(Java[ModuleManagerService]).autowired optional
  
  reference("ctx").exposes(Java[WebConnectionManagerService]) as SCA("fr.inria.antdroid.Web/srv-web-connection")
} 


class ExperimentWebModuleManager extends ExperimentComponent("fr.inria.bsense.webmanager"){
   this.uses(Bean[WebModuleManagerImpl])
  
    service("srv-web-manager") exposes Java[WebModuleManagerService]
    
    reference("ref-web-modules").exposes(Java[WebModuleService]).optional.multiple.autowired
} 


/**
 * 
 * 
 * 
 * @param _name   Module name
 * @param _xpname Experiment name
 */
abstract class ExperimentWebModuleComponent(_name: String) extends ExperimentComponent(_name)
{
  import frascala.sca._
 
  this.uses(Bean[WebModuleImpl])

  val propModuleURI = property[String]("props-module-name") is getModuleName
  val propModuleName = property[String]("props-module-url") is getModuleURL
  
  var srvModule = service("srv-web-modules") exposes Java[WebModuleService]

  
  def getModuleName: String
  def getModuleURL: String
  
} 

case class ServiceExperimentModule extends ExperimentWebModuleComponent("web-module-service"){
  def getModuleName = "REST"
  def getModuleURL  = "service/service.html"
}

case class ConfigurationExperimentModule extends ExperimentWebModuleComponent("web-module-configuration"){
  def getModuleName = "Configuration"
  def getModuleURL  = "configuration/configuration.html"
}


//-------------------------------------------------------------------------
//--- Service definition

trait WebModuleManagerService {
  def getModuleNames : Array[String]
  def getModuleURI(name : String) : String
  def getModules() : java.util.List[WebModuleService] 
}


trait WebModuleService {
  
  @Property(name="props-module-name") var _moduleName : String = null
  @Property(name="props-module-url") var _moduleURL : String = null
   
  def getModuleName : String = _moduleName
  def getModuleURL : String = _moduleURL
}

//-------------------------------------------------------------------------
//--- Implementation definition

class WebModuleManagerImpl extends WebModuleManagerService{
  
  @Reference(name = "ref-web-modules")
  val modules: java.util.List[WebModuleService] = null
  
  def getModuleNames : Array[String] = {
    for { i <- modules.toArray }
      yield i.asInstanceOf[WebModuleService].getModuleName
  }
  
  def getModuleURI(name : String) : String = {
    var r: String = null
    modules.toArray.foreach {
      case i: WebModuleService =>
        if (i.getModuleName equals name)
          r = i.getModuleURL
    }
    r
  }
  
  
  def getModules() : java.util.List[WebModuleService] = modules
  
}

class WebModuleImpl extends WebModuleService



