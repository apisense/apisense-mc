/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.node.web.cbse

import javax.servlet.Servlet
import fr.inria.bsense.node.cbse.ServerNodeComposite
import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.node.service.UserManagerService
import fr.inria.bsense.node.web.impl.WebConnectionManagerImpl
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.node.web.service.WebConnectionManagerService
import frascala.sca.{Java,Bean}
import frascala.frascati.{HTTP,SCA,WEB}
import fr.inria.bsense.db.api.cbse.{DBQueryComponent,DBQueryUpdateComponent}
import frascala.frascati.REST
import fr.inria.bsense.node.web.service.ServerConfigurationService
import fr.inria.bsense.node.web.service.ServerConfigurationComponent
import fr.inria.bsense.node.cbse.ServerNode
import fr.inria.apisense.intent.BindingIntent
import frascala.sca.COMPOSITE

//------------------------------------------------------------
// --- Composite definition

/**
 * 
 * SCA Composite
 * 
 * Main composite definition of node web site
 * 
 * 
 */
class WebComposite extends BSenseComposite("fr.inria.antdroid.Web"){

    val authentication = component(BindingIntent.authenticationTokenCpt)
    val authorization  = component(BindingIntent.authorizationCpt)
  
    
	val connection = component(new WebManagerComponent())
    connection.srvConnection as(REST("/webmanager"),Array(
    	authentication,
    	authorization
    ))
	promoteService(connection.srvConnection)
	
    val public = component(PublicWebModule())  
	//public.velocity as HTTP("/"+public.url)
    public.velocity as HTTP("/"+public._url)
    
    val admin = component(AdminWebModule())
    admin.velocity as HTTP("/"+admin._url)
    
	val scientist = component(ScientistWebModule())
	scientist.velocity as HTTP("/"+scientist._url)
	
    connection role public
    connection role admin
    connection role scientist
    
	component(ServerConfigurationComponent())
}

//------------------------------------------------------------
// --- Component definition


class WebManagerComponent extends BSenseComponent("cpt-web-manager"){
  this.uses(Bean[WebConnectionManagerImpl])
  
  val roles = property[String]("roles")
  
  val srvConnection = service("srv-web-connection") exposes Java[WebConnectionManagerService] 
  
  reference("userManager") as ServerNode.scaUrlUserManager
  
  def role[T <: WebModule]( cpt : T) : T = {
	 roles is (roles.getValue() + ";" + cpt.role + ":" + cpt.url)
	 cpt
  }
  
}

/**
 * 
 * SCA Component 
 * 
 * http {/website/public}
 * impl {/website/public}
 * index {}/index
 * 
 */
class WebModule(_name : String, var _url : String, _role : String) extends BSenseComponent(_name){
  this.uses(WEB(_url,"index.html"))
  
  var menus : property[String] = null
  
  val url = _url
  val role = _role
  
  reference("ctx").exposes(Java[WebConnectionManagerService]).autowired.optional
   
  val velocity = service("srv-velocity") exposes Java[Servlet]
  
  def page(name : String, url : String){
    if (menus == null)
      menus = property[String]("menus") is name + ":" + url
    else menus is (menus.getValue() + ";" + name + ":" + url)
  }
 
}


case class PublicWebModule extends WebModule("cpt-web-public","website/public","ROLE_ANONYMOUS")

case class ScientistWebModule extends WebModule("cpt-web-scientist","website/scientist","ROLE_SCIENTIST"){
  page("Create Experiment","create.html")
}
 
case class AdminWebModule extends WebModule("cpt-web-admin","website/admin","ROLE_ADMIN"){
  page("Create Account","create.html")
  page("Manage Account","account.html")
  page("Manage Database","database.html")
  page("Setup Server","configuration.html")
  
  reference("configuration") exposes Java[ServerConfigurationService] autowired
}



