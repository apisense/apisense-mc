/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.client

import scalaj.http.Http
import fr.inria.apisense.node.NodeJSONHelper._
import scalaj.http.HttpOptions
import scala.reflect.io.Streamable.Bytes
import scalaj.http.Base64


case class HiveClient() extends NodeClient() {

  lazy val URL_CONNECT    = service("/user/get/token")
  lazy val URL_DISCONNECT = service("/user/update/disconnect")

  def connect(username : String, password : String) = {
    val token = request {
     Http post(URL_CONNECT) params(
          ("username",username),
          ("password",MD5.hash(password))
     )
    } text;

    this.TOKEN = token;
    token
  }

  def disconnect() = request {
    val request = Http post(URL_DISCONNECT) headers(
        ("TOKEN",TOKEN)
    )
    this.TOKEN = null;
    request;
  }

  var userManager = new UserManagerClient(this)
  def store  = new StoreClient(this)
  def experimentStore  = new ExperimentStoreClient(this)
  def search = new SearchClient(this)
  def crowd = new CrowdClient(this)
  def query = new AdminQueryModuleClient(this)
}

//final val SERVICE_CREATE_XP         = HOST+"/store/scientist/add/experiment"
//final val SERVICE_DROP_XP           = HOST+"/store/scientist/delete/experiment"

case class StoreClient(hive : HiveClient) extends RequestTrait{

  lazy val URL_CREATE_EXPERIMENT = hive.service("/store/scientist/add/experiment")
  lazy val URL_DELETE_EXPERIMENT = hive.service("/store/scientist/delete/experiment")
  lazy val URL_UPDATE_EXPERIMENT = hive.service("/store/scientist//update/experiment")
  lazy val URL_GET_SUBSCRIBED    = hive.service("/store/user/get/subscribed")
  lazy val URL_SUBSCRIBE         = hive.service("/store/user/update/subscribe")
  lazy val URL_GET_EXPERIMENT    = hive.service("/store/user/get/experiment")

  def createExperiment(data : String) = request {
    Http post(URL_CREATE_EXPERIMENT) params(
        ("description", data)
    ) header("TOKEN",hive.TOKEN)
  }

  def updateExperimentScript(experimentName : String, data : Array[Byte]) = request{

    val URL = URL_UPDATE_EXPERIMENT+"/"+experimentName

    Http.postData(URL, data) header ("TOKEN",hive.TOKEN)

  }

  def getSubscribedExperiments() = request {
    Http post(URL_GET_SUBSCRIBED) header("TOKEN",hive.TOKEN)
  }

  def subscribe(experimentName : String, properties : String) = {
    request{
       Http post(URL_SUBSCRIBE) params(
    		  ("name",experimentName)
       ) header ("TOKEN",hive.TOKEN)
    } text
  }

  def downloadExperiment(experimentId : String) = {

      Http post(URL_GET_EXPERIMENT) param("xpId",experimentId) header("TOKEN",hive.TOKEN) asBytes
  }

}

case class ExperimentStoreClient(hive : HiveClient) extends RequestTrait{
  lazy val CREATE_EXPERIMENT       				= hive.service("/store/scientist/add/experiment")
  lazy val DELETE_EXPERIMENT					= hive.service("/store/scientist/delete/experiment")
  lazy val UPDATE_EXPERIMENT_DESCRIPTION		= hive.service("/store/scientist/update/description")
  lazy val UPDATE_EXPERIMENT					= hive.service("/store/scientist/update/experiment/{name}")

  def createExperiment(experimentName  : String, nodeUrl : String, description : String)  = request {

	  val node = """ {"experiment" : {
		"name" 	    : "%s",
	 	"type" 	    : "android",
	 	"version" 	    : "1.0",
	 	"language" 	: "javascript",
	 	"baseUrl" 	    : "%s",
	 	"description"  : "%s",
	 	"copyright"	: "None",
	 	"visible"	    : "false"
	  }} """.format(experimentName,nodeUrl,new String(Base64.encode(description.getBytes())))

	  Http post(CREATE_EXPERIMENT) param(
			  "description",node
	  ) header("TOKEN",hive.TOKEN)
  }

  def updateDescription(experimentDescription : String) = request {

	  Http post(UPDATE_EXPERIMENT_DESCRIPTION) param(
	      "description",experimentDescription) header(
	          "TOKEN",hive.TOKEN)
  }


}

case class SearchClient(hive : HiveClient) extends RequestTrait{
  lazy val URL_SEARCH_XP           					  = hive.service("/store/search/experiment")
  lazy val URL_SEARCH_XP_BY_NAME          			      = hive.service("/store/search/experiment/name")
  lazy val URL_SEARCH_XP_ALL       			          = hive.service("/store/search/experiment/all")
  lazy val URL_SEARCH_XP_BY_TYPE           			  = hive.service("/store/search/experiment/type")
  lazy val URL_SEARCH_XP_BY_TYPE_AND_LANGUAGES           = hive.service("/store/search/experiment/languages")


  def searchExperiment(names : Array[String]) = request {

    val builder = new StringBuilder
    names.foreach(name => builder append name+",")
    builder.deleteCharAt(builder.size -1)

    Http post(URL_SEARCH_XP_BY_NAME) params(
        ("name",builder toString)
    ) header("TOKEN",hive.TOKEN)

  }

}

case class CrowdClient(hive : HiveClient) extends RequestTrait{
  lazy val URL_MOBILE_SUBSCRIPTION = hive.service("/crowd/subscribe")
  lazy val URL_PUSH_MOBILE         = hive.service("/crowd/push/user")
  lazy val URL_PUSH_DEV_MOBILE     = hive.service("/crowd/push/dev/user")
  lazy val URL_PUSH_EXPERIMENT     = hive.service("/crowd/push/experiment")
  lazy val URL_PUSH_BROADCAST      = hive.service("/crowd/push/broadcast")

  def subscribeMobile(deviceId : String) = {
    request {
      Http post(URL_MOBILE_SUBSCRIPTION) params(
        ("deviceId",deviceId)
      ) header("TOKEN",hive.TOKEN)} text
  }

   def pushMessageToDevUser(username : String,password : String,message : String, configuration : String) = {
    request {
	  Http post(URL_PUSH_DEV_MOBILE) params(
    	("username",username),
    	("password",password),
    	("message",message),
    	("configuration",configuration)
      ) option(HttpOptions.readTimeout(1000)) header("TOKEN",hive.TOKEN)
    } text
  }

  def pushMessageToUser(userXpId : String,message : String, configuration : String) = {
    request {
	  Http post(URL_PUSH_MOBILE) params(
    	("userId",userXpId),
    	("message",message),
    	("configuration",configuration)
      ) option(HttpOptions.readTimeout(1000)) header("TOKEN",hive.TOKEN)
    } text
  }

  def pushMessageToExperimentUsers(xpName : String,message : String, configuration : String) = {
    request {
      Http post(URL_PUSH_EXPERIMENT) params(
    	("name",xpName),
    	("message",message),
    	("configuration",configuration)
      ) option(HttpOptions.readTimeout(1000)) header("TOKEN",hive.TOKEN) ;
    } text
  }

  def pushMessageToAllUsers(message : String, configuration : String) = {
    request {
      Http post(URL_PUSH_BROADCAST) params(
    	("message",message),
    	("configuration",configuration)
      ) option(HttpOptions.readTimeout(1000)) header("TOKEN",hive.TOKEN)
    } text
  }
}

case class UserManagerClient(hive : HiveClient) extends RequestTrait {
  lazy val URL_CREATE_ORGANIZATION = hive.service("/user/add/user/organization")
  lazy val URL_CREATE_BEE = hive.service("/user/add/user/bee")
  lazy val URL_DELETE_USER = hive.service("/user/delete/user")
  lazy val URL_DELETE_USERNAME = hive.service("/user/delete/username")
  lazy val URL_GET_USER_PROPERTY = hive.service("/user/get/user/property")
  lazy val URL_UPDATE_USER_PROPERTY = hive.service("/user/update/user/property")


  def deleteUser() = request{

	  Http post(URL_DELETE_USER) header("TOKEN",hive.TOKEN)

  }

  def createOrganization(
      name : String, description : String,email : String,
      username : String, password : String ) = request {

      Http post(URL_CREATE_ORGANIZATION) params(
        ("organization", name),
	    ("description", description),
	    ("username", username),
	    ("password",password),
		("email",email)
      )
  }

  def createMobileUser(
      name : String, description : String,email : String,
      username : String, password : String ) = request {

      Http post(URL_CREATE_BEE) params(
        ("organization", name),
	    ("description", description),
	    ("username", username),
	    ("password",password),
		("email",email)
      )
  }

}

case class AdminQueryModuleClient(hive : HiveClient) extends RequestTrait{

  lazy val URL_MPROCESS = hive.service("/admin/query/mprocess")

  def mprocess( query : String, params : String ) =
    Http.post(URL_MPROCESS) params(
    		("query",new String(Base64.encode(query.getBytes()))),
    		("params",params)
    ) header ("TOKEN",hive.TOKEN)  option(HttpOptions.readTimeout(10*60*1000)) asString

  def mprocess(query : String) =
    Http.post(URL_MPROCESS) params(
    		("query",new String(Base64.encode(query.getBytes())))
    ) header ("TOKEN",hive.TOKEN)  option(HttpOptions.readTimeout(10*60*1000)) asString
}

