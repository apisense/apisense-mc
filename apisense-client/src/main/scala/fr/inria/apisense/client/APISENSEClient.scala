/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.client

import scalaj.http.Http
import scalaj.http.HttpOptions


object APISENSEClient {
  def  hive  :  HiveClient      = new HiveClient()
  def  honey :  HoneyCombClient = new HoneyCombClient()


  def main(args: Array[String]) {


    //val honey = APISENSEClient.honey
    //honey.host("http://localhost:18000")
    //honey.connect("test", "test")

    val hive = APISENSEClient.hive
    hive.host("http://localhost:18001")
    hive.connect("admin", "admin")

    val query = """



    	declare variable $v1 external := "test";
    	declare variable $v2 external := "test";
    	$v1,$v2

    """

    val params = """[
     { "name" : "v1", "value" : "Coucou", "type" : "xs:string" },
     { "name" : "v2", "value" : "Coucou", "type" : "xs:string" }
    ]"""
     println(hive.query.mprocess(query,params));
   // println(honey.experiment("ModuleQuery").query.process(query));

  }

}


trait RequestTrait{
  import fr.inria.apisense.node.NodeJSONHelper._

  var TIME_OUT : Int = 60*1000

  def request( body : => scalaj.http.Http.Request ) = {

    val requestResult = parseJSON( body option(HttpOptions.readTimeout(TIME_OUT)) asString )

    if(requestResult \ "error" isEmpty)
       requestResult \ "success"
    else throw new Exception(requestResult.asJSONString)
  }

  def requestString( body : => scalaj.http.Http.Request ) = {
   body option(HttpOptions.readTimeout(TIME_OUT))asString
  }





}

abstract class NodeClient() extends RequestTrait {

  var TOKEN : String = null

  var _host : String = ""

  def host = _host
  def host(url : String) {this._host = url}

  def service(serviceUrl : String) = (_host+serviceUrl)

  def isConnected = TOKEN == null
  def connect(username : String, password : String) : String
  def connect(token : String) = { TOKEN = token }
  def disconnect()

}


object MD5 {
  def hash(s: String) = {
    val m = java.security.MessageDigest.getInstance("MD5")
    val b = s.getBytes("UTF-8")
    m.update(b, 0, b.length)
    new java.math.BigInteger(1, m.digest()).toString(16)
  }
}