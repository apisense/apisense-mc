/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.client

import scalaj.http.Http
import fr.inria.apisense.node.NodeJSONHelper._
import scalaj.http.HttpOptions
import scala.util.regexp.Base
import scalaj.http.Base64


class HoneyCombClient extends NodeClient() {

 lazy val URL_CONNECT    = service("/user-manager/get/token")
 lazy val URL_DISCONNECT = service("/user-manager/update/disconnect")

  def connect(username : String, password : String) = {
    val token = request {
     Http post(URL_CONNECT) params(
          ("username",username),
          ("password",MD5.hash(password))
     )
    } text;

    this.TOKEN = token;
    token
  }

  def disconnect()  = request {
	var r =  Http post(URL_DISCONNECT) headers(
        ("TOKEN",new String(TOKEN.toString()))
    )
    TOKEN = null
    r
  }

  val experimentManager = new ExperimentManagerClient(this)

  def userManager = new UserManagerHoneyClient(this)
  def experiment(name : String) = new ExperimentClient(name,this)
}

case class UserManagerHoneyClient(honey : HoneyCombClient) extends RequestTrait{

  lazy val SERVICE_CREATE_ADMIN     = honey.service("/user-manager/add/user/admin")
  lazy val SERVICE_CREATE_SCIENTIST = honey.service("/user-manager/add/user/scientist")


  def createScientistUser( fullname : String, username : String, password : String, email : String ) = request{

    Http.post(SERVICE_CREATE_SCIENTIST) params(
    	"fullname" -> fullname,
    	"username" -> username,
    	"password" -> password,
    	"email"    -> email
    ) header("TOKEN" , honey.TOKEN)
  }

}

case class ExperimentManagerClient(honey : HoneyCombClient) extends RequestTrait{
   lazy val URL_REGISTER_USER = honey.service("/experiment-manager/central/registration")
   lazy val URL_UNREGISTER_USER = honey.service("/experiment-manager/central/unregistration")
   lazy val URL_GET_REGISTER_USER = honey.service("/experiment-manager/get/regsitered")

   lazy val SERVICE_CREATE_EXPERIMENT = honey.service("/experiment-manager/add/xp");
   lazy val SERVICE_DROP_EXPERIMENT = honey.service("/experiment-manager/delete/xp");
   lazy val SERVICE_GET_EXPERIMENTS = honey.service("/experiment-manager/get/xps")
   lazy val SERVICE_GET_EXPERIMENT = honey.service("/experiment-manager/get/xp")
   lazy val SERVICE_GET_INFO = honey.service("/experiment-manager/info")

   lazy val SERVICE_REMOTE_START = honey.service("/experiment-manager/lifeCycle/remoteStart")
   lazy val SERVICE_REMOTE_STOP = honey.service("/experiment-manager/lifeCycle/remoteStop")
   lazy val SERVICE_STOP = honey.service("/experiment-manager/lifeCycle/stop")
   lazy val SERVICE_START = honey.service("/experiment-manager/lifeCycle/start")
   lazy val SERVICE_FINISH = honey.service("/experiment-manager/lifeCycle/finish")

   def createExperiment(
       name : String,
       nicename : String,
       description : String,
       copyright : String,
       spl : String) = request{

     Http.post(SERVICE_CREATE_EXPERIMENT) params(
    	"name" -> name,
   		"niceName" -> nicename,
        "description"-> description,
        "copyright"-> copyright,
        "spl" -> spl
     ) header("TOKEN",honey.TOKEN)
   }

   def info(name : String) = request{

     Http.post(SERVICE_GET_INFO) params(
    	"name" -> name
     ) header("TOKEN",honey.TOKEN)
   }

   def dropExperiment(name : String) = request{

     Http.post(SERVICE_DROP_EXPERIMENT) params(
    	"name" -> name
     ) header("TOKEN",honey.TOKEN)
   }

    def getExperiment(name : String) = request{

     Http.post(SERVICE_GET_EXPERIMENT) params(
    	"name" -> name
     ) header("TOKEN",honey.TOKEN)
    }

    def getExperiments() = request{

     Http.post(SERVICE_GET_EXPERIMENTS) header("TOKEN",honey.TOKEN)
    }

   def lifeCycleRemoteStart(experimentName : String) = request{
     Http post(SERVICE_REMOTE_START) params(
        ("name",experimentName)
     ) header ("TOKEN",honey.TOKEN)
   }

   def lifeCycleRemoteStop(experimentName : String) = request{
     Http post(SERVICE_REMOTE_STOP) params(
        ("name",experimentName)
     ) header ("TOKEN",honey.TOKEN)
   }

   def lifeCycleStop(experimentName : String) = request{
     Http post(SERVICE_STOP) params(
        ("name",experimentName)
     ) header ("TOKEN",honey.TOKEN)
   }

   def lifeCycleStart(experimentName : String) = request{
     Http post(SERVICE_START) params(
        ("name",experimentName)
     ) header ("TOKEN",honey.TOKEN)
   }

   def lifeCycleFinish(experimentName : String) = request{
     Http post(SERVICE_FINISH) params(
        ("name",experimentName)
     ) header ("TOKEN",honey.TOKEN)
   }

   def getRegisteredUser(experimentName : String) = request{
     Http post(URL_GET_REGISTER_USER) params(
        ("name",experimentName)
     ) header ("TOKEN",honey.TOKEN)
   }

   def registerUser( experimentName : String, userId : String, version : String ) = request {
     println(URL_REGISTER_USER)
     Http post(URL_REGISTER_USER) params(
        ("name",experimentName),
        ("userId",userId),
        ("version",version)
     )
     //FIXME : create central server account
     //header ("TOKEN",honey.TOKEN)
   }

   def unregisterUser( experimentName : String, userId : String ) = request {
	 println(URL_UNREGISTER_USER)
     Http post(URL_UNREGISTER_USER) params(
        ("name",experimentName),
        ("userId",userId)
     )
     //FIXME : create central server account
     //header ("TOKEN",honey.TOKEN)
   }

}



class ExperimentClient(_xpname : String, honey : HoneyCombClient){
  def service(url : String) = honey.service("/"+_xpname+url)
  def token = honey.TOKEN

  def scripting = new ScriptingModuleClient(this)
  def geo = new GeoJSONMapModuleClient(this)
  def publisher = new PublisherModuleClient(this);
  def collector = new CollectorModuleClient(this)
  def spi = new SPacialIndexClient(this)
  def query = new QueryModuleClient(this)
}

class QueryModuleClient(xp : ExperimentClient) extends RequestTrait{

  lazy val URL_PROCESS_FILE = xp.service("/query/process/file")
  lazy val URL_PROCESS = xp.service("/query/process")
  lazy val URL_MPROCESS = xp.service("/query/mprocess")
  lazy val URL_MPROCESS_FILE = xp.service("/query/mprocess/file")
  lazy val URL_GET = xp.service("/query/get/process")
  lazy val URL_GET_FILE = xp.service("/query/get/process/file")
  lazy val URL_GET_RESULT = xp.service("/get/process/result")

  def process( query : String ) =
    Http.post(URL_PROCESS) params(
    		("query",new String(Base64.encode(query.getBytes())))
    ) header ("TOKEN",xp.token)  option(HttpOptions.readTimeout(10*60*1000)) asString


  def mprocess( query : String, params : String ) =
    Http.post(URL_MPROCESS) params(
    		("query",new String(Base64.encode(query.getBytes()))),
    		("params",params)
    ) header ("TOKEN",xp.token)  option(HttpOptions.readTimeout(10*60*1000)) asString

  def mprocess(query : String) =
    Http.post(URL_MPROCESS) params(
    		("query",new String(Base64.encode(query.getBytes())))
    ) header ("TOKEN",xp.token)  option(HttpOptions.readTimeout(10*60*1000)) asString

  def mprocessFile( filename : String ) = {
    Http.post(URL_MPROCESS_FILE) params(
    		("filename",filename)
    ) header ("TOKEN",xp.token) option(HttpOptions.readTimeout(10*60*1000)) asString
  }

  def mprocessFile( filename : String, params : String ) = {
    Http.post(URL_MPROCESS_FILE) params(
    		("filename",filename),
    		("params",params)
    ) header ("TOKEN",xp.token) option(HttpOptions.readTimeout(10*60*1000)) asString
  }

  def processFile( filename : String ) = {
    Http.post(URL_PROCESS_FILE) params(
    		("filename",filename)
    ) header ("TOKEN",xp.token) option(HttpOptions.readTimeout(10*60*1000)) asString
  }

  def processFile( filename : String, params : String ) = {
    Http.post(URL_PROCESS_FILE) params(
    		("filename",filename),
    		("params",params)
    ) header ("TOKEN",xp.token) option(HttpOptions.readTimeout(10*60*1000)) asString
  }


  def getProcessFile( filename : String ) =  Http.get(URL_GET_FILE+"/"+filename) header ("TOKEN",xp.token) asString

  def getResultFile( filename : String ) =  Http.get(URL_GET_RESULT+"/"+filename) header ("TOKEN",xp.token) asString

  def getProcess() =  Http.get(URL_GET) header ("TOKEN",xp.token) asString



}

class PublisherModuleClient(xp : ExperimentClient) extends RequestTrait{

  lazy val URL_PUBLISH = xp.service("/publisher/update/publish")
  lazy val URL_UPDATE_SCRIPT = xp.service("/publisher/update/script")
  lazy val URL_UPDATE_CONFIGURATION = xp.service("/publisher/update/configuration")



  def publish() = request { Http.post(URL_PUBLISH) header ("TOKEN",xp.token) }

  def updateScript(filename : String, content : String) = request{

    Http.post(URL_UPDATE_SCRIPT) params(
    		("filename",filename),
    		("content",content)
    ) header ("TOKEN",xp.token)
  }

  def updateConfiguration(version : String, visible : Boolean, language : String, baseUrl : String, mainScript : String) = request {

    Http.post(URL_UPDATE_CONFIGURATION) params(
    		("version",version),
    		("visible",visible.toString),
    		("language",language),
    		("baseUrl",baseUrl),
    		("mainScript",mainScript)
    ) header ("TOKEN",xp.token)
  }

}

class ScriptingModuleClient(xp : ExperimentClient) extends RequestTrait {
  // /asl/delete/project  --> name
  // /asl/delete/script   --> name filename
  // /asl/get/log --> filter
  // /asl/clear/log -->
  lazy val URL_CREATE_PROJECT = xp.service("/asl/update/create")
  lazy val URL_START_PROJECT = xp.service("/asl/start")
  lazy val URL_STOP_PROJECT = xp.service("/asl/stop")
  lazy val URL_GET_PROJECTS   = xp.service("/asl/get/projects")
  lazy val URL_UPDATE_FILE   = xp.service("/asl/update/script")
  lazy val URL_GET_FILE   = xp.service("/asl/get/script/content")
  lazy val URL_PUBLISH_TASKS   = xp.service("/asl/publish/tasks")
  lazy val URL_PUBLISH_TASK   = xp.service("/asl/publish/task")

  def createScriptingFile(projectName : String, fileName : String,fileContent : String) = request{
    Http post(URL_UPDATE_FILE) params(
        ("name",projectName),
        ("filename",fileName),
        ("filecontent",new String(Base64.encode(fileContent.getBytes())))
    ) header ("TOKEN",xp.token)
  }

  def getScript(projectName : String, fileName : String) = {
    val result = request{ Http post(URL_UPDATE_FILE) params(
        ("name",projectName),
        ("filename",fileName)
    ) header ("TOKEN",xp.token) } text;
    Base64.decode(result)
  }

  def createScriptingProject(projectName : String) = request {
    Http post(URL_CREATE_PROJECT) params(
        ("name",projectName)
    ) header ("TOKEN",xp.token)
  }

  def getProjects() = request{Http post(URL_GET_PROJECTS) header ("TOKEN",xp.token)}

  def startProject(projectName : String) = request{
    Http post(URL_START_PROJECT) params(
        ("name",projectName)
    ) header ("TOKEN",xp.token)
  }

  def stopProject(projectName : String) = request{
    Http post(URL_STOP_PROJECT) params(
        ("name",projectName)
    ) header ("TOKEN",xp.token)
  }

  def publish(projectName : String, message : String) = request {
    Http post(URL_PUBLISH_TASK) params(
        ("name",projectName),
        ("message",message)
    ) header ("TOKEN",xp.token)
  }

   def publish(message : String) = request {
    Http post(URL_PUBLISH_TASKS) params(
        ("message",message)
    ) header ("TOKEN",xp.token)
  }

}

class CollectorModuleClient(xp : ExperimentClient) extends RequestTrait {

  lazy val URL_CREATE_MAP = xp.service("/upload/data")
  lazy val URL_GET_MAPS = xp.service("/delete")

  lazy val URL_GET_MEDIA = xp.service("/upload/get/media")
  lazy val URL_GET_USER_METRIC = xp.service("/upload/metrics/user")
  lazy val URL_GET_USER_METRIC_BETWEEN = xp.service("/upload/metrics/user")
  lazy val URL_GET_ALL_METRIC = xp.service("/upload/metrics/all")
  lazy val URL_GET_ALL_METRIC_BETWEEN = xp.service("/upload/metrics/all")


  def uploadData( userId : String, version : String, signature : String, data : Array[Byte] ) = request {

    val url =  URL_CREATE_MAP+"/%s/%s/%s".format(userId,version,signature)
    val request = Http postData(
        URL_CREATE_MAP+"/%s/%s/%s".format(userId,version,signature),
        data
    ) header("content-type","application/octet-stream ")

    println(request.headers)
    request
  }

  def getMedia( mediaId : String ) = {  Http.get(URL_GET_MEDIA+"/"+mediaId).asBytes }

  def getUserMetric( userId : String, date : String) = request{ Http.get(URL_GET_USER_METRIC+"/"+userId+"/"+date) }

  def getUserMetric( userId : String, startdate : String,enddate : String) = request{ Http.get(URL_GET_USER_METRIC_BETWEEN+"/"+userId+"/"+startdate+"/"+enddate) }

  def getAllMetric(date : String) = request{ Http.get(URL_GET_ALL_METRIC+"/"+date) }

  def getAllMetric(startdate : String,enddate : String) = request{ Http.get( URL_GET_ALL_METRIC_BETWEEN+"/"+startdate+"/"+enddate) }

}

class SPacialIndexClient(xp : ExperimentClient) extends RequestTrait {

  lazy val URL_CREATE_INDEX = xp.service("/spi/add/index")
  lazy val URL_DROP_INDEX = xp.service("/spi/delete/index")
  lazy val URL_ADD_COLLECTION = xp.service("/spi/add/collection")
  lazy val URL_ADD_POINT = xp.service("/spi/add/point")


  def createIndex(name : String, filter : String) = request {
    Http post(URL_CREATE_INDEX) params(
    	("name",name),
    	("filter",filter)
    ) header ("TOKEN",xp.token)
  }

   def addCollection(name : String, collection : String) = request {
    Http post(URL_ADD_COLLECTION) params(
    	("name",name),
    	("collection",collection)
    ) header ("TOKEN",xp.token)
  }

   def addPoint(name : String, point : String) = request {
      Http post(URL_ADD_COLLECTION) params(
    	("name",name),
    	("point",point)
    ) header ("TOKEN",xp.token)
   }

   def dropIndex(name : String) = request{

      Http post(URL_DROP_INDEX) params(
    	("name",name)
    ) header ("TOKEN",xp.token)

   }

}

class GeoJSONMapModuleClient(xp : ExperimentClient) extends RequestTrait {

  lazy val URL_CREATE_MAP = xp.service("/openmap/add/map")
  lazy val URL_GET_MAPS = xp.service("/openmap/get/maps")
  lazy val URL_GET_MAP = xp.service("/openmap/get/map")
  lazy val URL_GET_PROCESSED_MAP = xp.service("/openmap/get/processed/map")
  lazy val URL_GET_DBS = xp.service("/openmap/get/dbs")
  lazy val URL_UPDATE_FILTER = xp.service("/openmap/update/filter")
  lazy val URL_UPDATE_PROCCESS_FILTER = xp.service("/openmap/process/filter")

  def createMap(mapName : String) = request {
    Http post(URL_CREATE_MAP) params(
    	("name",mapName)
    ) header ("TOKEN",xp.token)
  }

  def getMaps() = request{
    Http.get(URL_GET_MAPS) header ("TOKEN",xp.token)
  }

  def getMap(mapName : String) = request{ Http get(URL_GET_MAP+"/"+mapName) header ("TOKEN",xp.token) }

  def getProcessedMap(mapName : String) = requestString{

    Http get(URL_GET_PROCESSED_MAP+"/"+mapName) header ("TOKEN",xp.token)

  }


  def getDatabases() = request{ Http get(URL_GET_DBS) header ("TOKEN",xp.token) }

  def updateQueryFilter(mapName : String, filters : String) = request {
    Http post(URL_UPDATE_FILTER) params(
    		("name",mapName),
    		("filters",filters)
    ) header ("TOKEN",xp.token)
  }

  def processQueryFilter(mapName : String) = request{
    Http post(URL_UPDATE_PROCCESS_FILTER) params(
        ("name",mapName)
    ) header ("TOKEN",xp.token)
  }
}





