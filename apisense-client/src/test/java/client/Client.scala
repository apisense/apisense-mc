package client

import fr.inria.apisense.client.APISENSEClient

object Client {

  def main(args: Array[String]) {

	  val h = APISENSEClient.hive
	  h.host("http://metronet-vm3.inrialpes.fr/hive")
	  h.connect("platon", "platon")

	  println(h.search.searchExperiment(Array("workdb")))

  }

}