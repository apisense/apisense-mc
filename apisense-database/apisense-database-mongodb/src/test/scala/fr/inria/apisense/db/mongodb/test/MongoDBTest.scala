package fr.inria.apisense.db.mongodb.test

import fr.inria.apisense.db.test.AbstractDBT
import fr.inria.apisense.db.test.AbstractDBT
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.db.api.cbse.DBQueryComponent
import fr.inria.bsense.db.api.cbse.DBQueryUpdateComponent
import frascala.sca.Bean
import org.junit.Test
import fr.inria.apisense.db.mongodb.MongoDBQueryImpl
import fr.inria.apisense.db.mongodb.MongoDBUpdateImpl

class MongoDBTest extends AbstractDBT {

  class AppT extends BSenseComposite("MongoDBApplicationTest") {
  
    val cptQuery  = component(new DBQueryComponent) uses Bean[MongoDBQueryImpl]
    val cptUpdate = component(new DBQueryUpdateComponent) uses Bean[MongoDBUpdateImpl]

    promoteService(cptQuery.srv)
    promoteService(cptUpdate.srv)

    def serviceQuery = runtime.>[IDBQueryService](cptQuery.srv.name)
    def serviceUpdate = runtime.>[IDBQueryUpdateService](cptUpdate.srv.name)

  }
  
  var _update  : IDBQueryUpdateService = null
  var _query   : IDBQueryService = null
  var _session : DatabaseClientSession = null
  
  @Test def test(){
    
    val app = new AppT
    app.save
    app.start
  
    _update = app.serviceUpdate
    _query = app.serviceQuery
    _session = new DatabaseClientSession
    _session.setHost("localhost")
    _session.setUsername("admin")
    _session.setPassword("admin")
    _session.setPort("27017")
    
    this.testcase
  }
  
  def update : IDBQueryUpdateService = _update
  def query : IDBQueryService = _query
  def session : DatabaseClientSession = _session
  
}