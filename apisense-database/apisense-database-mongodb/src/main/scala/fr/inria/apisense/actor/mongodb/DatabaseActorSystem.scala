package fr.inria.apisense.actor.mongodb

import akka.actor.ActorSystem
import akka.actor.Props

class MongoDBActorSystem {

  val system = ActorSystem("MongoDBActorSystem")
  
  val updateDB = system.actorOf(Props[MongoDBActor], name = "myactor")
  
  
  def send( cmd : MessageDB ){
    
    cmd match{
      
      case cmdu : UpdateMessageDB => updateDB ! cmd
      
    }
    
  }
  
}