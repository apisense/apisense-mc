package fr.inria.apisense.actor.mongodb

import akka.actor.ActorSystem
import akka.actor.Props
import fr.inria.bsense.db.api.DatabaseClientSession

object Main {

  
  def main(args: Array[String]) {
  
     implicit val  _session = new DatabaseClientSession
    _session.setHost("localhost")
    _session.setUsername("admin")
    _session.setPassword("admin")
    _session.setPort("27017")
    
     val db = new MongoDBActorSystem
     
     db send MessageDB.CreateCollection("mydb","mycollection")
     
  }
}