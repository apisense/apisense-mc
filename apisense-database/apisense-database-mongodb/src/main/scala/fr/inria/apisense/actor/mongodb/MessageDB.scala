package fr.inria.apisense.actor.mongodb

import fr.inria.bsense.db.api.DatabaseClientSession

/**
 * Abstraction of database message
 */
class MessageDB(_session : DatabaseClientSession)

/**
 * Message performing update operation
 */
class UpdateMessageDB(_session : DatabaseClientSession) extends MessageDB(_session)

object MessageDB{
  case class CreateDb(_dbname : String)(implicit val _session : DatabaseClientSession) extends UpdateMessageDB(_session) {}
  case class CreateCollection(_dbname : String, _collectionName : String)(implicit val _session : DatabaseClientSession) extends UpdateMessageDB(_session) {}
  case class DropDatabase(_dbname : String)(implicit val _session : DatabaseClientSession) extends UpdateMessageDB(_session) {}
  case class DropCollection(_dbname : String,_collectionName : String)(implicit val _session : DatabaseClientSession) extends UpdateMessageDB(_session) {}
}

/**
 * Message performing no update operation
 */
class QueryMessageDB(_session : DatabaseClientSession) extends MessageDB(_session)






