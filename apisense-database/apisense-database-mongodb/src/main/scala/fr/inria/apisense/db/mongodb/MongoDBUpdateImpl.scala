package fr.inria.apisense.db.mongodb

import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.ValuePair
import fr.inria.bsense.common.utils.Node
import com.mongodb.MongoClient
import fr.inria.bsense.common.utils.Log
import com.mongodb.BasicDBObject
import java.util.Date

class MongoDBUpdateImpl extends IDBQueryUpdateService {

    var client : MongoClient = null
  
	def create(session : DatabaseClientSession, dbName : String) {
	  
	  Log.d("create database "+dbName);
	  
	  exec(session){
	     
	    val db = client.getDB(dbName)
	    val coll = db.getCollection("db")
	    val result = coll.insert(new BasicDBObject("created",new Date()))
	    
	    if(result.getError() != null){
	      Log.w(result.getError())
	    }
	  }
	   
	}

	/**
	 * Creating a new Database
	 * @param session  Database client session
	 * @param dbName Name of database
	 */
	def create(session : DatabaseClientSession, dbName : String, rootNode : String) {}
	
	/**
	 * Add a new document in database
	 * @param session Database client session
	 * @param dbName Name of database
	 * @param jsonDocument JSON document
	 * @param collection Collection name
	 */
	def addDocument(session : DatabaseClientSession, dbName : String,collection : String, jsonDocument : String) : Int = 2
	/**
	 * Update document
	 * @param session Database client session
	 * @param dbName Name of database
	 * @param jsonDocument JSON document
	 * @param collection Collection name
	 */
	def updateDocument(session : DatabaseClientSession, dbName : String,jsonDocument : String, collection : String) {}
	
	
	def updateNode(session : DatabaseClientSession, dbName : String,collection : String,path : path, jsonDocument : String) {}
	
	
	def updateNode(session : DatabaseClientSession, dbName : String, nodeId : Int ,path : path, jsonDocument : String) {}
	
	/**
	 * Drop a collection in database
	 * @param session Database configuration
	 * @param dbName Name of database
	 * @param collection Collection name
	 */
	def dropCollection(session : DatabaseClientSession, dbName : String, collection : String) {}
	
	/**
	 * 
	 */
	def deleteNode(session: DatabaseClientSession, dbName: String, collection: String, path: path) {}
	
	/**
	 * 
	 */
	def deleteNodeFromId(session: DatabaseClientSession, dbName: String, nodeId: String, path: path) {}
	
	/**
	 * Sets field with value. 
	 * Exception thrown if severals nodes match with path
	 * @param path Path 
	 * @param session Database configuration
	 * @param dbName Name of database
	 * @param collection Collection name
	 */
	def set(session : DatabaseClientSession, dbName : String, collection : String, path : path, values : Array[ValuePair]) {}
	
	/**
	 * 
	 * 
	 * 
	 */
	def insert(session : DatabaseClientSession, dbName : String, collection : String, path : path, node : Node) {}
	
	/**
	 * 
	 * 
	 * 
	 */
	def insertInID(session : DatabaseClientSession, dbName : String, collection : String, path : path, node : Node) {}
	
	/**
	 * Sets all fields with value.
	 * @param session Database configuration
	 * @param dbName Name of database
	 * @param collection Collection name
	 */
	def setAll(session : DatabaseClientSession, dbName : String, collection : String, path : path, values : Array[ValuePair]) {}
	
	
	def drop(session : DatabaseClientSession,dbName : String) {
	  
	  Log.d("drop database "+dbName)
	  
      exec(session){
	    client.dropDatabase(dbName)
	  }
	}
	
	/**
	 * Execute Query
	 * @param query : execute query
	 * @return result of query
	 */
	def query(session : DatabaseClientSession, query : String) {}
	
	/**
	 * Execute Query
	 * @param query : execute query
	 * @return result of query
	 */
	def query(session : DatabaseClientSession, dbName : String, query : String) {}

	
	def command(session : DatabaseClientSession, command : String) {}
	
	/**
	 * 	Grants the specified permission to the specified [user].
	 *  The Glob syntax can be used to address more than one database or user. 
	 *  List of permission [NONE|READ|WRITE|CREATE|ADMIN]
	 */
	def grant(session : DatabaseClientSession, user : String, permission : String, database : String) {}
	
	/**
	 * 	Creates a user with the specified [name] and [password].
	 */
	def createUser(session : DatabaseClientSession, username : String, password : String) {}
	
	/**
	 * 	Drop a user with the specified [name].
	 */
	def dropUser(session : DatabaseClientSession, username : String) {}
	
	
	/**
   *
   * Closure to initialize database
   * session before run a database request
   */
  private def exec(session: DatabaseClientSession)(request: => Unit) =  {
    
    this.client = new MongoClient(session.host,session.port.toInt);
    
    try {
      request
      -1
    } catch {
      case e : Exception =>
        e.printStackTrace()
        1
    }finally{
       client.close
       client = null
    }
  }
	
  
}