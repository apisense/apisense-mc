package fr.inria.apisense.actor.mongodb

import akka.actor.Actor
import fr.inria.bsense.common.utils.Log
import com.mongodb.BasicDBObject
import java.util.Date
import fr.inria.bsense.db.api.DatabaseClientSession
import com.mongodb.WriteResult



class MongoDBActor extends Actor {

  override def preStart() = {
	  Log.d("create actor MongoDBActor")
  }
  
  override def preRestart(reason : Throwable, message : Option[Any]){
      Log.d("actor MongoDBActor restart "+reason.getLocalizedMessage())
  }
  
  
  
  def receive = {
    
    case cmd : MessageDB.CreateDb             => create(cmd)
    case cmd : MessageDB.CreateCollection     => createCollection(cmd)
    case cmd : MessageDB.DropDatabase         => dropDatabase(cmd)
    case cmd : MessageDB.DropCollection       => dropCollection(cmd)
    
    case _ => Log.d("receive unknow message")
  }
  
  
  
  def create( cmd : MessageDB.CreateDb ) = {
    
    Log.d("create database "+cmd._dbname)
    
    exec(cmd._session){
      mongo =>
          val db = mongo.getDB(cmd._dbname) 
          val collection = db.createCollection("_database",new BasicDBObject())
          collection.insert(new BasicDBObject());
    }
    
  }
  
  def createCollection(cmd : MessageDB.CreateCollection) = {
    
    Log.d("create collection "+cmd._dbname+"/"+cmd._collectionName)
    
    exec(cmd._session){
      mongo =>
          val db = mongo.getDB(cmd._dbname) 
          val collection = db.createCollection(cmd._collectionName,new BasicDBObject())
          collection.insert(new BasicDBObject());
    }
    
  }
  
  def dropDatabase(cmd : MessageDB.DropDatabase) = {
    
    Log.d("drop database "+cmd._dbname)
    
    exec(cmd._session){
      mongo => 
        mongo.dropDatabase(cmd._dbname)
        null
          
    }
  }
  
  def dropCollection(cmd : MessageDB.DropCollection) = {
    
    Log.d("drop database "+cmd._dbname)
    
    exec(cmd._session){
      mongo => 
        val db = mongo.getDB(cmd._collectionName)
        db.getCollection(cmd._collectionName).drop()
        null     
    }
  }
  
  
  /**
   *
   * Closure to initialize database
   * session before run a database request
   */
  private def exec(session: DatabaseClientSession)(request :  com.mongodb.MongoClient  => WriteResult) =  {
    
    val mongo = new com.mongodb.MongoClient(session.host,session.port.toInt);
    
    try {
        val result = request(mongo)
        if (result.getError() != null){
          Log.w(result.getError())
          Log.w(result.getLastError().toString())
          1
        }
        else -1
    } catch {
      case e : Exception =>
        e.printStackTrace()
        1
    }finally{
       mongo.close
    }
  }
  
  
  
}