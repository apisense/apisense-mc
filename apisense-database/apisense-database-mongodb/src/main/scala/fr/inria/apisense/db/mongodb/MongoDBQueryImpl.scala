package fr.inria.apisense.db.mongodb

import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.api.path
import fr.inria.bsense.common.utils.Node
import java.io.OutputStream
import com.mongodb.MongoClient
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.common.utils.Log

@Scope("CONVERSATION")
class MongoDBQueryImpl extends IDBQueryService {

  var client : MongoClient = null
  
  def beginConversation {}
  def endConversation {}

  def createSession(session: DatabaseClientSession){
    
    this.client = new MongoClient(session.host,session.port.toInt);
  }

  def endSession{
    
    if (this.client != null){
      this.client.close()
      this.client = null
    }
  }

  def open(dbName: String){}

  def close{}

  def exists(dbName: String): Boolean = { this.client.getDatabaseNames().contains(dbName) }

  def getDocumentId(dbName: String, path: path): Array[String] = null

  def getDocumentId(dbName: String, collection: String): String = ""
  
  def getDocumentId(dbName: String, collection: String, path: path): Array[String] = Array[String]()

  def getDocument(dbName: String, nodeId: String): Node = null

  def getDocument(dbName: String, path: path): Node = null
  
  def getDocuments(dbName: String, path: path): Node = null

  def getDocument(dbName: String, collection: String, path: path): Node = null

  def existsCollection(dbName: String, collection: String): Boolean = false

  def query(query: String): String = ""
  
  def query(query : String, outputStream : OutputStream) : String = ""

  def command(command: String): String = ""
    
    
  private def exec[T](body: => T): T = {
    try {

      body

    } catch {
      case e : Exception =>
        e.printStackTrace();
        throw e
    }

  }
  
}