package fr.inria.bsense.database.basex.test
import org.junit.Test

import java.io.File
import frascala.frascati.ScaComposite
import org.junit.Before
import fr.inria.bsense.db.basex.impl.BaseXUpdateQuery
import fr.inria.bsense.db.basex.impl.BaseXQuery
import org.junit.After
import org.junit.Assert
import scala.util.parsing.json.JSON
import scala.util.parsing.json.JSONObject
import fr.inria.bsense.common.cbse.BSenseComposite
import frascala.sca.Java
import frascala.sca.Bean
import frascala.sca.ScaService
import fr.inria.bsense.db.api.cbse.DBQueryComponent
import fr.inria.bsense.db.api.cbse.DBQueryUpdateComponent
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.ValuePair
import org.basex.BaseXServer
import fr.inria.bsense.db.api.ConfigurationDB



class AppT extends BSenseComposite("AppTest") {

  val cptQuery  = component(new DBQueryComponent) uses Bean[BaseXQuery]
  val cptUpdate = component(new DBQueryUpdateComponent) uses Bean[BaseXUpdateQuery]

  promoteService(cptQuery.srv)
  promoteService(cptUpdate.srv)

  def serviceQuery = runtime.>[IDBQueryService](cptQuery.srv.name)
  def serviceUpdate = runtime.>[IDBQueryUpdateService](cptUpdate.srv.name)

}

class BaseXT {

  var session: DatabaseClientSession = _
  var update: IDBQueryUpdateService = _
  var query: IDBQueryService = _
  var db = "MyTestDatbase"

  @Before
  def init() {

    // Load Basex Component
    val c = new AppT
    c save ()
    c start

    // Get Services
    this.update = c.serviceUpdate
    this.query = c.serviceQuery

  }

  @Test
  def test() {

    val bx = new BaseXServer("-p1985","-e1986")

    this.session = new DatabaseClientSession
    session.setHost("localhost")
    session.setUsername("admin")
    session.setPassword("admin")
    session.setPort("1985")

    query.createSession(session)
    testcase1
    testcase2

    query.endSession

    bx.stop()
  }

  /**
   * Create and drop database
   */
  def testcase1() {

    println("test : create database " + db)

    //create new database
    update.create(session, db)

    //test database
    Assert.assertTrue(query.exists(db))
    Assert.assertFalse(query.exists(db + "_fake"))

    //delete database
    update.drop(session, db)

    //test if database id dropped
    Assert.assertFalse(query.exists(db))
  }

  /**
   * Create and drop collection in database
   */
  def testcase2() {

    println("test : create collection " + db)

    //create new database
    update.create(session, db)

    //create new colllection
    update.addDocument(session, db, "collection", "{'apps' : {} }")
    update.addDocument(session, db, "collection", "{'apps' : {} }")
    update.addDocument(session, db, "collection", "{'apps' : {} }")
    update.addDocument(session, db, "collection", "{'apps' : {} }")
    update.addDocument(session, db, "collection", "{'document' : {} }")

    //check
    Assert.assertTrue(query.existsCollection(db, "collection"))

    update.setAll(session, db, "collection", path("apps"), Array(new ValuePair("description", "my description")))
    update.set(session, db, "collection", path("document"), Array(new ValuePair("description", "my description")))

    // Get node id
    val id = query.getDocumentId(db, "collection", path("document"))
    println(query.getDocument(db, id(0)) \\ "description")
    println(query.getDocument(db, "collection", path("document")) \\ "description")

    //drop collection
    update.dropCollection(session, db, "collection")

    Assert.assertFalse(query.existsCollection(db, "collection"))

    update.drop(session, db)
  }



}