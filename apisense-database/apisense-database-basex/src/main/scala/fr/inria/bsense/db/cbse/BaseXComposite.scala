/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.cbse

import fr.inria.bsense.db.api.cbse.DatabaseComposite
import frascala.sca.Bean
import fr.inria.bsense.db.basex.impl.BaseXQuery
import fr.inria.bsense.db.basex.impl.BaseXUpdateQuery
import org.basex.BaseXServer


/**
 * SCA Composite
 * 
 * Define composite database implementation
 * using [[http://basex.org BaseX]] an XML Database 
 * 
 */
class BaseXComposite extends DatabaseComposite {
	this.cptQuery.uses(Bean[BaseXQuery])
	this.cptQueryUpdate.uses(Bean[BaseXUpdateQuery])
}