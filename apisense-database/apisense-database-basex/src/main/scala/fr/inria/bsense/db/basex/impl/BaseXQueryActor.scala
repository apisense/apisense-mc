/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.basex.impl

import akka.actor.Actor
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.basex.impl.DBMessage.QUERY
import org.basex.server.ClientSession

object DBMessage{
  
  case class QUERY(session : DatabaseClientSession, query : String)
  
}

class BaseXQueryActor(_session : DatabaseClientSession) extends Actor{
  
  var basexClient : ClientSession = null
  
  override def preStart{
    
    basexClient = new ClientSession(
      _session.getHost(),
      _session.getPort().toInt,
      _session.getUsername(),
      _session.getPassword()
    )
  }
  
  override def receive = {
    case QUERY(session,query) =>  sender ! exec{ basexClient.execute(query) }
    case _ =>
  }
  
  def exec(body : => String ) : String = {
    
    try{
    	
      return body
    }
    catch{
      case e : Throwable => e.printStackTrace();""
    }finally{
    
      try{ basexClient.close }
      context.stop(self)
    }
  }
}