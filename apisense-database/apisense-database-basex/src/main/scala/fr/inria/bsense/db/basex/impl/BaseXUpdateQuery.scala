/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.basex.impl

import java.io.ByteArrayOutputStream
import org.basex.server.ClientQuery
import org.basex.server.ClientSession
import fr.inria.bsense.common.utils.MD5
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.api.ValuePair
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.Log



/**
 * Database update service implementation
 * using BaseX a XML database
 * @see BaseX : http://basex.org
 */
class BaseXUpdateQuery extends IDBQueryUpdateService {

  // BaseX session object
  private var client: ClientSession = null

  def create(session: DatabaseClientSession, dbName: String) {

    exec(session) {

      client.execute("CREATE DB " + dbName)
      client.execute("CREATE INDEX fulltext")
    }
  }

  def createIfNotExist(session: DatabaseClientSession, dbName: String){

    val query = """ XQUERY

    	if (db:exists('%s')) then ()
    	else db:create('%s')

    """.format(dbName,dbName);

    exec(session){

    	client.execute(query);
    }
  }

  def createIfNotExist(session: DatabaseClientSession, dbName: String, rootNode : String){

    val query = """ XQUERY

    	if (db:exists('%s')) then ()
    	else db:create('%s')

    """.format(dbName,dbName);

    val queryInsert = """ XQUERY insert node %s into db:open-pre('%s',0)""".format(rootNode,dbName);

    exec(session){

    	client.execute(query);
    	client.execute(queryInsert);
    }
  }

  def create(session: DatabaseClientSession, dbName: String, rootNode: String) {

    exec(session) {

      if (exist(dbName))
        throw new Exception("Database " + dbName + " already exist")

      client.execute("CREATE DB " + dbName)
      client.execute("CREATE INDEX fulltext")
      client.execute("XQUERY insert node " + rootNode + " into db:open-pre('" + dbName + "',0)")
    }
  }

  def addDocument(session: DatabaseClientSession, dbName: String, collection: String, jsonDocument: String) = {

    exec(session) {

      val node = new JSONLiftNodeParser(jsonDocument.replaceAll("'", "\""))

      // query => db:add('dbName','json:parse(jsonDocument)','collection')
      val query = new StringBuilder
      query append "XQUERY "
      query append "db:add("
      query append "'" + dbName + "'"
      query append ","
      query append "document { " + node.toXMLString + " }"
      query append ","
      query append "'" + collection + "'"
      query append ")"

      println(query.toString)
      client.execute(query toString)
    }
  }

  def updateDocument(session : DatabaseClientSession, dbName : String,jsonDocument : String, collection : String){

    exec(session){

      val node = new JSONLiftNodeParser(jsonDocument.replaceAll("'", "\""))
      val queryDelete = new StringBuilder
      queryDelete append "XQUERY "
      queryDelete append "delete node "
      queryDelete append "db:open('"+dbName+"','"+collection+"')/node()/*"

      client.execute(queryDelete toString)

      val queryAdd = new StringBuilder
      queryAdd append "XQUERY "
      queryAdd append "insert node "
      queryAdd append node.toXMLString+"/* "
      queryAdd append "into "
      queryAdd append "db:open('"+dbName+"','"+collection+"')/node()"

      client.execute(queryAdd toString)
    }
  }

  def updateNode(session : DatabaseClientSession, dbName : String,collection : String,path : path, jsonDocument : String){

    exec(session){

      val node = new JSONLiftNodeParser(jsonDocument.replaceAll("'", "\""))
      val query = new StringBuilder
      query append "XQUERY "
      query append "replace node "
      query append "db:open('"+dbName+"','"+collection+"')/" + PathBaseXConverter.toString(path) + " \n"
      query append " with "
      query append node.toXMLString

      client.execute(query toString)
    }
  }

  def updateNode(session : DatabaseClientSession, dbName : String, nodeId : Int ,path : path, jsonDocument : String){

    exec(session){

      val node = new JSONLiftNodeParser(jsonDocument.replaceAll("'", "\""))
      val query = new StringBuilder
      query append "XQUERY "
      query append " let $node := db:open-id('"+dbName+"',"+nodeId+")/" + PathBaseXConverter.toString(path) + " return \n"
      query append " if ($node) then "
      query append "   replace node $node with "+ node.toXMLString
      query append " else insert node  "+node.toXMLString+" into  db:open-id('"+dbName+"',"+nodeId+")"


      client.execute(query toString)
    }
  }

  def dropCollection(session: DatabaseClientSession, dbName: String, collection: String) {

    exec(session) {

      // query => db:delete('dbName','collection')
      val query = new StringBuilder
      query append "XQUERY "
      query append "db:delete("
      query append "'" + dbName + "'"
      query append ","
      query append "'" + collection + "'"
      query append ")"

      client.execute(query toString)
    }
  }

  def drop(session: DatabaseClientSession, dbName: String) {

    exec(session) {

      client.execute("DROP DATABASE " + dbName)
    }
  }

  def deleteNode(session: DatabaseClientSession, dbName: String, collection: String, path: path) {

    exec(session){

        // query => delete node db:open('dbname','collection')/path
        val query = new StringBuilder
        query append "XQUERY "
        query append "delete node db:open("
        query append """ '%s', """.format(dbName)
        query append """ '%s') """.format(collection)
        query append "/"
        query append PathBaseXConverter.toString(path)

        client.execute(query.toString)
      }
  }

  def deleteNodeFromId(session: DatabaseClientSession, dbName: String, nodeId: String, path: path) {

    exec(session){

        // query => delete node db:open-id('dbname',nodeId)/path
        val query = new StringBuilder
        query append "XQUERY "
        query append "delete node db:open-id("
        query append """ '%s', """.format(dbName)
        query append """ %s) """.format(nodeId)
        query append "/"
        query append PathBaseXConverter.toString(path)

        client.execute(query.toString)
      }
  }

  def set(session: DatabaseClientSession, dbName: String, collection: String, path: path, valuePairs: Array[ValuePair]) {

    exec(session) {

      valuePairs.foreach {
        vp =>
          val query = new StringBuilder
          query append "let $base := db:open('" + dbName + "','" + collection + "')/" + PathBaseXConverter.toString(path) + " \n"
          query append "let $field := $base/" + vp.getField() + " \n"
          query append "return \n"
          query append "if ($field) \n"
          query append "then replace value of node $field with '" + vp.getValue() + "' \n"
          query append "else insert node <" + vp.getField() + ">" + vp.getValue() + "</" + vp.getField() + "> into $base"

          client.execute("XQUERY " + query toString)
      }
    }
  }

  def setAll(session: DatabaseClientSession, dbName: String, collection: String, path: path, valuePairs: Array[ValuePair]) {

    exec(session) {

      valuePairs.foreach {
        vp =>
          val query = new StringBuilder
          query append "let $base := db:open('" + dbName + "','" + collection + "')/" + PathBaseXConverter.toString(path) + " \n"
          query append "let $field := $base/" + vp.getField() + " \n"
          query append "return \n"
          query append "if ($field) \n"
          query append "then for $i in $field return replace value of node $i with '" + vp.getValue() + "' \n"
          query append "else for $i in $base  return insert node <" + vp.getField() + ">" + vp.getValue() + "</" + vp.getField() + "> into $i"

          client.execute("XQUERY " + query toString)
      }
    }
  }

  def insert(session : DatabaseClientSession, dbName : String, collection : String, path : path, node : Node){

     exec(session) {

       var query = """

         let $base := db:open('%s','%s')/%s
         return
    		   insert node %s into $base

       """
       query = query.format(
           dbName,collection,
           PathBaseXConverter.toString(path),
           node.toXMLString)

       client.execute("XQUERY "+query)
     }
  }

  def insertInID(session : DatabaseClientSession, dbName : String, nodeId : String, path : path, node : Node){

    exec(session) {

    var query = """

         let $base := db:open-id('%s',%s)/%s
         return
    		   insert node %s into $base

       """

      query = query.format(
           dbName,nodeId,
           PathBaseXConverter.toString(path),
           node.toXMLString)

       client.execute("XQUERY "+query)
    }




  }


  def query(session: DatabaseClientSession, vNames: Array[String], vValues: Array[String], vTypes: Array[String],
    query: String) {

    exec(session) {

      val cQuery = new ClientQuery(query, client, new ByteArrayOutputStream)
      for (i <- 0 until vValues.length) {
        cQuery.bind(vNames(i), vValues(i), vTypes(i))
      }

      cQuery.execute
    }
  }

  def query(session: DatabaseClientSession, query: String) {

    exec(session) {

      client.execute("XQUERY " + query)
    }
  }

  def query(session: DatabaseClientSession, dbName: String, query: String) {

    exec(session) {

      client.execute("OPEN " + dbName)
      client.execute("XQUERY " + query)
    }
  }

  def command(session: DatabaseClientSession, command: String) {

    exec(session) {

      client.execute(command)
    }
  }

  def grant(session: DatabaseClientSession, user: String, permission: String, database: String) {

    exec(session) {

      val buffer = new StringBuilder();
      buffer.append("GRANT ")
      buffer.append(permission)
      buffer.append(" on ")
      buffer.append(database)
      buffer.append(" to ")
      buffer.append(user)

      client.execute(buffer toString)
    }
  }

  def createUser(session: DatabaseClientSession, username: String, password: String) {

    exec(session) {

      var buffer = new StringBuilder();
      buffer.append("CREATE USER ")
      buffer.append(username)
      buffer.append(" ")
      buffer.append(MD5.hash(password))

      client.execute(buffer.toString())

      buffer = new StringBuilder();
      buffer.append("GRANT NONE TO ")
      buffer.append(username)

      client.execute(buffer.toString())
    }
  }

  def dropUser(session: DatabaseClientSession, username: String) {

    exec(session) {

      val buffer = new StringBuffer();
      buffer.append("DROP USER ")
      buffer.append(username)

      client.execute(buffer.toString())
    }
  }

  /**
   *
   * Return true if database 'dbName' exist
   * else false
   *
   * @param dbName Name of database
   *
   * @return true if database 'dbName' exist else false
   */
  private def exist(dbName: String): Boolean = {

    try {
      client.execute("CHECK " + dbName)
      true
    } catch { case e : Exception => false }
  }

  /**
   *
   * Closure to initialize database
   * session before run a database request
   */
  private def exec(session: DatabaseClientSession)(request: => Unit) =  {

    client = new ClientSession(
      session.getHost(),
      session.getPort().toInt,
      session.getUsername(),
      session.getPassword())

    try {
      request
      -1
    } catch {
      case e : Exception =>
        e.printStackTrace()
        1
    }finally{

       client.close
       client = null
    }
  }
}