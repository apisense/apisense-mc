/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.basex.impl

import scala.Array.apply
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.XMLNodeParser
import java.io.OutputStream
import fr.inria.bsense.common.utils.XMLLiftNodeParser
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.common.utils.Log
import org.basex.server.ClientSession
import org.basex.server.ClientQuery



/**
 * Database query service implementation
 * using BaseX a XML database
 * @see BaseX : http://basex.org
 */
@Scope("CONVERSATION")
class BaseXQuery extends IDBQueryService {

  var client: ClientSession = null;

  def beginConversation() {}
  def endConversation() {}

  def createSession(atClient: DatabaseClientSession) {

    client = new ClientSession(
      atClient.getHost(),
      atClient.getPort().toInt,
      atClient.getUsername(),
      atClient.getPassword())
  }

  def open(dbName: String) {
    client.execute("OPEN  " + dbName)
  }

  def exists(dbName: String): Boolean = {
    // build query db:exist('dbname')
    val q = new StringBuilder
    q append "XQUERY "
    q append "db:exists("
    q append "'" + dbName + "'"
    q append ")"

    client.execute(q toString).toBoolean
  }

  def existsCollection(dbName: String, collection: String): Boolean = {

    // build query db:exist('dbname','collection')
    val q = new StringBuilder
    q append "XQUERY "
    q append "db:exists("
    q append "'" + dbName + "'"
    q append ","
    q append "'" + collection + "'"
    q append ")"

    client.execute(q toString).toBoolean
  }

  def getDocumentId(dbName: String, collection: String, path: path): Array[String] = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:node-id("
    query append "db:open('" + dbName + "','" + collection + "')"
    query append "//" + PathBaseXConverter.toString(path)
    query append ")"

    val r = client.execute(query toString)
    if (r.isEmpty())
      Array[String]()
    else r.split(" ")

  }

  def getDocumentId(dbName: String, collection: String): String = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:node-id("
    query append "db:open('" + dbName + "','" + collection + "')"
    query append ")"

    client.execute(query toString)
  }

  def getDocumentId(dbName: String, path: path): Array[String] = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:node-id("
    query append "db:open('" + dbName + "')"
    query append "//" + PathBaseXConverter.toString(path)
    query append ")"

    val r = client.execute(query toString)
    if (r.isEmpty())
      Array[String]()
    else r.split(" ")
  }

  def getDocument(dbName: String, nodeId: String): Node = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:open-id('" + dbName + "'," + nodeId + ")"

    exec{ new XMLNodeParser(client.execute(query toString)) }
  }



  def getDocument(dbName: String, path: path): Node = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:open('" + dbName + "')"
    query append "//" + PathBaseXConverter.toString(path)

    exec{ NodeParser.parseXML(client.execute(query toString)) }
  }

  def getDocuments(dbName: String, path: path): Node = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:open('" + dbName + "')"
    query append "//" + PathBaseXConverter.toString(path)

    exec{ new XMLNodeParser("<array>"+client.execute(query toString)+"</array>") }
  }

  def getDocument(dbName: String, collection: String, path: path): Node = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:open('" + dbName + "','" + collection + "')"
    query append "//" + PathBaseXConverter.toString(path)

    //Log.d("exec : "+query.toString)

    exec{ NodeParser.parseXML(client.execute(query toString)) }
  }

  def count(dbname : String, collection : String, path : path) : Integer = {


    val query = new StringBuilder
    query append "XQUERY "
    query append "count(db:open('" + dbname + "','" + collection + "')"
    query append "//" + PathBaseXConverter.toString(path)+")"

    exec{ client.execute(query toString).toInt }
  }

  def info(dbname : String) : Node = {

    val query = new StringBuilder
    query append "XQUERY "
    query append "db:info('" + dbname + "')"

    exec{ NodeParser.parseXML(client.execute(query toString)) }
  }

  def close() {
    client.execute("CLOSE")
  }

  def query(query: String): String = {
    client.execute("XQUERY " + query)
  }

  def query(query : String, outputStream : OutputStream) = {

     val cQuery = new ClientQuery(query,client,outputStream)
     cQuery.execute
     cQuery.info
  }

   /**
   * Generic command to execute query
   * Query language depend of the Database implementation
   * @param query : execute query
   * @return result of query
   */
  def queryWithParams(query: String, outputStream : OutputStream, params : Array[(String,Any,String)] ): String = {

	  val cQuery = new ClientQuery(query,client,outputStream)

	  params.foreach{
	    t =>
	    	cQuery.bind(t._1,t._2,t._3)
	  }

	  cQuery.execute
	  cQuery.info
  }

  def command(command: String) = {
    client.execute(command)
  }

  private def exec[T](body: => T): T = {
    try {

      body

    } catch {
      case e : Exception =>
        e.printStackTrace();
        throw e
    }

  }

  def endSession() {

    if (client != null)
      client.close()
  }

}