/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.api.service

import org.osoa.sca.annotations.Conversational
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.db.api._



trait IDBQueryUpdateService {

    /**
	 * Creating a new Database
	 * @param dbName Name of Database
	 */
	def create(session : DatabaseClientSession, dbName : String)

	/**
	 * Creating a new Database
	 * @param session  Database client session
	 * @param dbName Name of database
	 */
	def create(session : DatabaseClientSession, dbName : String, rootNode : String)


    def createIfNotExist(session: DatabaseClientSession, dbName: String, rootNode : String)


    def createIfNotExist(session: DatabaseClientSession, dbName: String)

	/**
	 * Add a new document in database
	 * @param session Database client session
	 * @param dbName Name of database
	 * @param jsonDocument JSON document
	 * @param collection Collection name
	 */
	def addDocument(session : DatabaseClientSession, dbName : String,collection : String, jsonDocument : String) : Int

	/**
	 * Update document
	 * @param session Database client session
	 * @param dbName Name of database
	 * @param jsonDocument JSON document
	 * @param collection Collection name
	 */
	def updateDocument(session : DatabaseClientSession, dbName : String,jsonDocument : String, collection : String)


	def updateNode(session : DatabaseClientSession, dbName : String,collection : String,path : path, jsonDocument : String)


	def updateNode(session : DatabaseClientSession, dbName : String, nodeId : Int ,path : path, jsonDocument : String)

	/**
	 * Drop a collection in database
	 * @param session Database configuration
	 * @param dbName Name of database
	 * @param collection Collection name
	 */
	def dropCollection(session : DatabaseClientSession, dbName : String, collection : String)

	/**
	 *
	 */
	def deleteNode(session: DatabaseClientSession, dbName: String, collection: String, path: path)

	/**
	 *
	 */
	def deleteNodeFromId(session: DatabaseClientSession, dbName: String, nodeId: String, path: path)

	/**
	 * Sets field with value.
	 * Exception thrown if severals nodes match with path
	 * @param path Path
	 * @param session Database configuration
	 * @param dbName Name of database
	 * @param collection Collection name
	 */
	def set(session : DatabaseClientSession, dbName : String, collection : String, path : path, values : Array[ValuePair])

	/**
	 *
	 *
	 *
	 */
	def insert(session : DatabaseClientSession, dbName : String, collection : String, path : path, node : Node)

	/**
	 *
	 *
	 *
	 */
	def insertInID(session : DatabaseClientSession, dbName : String, collection : String, path : path, node : Node)

	/**
	 * Sets all fields with value.
	 * @param session Database configuration
	 * @param dbName Name of database
	 * @param collection Collection name
	 */
	def setAll(session : DatabaseClientSession, dbName : String, collection : String, path : path, values : Array[ValuePair])

	/**
	 * Remove Database
	 * @param dbName Name of Database
	 */
	def drop(session : DatabaseClientSession,dbName : String)

	/**
	 * Execute Query
	 * @param query : execute query
	 * @return result of query
	 */
	def query(session : DatabaseClientSession, query : String)

	/**
	 * Execute Query
	 * @param query : execute query
	 * @return result of query
	 */
	def query(session : DatabaseClientSession, dbName : String, query : String)


	def command(session : DatabaseClientSession, command : String)

	/**
	 * 	Grants the specified permission to the specified [user].
	 *  The Glob syntax can be used to address more than one database or user.
	 *  List of permission [NONE|READ|WRITE|CREATE|ADMIN]
	 */
	def grant(session : DatabaseClientSession, user : String, permission : String, database : String)

	/**
	 * 	Creates a user with the specified [name] and [password].
	 */
	def createUser(session : DatabaseClientSession, username : String, password : String)

	/**
	 * 	Drop a user with the specified [name].
	 */
	def dropUser(session : DatabaseClientSession, username : String)

}