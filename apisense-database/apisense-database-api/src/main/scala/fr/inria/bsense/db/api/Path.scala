/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.api

case class path(path : String){
  var expression : Term = null
  def hasExpression : Boolean = expression != null
  def withs (expression : Term) = { this.expression = expression; this }
}

trait Term
case class field(name : String) extends Term with OperatorBuilder
case class attfield(name : String) extends Term with OperatorBuilder
case class expr(term : Term)    extends Term with AssociationBuilder
case class value(value : Any)   extends Term 
case class OperatorExpr(left : Term, operator : Operator, right : Term) extends Term with AssociationBuilder
case class AssociateExpr(left : Term, operator : Association, right : Term) extends Term with AssociationBuilder

trait OperatorBuilder extends Term {
  def == ( term : Term )  = OperatorExpr(this,OperatorEqual(),term)
  def != ( term : Term )  = OperatorExpr(this,OperatorDiff(),term)
  def <= ( term : Term )  = OperatorExpr(this,OperatorInfEqual(),term)
  def >= ( term : Term )  = OperatorExpr(this,OperatorSupEqual(),term)
  def < ( term : Term )  = OperatorExpr(this,OperatorInf(),term)
  def > ( term : Term )  = OperatorExpr(this,OperatorSup(),term)
}

trait AssociationBuilder extends Term {
  def and(term : Term) = AssociateExpr(this,AssociationAnd(),term)
}

class Operator extends Term
case class OperatorEqual extends Operator
case class OperatorDiff   extends Operator
case class OperatorSup extends Operator
case class OperatorSupEqual extends Operator
case class OperatorInf extends Operator
case class OperatorInfEqual extends Operator

class Association extends Term
case class AssociationAnd extends Association

object PathBaseXConverter{
  
  def toString(p : path) = {
    val str = new StringBuilder
    str append p.path
    if (p hasExpression){
      str append("[")
      str append termToString(p.expression)
      str append("]")
    }
    str toString
  }
  
  def termToString(term : Term) : String = {
    
    val str = new StringBuilder
    term match{
      case v : AssociateExpr =>
        str append termToString(v.left)
        str append termToString(v.operator)
        str append termToString(v.right)
      case v : OperatorExpr =>
        str append termToString(v.left)
        str append termToString(v.operator)
        str append termToString(v.right)
      case v : field =>
        str append v.name
       case v : attfield =>
        str append "@"+v.name
      case v : value =>
        v.value match{
          case value : String => str append "'"+v.value+"'"
          case _ => str append v.value
        }
      case v : OperatorEqual =>
        str append " = "
      case v : OperatorDiff =>
        str append " != "
      case v : OperatorSup =>
        str append " > "
      case v : OperatorSupEqual =>
        str append " >= "
      case v : OperatorInf =>
        str append " < "
      case v : OperatorInfEqual =>
        str append " <= "
      case v : expr =>
        str append "("
        str append termToString(v.term)
        str append ")"
      case v : AssociationAnd =>
        str append " and "
      case _ => 
    }
    str toString
  }
}