/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.api.service

import org.osoa.sca.annotations.{ Conversational, Init, EndsConversation }
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.db.api.path
import fr.inria.bsense.common.utils.Node
import java.io.OutputStream


@Conversational
trait IDBQueryService {

  /**
   * Open SCA conversation
   */
  @Init def beginConversation

  /**
   * Close SCA conversation
   */
  @EndsConversation def endConversation

  /**
   * Initialize Client session and open
   * a conversation with database
   * @param client
   */
  def createSession(client: DatabaseClientSession)

  /**
   * Close database session
   */
  def endSession

  /**
   * Open Database
   * @param dbName name of Database
   */
  def open(dbName: String)

  /**
   * Close opened database
   */
  def close

  /**
   * Return True if the database 'dbName' exist
   * @param dbName Name of Database
   */
  def exists(dbName: String): Boolean

  /**
   * Get id of node from path
   * Exception thrown if severals nodes match with path
   * @param path Path
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocumentId(dbName: String, path: path): Array[String]

  /**
   *
   * Get document id
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocumentId(dbName: String, collection: String): String

  /**
   * Get id of node from path
   * Exception thrown if severals nodes match with path
   * @param path Path
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocumentId(dbName: String, collection: String, path: path): Array[String]

  /**
   * Get node from Id
   * Exception thrown if severals nodes match with path
   * @param nodeId Id of node
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocument(dbName: String, nodeId: String): Node

  /**
   * Get node from Id
   * Exception thrown if severals nodes match with path
   * @param nodeId Id of node
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocument(dbName: String, path: path): Node

   /**
   * Get node from path
   * Result will be encapsulate by node <array> ... </array>
   * @param nodeId Id of node
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocuments(dbName: String, path: path): Node


  def count(dbname : String, collection : String, path : path) : Integer

  def info(dbname : String) : Node

  /**
   * Get node from Id
   * Exception thrown if severals nodes match with path
   * @param nodeId Id of node
   * @param dbName Name of database
   * @param collection Collection name
   */
  def getDocument(dbName: String, collection: String, path: path): Node

  /**
   * Return True if a collection 'collection' exist in the database 'dbName'
   * else False
   * @param dbName Name of database
   * @param collection Name of Collection
   */
  def existsCollection(dbName: String, collection: String): Boolean

  /**
   * Generic command to execute query
   * Query language depend of the Database implementation
   * @param query : execute query
   * @return result of query
   */
  def query(query: String): String

   /**
   * Generic command to execute query
   * Query language depend of the Database implementation
   * @param query : execute query
   * @return result of query
   */
  def queryWithParams(query: String, outputStream : OutputStream, params : Array[(String,Any,String)] ): String

  /**
   * Generic command to execute query
   * Query language depend of the Database implementation
   * Put result in OutputStream
   * @param query : execute query
   * @return result of query
   */
  def query(query : String, outputStream : OutputStream) : String

  def command(command: String): String

}