/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.api.cbse

import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.common.cbse.BSenseComponent
import frascala.sca.Java
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService


/**
 * 
 * 
 */
object DatabaseComponent {
  
  final val COMPOSITE_NAME  : String = "DatabaseComponent"
  
  //Components 
  final val CPT_SERVER  = "at-db-cpt-server"
  final val CPT_QUERY   = "at-db-cpt-query"
  final val CPT_QUERY_UPDATE  = "at-db-cpt-query-update"  
  
  //Services
  final val SERVICE_QUERY = "srv-database-query"
  final val SERVICE_QUERY_UPDATE = "srv-database-update"
}

/**
 * 
 */
class DBQueryComponent extends BSenseComponent("DBQueryComponent"){
  val srv = service(DatabaseComponent.SERVICE_QUERY).exposes(Java[IDBQueryService])
}

/**
 * 
 */
class DBQueryUpdateComponent extends BSenseComponent("DBQueryUpdateComponent"){
  val srv = service(DatabaseComponent.SERVICE_QUERY_UPDATE).exposes(Java[IDBQueryUpdateService])
}


/**
 * 
 */
object DatabaseComposite{
  final val COMPOSITE_NAME = "fr.inria.antdroid.Database"
}

/**
 * 
 * 
 * 
 * 
 */
class DatabaseComposite extends BSenseComposite(DatabaseComposite.COMPOSITE_NAME){
  
  val cptQuery = component(new DBQueryComponent())
  
  val cptQueryUpdate = component(new DBQueryUpdateComponent())
  
  promoteService(cptQuery.srv)
  promoteService(cptQueryUpdate.srv)
}

