/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.db.api;

import scala.beans.BeanProperty


@serializable class DatabaseClientSession{

  @BeanProperty
  var username : String = _
  
  @BeanProperty
  var password : String = _
  
  @BeanProperty
  var host : String = _
  
  @BeanProperty
  var port : String = _
}