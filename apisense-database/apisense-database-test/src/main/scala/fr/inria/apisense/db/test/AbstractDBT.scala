/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.apisense.db.test

import fr.inria.bsense.test.ScalaTestCase
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.DatabaseClientSession
import org.junit.Assert

abstract class AbstractDBT extends ScalaTestCase {

  def update : IDBQueryUpdateService

  def query : IDBQueryService

  def session : DatabaseClientSession

  def testcase(){
    createdb
    testcase3
  }

  /* Test create drop and exist method */
  def createdb(){

     query.createSession(session)

     canfail("drop database if exist "){update.drop(session, "db_test")}

     notfail("create database "){update.create(session, "db_test")}

     notfail("Test if database exist"){Assert.assertTrue(query.exists("db_test"))}

     notfail("drop database"){ update.drop(session, "db_test") }

     notfail("Test if database exist"){Assert.assertFalse(query.exists("db_test"))}

     query.endSession

  }

  def testcase3(){

    query.createSession(session)

    val dbname = "DBTestCase3"

    update.create(session, dbname)

    Assert.assertFalse(query.existsCollection(dbname, "collection"))

    update.createIfNotExist(session, dbname)

    Assert.assertFalse(query.existsCollection(dbname, "collection"))

    update.addDocument(session, dbname, "collection", "{'apps' : {} }")

    Assert.assertTrue(query.existsCollection(dbname, "collection"))

    update.createIfNotExist(session, dbname)

    Assert.assertTrue(query.existsCollection(dbname, "collection"))

    query.endSession
  }



}