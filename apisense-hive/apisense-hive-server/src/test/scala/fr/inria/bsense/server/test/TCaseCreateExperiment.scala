package fr.inria.bsense.server.test


import scala.io.Source
import scala.tools.nsc.io.Streamable
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream
import fr.inria.bsense.common.http.HttpRequest
import org.apache.cxf.helpers.IOUtils
import fr.inria.bsense.test.ScalaTestCase
import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.common.utils.Http

object TCaseCreateExperiment extends ScalaTestCase{

  final val http = new HttpRequest();

  final val SERVICE_CREATE_EXPERIMENT  = ServerT.HOST+"/store/scientist/add/experiment"
  final val SERVICE_UPDATE_DESCRIPTION = ServerT.HOST+"/store/scientist/update/description"
  final val SERVICE_UPDATE_SCRIPTS	   = ServerT.HOST+"/store/scientist/update/experiment"
  final val SERVICE_DELETE_EXPERIMENT  = ServerT.HOST+"/store/scientist/delete/experiment"

  final val EXPERIMENT_NAME = "MyExperimentTest"

  def testcase(){
	  this.createExperiment
	  //this.updateExperimentDescription
	  this.updateExperimentScripts
  }


  def createExperiment(){
      for (i <- 0 until 5) {createExperiment(TCaseCreateUser.org1Token, resource("application"+i+".json")) }
  }

  def createExperiment(token : String, data : String) = {

    Http post(SERVICE_CREATE_EXPERIMENT) param(Map(
    	"description" -> data
    )) header(Map("TOKEN" -> token)) asString

  }

  def updateExperimentDescription(){

    for (i <- 0 until 5) {

    	val data = resource("application"+i+".json");
    	jnotfail("update experiment description"){

    		Http post(SERVICE_UPDATE_DESCRIPTION) param(Map(
    				"description" -> data
    		)) header(Map("Token" -> TCaseCreateUser.org1Token)) asString
    	}
	}
  }

  def updateExperimentScripts(){

    val stream = TCaseCreateExperiment.getClass.getClassLoader.getResourceAsStream("Application2.zip");
    val data = IOUtils.readBytesFromStream(stream)

    http.addHeader("Token",TCaseCreateUser.org1Token)
    http.postData(SERVICE_UPDATE_SCRIPTS+"/"+"MyExperimentTest0", data)

    jmustfail("Update experiment with no authorization"){
        http.addHeader("Token",TCaseCreateUser.org2Token)
        http.postData(SERVICE_UPDATE_SCRIPTS+"/"+"MyExperimentTest0", data)
    }

  }

  private def resource(name : String) = {
     val stream = TCaseCreateExperiment.getClass.getClassLoader.getResourceAsStream(name)
     Source.fromInputStream(stream).getLines.mkString("\n")
  }

}