package fr.inria.bsense.server.test

import fr.inria.{apisense => apisense}
import fr.inria.bsense.test.ScalaTestCase
import fr.inria.bsense.common.utils.Http
import fr.inria.bsense.server.GCMConfiguration
import fr.inria.apisense.client.APISENSEClient
import fr.inria.bsense.common.utils.Log
import fr.inria.apisense.client.MD5

object TCaseCrowd extends ScalaTestCase{
  import ServerT._


  final val SERVICE_MOBILE_SUBSCRIPTION = service("/crowd/subscribe")
  final val SERVICE_PUSH_MOBILE         = service("/crowd/push/user")
  final val SERVICE_PUSH_EXPERIMENT     = service("/crowd/push/experiment")
  final val SERVICE_PUSH_BROADCAST      = service("/crowd/push/broadcast")

  def testcase(){
    import fr.inria.apisense.client.APISENSEClient._;
    import apisense.node.NodeJSONHelper._

    // get a new hive client instance
    val hv = APISENSEClient.hive
    hv.host(HOST)


    canfail("create organization"){
       hv.userManager.createOrganization("","","","crowdOrg1", "password")
    }

    canfail("create user"){hv.userManager.createMobileUser("","","","crowdUser1", "password")}
    canfail("create user"){hv.userManager.createMobileUser("","","","crowdUser2", "password")}
    canfail("create user"){hv.userManager.createMobileUser("","","","crowdUser3", "password")}



    val mobileToken1 = hv connect("crowdUser1","password")
    val mobileToken2 = hv connect("crowdUser2","password")
    val mobileToken3 = hv connect("crowdUser3","password")

    val orgToken    = hv connect("crowdOrg1","password")

    hv connect(orgToken)

    canfail("create experiment"){
        hv.store createExperiment("""
          {"experiment" : {
             "name" 	   : "crowdXp",
   	         "type" 	   : "android",
             "version" 	   : "1.0",
	         "language"    : "javascript",
	         "description" : "a simple descrition",
	         "copyright"   : "Inria 2012",
	         "visible"	   : "true"
          }}
        """)
    }

   // set development API key of Google API Access
   GCMConfiguration gcmKey "AIzaSyA3ySFfS_ykMm3VVEnirjENjojj6izDP1k"



   canfail("subscribe user to experiment"){hv.connect(mobileToken1);hv.store.subscribe("crowdXp","{}") }
   canfail("subscribe user to experiment"){hv.connect(mobileToken2);hv.store.subscribe("crowdXp","{}") }
   canfail("subscribe user to experiment"){hv.connect(mobileToken3);hv.store.subscribe("crowdXp","{}") }

   val userXpId = hv.store.getSubscribedExperiments \\ "userId" text;

   hv.crowd.subscribeMobile("APA91bFMraD0WwmsJcVrx-NifQpHMsHmtUyFaVG0iLpW4EazuUdKIFEUqe1lEC3DrI1V7FjlIFEhBtUGRPNxfpH5LX_q2RBMfpEGHxiwjrfZv0sgPX6uE1szh_isV_Jsrr9XZivHZ76x2PwG1PPuN3x80-cZYPq6mg")

   hv.connect(orgToken)

   canfail(" push message "){
     val m2 = hv.crowd.pushMessageToExperimentUsers("crowdXp", "{}","{}")
     val m3 = hv.crowd.pushMessageToDevUser("crowdUser1",MD5.hash("password"),"{$message : \"hello\"}","{}")
     Log.d(m2)
     Log.d(m3)
   }
  }

}