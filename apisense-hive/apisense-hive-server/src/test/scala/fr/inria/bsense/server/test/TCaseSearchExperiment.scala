package fr.inria.bsense.server.test

import fr.inria.bsense.test.ScalaTestCase
import fr.inria.bsense.common.http.HttpRequest
import org.apache.http.message.BasicNameValuePair
import net.liftweb.json.JsonAST.JArray
import org.junit.Assert
import net.liftweb.json.JsonAST.JObject
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.Http._
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.common.utils.NodeParser

object TCaseSearchExperiment extends ScalaTestCase {

  final val SERVICE_SEARCH_XP           					  = ServerT.HOST+"/store/search/experiment"
  final val SERVICE_SEARCH_XP_BY_NAME          			      = ServerT.HOST+"/store/search/experiment/name"
  final val SERVICE_SEARCH_XP_ALL       			          = ServerT.HOST+"/store/search/experiment/all"
  final val SERVICE_SEARCH_XP_BY_TYPE           			  = ServerT.HOST+"/store/search/experiment/type"
  final val SERVICE_SEARCH_XP_BY_TYPE_AND_LANGUAGES           = ServerT.HOST+"/store/search/experiment/languages"
  
  final val http = new HttpRequest();
  
  def testcase(){
    this.testSearchByName
    this.testSearchAll
    this.testSimpleSearch
    this.testLimitSearch
    this.testSearchByType
  }
  
  def testSearchByName(){
    
    val JArray(searchResult) =  jnotfail("search by name "){ 
      
      post(SERVICE_SEARCH_XP_BY_NAME) param(Map(
          "name" -> "MyExperimentTest0,MyExperimentTest1"
      )) asString;
      
    } \\ "experiment"
    
    Assert.assertTrue(searchResult.length >= 2)
  }
  
  
  def getExperiment(name : String) = {
    
   NodeParser.parJSON(
     post(SERVICE_SEARCH_XP_BY_NAME) param(Map(
           "name" -> name
     )) asString
   )
  }
  
  def testSearchAll(){
    
     val JArray(searchResult) = jnotfail(" search all "){ 
       
      val result = post(SERVICE_SEARCH_XP_ALL) param(Map(
          "index" -> "1",
          "limit" -> "100"
      )) asString
      
      println(result)
      result
      
     } \\ "experiment"
     
     Assert.assertTrue(searchResult.length >= 5)
  }
  
  def testSimpleSearch(){
    
    val JArray(searchResult) = jnotfail(" search specific experiment "){ 
      post(SERVICE_SEARCH_XP) param(Map("name"->"MyExperimentTest","index" -> "1","limit" -> "100")) asString } \\ "experiment"
  }
  
  def testLimitSearch(){
    
    val JArray(searchResult) = jnotfail(" search with limited result "){ 
      post(SERVICE_SEARCH_XP) param(Map("name"->"MyExperimentTest","index" -> "1","limit" -> "2")) asString } \\ "experiment"
    
    Assert.assertTrue(searchResult.length >= 2)
  }
  
  def testSearchByType(){
    
    val JArray(searchResult) =  jnotfail("Test limit search "){
    
       post(SERVICE_SEARCH_XP_BY_TYPE) param(Map(
          "name" -> "MyExperimentTest",
          "type" -> "android",
          "index"-> "1",
          "limit"-> "100"
       )) asString 
    
    } \\ "experiment"
    
    Assert.assertTrue(searchResult.length >= 5)
    
  }
    
}