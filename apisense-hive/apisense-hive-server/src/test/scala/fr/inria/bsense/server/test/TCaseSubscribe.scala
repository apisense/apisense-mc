package fr.inria.bsense.server.test

import fr.inria.bsense.common.http.HttpRequest
import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.test.ScalaTestCase
import net.liftweb.json.JsonAST.JString
import java.util.zip.ZipInputStream
import org.junit.Assert
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.Http._
import fr.inria.bsense.common.utils.LiftWebNode
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.common.utils.Node

object TCaseSubscribe extends ScalaTestCase {

  final val SERVICE_SUBSCRIBE_EXPERIMENT           = ServerT.HOST+"/store/user/update/subscribe"
  final val SERVICE_UNSUBSCRIBE_EXPERIMENT         = ServerT.HOST+"/store/user/update/unsubscribe"
  final val SERVICE_SUBSCRIBTION_ID          	   = ServerT.HOST+"/store/user/get/userXpId"
  final val SERVICE_DOWNLOAD_EXPERIMENT            = ServerT.HOST+"/store/user/get/experiment"
  final val SERVICE_GET_USER_INFO           	   = ServerT.HOST+"/user/get/user"
  final val SERVICE_GET_SUBSCRIBED_XP          	   = ServerT.HOST+"/store/user/get/subscribed"


  final val http = new HttpRequest();

  def testcase(){

    this.testSuscribe()
    val id = this.testDownload
    this.testDownload;
    this.testUnsuscribe(id)
  }

  /*
   * Test mobile user subscription to an experiment
   */
  def testSuscribe(){


    var p = Map("name" -> "MyExperimentTest0","properties" -> """{ "operator" : "Operator1" }""")
    var h = Map("Token"->TCaseCreateUser.apisToken)

    jcanfail("Subcribe mobile user to experiment"){

      post(SERVICE_SUBSCRIBE_EXPERIMENT) param p header h asString
    }

    jmustfail("Subcribe mobile user already registered to an experiment"){

       post(SERVICE_SUBSCRIBE_EXPERIMENT) param p header h asString
    }
  }


  def getSubscribedExperiment( token : String ) = {

    var h = Map("TOKEN"-> token)

    NodeParser.parJSON(
      post(SERVICE_GET_SUBSCRIBED_XP) header h asString
    )
  }


  /*
   * Test experiment download
   */
  def testDownload() = {

    var h = Map("Token"->TCaseCreateUser.apisToken)

    var experiment : Node = null;
    notfail("Get Experiment subscription id"){
      var requestResult = post(SERVICE_GET_SUBSCRIBED_XP) header h asString

      Log.d("requestResult "+requestResult)
      experiment = NodeParser.parJSON(requestResult)
    }

    val xpId   = experiment \"experiments" \ "experiment" \ "experimentId" text;
    val userId = experiment \"experiments" \ "experiment" \ "userId" text;

    //Log.i("Download experiment "+userId+" "+xpId)

    // download zip experiment
    // and check file numbers in
    // zip
    val p = Map("xpId" -> xpId);
    val zipExperiment = new ZipInputStream(post(SERVICE_DOWNLOAD_EXPERIMENT) param p header h asStream)

    var element = 0
    var zipEntry = zipExperiment.getNextEntry();
    while (zipEntry != null) {
     element = element + 1
      zipEntry = zipExperiment.getNextEntry()
    }
    zipExperiment.close();

    xpId
  }

  /*
   * Test experiment unsubscription
   */
  def testUnsuscribe(xpId : String){

    var h = Map("Token"->TCaseCreateUser.apisToken)
    var p = Map("xpId"->xpId)


    jnotfail("Unsuscribe to experiment"){

      post(SERVICE_UNSUBSCRIBE_EXPERIMENT) param p header h asString
    }

    p = Map("name"->"MyExperimentTest0","properties"->"""{ "operator" : "Operator1" }""")

    jnotfail("Subscribe to the unsubscribe experiment"){

      post(SERVICE_SUBSCRIBE_EXPERIMENT) param p header h asString
    }

  }



}