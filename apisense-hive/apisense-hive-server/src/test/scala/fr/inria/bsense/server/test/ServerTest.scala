package fr.inria.bsense.server.test

import org.junit.Before
import org.junit.Test
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.server.run.StartServer
import fr.inria.bsense.test.ScalaTestCase
import fr.inria.apisense.client.APISENSEClient




object ServerT{

	final val HOST = "http://localhost:18000";

	final val SERVICE_USER_MANAGER_GET_PROPERTY = HOST+"/user/get/user/property";
	final val SERVICE_USER_MANAGER_UPDATE_PROPERTY = HOST+"/user/update/user/property";
	final val SERVICE_CREATE_Organization = HOST+"/user/add/user/organization";
	final val SERVICE_CREATE_BEE        = HOST+"/user/add/user/bee";
	final val SERVICE_DROP_USER         = HOST+"/user/delete/user";
	final val SERVICE_DROP_USERNAME     = HOST+"/user/delete/username";
	final val SERVICE_CONNECT           = HOST+"/user/get/token";
	final val SERVICE_CREATE_XP         = HOST+"/store/scientist/add/experiment"
	final val SERVICE_DROP_XP           = HOST+"/store/scientist/delete/experiment"


    def service(serviceUrl : String) = HOST + serviceUrl
}

class ServerTest extends ScalaTestCase{

	final val http = new HttpRequest();

	@Before
	def before(){
	    System.setProperty("java.util.logging.config.file","src/main/resources/logging.properties")
	    System.setProperty("org.ow2.frascati.binding.uri.base",ServerT.HOST)
		//System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiWebExplorer");
		StartServer.start();
	}

	@Test
	def test(){

		TCaseCreateUser.testcase
		TCaseCreateExperiment.testcase
	    TCaseSearchExperiment.testcase
		TCaseSubscribe.testcase
		TCaseDropExperiment.testcase
		TCaseDropUser.testcase
	    //TCaseCrowd.testcase
	}

}