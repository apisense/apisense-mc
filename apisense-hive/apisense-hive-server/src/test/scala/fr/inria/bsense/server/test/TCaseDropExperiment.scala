package fr.inria.bsense.server.test

import fr.inria.bsense.test.ScalaTestCase
import fr.inria.bsense.common.http.HttpRequest
import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.common.utils.Http._

object TCaseDropExperiment extends ScalaTestCase{

  final val http = new HttpRequest();
  
  def testcase(){
    
   var p = Map("name" -> (TCaseCreateExperiment.EXPERIMENT_NAME+"0"),"properties" -> "{}")
   var h = Map("Token"->TCaseCreateUser.org2Token)
    
    
    jmustfail("Try to delete experiment with no authorization")(
        
        post(ServerT.SERVICE_DROP_XP) param p header h asString 
    )
    
    
     for (i <- 0 until 5){
       
        h = Map("Token"->TCaseCreateUser.org1Token)
        p = Map("name" -> (TCaseCreateExperiment.EXPERIMENT_NAME+i),"properties" -> "{}")
        
    	jnotfail("Delete experiment "+TCaseCreateExperiment.EXPERIMENT_NAME+i)(
    	    post(ServerT.SERVICE_DROP_XP) param p header h asString
    	)
     }
  }
  
}