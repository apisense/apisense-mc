package fr.inria.bsense.server.test

import fr.inria.bsense.test.ScalaTestCase
import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.common.utils.MD5
import net.liftweb.json.JsonAST.JString
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.Http
import org.junit.Assert

object TCaseCreateUser extends ScalaTestCase{

	final val http = new HttpRequest();

	var password = "password";

	var org1  = "org1";
	var org2  = "org2";
    var apis = "apisUser";

	var org1Token : String = "";
	var org2Token : String = "";
	var apisToken : String = "";
	var adminToken : String = "";

	def testcase(){

	  this.org1Token  = this.createOrganisation(org1)
	  this.org2Token  = this.createOrganisation(org2)
	  this.apisToken  = this.createBeeUser(apis)
	  this.adminToken = this.connect("admin", "admin")

	  this.testSecurity
	}

	//-------------------------------------------------------------------- Create scientist user


	def connect(username : String, password : String) = {

	  var JString(token) = jnotfail("connection of user "+username){

	    Http post(ServerT.SERVICE_CONNECT) param(Map(
	       "username" -> username,
	       "password" -> MD5.hash(password)
	     )) asString ;

	  }

	  token
	}

	def createOrganisation(orgName : String) = {

	  Http post(ServerT.SERVICE_CREATE_Organization) param(Map(
	    		"organization" -> orgName,
	    		"description"  -> "none",
	    		"username"	   -> orgName,
	    		"password"	   -> password,
				"email"        -> "scientist.bsense.com")
	  ) asString

	   val JString(token) = jnotfail("connection with real user"){
	     Http post(ServerT.SERVICE_CONNECT) param(Map(
	       "username" -> orgName,
	       "password" -> MD5.hash(password)
	     )) asString
	    }

	  token
	}

	def createBeeUser(username : String) = {

	   Http post(ServerT.SERVICE_CREATE_BEE) param(Map(
	    		"organization" -> username,
	    		"description"  -> "none",
	    		"username"	   -> username,
	    		"password"	   -> password,
				"email"        -> "scientist.bsense.com")
	    ) asString

	    val JString(token) = jnotfail("user connection"){

		  Http post(ServerT.SERVICE_CONNECT) param(Map(
	         "username" -> username,
	         "password" -> MD5.hash(password)
	       )) asString
	    }



	   token
	}

	def testCreateUser(){

		// Update user property
		// in local database
		jnotfail("update user property"){
		   Http post(ServerT.SERVICE_USER_MANAGER_UPDATE_PROPERTY) param Map(
	    		"name"  -> "myproperty",
				"value" -> "'myvalue'"
		  ) header Map( "TOKEN" -> this.apisToken ) asString
		}

		// check user property modification
		val JString(updatedProperty) = jnotfail("check user property update"){

		   Http post(ServerT.SERVICE_USER_MANAGER_GET_PROPERTY) param Map(
				"name" -> "myproperty"
		  ) header Map( "TOKEN" -> this.apisToken ) asString

		} \ "myproperty"

		Assert.assertTrue(updatedProperty equals "myvalue")
    }


	def testSecurity(){

	  jmustfail("connection with bad password"){ Http post(ServerT.SERVICE_CONNECT) param(Map(
	       "username" -> org1,
	       "password" -> MD5.hash("none")
	     )) asString ;
	  }

	  jmustfail("connection with bad username"){ Http post(ServerT.SERVICE_CONNECT) param(Map(
	       "username" -> "none",
	       "password" -> MD5.hash(password)
	     )) asString ;
	  }


	  mustfail("drop user without token"){
	      Http post(ServerT.SERVICE_DROP_USER) asString
	  }

	  mustfail("drop user with no authorization"){
	      Http post(ServerT.SERVICE_DROP_USERNAME) header(
	          Map("TOKEN" -> org1Token)) param(
	              Map("username" -> "tmp")) asString
	  }

	}


}