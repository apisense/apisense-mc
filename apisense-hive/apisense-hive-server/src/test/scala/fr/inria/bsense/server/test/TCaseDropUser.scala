package fr.inria.bsense.server.test

import fr.inria.bsense.test.ScalaTestCase
import fr.inria.bsense.common.utils.Log
import org.apache.http.message.BasicNameValuePair
import fr.inria.bsense.common.http.HttpRequest
import fr.inria.bsense.common.utils.Http._

object TCaseDropUser extends ScalaTestCase{

  final val http = new HttpRequest();

  
  def testcase(){
    this.dropBeeUser
  }
  
  def dropBeeUser(){
    
    var h = Map("Token"->TCaseCreateUser.org1Token)
    println(TCaseCreateUser.org1Token)
    println(TCaseCreateUser.org2Token)
    println(TCaseCreateUser.apisToken)
	jnotfail("Dropt scientist user"){
       post(ServerT.SERVICE_DROP_USER) header Map("Token"->TCaseCreateUser.org1Token) asString;
       post(ServerT.SERVICE_DROP_USER) header Map("Token"->TCaseCreateUser.org2Token) asString;
       post(ServerT.SERVICE_DROP_USER) header Map("Token"->TCaseCreateUser.apisToken) asString
    }
    
  }
  
  
}