/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.functionalitities

import org.osoa.sca.annotations.Reference
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.api.DatabaseClientSession
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.ConfigurationDB
import fr.inria.bsense.common.utils.Configuration
import fr.inria.bsense.server.utils.CentralDBConfiguration

trait TraitDatabase {

  @Reference(name=DatabaseComponent.SERVICE_QUERY)
  val queryService : IDBQueryService = null
  
  @Reference(name=DatabaseComponent.SERVICE_QUERY_UPDATE)
  val updateService : IDBQueryUpdateService = null
  
  final val session : DatabaseClientSession = {
    
    val s = new DatabaseClientSession
    s.setHost(CentralDBConfiguration host)
    s.setPort(CentralDBConfiguration port)
    s.setUsername(CentralDBConfiguration user)
    s.setPassword(CentralDBConfiguration password)
    s
  }
  
  def exec(body: => Any): String = {
			import fr.inria.bsense.common.utils._

			try {

				queryService.beginConversation
				queryService.createSession(session)

				Message.success(body)

			} 
			catch { 
			  case e : Exception =>
			    e.printStackTrace
			    Message.exception(e); 
			}
			finally {
				try { queryService.endSession }
				catch { case e : Exception => e.printStackTrace() }
			}
	}
  
}