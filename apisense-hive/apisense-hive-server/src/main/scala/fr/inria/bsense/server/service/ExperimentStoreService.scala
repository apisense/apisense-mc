/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.service

import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.FormParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext

object ExperimentStoreService{
  
  final val DATABASE_STORE = "Store"
    
  final val DATABASE_DELETED_EXPERIMENT = "Experiment_Deleted"
    
  final val DATABASE_EXPERIMENT_OWNER = "Experiment_Owner"
}
  

/**
 * 
 * 
 * 
 * 
 */
trait ExperimentStoreService {

  @POST
  @Path("/add/experiment")
  def createExperiment(
      @Context sc : SecurityContext,
      @FormParam("description")  description : String 
  ) : String
  
  @POST
  @Path("/delete/experiment")
  def deleteExperiment(
      @Context sc : SecurityContext,
      @FormParam("name") name : String
  ) : String
  
  @POST
  @Path("/update/description")
  def updateDescription(
      @Context sc : SecurityContext, 
      @FormParam("description") description : String 
  ) : String

  @POST
  @Path("/update/experiment/{name}")
  @Consumes(Array("application/octet-stream"))
  def updateScript(
		  @Context sc : SecurityContext, 
		  @PathParam("name") name : String,
		  experiment : Array[Byte]
  ) : String

}