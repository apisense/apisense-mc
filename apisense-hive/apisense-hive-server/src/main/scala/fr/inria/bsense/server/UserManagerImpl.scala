/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server

import org.osoa.sca.annotations.Scope
import org.osoa.sca.annotations.Reference
import org.osoa.sca.annotations.Init
import fr.inria.bsense.server.functionalitities.TraitDatabase
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.field
import fr.inria.bsense.db.api.value
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.server.service.UserManagerService
import fr.inria.bsense.server.service.UserManagerException
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.http.Base64
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.apisense.token.TokenManager
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.apisense.token.TokenManagerComposite
import fr.inria.bsense.common.utils.MD5
import javax.ws.rs.core.SecurityContext
import fr.inria.apisense.security.UserAuthorityPrincipal
import fr.inria.apisense.security.APISecureContext
import fr.inria.apisense.security.UserSecurityContext
import fr.inria.bsense.db.api.ValuePair

@Scope("COMPOSITE")
class UserManagerImpl extends UserManagerService with TraitDatabase with LifeCycleService {


	@Reference(name = TokenManager.TOKEN_MANAGER_SERVICE)
	var tokenService: TokenServiceManager = null

	//------------------------------------------------------------------------- Init

	def onStart(cpt : BSenseComposite) { exec {

		// setup database
	    // create database user if not exist
	    // create admin account if not exist

		updateService.createIfNotExist(session, UserManagerService.DATABASE_USER)

		if (!queryService.existsCollection(UserManagerService.DATABASE_USER, "admin")) {
			val doc = generateUserDocument("admin", "admin", MD5.hash("admin"), "admin@email.com", UserManagerService.ROLE_ADMIN)
			updateService.addDocument(session, UserManagerService.DATABASE_USER, "admin", doc)
		}

	}}

	def onStop(){}


	def createUser(fullname: String, username: String, password: String, email: String, role: String) = exec {

	    // exception if user already exist
	    if (queryService.existsCollection(UserManagerService.DATABASE_USER, username))
	    	throw UserManagerException("User " + username + " already exist")

	    val hashPassword = MD5.hash(password)

		// create user node
		val doc = generateUserDocument(fullname, username, hashPassword, email, role)

		// add document in database
		updateService.addDocument(session, UserManagerService.DATABASE_USER, username, doc toString)
	}

	def createBeeUser(fullname: String, username: String, password: String, email: String) = {
		this.createUser(fullname, username, password, email, UserManagerService.ROLE_BEE)
	}

	def createOrganization(organisation: String,orgDesc : String, username: String, password: String, email: String) = exec {

	   // exception if user already exist
	   if (queryService.existsCollection(UserManagerService.DATABASE_USER, username))
	    	throw UserManagerException("User " + username + " already exist")

	   val hashPassword = MD5.hash(password)

	   val doc = new StringBuilder
	   doc append "{ 'user' : "
	   doc append "{"
	   doc append " 'organisation' : '" + organisation + "',"
	   doc append " 'orgDescription' : '" + Base64.encodeToString(orgDesc.getBytes(),false) + "',"
	   doc append " 'username' : '" + username + "',"
	   doc append " 'password' : '" + hashPassword + "',"
	   doc append " 'experiments' : {},"
	   doc append " 'email' : '" + email + "',"
	   doc append " 'role' : '" + UserManagerService.ROLE_ORGANIZATION + "'"
	   doc append "}}"
	   doc toString

	   updateService.addDocument(session, UserManagerService.DATABASE_USER, username, doc toString)

	}

	def dropUser(sc : SecurityContext) = exec {

	    val context = sc.asInstanceOf[APISecureContext]
	    val userId = context.getUserPrincipal().getName()
	    val username = context.username
	    val token = context.token

	    Log.i("drop user "+context.username+" with node id "+
	        userId+" and token "+token)

		updateService.dropCollection(session,
				UserManagerService.DATABASE_USER, username)

		this.tokenService.removeUserPrincipal(token)

	}

	def dropUser(sc : SecurityContext, username : String) = exec {
		updateService.dropCollection(session,
				UserManagerService.DATABASE_USER, username)
	}

	def getUser(sc : SecurityContext): String = exec {

	    val userId = sc.getUserPrincipal().getName()

	    queryService.getDocument(
		    UserManagerService.DATABASE_USER,userId)
	}

	def getUserProperty(sc : SecurityContext,propertyName : String) = exec {

	  queryService.getDocument(
	      UserManagerService.DATABASE_USER,
	      sc.getUserPrincipal().getName()) \\ propertyName

	}

	def updateProperty(sc : SecurityContext, propertyName : String, nodeValue : String) = exec {

	  if (nodeValue.equals("")){
	    updateService.deleteNodeFromId(
	      session,
	      UserManagerService.DATABASE_USER,
	      sc.getUserPrincipal().getName(),
	      path(propertyName))
	  }else{
	     updateService.updateNode(
	      session,
	      UserManagerService.DATABASE_USER,
	      sc.getUserPrincipal().getName().toInt,
	      path(propertyName), "{ '"+propertyName+"' : "+nodeValue+" }")
	  }
	}



	def getUsers(): String = exec {

		queryService.getDocuments(
		    UserManagerService.DATABASE_USER, path("user"))
	}

	def connect(username : String, password : String): String = exec {

		// verify in database is a collection has the
	    // same name that user
		if (!queryService.existsCollection(
				UserManagerService.DATABASE_USER, username))
			throw UserManagerException("User " + username + " not exist")

		// search user node id with good username
	    // and password
		val userId = queryService.getDocumentId(
		    UserManagerService.DATABASE_USER,
		    username,
		    path("user") withs field("password") == value(password))

		// if empty result throw exception
		if (userId.length == 0)
			throw UserManagerException("Bad password")

		val role = queryService.getDocument(
		    UserManagerService.DATABASE_USER, username,path("role")) text;

		// create new security context for user connection
	    // with associate roles
	    val apisecure = new APISecureContext(
	        userId(0),
	        username,
	        Array(role)
	    )

	    // inject new security context
	    // in token manager
		this.tokenService.createToken(apisecure)
	}

	def disconnect(sc : SecurityContext) : String = { sc match {

  		case user : APISecureContext =>

  		  tokenService.removeUserPrincipal(user.token)

  		  Message.success("disconnected")

  		case _ =>

  		  Log.w("Cannot cast security context to "+classOf[APISecureContext].getName())

  		  Message.exception("DisconnectionException", "")
     }
  }


	private def generateUserDocument(fullname: String, username: String, password: String, email: String, role: String): String = {
		val doc = new StringBuilder
		doc append "{ 'user' : "
		doc append "{"
		doc append " 'fullname' : '" + fullname + "',"
		doc append " 'username' : '" + username + "',"
		doc append " 'password' : '" + password + "',"
		doc append " 'experiments' : {},"
		doc append " 'email' : '" + email + "',"
		doc append " 'role' : '" + role + "'"
		doc append "}}"
		doc toString
	}
}

object Main{

  def main(args: Array[String]) {
		println(MD5.hash("admin"))
  }

}
