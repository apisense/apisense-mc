/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import fr.inria.bsense.common.http.Base64
import fr.inria.bsense.common.utils.IOUtils
import fr.inria.bsense.common.utils.Message
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import org.json.simple.parser.JSONParser
import org.json.simple.JSONObject
import scala.util.parsing.json.JSONArray
import java.io.OutputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import fr.inria.bsense.server.functionalitities.TraitDatabase
import fr.inria.bsense.server.service.QueryService


class QueryImpl  extends QueryService with TraitDatabase{


  def mprocess( sc : SecurityContext,  query : String, params : String) : Response = {

    val response = Response.ok()

    val stream : ByteArrayOutputStream = new ByteArrayOutputStream;

    var paramsTuples : Array[(String,Any,String)] = null;
    var error : String = null;
    var queryInfo : String = ""
    exec{

    	try{

    		if (params != null){

    			paramsTuples = paramStringToTuple(params);
    			queryInfo = queryService.queryWithParams(
    					new String(Base64.decode(query.getBytes())),
    					stream,
    					paramsTuples)
    		}
    		else{

    			queryInfo = queryService.query(new String(Base64.decode(query.getBytes())), stream)
    		}

    	}catch{ case th : Throwable => error = th.getMessage() }
    }


    if (error == null){

    	Response.ok(stream.toByteArray()).build()
    }else{
      println(error)
      Response.serverError().entity(error).build()
    }
  }




  private def paramStringToTuple( params : String) = {

    var paramsTuples : Array[(String,Any,String)] = null;

  	new JSONParser().parse(params) match {

  	case json : org.json.simple.JSONObject =>

  		paramsTuples = new Array(1);
  		paramsTuples(0) = (
		  json.get("name").toString,
		  json.get("value"),
		  json.get("type").toString
		)

  	case jsonArray : org.json.simple.JSONArray =>

  		paramsTuples = new Array(jsonArray.size());
  		for (i <- 0 until jsonArray.size()){

  			jsonArray.get(i) match{

  				case json : org.json.simple.JSONObject =>

  					paramsTuples(i) = (
  						json.get("name").toString,
  						json.get("value"),
  						json.get("type").toString
  					)
  			}
  		}
  	}
  	paramsTuples
  }


}