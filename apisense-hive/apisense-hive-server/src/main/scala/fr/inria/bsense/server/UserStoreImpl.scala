/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server

import java.util.Date
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.MD5
import fr.inria.bsense.db.api.field
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.value
import fr.inria.bsense.server.functionalitities.TraitDatabase
import fr.inria.bsense.server.service.ExperimentStoreService
import fr.inria.bsense.server.service.UserManagerService
import fr.inria.bsense.server.service.UserStoreService
import fr.inria.bsense.server.utils.ServerConfiguration
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.XMLLiftNodeParser
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.XMLNodeParser
import fr.inria.apisense.client.APISENSEClient

case class ExperimentNotExistException(message : String) extends Exception(message)

class UserStoreImpl extends UserStoreService with TraitDatabase{

  case class AlreadySubscribedException    extends Exception("User is already subscribed to the experiment")
  case class NoSubscribeException 		   extends Exception("User no participate to the experiment")
  case class NoExistingExperimentException extends Exception("Experiment not exist")

  def subscribeExperiment( sc : SecurityContext, xpName : String, deviceProperties : String ) : String = exec {

    // get user id from request context
	val userId = sc.getUserPrincipal().getName()

    // check if experiment exist
    var exist : Boolean = queryService.existsCollection(ExperimentStoreService.DATABASE_STORE,xpName);
	if (!exist){
		throw ExperimentNotExistException("Experiment "+xpName+" not exist")
	}

	// create users collection if not exist
	if (!queryService.existsCollection(xpName,"users")){
	  updateService.addDocument(session, xpName, "users", """{ "users" : {} }""")
	}

	// check if user is already subscribed to the experiment
	var nodeResult : Node = queryService.getDocument(xpName, "users", path("users/user") withs field("userId") == value(userId))
	if (!nodeResult.isNull){

	  throw AlreadySubscribedException()
	}

	// generate new subscription ID
	val id = MD5.hash(xpName + userId + new Date().getTime())

	// insert user subscription in database
	val node = new StringBuffer
	node append "<experiment>"
	node append "<userId>"+id+"</userId>"
	node append "<experimentId>"+queryService.getDocumentId(ExperimentStoreService.DATABASE_STORE, xpName)+"</experimentId>"
	node append "</experiment>"

	updateService.insertInID(session, UserManagerService.DATABASE_USER, userId, path("experiments"), new XMLNodeParser(node.toString()))

	updateService.insert(session, xpName, "users", path("users"), new XMLNodeParser("<user><userId>"+userId+"</userId><userXpId>"+id+"</userXpId></user>"))

	// return user id
	id
  }

  def getSubscribedExperiment(sc : SecurityContext) = exec { queryService.getDocument(UserManagerService.DATABASE_USER,sc.getUserPrincipal().getName()) \ "experiments" }


  def unsubscribeExperiment(sc : SecurityContext, xpId : String ) = exec {

    val userId = sc.getUserPrincipal().getName()

    // Delete reference in user database
    // with experiment subscription
    updateService.deleteNodeFromId(
        session,
        UserManagerService.DATABASE_USER,
        userId,
        path("experiments/experiment") withs field("experimentId") == value(xpId))

   	// Get experiment name from experiment ID
    val experimentNode = queryService.getDocument(ExperimentStoreService.DATABASE_STORE, xpId)

    var experimentName  = experimentNode \\ "name" text;

    var sensingNodeHost : String = null
    if (!(experimentNode \\ "baseUrl" isNull)){
        sensingNodeHost = experimentNode \\ "baseUrl" text;
        if (sensingNodeHost.endsWith("/")){
           sensingNodeHost = sensingNodeHost.substring(0,sensingNodeHost.lastIndexOf('/'))
        }
        sensingNodeHost = sensingNodeHost.substring(0,sensingNodeHost.lastIndexOf('/'))
    }

    if (sensingNodeHost != null){

     val honey = APISENSEClient.honey
     honey.host(sensingNodeHost)
     try{
    	  honey.experimentManager.unregisterUser(experimentName, userId)
     }catch{
    	 case th : Throwable => th.printStackTrace()
     }
    }

    updateService.deleteNode(session, experimentName,"users", path("users/user") withs field("userId") == value(userId))

    Log.i("User "+userId+" unsubscribed to experiment with id "+xpId+" named "+experimentName)
  }

  def downloadExperiment( sc : SecurityContext, xpId : String ) : Array[Byte] = {

      val userId = sc.getUserPrincipal().getName()

      var sensingNodeHost : String = null
      var experimentName  : String = null
      var experimentVersion  : String = null
      var zipFile : String = null

      val message = exec {

    	  // get experiment document from experiment ID
          val experimentDoc =  queryService.getDocument(
              ExperimentStoreService.DATABASE_STORE,
              xpId)

          // get experiment name and version
          experimentName    = experimentDoc \\ "name" text;
          experimentVersion = experimentDoc \\ "version" text;

          // get sensing node URL
          if (!(experimentDoc \\ "baseUrl" isNull)){
            sensingNodeHost = experimentDoc \\ "baseUrl" text;
            if (sensingNodeHost.endsWith("/")){
              sensingNodeHost = sensingNodeHost.substring(0,sensingNodeHost.lastIndexOf('/'))
            }
            sensingNodeHost = sensingNodeHost.substring(0,sensingNodeHost.lastIndexOf('/'))
          }

          if ( ! queryService.exists(experimentName))
           throw NoExistingExperimentException()

         // get ZIP File of experiment
         zipFile = ServerConfiguration("bee-experiments/"+experimentName).compositeFolder+"/"+experimentName+".zip";
      }

      if (zipFile == null)
        throw NoSubscribeException()

      // send a message to sensing node
      // to prevent a new user registration
      if (sensingNodeHost != null){

        val honey = APISENSEClient.honey
    	honey.host(sensingNodeHost)

    	try{

    	  honey.experimentManager.registerUser(experimentName, userId, experimentVersion)
    	}
    	catch{ case th : Throwable => Log.e(th) }
      }

      // return experiment
      fr.inria.bsense.common.utils.FileUtil.toByteArray(zipFile)
  }
}