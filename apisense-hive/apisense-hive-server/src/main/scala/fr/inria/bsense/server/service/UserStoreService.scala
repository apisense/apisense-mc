/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.service

import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.FormParam
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context

trait UserStoreService {

  @POST
  @Path("/update/subscribe")
  def subscribeExperiment(
      @Context sc : SecurityContext,
      @FormParam("name") xpName : String,
      @FormParam("properties") deviceProperties : String ) : String
  
  @POST
  @Path("/update/unsubscribe")
  def unsubscribeExperiment(
      @Context sc : SecurityContext,
      @FormParam("xpId") xpId : String) : String
      
  @POST
  @Path("/get/subscribed")
  def getSubscribedExperiment(@Context sc : SecurityContext) :String  
  
  //@POST
  //@Path("/get/userXpId")
  //def getUserSubscriptionId(@Context sc : SecurityContext, @FormParam("id")xpId : String) : String
 
  @POST
  @Path("/get/experiment")
  def downloadExperiment( 
      @Context sc : SecurityContext,
      @FormParam("xpId") xpId : String ) : Array[Byte]
 
}