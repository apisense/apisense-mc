/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.utils

import fr.inria.bsense.common.utils.FileConfiguration
import fr.inria.bsense.common.utils.Configuration


object ServerConfiguration{
  
  final val FILE_CONFIGURATION_PROPERTY = "fr.inria.bsense.central.path"
  
}

case class ServerConfiguration(str : String) extends 
		FileConfiguration(ServerConfiguration.FILE_CONFIGURATION_PROPERTY,str){
}


object CentralDBConfiguration extends Configuration(ServerConfiguration.FILE_CONFIGURATION_PROPERTY,"database-basex.properties"){

  final val PROPERTY_HOST     = "database.host"
  final val PROPERTY_PORT     = "database.port"
  final val PROPERTY_PORTS    = "database.ports"
  final val PROPERTY_USER     = "database.user"
  final val PROPERTY_PASSWORD = "database.password"
  
  default(PROPERTY_HOST,"localhost")
  default(PROPERTY_PORT,"1990")
  default(PROPERTY_PORTS,"1991")
  default(PROPERTY_USER,"admin")
  default(PROPERTY_PASSWORD,"admin")

  def host = get(PROPERTY_HOST)
  def port = get(PROPERTY_PORT)
  def ports= get(PROPERTY_PORTS)
  def user = get(PROPERTY_USER)
  def password = get(PROPERTY_PASSWORD)
}