/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.run

import fr.inria.bsense.server.cbse.ServerComposite
import fr.inria.bsense.db.cbse.BaseXComposite
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.server.utils.ServerConfiguration
import org.basex.BaseXServer
import java.io.File
import scala.io.Source
import java.io.BufferedReader
import java.io.InputStreamReader
import fr.inria.bsense.common.http.HttpRequest
import org.apache.http.conn.HttpHostConnectException
import fr.inria.apisense.token.TokenManagerComposite
import fr.inria.apisense.token.ImplTokenManagerMap
import fr.inria.bsense.server.GCMConfiguration
import java.net.InetAddress
import java.net.NetworkInterface

object StartServer {


  def main(args: Array[String]) {

    val uri = System.getProperty("fr.inria.apisense.hive.uri","localhost:18001")

    System.setProperty("org.ow2.frascati.binding.uri.base","http://"+uri)

    start()

    Log.d("server is started")
  }


  def AsynchStart(codePath : String) = {

    val http = new HttpRequest();

    var current = new File(codePath).getAbsolutePath()
    current = current.substring(0, current.lastIndexOf("/"))

    val command = new StringBuilder
    command append "/usr/bin/mvn -f "
    command append current
    command append "/apisense-hive/apisense-hive-server/pom.xml -P start"

    println("start command "+command.toString)

    val centralServerProcess = Runtime.getRuntime().exec(command toString);
    new Thread(new Runnable(){
       def run(){
          val in = new BufferedReader(new InputStreamReader(centralServerProcess.getInputStream()))
          var line = in.readLine
          try {
            while (line != null) {
             print("Server Log : "+line+"\n")
             line = in.readLine
            }
           }
        }
    }).start()
    print("waiting central server ...")

    var ready = false;
    while(!ready){
       try{
           http.get("http://localhost:18001/public/ping",Array[CharSequence]())
           ready = true;
        }
        catch{
          case httpException : HttpHostConnectException =>
           Thread.sleep(1000)
          case e : Throwable =>
           Thread.sleep(1000)
           print("...")
        }
      }
      println("server ready")

      centralServerProcess

  }

  def start() {

    // define main directory where all
    // files of apisense will be written
    val home = System.getProperty("user.home")

    // set home property
    System.setProperty(ServerConfiguration.FILE_CONFIGURATION_PROPERTY,home+"/BEES_CENTRAL")

    // set home property for all file of frascala
    System.setProperty("fr.inria.frascala",home+"/BEES_CENTRAL")

    // set basex database home property
    System.setProperty("org.basex.path",home+"/BEES_CENTRAL/BaseXData")

    // init home folder
    ServerConfiguration("")

    // run basex server
    try{ val bx = new BaseXServer("-p1990","-e1991") }

    // create new instance of frascati
    val frascati = org.ow2.frascati.FraSCAti.newFraSCAti()

    val tokenManager = new TokenManagerComposite[ImplTokenManagerMap]

    tokenManager save;
    tokenManager start frascati;

	val compositeServer = new ServerComposite();
	compositeServer save;
	compositeServer.start(frascati);

	val compositeDatabase = new BaseXComposite();
	compositeDatabase.save;
	compositeDatabase.start(frascati);
  }

}