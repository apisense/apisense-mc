/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.service

import javax.ws.rs._

trait SearchService {

  /**
   *
   * Return experiment informations stored in database
   *
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment")
  def search(@FormParam("name") name : String,@FormParam("index")index : Int,@FormParam("limit")limit : Int) : String

  /**
   *
   * Return experiment informations from list of experiment id
   *
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment/id")
  def searchFromId(@FormParam("ids") ids : String) : String

  /**
   *
   * Return experiment informations from list of experiment id
   *
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment/uuid")
  def searchFromUUID(@FormParam("uuids") ids : String) : String


  /**
   *
   * Return experiment informations from list of experiment name
   * @param name List of experiments name. Each name must be separated by token ,
   *
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment/name")
  def search(@FormParam("name") name : String) : String

  /**
   * Return experiment informations stored in database
   *
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment/all")
  def search(@FormParam("index")index : Int,@FormParam("limit")limit : Int) : String


  /**
   *
   * Return experiment informations with a specific type stored in database
   *
   * @param type Type of experiment
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment/type")
  def search(@FormParam("name") name : String,@FormParam("type") xpType : String, @FormParam("index")index : Int,@FormParam("limit")limit : Int) : String

  /**
   *
   * Return experiment informations with a specific type and specific language
   * stored in database
   *
   * @param language  Language of script experiment. Each language must be separate by ','
   * @param type      Type of experiment
   * @param index
   * @param limit
   */
  @POST
  @Path("/experiment/languages")
  def search(@FormParam("name") name : String,@FormParam("languages") language : String,  @FormParam("type") xpType : String, @FormParam("index")index : Int,@FormParam("limit")limit : Int) : String


}