/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.cbse


import fr.inria.bsense.common.cbse.BSenseComponent
import fr.inria.bsense.common.cbse.BSenseComposite
import fr.inria.bsense.db.api.cbse.DBQueryComponent
import fr.inria.bsense.db.api.cbse.DBQueryUpdateComponent
import fr.inria.bsense.db.api.cbse.DatabaseComponent
import fr.inria.bsense.db.api.service.IDBQueryService
import fr.inria.bsense.db.api.service.IDBQueryUpdateService
import fr.inria.bsense.db.basex.impl.BaseXQuery
import fr.inria.bsense.db.basex.impl.BaseXUpdateQuery
import fr.inria.bsense.server.ExperimentStoreImpl
import fr.inria.bsense.server.GCMMobileQueryImpl
import fr.inria.bsense.server.PublicImpl
import fr.inria.bsense.server.SearchServiceImpl
import fr.inria.bsense.server.UserManagerImpl
import fr.inria.bsense.server.UserStoreImpl
import fr.inria.bsense.server.service.ExperimentStoreService
import fr.inria.bsense.server.service.MobileQueryService
import fr.inria.bsense.server.service.PublicService
import fr.inria.bsense.server.service.SearchService
import fr.inria.bsense.server.service.UserManagerService
import fr.inria.bsense.server.service.UserStoreService
import frascala.frascati.REST
import frascala.frascati.SCA
import frascala.sca.Bean
import frascala.sca.Java
import fr.inria.apisense.token.TokenManager
import fr.inria.apisense.token.TokenServiceManager
import fr.inria.apisense.intent.BindingIntent
import fr.inria.apisense.intent.AuthorizationIntent
import Server._
import fr.inria.bsense.server.service.CrowdService
import fr.inria.bsense.server.CrowdImpl
import fr.inria.bsense.server.QueryImpl
import fr.inria.bsense.server.service.QueryService


object Server{

  final val COMPOSITE = "fr.inria.antdroid.Server"

  final val COMPONENT_USER_MANAGER = "cpt-user-manager"

  final val COMPONENT_EXPERIMENT_STORE = "cpt-experiment-store"

  final val SERVICE_USER_MANAGER_REST   = "srv-rest-user-manager"

  final val SERVICE_USER_MANAGER   = "srv-user-manager"

  final val SERVICE_EXPERIMENT_STORE   = "srv-experiment-store"


  final val authority : AuthorizationIntent = BindingIntent.authorization

}

class ServerComposite extends BSenseComposite(Server.COMPOSITE){


  val authorityManager = BindingIntent.authorization


  val cptUserManager = component(UserManagerComponent())

  component(ExperimentStoreComponent())

  component(SearchComponent())

  component(CrowdComponent())

  component(new UserStoreComponent())

  component(new AdminQueryComponent())

  component(new DBQueryComponent()).uses(Bean[BaseXQuery])

  component(new DBQueryUpdateComponent()).uses(Bean[BaseXUpdateQuery])

  new component("cpt-ping"){
    service("srv-ping") exposes Java[PublicService] as REST("/public")
  } uses Bean[PublicImpl]


  //------- promote service

  promoteService(cptUserManager.srvUser)


  /* connect component */

  //wire(cptMobileQuery.userManagerService,cptUserManager.srvUser)
}


/**
 *
 * ' User manager component '
 *
 * This component provide service to manage
 * user subscription and connection
 *
 */
case class UserManagerComponent() extends BSenseComponent(Server.COMPONENT_USER_MANAGER){
  this.uses(Bean[UserManagerImpl])


  // ----------------------------- declare user manager service

  // authorization rules
  authority rule ("/user/delete/username",Array("ROLE_ADMIN"))
  authority rule ("/user/delete/user$",Array("ROLE_ORGANIZATION","ROLE_BEE"))

  // create and bind service
  val srvUser = service(Server.SERVICE_USER_MANAGER_REST) exposes Java[UserManagerService]
  srvUser as(REST("/user"),Array(
	BindingIntent.authenticationToken,
	authority
  ))

  // ----------------------------- declare component reference

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery autowired

  val refUpdate = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdate autowired

  val refToken = reference(TokenManager.TOKEN_MANAGER_SERVICE) exposes Java[TokenServiceManager]
  refToken as TokenManager.getAddress
}

/**
 *
 * ' Experiment store component '
 *
 */
case class ExperimentStoreComponent extends BSenseComponent(Server.COMPONENT_EXPERIMENT_STORE){
  this.uses(Bean[ExperimentStoreImpl])

  // ----------------------------- declare experiment store service

  // authorization intent
  authority rule ("/store/scientist/*",Array("ROLE_ORGANIZATION"))

  val srvStore  = service(Server.SERVICE_USER_MANAGER) exposes Java[ExperimentStoreService]
  srvStore as(REST("/store/scientist"),Array(
      BindingIntent.authenticationToken,
	  authority
  ))

  // ----------------------------- declare component reference

  val refCrowdService = reference(CrowdService.NAME)  exposes Java[CrowdService]
  refCrowdService autowired

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery autowired

  val refUpdate = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdate autowired

}

/**
 *
 * ' User store component '
 *
 */
case class UserStoreComponent extends BSenseComponent("cpt-store-user"){
   this.uses(Bean[UserStoreImpl])

  // ----------------------------- declare user store service

  // authorization rules
  authority rule ("/store/user/*",Array("ROLE_BEE"))

  val srvUser = service("srv-user-store") exposes Java[UserStoreService]
  srvUser as(REST("/store/user"),Array(
      BindingIntent.authenticationToken,
      authority
  ))

  // ----------------------------- declare component reference

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery autowired

  val refUpdate = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdate autowired

}

/**
 *
 * ' Experiment search component '
 *
 * This component provide service to search experiment
 * created by scientists in database
 */
case class SearchComponent extends BSenseComponent("cpt-search"){
   this.uses(Bean[SearchServiceImpl])

  // ----------------------------- declare experiment search service

  val srvSearch = service("srv-search") exposes Java[SearchService]
  srvSearch as REST("/store/search")

  // ----------------------------- declare component reference

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery autowired

  val refUpdate = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdate autowired
}




object CrowdComponent{ final val NAME : String = "cpt-crowd" }

case class CrowdComponent extends BSenseComponent(CrowdComponent.NAME){
   this.uses(Bean[CrowdImpl])

   val srvCrowd = service(CrowdService.NAME) exposes Java[CrowdService]
   srvCrowd as(REST("/crowd"),Array(
	  BindingIntent.authenticationToken,
	  authority
   ))

  // ----------------------------- declare component reference

  var refUser = reference(UserManagerService.SCA_SERVICE) exposes Java[UserManagerService] autowired

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery autowired

  val refUpdate = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdate autowired

}

/**
 *
 * ' Experiment store component '
 *
 */
case class AdminQueryComponent extends BSenseComponent("cpt-admin-query"){
  this.uses(Bean[QueryImpl])

  // ----------------------------- declare experiment store service

  // authorization intent
  authority rule ("/admin/query/*",Array("ROLE_ADMIN"))

  val srvStore  = service("srv-admin-query") exposes Java[QueryService]
  srvStore as(REST("/admin/query"),Array(
      BindingIntent.authenticationToken,
	  authority
  ))

  // ----------------------------- declare component reference

  val refQuery = reference(DatabaseComponent.SERVICE_QUERY)  exposes Java[IDBQueryService]
  refQuery autowired

  val refUpdate = reference(DatabaseComponent.SERVICE_QUERY_UPDATE) exposes Java[IDBQueryUpdateService]
  refUpdate autowired

}

