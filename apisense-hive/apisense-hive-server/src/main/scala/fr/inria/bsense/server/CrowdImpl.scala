/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server

import java.util.ArrayList
import org.osoa.sca.annotations.Scope
import org.osoa.sca.annotations.Reference
import com.google.android.gcm.server.Message
import com.google.android.gcm.server.Message.Builder
import com.google.android.gcm.server.MulticastResult
import com.google.android.gcm.server.Result
import com.google.android.gcm.server.Sender
import fr.inria.bsense.common.utils.{Message => APISMessage}
import fr.inria.bsense.common.utils.Configuration
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.XMLNode
import fr.inria.bsense.db.api.field
import fr.inria.bsense.db.api.path
import fr.inria.bsense.db.api.value
import fr.inria.bsense.server.functionalitities.TraitDatabase
import fr.inria.bsense.server.service.CrowdService
import fr.inria.bsense.server.service.UserManagerService
import fr.inria.bsense.server.utils.ServerConfiguration
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.NodeParser
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.cbse.BSenseComposite
import org.json.simple.JSONValue
import org.json.simple.JSONArray
import org.json.simple.JSONObject


object GCMConfiguration extends Configuration(ServerConfiguration.FILE_CONFIGURATION_PROPERTY,"gcm.properties"){

  val KEY_GCM = "gcm.key"

  default(KEY_GCM,"xxxxxx")

  def gcmKey = get(KEY_GCM)
  def gcmKey(key : String) { put(KEY_GCM,key) }
}

/**
 *
 * Implementation of CrowdService using
 * Google Cloud Messaging ''http://developer.android.com/google/gcm/index.html''
 * to push message into mobile device
 *
 */
@Scope("COMPOSITE")
class CrowdImpl extends CrowdService with TraitDatabase with LifeCycleService{

  @Reference(name = UserManagerService.SCA_SERVICE )
  var userManager : UserManagerService = null

   def onStart( composite : BSenseComposite ){

   }


  def onStop(){}

  /*
   * register in database GCM user Id
   */
  def registerDevice( sc : SecurityContext, id : String ) : String = {  userManager.updateProperty(sc, "deviceId", "'"+id+"'") }


  def unregisterDevice( sc : SecurityContext ) : String = {  userManager.updateProperty(sc, "deviceId", "") }

  private def createMessage(config : String) : Builder = {

       Log.d("load message configuration "+config)

       var messageBuilder = new Message.Builder();


       val jsonConfig = org.json.simple.JSONValue.parse(config).asInstanceOf[JSONObject];
       if (jsonConfig.containsKey("timeToLive")){
         messageBuilder = messageBuilder.timeToLive(jsonConfig.get("timeToLive").toString.toInt)
       }

       if (jsonConfig.containsKey("collapseKey")){
         messageBuilder = messageBuilder.collapseKey(jsonConfig.get("collapseKey").toString)
       }

       if (jsonConfig.containsKey("delayWhileIdle")){
         messageBuilder = messageBuilder.delayWhileIdle(jsonConfig.get("delayWhileIdle").toString.toBoolean)
       }

       messageBuilder
  }


  private def buildGCMResult(result : Result) : Node = {

    var nodeResult : Node = new XMLNode(<messageResult></messageResult>);
    nodeResult = nodeResult.:<("messageId",result.getMessageId());
    if (result.getErrorCodeName() != null){
      nodeResult = nodeResult :< ("error",result.getErrorCodeName());
      Log.w("gcm push error : "+result.getErrorCodeName()+" "+result.getCanonicalRegistrationId())

    }

    nodeResult
  }

  private def buildGCMMulticastResult(results : MulticastResult) : Node = {

    var nodeResult : Node = new XMLNode(<multicastResult></multicastResult>);
    nodeResult = nodeResult :<("failure",results.getFailure())
    nodeResult = nodeResult :<("multicastId",results.getMulticastId())
    nodeResult = nodeResult :<("success",results.getSuccess())
    nodeResult = nodeResult :<("total",results.getTotal())

    var nodeResults : Node = new XMLNode(<results></results>);

    val iterator = results.getResults().iterator()
    while(iterator.hasNext()){

        val result = iterator.next()
        var node : Node = new XMLNode(<messageResult></messageResult>)
        node = node.:<("messageId",result.getMessageId());
        if (result.getErrorCodeName() != null){
           node = nodeResult :< ("error",result.getErrorCodeName());
           Log.w("gcm push error : "+result.getErrorCodeName())
        }

        nodeResults = nodeResults :< node
    }

    nodeResult = nodeResult :< nodeResults
    nodeResult
  }

   def pushMessageToDevUser(
       sc : SecurityContext,
       username : String,
       password : String,
       message : String,
       conf : String
  ) : String = {

    var builder : Builder = createMessage(conf)
    builder = builder.addData("data", message)
    builder = builder.addData("type","UserMessage")

    var device : String = null
    var errorMessage : String = null

    exec {

      val nodeUser = queryService.getDocument(UserManagerService.DATABASE_USER,username,path("user"))
      if (!nodeUser.isNull){
        val _password = nodeUser \\ "password" text;
        if (_password == password){
          val deviceNode = nodeUser \\ "deviceId"
          if (!deviceNode.isNull){
            device = deviceNode text;
          } else errorMessage = "User "+username+" has no device registered"
        } else errorMessage = "Bad password "
      } else errorMessage = "Cannot find user "+username
    }

    if (device == null){

      if (errorMessage == null){errorMessage = "Cannot find user "+username}

      var nodeResult : Node = new XMLNode(<messageResult></messageResult>);
      nodeResult = nodeResult :<("error",errorMessage)

      return APISMessage.success(nodeResult);
    }

    // create new gcm sender instance
    // and send message
    val gcmSender = new Sender(GCMConfiguration gcmKey)
    val messageResult = gcmSender.send(builder.build, device, 2)

    APISMessage.success(buildGCMResult(messageResult))
  }

  def pushMessageToUser(sc : SecurityContext,usersIds : String, message : String, conf : String) : String = {

    Log.d("push message to users  "+usersIds)

    var builder : Builder = createMessage(conf)
    builder = builder.addData("data", message)
    builder = builder.addData("type","UserMessage")

    var devices : java.util.List[String] = new ArrayList[String]()

    val users = JSONValue.parse(usersIds).asInstanceOf[JSONArray]
    exec {

      0 to (users.size() -1) foreach{
        i =>

          val userId = users.get(i).asInstanceOf[String];
          val nodeResult = queryService.getDocument(
        		  UserManagerService.DATABASE_USER,
        		  path("user") withs field("//userId") == value(userId))

          if (!nodeResult.isNull){

            val gcmId = nodeResult \\ "deviceId";
            if (!gcmId.isNull){

              devices.add(gcmId text)

            } else Log.d("User "+userId+" has no device registered")


          }else Log.w("Cannot find user "+userId)
      }
    }

    if (devices.isEmpty()){

      var nodeResult : Node = new XMLNode(<multicastResult></multicastResult>);
      nodeResult = nodeResult :<("failure",users.size())
      nodeResult = nodeResult :<("multicastId","0")
      nodeResult = nodeResult :<("success",0)
      nodeResult = nodeResult :<("total",users.size())

      return APISMessage.success(nodeResult);
    }

    // create new gcm sender instance
    // and send message
    val gcmSender = new Sender(GCMConfiguration gcmKey)
    val messageResult =  gcmSender.send(builder.build,devices, 2)

    APISMessage.success(buildGCMMulticastResult(messageResult))
  }

  def pushMessageToExperimentUser(sc : SecurityContext, experimentName : String,message : String, conf : String) : String = {

    Log.d("Push message to user of experiments "+experimentName)

    var builder : Builder = createMessage(conf)
    if (builder == null)
      return APISMessage.exception("JSONParserException",""" cannot parse json %s """.format(conf))

    builder = builder.addData("data", message)
    builder = builder.addData("type","ExperimentMessage")
    builder = builder.addData("name",experimentName)

    var devices : java.util.List[String] = new ArrayList[String]()
    var error : String = ""

    // request in Experiment database
    // Query 1 : search all users subscribed to the experiment
    // Query 2 : for all subscribed users, get mobile device Id
    // All devices Id are inserted in devices list
    val queryResult = exec{

      try{

        val xpUserIds =  queryService.getDocuments(experimentName, path("userId"))
        xpUserIds \\ "array" foreachChild{
          user =>
             val deviceIdNode = queryService.getDocument(UserManagerService.DATABASE_USER,user text) \\ "deviceId";
             if (!deviceIdNode.isNull){

              Log.d("add user "+(user text))
              devices.add(deviceIdNode text)
             }
             else Log.w("No mobile phone registered for user "+user)
        }
        xpUserIds

      }catch{case e : Throwable => error = e.getMessage()}
    }

    if (devices.isEmpty()) return APISMessage.success("No users has been found")

    // create new gcm sender instance
    // and send message

    val gcmSender = new Sender(GCMConfiguration gcmKey)
    val messageResult =  gcmSender.send(builder.build,devices, 2)

    APISMessage.success(buildGCMMulticastResult(messageResult))
  }

  def broadcastCrowd(sc : SecurityContext, message : String , conf : String) : String = {

    var builder : Builder = createMessage(conf)
    if (builder == null)
      return APISMessage.exception("JSONParserException",""" cannot parse json %s """.format(conf))

    builder = builder.addData("data", message)
    builder = builder.addData("type","BroadcastMessage")


    var devices : java.util.List[String] = new ArrayList[String]()


    // request in User database
    // Query : Get all registered devices Id
    // All results are inserted in devices list
    val queryResult = exec {

      val resultNode =  queryService.getDocuments(UserManagerService.DATABASE_USER, path("deviceId"))
      resultNode.foreachChild(
          node =>
            devices.add(node text)
      )
      resultNode
    }

    if (devices.isEmpty()) return APISMessage.exception("Users not found",queryResult)

    // create new gcm sender
    Log.d("send broadcast message with key "+GCMConfiguration.gcmKey)
    val gcmSender = new Sender(GCMConfiguration gcmKey)
    val messageResult = gcmSender.send(builder.build,devices, 2)

    APISMessage.success(buildGCMMulticastResult(messageResult))
  }



}