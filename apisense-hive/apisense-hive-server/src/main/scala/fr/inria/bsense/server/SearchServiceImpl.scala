/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server

import fr.inria.bsense.server.functionalitities.TraitDatabase
import fr.inria.bsense.common.utils.Message
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.server.service.SearchService
import fr.inria.bsense.server.service.ExperimentStoreService
import fr.inria.bsense.common.utils.XMLNodeParser

class SearchServiceImpl extends SearchService with TraitDatabase{

  def search(name : String) : String = exec {

    Log.i("search experiment by name "+name)

    val query = """

     let $names := fn:tokenize("%s",",")
     return db:open('%s')/experiment[ $names = name/text() ]

    """

    build{ queryService.query(query.format(name,ExperimentStoreService.DATABASE_STORE)) }
  }

  def searchFromUUID( uuids : String) : String =  exec {

    val query = """

     let $uuid := fn:tokenize("%s",",")
     return db:open('%s')/experiment[ $uuid = name/text() ]

    """

    build{ queryService.query(query.format(uuids,ExperimentStoreService.DATABASE_STORE)) }

  }


  def searchFromId(ids : String) : String = exec {

    val query = """

     let $ids := fn:tokenize("%s",",")
     return
       for $id in $ids
           return
    			try{
    				db:open-id("%s",xs:integer($id))
    			}catch *{()}
    """

    build{ queryService.query(query.format(ids,ExperimentStoreService.DATABASE_STORE)) }

  }

  def search(index : Int,limit : Int) : String = exec{

    Log.i("search all experiments from "+index+" to "+limit)


    val query = """

     let $r := db:open('%s')/experiment[visible = "true"]
     return
           $r[position() = %s to %s ]
    """
    Log.d(query.format(ExperimentStoreService.DATABASE_STORE,index,((index+limit)-1)));

    build{ queryService.query(query.format(ExperimentStoreService.DATABASE_STORE,index,((index+limit)-1))) }
  }


  def search(name : String, index : Int,limit : Int) : String = exec {

    val query = """

     let $r := db:open('%s')/experiment[ name/text() contains text '%s' using fuzzy ]
     return
           $r[position() = %s to %s ]
    """

    build{ queryService.query(query.format(ExperimentStoreService.DATABASE_STORE,name,index,((index+limit)-1))) }
  }


  def search(name : String, xpType : String, index : Int,limit : Int) : String = exec{

    val query = """

     let $r := db:open('%s')/experiment[ name/text() contains text '%s' using fuzzy and type/text() = '%s' ]
     return
           $r[position() = %s to %s ]
    """

    build{
    	queryService.query(query.format(ExperimentStoreService.DATABASE_STORE,name,xpType,index,((index + limit)-1)))
    }
  }

  def search(name : String, language : String, xpType : String,index : Int,limit : Int) : String = exec{

    val query = """

     let $languages := fn:tokenize("%s",",")
     let $r := db:open('%s')/experiment[ name/text() contains text '%s' using fuzzy and type/text() = '%s' and ($languages = language/text()) ]
     return
           $r[position() = %s to %s ]
    """
    build{
    	queryService.query(query.format(language,ExperimentStoreService.DATABASE_STORE,name,xpType,index,((index + limit)-1)))
    }

  }

  def build( body : => String ) = new XMLNodeParser("<queryResult>"+body+"</queryResult>")




}