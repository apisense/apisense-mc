/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server

import java.io.ByteArrayInputStream
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream
import scala.collection.mutable.HashMap
import scala.io.Source
import org.osoa.sca.annotations.Scope
import fr.inria.bsense.common.service.LifeCycleService
import fr.inria.bsense.common.utils.Log
import fr.inria.bsense.server.functionalitities.TraitDatabase
import fr.inria.bsense.server.service.ExperimentStoreService
import fr.inria.bsense.server.service.UserManagerService
import fr.inria.bsense.server.utils.ServerConfiguration
import fr.inria.bsense.common.cbse.BSenseComposite
import javax.ws.rs.core.SecurityContext
import fr.inria.bsense.common.utils.Node
import fr.inria.bsense.common.utils.JSONLiftNodeParser
import fr.inria.bsense.common.utils.IoUtils
import fr.inria.bsense.common.utils.IOUtils
import org.osoa.sca.annotations.Reference
import fr.inria.bsense.server.service.CrowdService
import fr.inria.bsense.common.utils.NodeParser


//@Scope("COMPOSITE")
class ExperimentStoreImpl extends ExperimentStoreService with TraitDatabase with LifeCycleService {

    @Reference(name = CrowdService.NAME)
    var crowdService : CrowdService = null;

    def onStart(composite : BSenseComposite) { exec {

    	  // setup database
          // create store database if not exist
          // create association database between experiment and owner

		  updateService.createIfNotExist(session, ExperimentStoreService.DATABASE_STORE)

		  // Check if user database exist else create it
		  if (!queryService.exists(ExperimentStoreService.DATABASE_EXPERIMENT_OWNER)){
			Log.i("Create "+ExperimentStoreService.DATABASE_EXPERIMENT_OWNER+" database")
			updateService.create(session, ExperimentStoreService.DATABASE_EXPERIMENT_OWNER)
		  }
		}
    }

    def onStop(){}


    def createExperiment( sc : SecurityContext, description : String ) : String = exec{

       val userId = sc.getUserPrincipal().getName()

	   var jsonNode  : Node = new JSONLiftNodeParser(description)

	   // verify if manifest file
	   // contains at least this properties
	   verify("name",jsonNode)
	   verify("version",jsonNode)
	   verify("type",jsonNode)

	   val name = jsonNode \\ "name" text;

	   var exist : Boolean = queryService.existsCollection(ExperimentStoreService.DATABASE_STORE,name);
	   if (exist){
		  throw new Exception("Experiment "+name+" already exist")
	   }

	   // add information about organization provider
	   val userNode = queryService.getDocument(UserManagerService.DATABASE_USER, userId)
	   val organization = userNode \\ "organisation" text
	   val orgDescription  = userNode \\ "orgDescription" text;
       jsonNode = jsonNode  :< ("organization",organization)
	   jsonNode = jsonNode  :< ("orgDescription",orgDescription)

	   // create experiment description document in store database
	   updateService.addDocument(session,
	       ExperimentStoreService.DATABASE_STORE,
	       name, jsonNode.toString )

	   // get node id of created document
	   val xpId = queryService.getDocumentId(ExperimentStoreService.DATABASE_STORE, name)
	   jsonNode = jsonNode :< ("id",xpId)

	   // update document with node id
	   updateService.updateDocument(session, ExperimentStoreService.DATABASE_STORE, jsonNode.toString, name)

	   // create link experiment and author
	   val as = new StringBuilder
       as append "{ 'association' :"
       as append "{"
       as append "'authorId' : '" + userId + "',"
       as append "'experimentId' : '" + queryService.getDocumentId(ExperimentStoreService.DATABASE_STORE, name) + "'"
       as append "}}"
       updateService.addDocument(session, ExperimentStoreService.DATABASE_EXPERIMENT_OWNER, name, as toString)

       //create experiment database
       updateService.createIfNotExist(session, name)
     }

	 def deleteExperiment( sc : SecurityContext,  experimentName : String) : String = exec {

	    val userId = sc.getUserPrincipal().getName()

		autorization(userId, experimentName)

		Log.i("Delete experiment "+experimentName)

		// remove experiment description from store database
		updateService.dropCollection(session, ExperimentStoreService.DATABASE_STORE, experimentName)

		//remove all associations between experiment and authors
		updateService.dropCollection(session, ExperimentStoreService.DATABASE_EXPERIMENT_OWNER, experimentName);
		updateService.drop(session, experimentName);

		ServerConfiguration("bee-experiments/"+experimentName).delete
	}

	 def updateDescription(sc : SecurityContext, description : String ) : String = exec {

	    val userId = sc.getUserPrincipal().getName()

	    // check experiment description
		var jsonNode : Node = new JSONLiftNodeParser(description)
		verify("name",jsonNode)
		verify("version",jsonNode)
		verify("type",jsonNode)

		// get experiment name
		val name = jsonNode \\ "name" text;

	    // check is user is authorized to modify
	    // experiment

		autorization(userId,name)

		// add information about organization provider
	    val userNode = queryService.getDocument(UserManagerService.DATABASE_USER, userId)
	    val organization = userNode \\ "organisation" text
	    val orgDescription  = userNode \\ "orgDescription" text;
        jsonNode = jsonNode  :< ("organization",organization)
	    jsonNode = jsonNode  :< ("orgDescription",orgDescription)

	    val xpId = queryService.getDocumentId(ExperimentStoreService.DATABASE_STORE, name)
	    jsonNode = jsonNode :< ("id",xpId)

		updateService.updateDocument(session, ExperimentStoreService.DATABASE_STORE, jsonNode.toString, name)

		updateZipExperiment(name)

		// notify all users subscribed to the experiment
		// that a new update is available
		NodeParser.parJSON("{}")
		//NodeParser.parJSON(crowdService.pushMessageToExperimentUser(sc, name, """ { "$command" : "update" } """, """ {"collapseKey" : "UpdateExperimentDescription"} """))
	 }

	 def updateScript( sc : SecurityContext , name : String, scripts : Array[Byte]) : String = exec {

	   autorization(sc.getUserPrincipal().getName(),name)

	   // convert byte array into zip
	   val xpZIP =  new ZipInputStream(new ByteArrayInputStream(scripts));

	   // check if zip file contains at least one file
	   var zipEntry = xpZIP.getNextEntry();
	   if (zipEntry == null)
			throw new Exception("Cannot parse experiment zip")

	   // Store all script files in local
	   // service file system
	   val map = new HashMap[String, String]
	   while (zipEntry != null) {

			val entryName = zipEntry.getName
			if (!zipEntry.isDirectory())
				map += entryName -> Source.fromInputStream(xpZIP).getLines().mkString("\n")

			zipEntry = xpZIP.getNextEntry();
	   }

	   val experimentFolder =  ServerConfiguration("bee-experiments/"+name)
	   experimentFolder.deleteAllFiles

	   // write all entry file in local server files system
	   map.keySet.foreach{  key =>  experimentFolder.updateFileWithParent(key,map(key)) }

	   updateZipExperiment(name)
	}

	private def updateZipExperiment(name : String){

	  val folder = ServerConfiguration("bee-experiments/"+name)

	  val zipFile = new File(folder.compositeFolder+"/"+name+".zip")
	  val zip = new ZipOutputStream(new FileOutputStream(zipFile))

	  IOUtils.zip(folder.getFile, zip)

	  zip.putNextEntry(new ZipEntry("manifest.bs"))

	  val docId = queryService.getDocumentId(ExperimentStoreService.DATABASE_STORE, name)
	  val manifest = queryService.getDocument(ExperimentStoreService.DATABASE_STORE, docId).toJSONString

	  zip.write(manifest.getBytes)

	  zip.close
	  zip.flush
	}


	/**
	 * Verify if experiment exist
	 * and if authorId is owner of experiment 'name'
	 */
	private def autorization(authorId: String, name: String) {

		// Check if experiment exist
		if (!(queryService.existsCollection(ExperimentStoreService.DATABASE_STORE, name)))
			throw new Exception("Cannot find experiment " + name)

		val nodeId = queryService.getDocumentId(ExperimentStoreService.DATABASE_EXPERIMENT_OWNER, name)


		var authorized = false

		val doc = queryService.getDocument(ExperimentStoreService.DATABASE_EXPERIMENT_OWNER, nodeId) \ "authorId"
		doc.foreach{ authorIdNode =>
		    //println("check "+authorIdNode.toXMLString+" "+authorId)
			if (authorIdNode.text.equals(authorId)) authorized = true
		}

		if (!authorized) throw new Exception("Unauthrorized to update experiment "+name)

	}

	private def verify(key : String,node : Node){
		val elem = node \\ key text;
		if (elem == null){
			throw new Exception("Bad Bsense application format : Cannot find")
		}
	}
}