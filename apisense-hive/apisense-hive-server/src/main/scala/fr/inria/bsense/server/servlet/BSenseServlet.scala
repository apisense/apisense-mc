/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.servlet

import org.apache.cxf.transport.servlet.CXFServlet
import javax.servlet.ServletConfig
import fr.inria.bsense.server.run.StartServer
import org.apache.cxf.transport.servlet.CXFNonSpringServlet

class BSenseServlet extends CXFNonSpringServlet{

   val BOOTSTRAP_PROPERTY = "org.ow2.frascati.bootstrap"

   override def  init(servletConfig : ServletConfig){
    super.init(servletConfig)

    //System.setProperty(BOOTSTRAP_PROPERTY, "org.ow2.frascati.bootstrap.FraSCAtiWebExplorer");

    System.setProperty("org.ow2.frascati.binding.uri.base","")

    StartServer.start

   }

}