/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.service

import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.FormParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext


/**
 *
 * Service enable to push messages for all mobile phones being
 * APISENSE application installed
 *
 *
 *
 *
 */

object CrowdService  { final val NAME  = "srv-crowd" }

trait CrowdService {

  /**
   *
   * Service available for mobile user
   *
   * Register mobile device Id
   *
   */
  @POST
  @Path("/subscribe")
  def registerDevice(
      @Context sc : SecurityContext,
      @FormParam("deviceId") mobileId : String
  ) : String


  @POST
  @Path("/unsubscribe")
  def unregisterDevice( @Context sc : SecurityContext ) : String

  /**
   *
   * Push a message to a device in develop mode
   *
   * @param username Username of the developer user
   * @param password Password of the developer user MD5 encoded
   * @param message  Message to send
   * @param configuration Message configuration
   */
  @POST
  @Path("/push/dev/user")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def pushMessageToDevUser(
      @Context sc : SecurityContext,
      @FormParam("username")  username : String,
      @FormParam("password")  password : String,
      @FormParam("message")   message : String,
      @FormParam("configuration") conf : String
  ) : String

  /**
   *
   * Send message to a mobile user
   *
   * @Param userId UserId generated of the subscription of the user
   *               to a given experiment
   * @Param message Message to send under JSON format
   * @Param configuration Message configuration under JSON Format
   *                      Message configuration depend of service implementation
   *
   */
  @POST
  @Path("/push/user")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def pushMessageToUser(
      @Context sc : SecurityContext,
      @FormParam("userId")  userId : String,
      @FormParam("message") message : String,
      @FormParam("configuration") conf : String
  ) : String


  /**
   *
   * Send a message to all users subscribed to an experiment
   *
   * @Param name Experiment name
   * @Param message Message to send under JSON format
   * @Param configuration Message configuration under JSON Format
   *                      Message configuration depend of service implementation
   */
  @POST
  @Path("/push/experiment")
  def pushMessageToExperimentUser(
      @Context sc : SecurityContext,
      @FormParam("name") experimentName : String,
      @FormParam("message") message : String,
      @FormParam("configuration") conf : String
   ) : String

   /**
    *
    * Send a message to all APISENSE users
    *
    * @Param message Message to send under JSON format
    * @Param configuration Message configuration under JSON Format
    *                      Message configuration depend of service implementation
    */
  @POST
  @Path("/push/broadcast")
  def broadcastCrowd(
      @Context sc : SecurityContext,
      @FormParam("message")message : String,
      @FormParam("configuration") conf : String
   ) : String

}