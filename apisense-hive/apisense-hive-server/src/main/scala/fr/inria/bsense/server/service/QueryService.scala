/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.service;

import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.FormParam
import javax.ws.rs.core.Response
import javax.ws.rs.GET
import javax.ws.rs.PathParam
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.Context

trait QueryService  {

  /**
   * Execute query in database and return result
   * Statistic and a subset of result is returned
   */
  @POST
  @Path("/mprocess")
  def mprocess(
      @Context sc : SecurityContext ,
      @FormParam("query") query : String,
      @FormParam("params") params : String) : Response

}