/**
 * Copyright © 2011-2014, Inria, Lille-I et Cnrs.
 * Ce logiciel ne peut être copié, vendu ou redistribué, sous aucun prétexte,
 * sans l'autorisation écrite préalable de Inria, Lille-I et Cnrs.
 *
 * APISENSE v2.0 - propriété Inria, Lille-I et Cnrs
 */
package fr.inria.bsense.server.service

import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Consumes
import javax.ws.rs.FormParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.SecurityContext


object UserManagerService{
  final val ROLE_ADMIN = "ROLE_ADMIN"
  final val ROLE_BEE = "ROLE_BEE"
  final val ROLE_ORGANIZATION  = "ROLE_ORGANIZATION"
  final val DATABASE_USER = "Users"
    
  final val SCA_SERVICE = "srv-user-manager"
}

case class UserManagerException(_message: String) extends Exception(_message)

trait UserManagerService {

  /**
   * Create a new user
   * 
   * @param fullname Full name of the user
   * @param username Login of the user.
   * @param password Password of the user
   * @param role Role of the user
   * 
   * @return Unique id registered user
   *  
   * @throws Exception if ''username'' is not unique
   */
  def createUser(fullname : String, username : String, password : String, email : String, role : String) : String
    
  /**
   * Create user with role SCIENTIST_USER
   * 
   * @param fullname Full name of the user
   * @param username Login of the user. An Exception is throw if the `username` already exist
   * @param password Password of the user
   * @param role Role of the user
   * @return Unique id of the user 
   */
  @POST
  @Path("/add/user/organization")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def createOrganization(
      @FormParam("organization") organization : String,
      @FormParam("description") orgDescription : String,
      @FormParam("username") username : String,
      @FormParam("password") password : String,
      @FormParam("email")    email    : String) : String
      
  
  /**
   * Create user with role BEE_USER
   * 
   * @param fullname Full name of the user
   * @param username Login of the user. An Exception is throw if the `username` already exist
   * @param password Password of the user
   * @param role Role of the user
   * @return Unique id of the user 
   */
  @POST
  @Path("/add/user/bee")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def createBeeUser(
      @FormParam("fullname") fullname : String,
      @FormParam("username") username : String,
      @FormParam("password") password : String,
      @FormParam("email")    email    : String) : String
  
  
  
  /**
   * Drop user account
   */
  @POST
  @Path("/delete/user")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def dropUser( @Context sc : SecurityContext) : String
  
  /**
   * ADMIN Method
   * 
   * Drop user 
   * @param username Login of the user to drop
   * 
   */
  @POST
  @Path("/delete/username")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def dropUser(@Context sc : SecurityContext, @FormParam("username") username : String) : String
  
  /**
   * 
   * Return property node of user
   * 
   */
  @POST
  @Path("/get/user/property")
  def getUserProperty(@Context sc : SecurityContext,@FormParam("name") propertyName : String) : String
  
  @POST
  @Path("/update/user/property")
  def updateProperty(
      @Context sc : SecurityContext,
      @FormParam("name") propertyName : String,
      @FormParam("value") nodeValue : String) : String
  
  /**
   * 
   * Return user node 
   * 
   * @param userId node id
   * @return user node
   */
  @POST
  @Path("/get/user")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def getUser(@Context sc : SecurityContext) : String
  
  @POST
  @Path("/get/users")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def getUsers(): String 
  
  @POST
  @Path("get/token")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def connect(@FormParam("username")username : String,@FormParam("password") password : String) : String
    
  @POST
  @Path("update/disconnect")
  @Consumes(Array("application/x-www-form-urlencoded"))
  def disconnect(@Context sc : SecurityContext) : String 
  
}